#ifndef adkInstance_inl__
#define adkInstance_inl__

#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#endif

#include "adkInstance.h"
#include "vulkan.h"
#include "adkMath.h"
#include "adkLog.h"
#include "adkAssert.h"
#include "adkMemoryPool.inl"

typedef struct AdkInstance
{
  uint32_t primaryDeviceIndex;
  adkAllocationCallbacks *pAllocationCallbacks;
  VkAllocationCallbacks *pVkAllocationCallbacks;
  AdkInstanceFlags flags;
  VkInstance vkInstance;
  VkDebugReportCallbackEXT vkDebugReportCallback;
  uint32_t vkDeviceCount;
  VkPhysicalDevice *pVkPhysicalDevices;
  VkDevice *pVkDevices;
  AdkRange<uint32_t> *pVkDeviceQueueFamilyRanges;
  uint32_t vkQueueFamilyCount;
  VkQueueFamilyProperties *pVkQueueFamilyProperties;
  AdkRange<uint32_t> *pVkQueueFamilyQueueRange;
  uint32_t vkQueueCount;
  VkQueue *pVkQueues;
  bool *pVkQueuesConsumed;
  VkCommandPool *pvkCommandPools;
  VkCommandPool *pvkResetCommandPools;
  AdkMemoryPool *pMemoryPool;
} AdkInstance;

static inline void adkInstance_GetQueueFamilyIndex(AdkInstance *pInstance, uint32_t deviceIndex, VkQueueFlags requiredFlags, uint32_t *pQueueFamilyIndex)
{
  for (uint32_t i = 0; i < pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].length; ++i)
  {
    uint32_t queueFamilyIndex = pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].start + i;

    if ((requiredFlags & pInstance->pVkQueueFamilyProperties[queueFamilyIndex].queueFlags) == requiredFlags)
    {
      (*pQueueFamilyIndex) = queueFamilyIndex;
      return;
    }
  }

  ADK_ASSERT(false, "Couldn't find queue family on device index %d with required flags %d", deviceIndex, requiredFlags);
}

#endif // adkInstance_inl__