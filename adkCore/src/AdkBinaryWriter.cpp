#include "AdkBinaryWriter.h"
#include "adkMemory.h"
#include "adkString.h"

typedef struct AdkBinaryWriter
{
  const adkAllocationCallbacks *pAllocator;
  size_t bufferSize;
  size_t bufferOffset;
  uint8_t *pBuffer;
  bool success;
} AdkBinaryWriter;

template<typename T>
static bool _WriteLittleEndian(AdkBinaryWriter *pBinaryWriter, T value)
{
  pBinaryWriter->success = pBinaryWriter->success && pBinaryWriter->bufferOffset + sizeof(value) <= pBinaryWriter->bufferSize;

  if (pBinaryWriter->success)
  {
    for (uint32_t i = 0; i < sizeof(T); ++i)
      pBinaryWriter->pBuffer[pBinaryWriter->bufferOffset + i] = (value >> (8 * i)) & 0xFF;

    pBinaryWriter->bufferOffset += sizeof(value);
  }

  return pBinaryWriter->success;
}

static bool _Write(AdkBinaryWriter *pBinaryWriter, size_t valueSize, const void *pValue)
{
  pBinaryWriter->success = pBinaryWriter->success && pBinaryWriter->bufferOffset + valueSize <= pBinaryWriter->bufferSize;

  if (pBinaryWriter->success)
  {
    memcpy(&pBinaryWriter->pBuffer[pBinaryWriter->bufferOffset], pValue, valueSize);
    pBinaryWriter->bufferOffset += valueSize;
  }

  return pBinaryWriter->success;
}

void adkBinaryWriter_Create(const AdkBinaryWriterCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryWriter **ppBinaryWriter)
{
  AdkBinaryWriter *pBinaryWriter = adkAllocTypeCall(AdkBinaryWriter, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pBinaryWriter->pAllocator = pAllocator;
  pBinaryWriter->bufferSize = pCreateInfo->bufferSize;
  pBinaryWriter->pBuffer = (uint8_t*)pCreateInfo->pBuffer;
  pBinaryWriter->success = true;

  *ppBinaryWriter = pBinaryWriter;
}

void adkBinaryWriter_Destroy(AdkBinaryWriter **ppBinaryWriter)
{
  if (*ppBinaryWriter == nullptr)
    return;

  AdkBinaryWriter *pWriter = *ppBinaryWriter;

  adkFreeCall(*ppBinaryWriter, pWriter->pAllocator);
}

void adkBinaryWriter_Reset(AdkBinaryWriter *pBinaryWriter)
{
  pBinaryWriter->bufferOffset = 0;
  pBinaryWriter->success = true;
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, bool value)
{
  return _WriteLittleEndian(pBinaryWriter, (int8_t)(value ? 1 : 0));
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int8_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int16_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int32_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int64_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint8_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint16_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint32_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint64_t value)
{
  return _WriteLittleEndian(pBinaryWriter, value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, float value)
{
  return _Write(pBinaryWriter, sizeof(value), &value);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, double value)
{
  return _Write(pBinaryWriter, sizeof(value), &value);
}

bool adkBinaryWriter_WriteString(AdkBinaryWriter *pBinaryWriter, const char *pValue)
{
  size_t stringLength = adkStrLen(pValue) + 1;
  return _Write(pBinaryWriter, stringLength, pValue);
}

bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, size_t valueLength, const void *pValue)
{
  return _Write(pBinaryWriter, valueLength, pValue);
}

size_t adkBinaryWriter_GetOffset(AdkBinaryWriter *pBinaryWriter)
{
  return pBinaryWriter->bufferOffset;
}

bool adkBinaryWriter_GetSuccess(AdkBinaryWriter *pBinarWriter)
{
  return pBinarWriter->success;
}