#ifndef adkInstance_h__
#define adkInstance_h__

#include <stdint.h>
#include "adkMemory.h"
#include "adkTypedefs.h"

struct AdkInstance;

typedef enum AdkInstanceFlagsBits
{
  ADK_INSTANCE_DEBUG_BIT = 0x00000001,
} AdkInstanceFlagsBits;
typedef AdkFlags AdkInstanceFlags;

typedef struct adkInstanceCreateInfo
{
  AdkInstanceFlags flags;
  const char *pApplicationName;
  uint32_t applicationVersion;
} adkInstanceCreateInfo;

AdkResult adkInstance_Create(const adkInstanceCreateInfo *pCreateInfo, adkAllocationCallbacks *pAllocationCallbacks, AdkInstance **ppInstance);
void adkInstance_Destroy(AdkInstance *pInstance);

#endif // adkInstance_h__
