#include "AdkBinaryReader.h"
#include "adkMemory.h"
#include "adkString.h"

typedef struct AdkBinaryReader
{
  const adkAllocationCallbacks *pAllocator;
  size_t bufferSize;
  size_t bufferOffset;
  const uint8_t *pBuffer;
  bool success;
} AdkBinaryReader;

void adkBinaryReader_Create(const AdkBinaryReaderCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryReader **ppBinaryReader)
{
  AdkBinaryReader *pBinaryReader = adkAllocTypeCall(AdkBinaryReader, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pBinaryReader->pAllocator = pAllocator;
  pBinaryReader->bufferSize = pCreateInfo->bufferSize;
  pBinaryReader->pBuffer = (const uint8_t*)pCreateInfo->pBuffer;
  pBinaryReader->success = true;

  *ppBinaryReader = pBinaryReader;
}

void adkBinaryReader_Destroy(AdkBinaryReader **ppBinaryReader)
{
  if (*ppBinaryReader == nullptr)
    return;

  AdkBinaryReader *pBinaryReader = *ppBinaryReader;

  adkFreeCall(*ppBinaryReader, pBinaryReader->pAllocator);
}

void adkBinaryReader_Reset(AdkBinaryReader *pBinaryReader)
{
  pBinaryReader->bufferOffset = 0;
  pBinaryReader->success = true;
}

template<typename T>
static bool _ReadLittleEndian(AdkBinaryReader *pBinaryReader, T *pValue)
{
  pBinaryReader->success = pBinaryReader->success && pBinaryReader->bufferOffset + sizeof(T) <= pBinaryReader->bufferSize;

  if (pBinaryReader->success)
  {
    for (uint32_t i = 0; i < sizeof(T); ++i)
    {
      uint8_t byte = pBinaryReader->pBuffer[pBinaryReader->bufferOffset + (sizeof(T) - i - 1)];
      T shifted = *pValue << 8;
      *pValue = shifted | byte;
    }

    pBinaryReader->bufferOffset += sizeof(T);
  }

  return pBinaryReader->success;
}

static bool _Read(AdkBinaryReader *pBinaryReader, size_t valueSize, void *pValue)
{
  pBinaryReader->success = pBinaryReader->success && pBinaryReader->bufferOffset + valueSize <= pBinaryReader->bufferSize;
  if (pBinaryReader->success)
  {
    memcpy(pValue, &pBinaryReader->pBuffer[pBinaryReader->bufferOffset], valueSize);
    pBinaryReader->bufferOffset += valueSize;
  }

  return pBinaryReader->success;
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, bool *pValue)
{
  int8_t value = 0;
  bool success = _ReadLittleEndian(pBinaryReader, &value);
  *pValue = (value != 0);
  return success;
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int8_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int16_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int32_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int64_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint8_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint16_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint32_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint64_t *pValue)
{
  return _ReadLittleEndian(pBinaryReader, pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, float *pValue)
{
  return _Read(pBinaryReader, sizeof(*pValue), pValue);
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, double *pValue)
{
  return _Read(pBinaryReader, sizeof(*pValue), pValue);
}

bool adkBinaryReader_ReadString(AdkBinaryReader *pBinaryReader, size_t valueLength, char *pValue)
{
  if (pBinaryReader->success)
  {
    size_t offset = pBinaryReader->bufferOffset;
    while (offset < pBinaryReader->bufferSize && pBinaryReader->pBuffer[offset])
      offset++;

    ++offset; //Include the '\n'.

    size_t copySize = offset - pBinaryReader->bufferOffset;

    pBinaryReader->success = offset < pBinaryReader->bufferSize && copySize < valueLength;

    if (pBinaryReader->success)
    {
      memcpy(pValue, &pBinaryReader->pBuffer[pBinaryReader->bufferOffset], copySize);
      pBinaryReader->bufferOffset = offset;
    }
  }

  return pBinaryReader->success;
}

bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, size_t valueLength, void *pValue)
{
  return _Read(pBinaryReader, valueLength, pValue);
}

size_t adkBinaryReader_GetOffset(AdkBinaryReader *pBinaryReader)
{
  return pBinaryReader->bufferOffset;
}

bool adkBinaryReader_GetSuccess(AdkBinaryReader *pBinaryReader)
{
  return pBinaryReader->success;
}