#ifndef adkFont_h__
#define adkFont_h__

#include "adkTypedefs.h"
#include "adkMath.h"

struct AdkFont;
struct AdkInstance;
struct AdkFontReader;

typedef enum AdkFontTextMeshFlagBits
{
  ADK_FONT_TEXT_MESH_WORD_WRAP_BIT = 0x00000001,
} AdkFontTextMeshFlagBits;
typedef AdkFlags AdkFontTextMeshFlags;

typedef struct AdkFontCreateInfo
{
  AdkFontReader *pFontReader;
  const char *pFilename;
  uint32_t fontSize;
} AdkFontCreateInfo;

typedef struct AdkFontTextMeshInfo
{
  AdkFontTextMeshFlags flags;
  char *pCharacters;
  adkRect<int32_t> bounds;
  adkMatrix4x4<float> matrix;
} AdkFontTextMeshInfo;

AdkResult adkFont_Create(AdkInstance *pInstance, const AdkFontCreateInfo *pCreateInfo, AdkFont **ppFont);
void adkFont_Destroy(AdkInstance *pInstance, AdkFont **ppFont);
void adkFont_GetTextMesh(AdkFont *pFont, const AdkFontTextMeshInfo *pTextMeshInfo, uint32_t *pVertexCount, adkVector3<float> *pPositions, adkVector2<float> *pUvs);
void adkFont_GetBitmap(AdkFont *pFont, adkVector2<uint32_t> *pExtent, char *pBitmap);

#endif // adkFont_h__
