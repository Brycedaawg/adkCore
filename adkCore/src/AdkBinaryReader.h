#ifndef AdkBinaryReader_h__
#define AdkBinaryReader_h__

#include "adkTypedefs.h"

struct AdkBinaryReader;
struct adkAllocationCallbacks;

typedef struct AdkBinaryReaderCreateInfo
{
  size_t bufferSize;
  const void *pBuffer;
} AdkBinaryReaderCreateInfo;

void adkBinaryReader_Create(const AdkBinaryReaderCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryReader **ppBinaryReader);
void adkBinaryReader_Destroy(AdkBinaryReader **ppBinaryReader);
void adkBinaryReader_Reset(AdkBinaryReader *pBinaryReader);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, bool *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int8_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int16_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int32_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, int64_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint8_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint16_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint32_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, uint64_t *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, float *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, double *pValue);
bool adkBinaryReader_ReadString(AdkBinaryReader *pBinaryReader, size_t valueLength, char *pValue);
bool adkBinaryReader_Read(AdkBinaryReader *pBinaryReader, size_t valueLength, void *pValue);
size_t adkBinaryReader_GetOffset(AdkBinaryReader *pBinaryReader);
bool adkBinaryReader_GetSuccess(AdkBinaryReader *pBinaryReader);

#endif // AdkBinaryReader_h__
