#ifndef AdkClip_h__
#define AdkClip_h__

#include "adkTypedefs.h"
#include "AdkClipBody.h"
#include "AdkClipMesh.h"

#include "AdkOctree.h"

#define ADK_CLIP_MILLISECONDS_PER_STEP (1000 / 30)

struct AdkClip;
struct adkAllocationCallbacks;
struct AdkLineRenderer;
template <typename T> struct adkVector3;

typedef struct AdkClipFindProperties
{
  AdkClipBodyInstance *pInstance;
} AdkClipFindProperties;

void adkClip_Create(const adkAllocationCallbacks *pAllocator, AdkClip **ppClip);
void adkClip_Destroy(AdkClip **ppClip);
void adkClip_SetLayer(AdkClip *pClip, uint32_t layer, bool collides, uint32_t mask);
void adkClip_AddBody(AdkClip *pClip, AdkClipBody *pBody, uint32_t layer);
void adkClip_AddMesh(AdkClip *pClip, AdkClipMesh *pMesh, uint32_t layer);
void adkClip_RemoveBody(AdkClip *pClip, AdkClipBody *pBody);
void adkClip_RemoveMesh(AdkClip *pClip, AdkClipMesh *pMesh);
void adkClip_Update(AdkClip *pClip, uint64_t deltaTime, AdkLineRenderer *pLineRenderer = nullptr);
void adkClip_Find(AdkClip *pClip, const adkVector3<float> &position, float radius, uint32_t mask, uint32_t foundPositionsLength, uint32_t *pFoundCount, AdkClipFindProperties *pFound);
bool adkClip_Spherecast(AdkClip *pClip, const adkVector3<float>& start, const adkVector3<float> &velocity, float radius, uint32_t mask, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer = nullptr);

#endif // AdkClip_h__
