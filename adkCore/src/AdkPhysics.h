#ifndef AdkPhysics_h__
#define AdkPhysics_h__

#include "adkMath.h"
#include "adkMemory.h"
#include "AdkRigidbody.h"
#include "AdkPhysicsMesh.h"
#include "AdkTime.h"

struct AdkPhysics;

typedef struct AdkPhysicsCreateInfo
{
  uint64_t timeStep;
  uint32_t rigidbodyBufferCount;
  AdkRigidbodyBuffer **ppRigidbodyBuffers;
  uint32_t physicsMeshCount;
  AdkPhysicsMesh **ppPhysicsMeshes;
} AdkPhysicsCreateInfo;

void adkPhysics_Create(const AdkPhysicsCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkPhysics **ppPhysics);
void adkPhysics_Destroy(AdkPhysics **ppPhysics);
void adkPhysics_ResetTime(AdkPhysics *pPhysics);
void adkPhysics_Update(AdkPhysics *pPhysics, AdkLineRenderer *pLineRenderer = nullptr);
bool adkPhysics_Spherecast(AdkPhysics *pPhysics, const adkVector3<float> &start, const adkVector3<float> &end, float radius, AdkSphereSweepCollision *pCollision);

#endif // AdkPhysics_h__
