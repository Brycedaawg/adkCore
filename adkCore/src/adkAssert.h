#ifndef adkAssert_h__
#define adkAssert_h__

#include <assert.h>
#include "adkLog.h"
#define ADK_ASSERT(value, message, ...) (void)(!!(value) || (adkLog("Assertion failed: ("), adkLog(message), adkLog("), file " __FILE__ ", line %d", __LINE__), __debugbreak(), exit(1), 0))

#endif // adkAssert_h__