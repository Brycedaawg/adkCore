#ifndef adkRenderPipeline_h__
#define adkRenderPipeline_h__

#include <stdint.h>
#include "adkMath.h"

struct AdkRenderPipeline;
struct AdkInstance;
struct AdkBuffer;
struct AdkTexture;
struct AdkShader;
struct adkWindow;
struct AdkQueue;

typedef enum AdkRenderPipelineFlagsBits
{
  ADK_RENDER_PIPELINE_CLEAR_COLOR_BIT = 0x00000001,
  ADK_RENDER_PIPELINE_CLEAR_DEPTH_BIT = 0x00000002,
  ADK_RENDER_PIPELINE_DEPTH_READ_BIT = 0x00000004,
  ADK_RENDER_PIPELINE_DEPTH_WRITE_BIT = 0x00000008,
  ADK_RENDER_PIPELINE_MUTABLE_BIT = 0x00000010,
  ADK_RENDER_PIPELINE_INDEXED_BIT = 0x00000020,
} AdkRenderPipelineFlagsBits;
typedef AdkFlags AdkRenderPipelineFlags;

typedef enum AdkAttributeInputRate
{
  ADK_ATTRIBUTE_INPUT_RATE_VERTEX,
  ADK_ATTRIBUTE_INPUT_RATE_INSTANCE,
} AdkAttributeInputRate;

typedef enum AdkPrimitiveTopology
{
  ADK_PRIMITIVE_TOPOLOGY_POINT_LIST,
  ADK_PRIMITIVE_TOPOLOGY_LINE_LIST,
  ADK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
  ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
  ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
  ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
  ADK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY,
  ADK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY,
  ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY,
  ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY,
  ADK_PRIMITIVE_TOPOLOGY_PATCH_LIST,
} AdkPrimitiveTopology;

typedef enum AdkPolygonMode
{
  ADK_POLYGON_MODE_FILL,
  ADK_POLYGON_MODE_LINE,
  ADK_POLYGON_MODE_POINT,
} AdkPolygonMode;

typedef enum AdkBlendFactor
{
  ADK_BLEND_FACTOR_ZERO,
  ADK_BLEND_FACTOR_ONE,
  ADK_BLEND_FACTOR_SRC_COLOR,
  ADK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
  ADK_BLEND_FACTOR_DST_COLOR,
  ADK_BLEND_FACTOR_ONE_MINUS_DST_COLOR,
  ADK_BLEND_FACTOR_SRC_ALPHA,
  ADK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
  ADK_BLEND_FACTOR_DST_ALPHA,
  ADK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
  ADK_BLEND_FACTOR_CONSTANT_COLOR,
  ADK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR,
  ADK_BLEND_FACTOR_CONSTANT_ALPHA,
  ADK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA,
  ADK_BLEND_FACTOR_SRC_ALPHA_SATURATE,
} AdkBlendFactor;

typedef enum AdkRenderTargetType
{
  ADK_RENDER_TARGET_TYPE_WINDOW,
  ADK_RENDER_TARGET_TYPE_TEXTURE,
} AdkRenderTargetType;

typedef enum AdkSpecializationType
{
  ADK_SPECIALIZATION_TYPE_UINT32,
} AdkSpecializationType;

typedef union AdkRenderTarget
{
  adkWindow *pWindow;
  AdkTexture *pTexture;
} AdkRenderTarget;

typedef struct AdkAttributeInfo
{
  uint32_t location;
  AdkAttributeInputRate inputRate;
  uint32_t elementStride;
  AdkBuffer *pBuffer;
} AdkAttributeInfo;

typedef struct AdkBufferUniformInfo
{
  uint32_t binding;
  AdkBuffer *pBuffer;
} AdkBufferUniformInfo;

typedef struct AdkSamplerUniformInfo
{
  uint32_t binding;
  uint32_t count;
  AdkTexture **ppTextures;
} AdkSamplerUniformInfo;

typedef struct AdkRenderPipelineBlendInfo
{
  AdkBool32 enable;
  AdkBlendFactor srcColor;
  AdkBlendFactor dstColor;
  AdkBlendFactor srcAlpha;
  AdkBlendFactor dstAlpha;
} AdkRenderPipelineBlendInfo;

typedef union AdkSpecializationValue
{
  uint32_t uint32;
} AdkSpecializationValue;

typedef struct AdkSpecializationInfo
{
  uint32_t constantId;
  AdkSpecializationType type;
  AdkSpecializationValue value;
} AdkSpecializationInfo;

typedef struct AdkRenderPipelineCreateInfo
{
  AdkRenderPipelineFlags flags;
  AdkPrimitiveTopology primitiveTopology;
  AdkPolygonMode polygonMode;
  AdkRenderPipelineBlendInfo blending;
  uint32_t attributeCount;
  AdkAttributeInfo *pAttributes;
  uint32_t bufferUniformCount;
  AdkBufferUniformInfo *pBufferUniforms;
  uint32_t samplerUniformCount;
  AdkSamplerUniformInfo *pSamplerUniforms;
  uint32_t specializationCount;
  AdkSpecializationInfo *pSpecializations;
  AdkBuffer *pIndices;
  AdkShader *pShader;
  AdkRenderTargetType colorTargetType;
  AdkRenderTarget colorTarget;
  AdkRenderTargetType depthTargetType;
  AdkRenderTarget depthTarget;
  adkRect<uint32_t> viewport;
  uint32_t clearColor;
  AdkQueue *pGraphicsQueue;
} AdkRenderPipelineCreateInfo;

typedef struct AdkRenderPipelineRenderInfo
{
  adkRect<uint32_t> viewport;
} AdkRenderPipelineRenderInfo;

void adkRenderPipeline_Create(AdkInstance *pInstance, const AdkRenderPipelineCreateInfo *pCreateInfo, AdkRenderPipeline **ppRenderPipeline);
void adkRenderPipeline_Destroy(AdkInstance *pInstance, AdkRenderPipeline **ppRenderPipeline);
void adkRenderPipeline_Render(AdkInstance *pinstance, AdkRenderPipeline *pRenderPipeline, uint32_t vertexCount, uint32_t instanceCount);
void adkRenderPipeline_SetViewport(AdkRenderPipeline *pRenderPipeline, const adkRect<uint32_t> *pViewport);

#endif // adkRenderPipeline_h__
