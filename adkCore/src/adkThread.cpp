#include "adkThread.h"
#include "adkMemory.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

typedef struct AdkThread
{
#ifdef _WIN32
  HANDLE handle;
#endif
} AdkThread;

/*
typedef struct AdkMutex
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t counter;
} AdkMutex;*/

void adkThread_Create(AdkThreadFunction *pBegin, void *pData, const adkAllocationCallbacks *pAllocator, AdkThread **ppThread)
{
  AdkThread *pThread = adkAllocTypeCall(AdkThread, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

#ifdef _WIN32
  pThread->handle = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)pBegin, pData, 0, nullptr);
#endif

  (*ppThread) = pThread;
}

void adkThread_Destroy(const adkAllocationCallbacks *pAllocator, AdkThread **ppThread)
{
  AdkThread *pThread = (*ppThread);

  adkThread_Join(pThread);
  CloseHandle(pThread->handle);

  adkFreeCall(pThread, pAllocator);
  (*ppThread) = nullptr;
}

void adkThread_Join(AdkThread *pThread)
{
#ifdef _WIN32
  WaitForSingleObject(pThread->handle, INFINITE);
#endif
}

void adkSleep(uint64_t timeout)
{
#ifdef _WIN32
  Sleep((DWORD)timeout);
#else
  usleep((useconds_t)(timeout * 1000));
#endif
}

/*
void adkMutex_Create(const adkAllocationCallbacks *pAllocator, AdkMutex **ppMutex)
{
  AdkMutex *pMutex = adkAllocTypeCall(AdkMutex, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pMutex->pAllocator = pAllocator;

  *ppMutex = pMutex;
}

void adkMutex_Destroy(AdkMutex **ppMutex)
{
  AdkMutex *pMutex = *ppMutex;

  adkFreeCall(*ppMutex, pMutex->pAllocator);
}

void adkMutex_Lock(AdkMutex *pMutex)
{
  while (adkCompareAndSwap(&pMutex->counter, 0, 1))
  {

  }
}

bool adkMutex_TryLock(AdkMutex *pMutex)
{
  return adkCompareAndSwap(&pMutex->counter, 0, 1) == 1;
}

void adkMutex_Release(AdkMutex *pMutex)
{
  adkCompareAndSwap(&pMutex->counter, 1, 0);
}
*/

uint32_t adkCompareAndSwap(volatile uint32_t *pDestination, uint32_t compare, uint32_t swap)
{
#ifdef _WIN32
  return InterlockedCompareExchange(pDestination, swap, compare);
#endif
}

uint64_t adkCompareAndSwap(volatile uint64_t *pDestination, uint64_t compare, uint64_t swap)
{
#ifdef _WIN32
  return InterlockedCompareExchange64((int64_t*)pDestination, (int64_t)swap, (int64_t)compare);
#endif
}