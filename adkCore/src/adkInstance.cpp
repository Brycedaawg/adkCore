#include "adkInstance.inl"
#include "adkVkTools.inl"

#ifdef _WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#define PLATFORM_SURFACE_EXTENSION_NAME VK_KHR_WIN32_SURFACE_EXTENSION_NAME
#endif

static const char ENGINE_NAME[] = "Afterlife Development Kit";
static const uint32_t ENGINE_VERSION = 1;
static const char *DEBUG_ENABLED_LAYER_NAMES[] = { "VK_LAYER_LUNARG_standard_validation" };
static const char *DEBUG_INSTANCE_ENABLED_EXTENSION_NAMES[] = { VK_KHR_SURFACE_EXTENSION_NAME, PLATFORM_SURFACE_EXTENSION_NAME, VK_EXT_DEBUG_REPORT_EXTENSION_NAME };
static const char *RELEASE_INSTANCE_ENABLED_EXTENSION_NAMES[] = { VK_KHR_SURFACE_EXTENSION_NAME, PLATFORM_SURFACE_EXTENSION_NAME };
static const char *DEVICE_ENABLED_EXTENSION_NAMES[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, "VK_KHR_maintenance1" };

static VKAPI_ATTR VkBool32 VKAPI_CALL _DebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT /*objectType*/, uint64_t /*object*/, size_t /*location*/, int32_t /*messageCode*/, const char * /*pLayerPrefix*/, const char *pMessage, void * /*pUserData*/)
{
  adkLog("%s\n\n", pMessage);

  if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
    __debugbreak();

  return VK_FALSE;
}

AdkResult adkInstance_Create(const adkInstanceCreateInfo *pCreateInfo, adkAllocationCallbacks *pAllocationCallbacks, AdkInstance **ppInstance)
{
  AdkInstance *pInstance = adkAllocTypeCall(AdkInstance, 1, adkAF_Zero, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pInstance->primaryDeviceIndex = 0;

  VkAllocationCallbacks vkAllocationCallbacks, *pVkAllocationCallbacks = _AdkToVkAllocationCallbacks(pAllocationCallbacks, &vkAllocationCallbacks);

  VkApplicationInfo applicationInfo;
  applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  applicationInfo.pNext = nullptr;
  applicationInfo.pApplicationName = pCreateInfo->pApplicationName;
  applicationInfo.applicationVersion = pCreateInfo->applicationVersion;
  applicationInfo.pEngineName = ENGINE_NAME;
  applicationInfo.engineVersion = ENGINE_VERSION;
  applicationInfo.apiVersion = VK_API_VERSION;

  VkInstanceCreateInfo instanceCreateInfo;
  instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instanceCreateInfo.pNext = nullptr;
  instanceCreateInfo.flags = 0;
  instanceCreateInfo.pApplicationInfo = &applicationInfo;

  if (pCreateInfo->flags & ADK_INSTANCE_DEBUG_BIT)
  {
    instanceCreateInfo.enabledLayerCount = ADKARRAYSIZE(DEBUG_ENABLED_LAYER_NAMES);
    instanceCreateInfo.ppEnabledLayerNames = DEBUG_ENABLED_LAYER_NAMES;
    instanceCreateInfo.enabledExtensionCount = ADKARRAYSIZE(DEBUG_INSTANCE_ENABLED_EXTENSION_NAMES);
    instanceCreateInfo.ppEnabledExtensionNames = DEBUG_INSTANCE_ENABLED_EXTENSION_NAMES;
  }
  else
  {
    instanceCreateInfo.enabledLayerCount = 0;
    instanceCreateInfo.ppEnabledLayerNames = nullptr;
    instanceCreateInfo.enabledExtensionCount = ADKARRAYSIZE(RELEASE_INSTANCE_ENABLED_EXTENSION_NAMES);
    instanceCreateInfo.ppEnabledExtensionNames = RELEASE_INSTANCE_ENABLED_EXTENSION_NAMES;
  }

  VkInstance vkInstance;
  ADK_ASSERT_VK_RESULT(vkCreateInstance(&instanceCreateInfo, pVkAllocationCallbacks, &vkInstance));

  VkDebugReportCallbackEXT vkDebugReportCallback;

  if (pCreateInfo->flags & ADK_INSTANCE_DEBUG_BIT)
  {
    PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(vkInstance, "vkCreateDebugReportCallbackEXT"));

    VkDebugReportCallbackCreateInfoEXT vkDebugReportCallbackCreateInfo;
    vkDebugReportCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    vkDebugReportCallbackCreateInfo.pNext = nullptr;
    vkDebugReportCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    vkDebugReportCallbackCreateInfo.pfnCallback = _DebugReportCallback;
    vkDebugReportCallbackCreateInfo.pUserData = nullptr;

    ADK_ASSERT_VK_RESULT(vkCreateDebugReportCallbackEXT(vkInstance, &vkDebugReportCallbackCreateInfo, pVkAllocationCallbacks, &vkDebugReportCallback));
  }
  else
  {
    vkDebugReportCallback = VK_NULL_HANDLE;
  }

  uint32_t deviceCount;
  vkEnumeratePhysicalDevices(vkInstance, &deviceCount, nullptr);
  VkPhysicalDevice *pVkPhysicalDevices = adkAllocTypeCall(VkPhysicalDevice, deviceCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  vkEnumeratePhysicalDevices(vkInstance, &deviceCount, pVkPhysicalDevices);

  uint32_t vkQueueFamilyCount = 0;
  AdkRange<uint32_t> *pVkDeviceQueueFamilyRanges = adkAllocTypeCall(AdkRange<uint32_t>, deviceCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < deviceCount; ++i)
  {
    uint32_t deviceQueueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(pVkPhysicalDevices[i], &deviceQueueFamilyCount, nullptr);
    pVkDeviceQueueFamilyRanges[i].start = vkQueueFamilyCount;
    pVkDeviceQueueFamilyRanges[i].length = deviceQueueFamilyCount;
    vkQueueFamilyCount += deviceQueueFamilyCount;
  }

  VkQueueFamilyProperties *pVkQueueFamilyProperties = adkAllocTypeCall(VkQueueFamilyProperties, vkQueueFamilyCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < deviceCount; ++i)
  {
    uint32_t deviceQueueFamilyCount = pVkDeviceQueueFamilyRanges[i].length;
    vkGetPhysicalDeviceQueueFamilyProperties(pVkPhysicalDevices[i], &deviceQueueFamilyCount, pVkQueueFamilyProperties + pVkDeviceQueueFamilyRanges[i].start);
  }

  uint32_t vkQueueCount = 0;
  AdkRange<uint32_t> *pVkQueueFamilyQueueRanges = adkAllocTypeCall(AdkRange<uint32_t>, vkQueueFamilyCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < vkQueueFamilyCount; ++i)
  {
    pVkQueueFamilyQueueRanges[i].start = vkQueueCount;
    pVkQueueFamilyQueueRanges[i].length = pVkQueueFamilyProperties[i].queueCount;
    vkQueueCount += pVkQueueFamilyProperties[i].queueCount;
  }

  float *pQueuePriorities = adkAllocTypeCall(float, vkQueueCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

  for (uint32_t i = 0; i < vkQueueCount; ++i)
    pQueuePriorities[i] = 0.5f;

  VkDevice *pVkDevices = adkAllocTypeCall(VkDevice, deviceCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pInstance->pVkQueues = adkAllocTypeCall(VkQueue, vkQueueCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pInstance->pVkQueuesConsumed = adkAllocTypeCall(bool, vkQueueCount, adkAF_Zero, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < deviceCount; ++i)
  {
    VkDeviceQueueCreateInfo *pDeviceQueueCreateInfos = adkAllocTypeCall(VkDeviceQueueCreateInfo, pVkDeviceQueueFamilyRanges[i].length, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

    for (uint32_t j = 0; j < pVkDeviceQueueFamilyRanges[i].length; ++j)
    {
      uint32_t queueFamilyIndex = pVkDeviceQueueFamilyRanges[i].start + j;

      pDeviceQueueCreateInfos[j].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
      pDeviceQueueCreateInfos[j].pNext = nullptr;
      pDeviceQueueCreateInfos[j].flags = 0;
      pDeviceQueueCreateInfos[j].queueFamilyIndex = j;
      pDeviceQueueCreateInfos[j].queueCount = pVkQueueFamilyProperties[queueFamilyIndex].queueCount;
      pDeviceQueueCreateInfos[j].pQueuePriorities = pQueuePriorities + pVkQueueFamilyQueueRanges[queueFamilyIndex].start;
    }

    VkPhysicalDeviceFeatures physicalDeviceFeatures;
    vkGetPhysicalDeviceFeatures(pVkPhysicalDevices[i], &physicalDeviceFeatures);

    VkDeviceCreateInfo deviceCreateInfo;
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext = nullptr;
    deviceCreateInfo.flags = 0;
    deviceCreateInfo.queueCreateInfoCount = pVkDeviceQueueFamilyRanges[i].length;
    deviceCreateInfo.pQueueCreateInfos = pDeviceQueueCreateInfos;

    if (pCreateInfo->flags & ADK_INSTANCE_DEBUG_BIT)
    {
      deviceCreateInfo.enabledLayerCount = ADKARRAYSIZE(DEBUG_ENABLED_LAYER_NAMES);
      deviceCreateInfo.ppEnabledLayerNames = DEBUG_ENABLED_LAYER_NAMES;
    }
    else
    {
      deviceCreateInfo.enabledLayerCount = 0;
      deviceCreateInfo.ppEnabledLayerNames = nullptr;
    }

    deviceCreateInfo.enabledExtensionCount = ADKARRAYSIZE(DEVICE_ENABLED_EXTENSION_NAMES);
    deviceCreateInfo.ppEnabledExtensionNames = DEVICE_ENABLED_EXTENSION_NAMES;
    deviceCreateInfo.pEnabledFeatures = &physicalDeviceFeatures;

    ADK_ASSERT_VK_RESULT(vkCreateDevice(pVkPhysicalDevices[i], &deviceCreateInfo, pVkAllocationCallbacks, pVkDevices + i));

    adkFreeCall(pDeviceQueueCreateInfos, pAllocationCallbacks);

    for (uint32_t j = 0; j < pVkDeviceQueueFamilyRanges[i].length; ++j)
    {
      uint32_t queueFamilyIndex = pVkDeviceQueueFamilyRanges[i].start + j;

      for (uint32_t k = 0; k < pVkQueueFamilyProperties[queueFamilyIndex].queueCount; ++k)
      {
        uint32_t queueIndex = pVkQueueFamilyQueueRanges[queueFamilyIndex].start + k;
        vkGetDeviceQueue(pVkDevices[i], j, k, pInstance->pVkQueues + queueIndex);
      }
    }
  }

  adkFreeCall(pQueuePriorities, pAllocationCallbacks);

  VkCommandPool *pVkCommandPools = adkAllocTypeCall(VkCommandPool, 2 * vkQueueCount, adkAF_None, pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  VkCommandPool *pVkResetCommandPools = pVkCommandPools + vkQueueFamilyCount;

  for (uint32_t i = 0; i < deviceCount; ++i)
  {
    for (uint32_t j = 0; j < pVkDeviceQueueFamilyRanges[i].length; ++j)
    {
      uint32_t queueFamilyIndex = pVkDeviceQueueFamilyRanges[i].start + j;

      for (uint32_t k = 0; k < pVkQueueFamilyProperties[queueFamilyIndex].queueCount; ++k)
      {
        VkCommandPoolCreateInfo vkCommandPoolCreateInfo;
        vkCommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        vkCommandPoolCreateInfo.pNext = nullptr;
        vkCommandPoolCreateInfo.flags = 0;
        vkCommandPoolCreateInfo.queueFamilyIndex = j;
        ADK_ASSERT_VK_RESULT(vkCreateCommandPool(pVkDevices[i], &vkCommandPoolCreateInfo, pVkAllocationCallbacks, &pVkCommandPools[pVkQueueFamilyQueueRanges[queueFamilyIndex].start + k]));

        vkCommandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        ADK_ASSERT_VK_RESULT(vkCreateCommandPool(pVkDevices[i], &vkCommandPoolCreateInfo, pVkAllocationCallbacks, &pVkCommandPools[pVkQueueFamilyQueueRanges[queueFamilyIndex].start + k]));
      }
    }
  }

  adkMemoryPool_Create(pAllocationCallbacks, &pInstance->pMemoryPool);

  pInstance->pAllocationCallbacks = pAllocationCallbacks;
  pInstance->pVkAllocationCallbacks = pVkAllocationCallbacks;
  pInstance->flags = pCreateInfo->flags;
  pInstance->vkInstance = vkInstance;
  pInstance->vkDebugReportCallback = vkDebugReportCallback;
  pInstance->vkDeviceCount = deviceCount;
  pInstance->pVkPhysicalDevices = pVkPhysicalDevices;
  pInstance->pVkDevices = pVkDevices;
  pInstance->pVkDeviceQueueFamilyRanges = pVkDeviceQueueFamilyRanges;
  pInstance->vkQueueFamilyCount = vkQueueFamilyCount;
  pInstance->pVkQueueFamilyProperties = pVkQueueFamilyProperties;
  pInstance->pVkQueueFamilyQueueRange = pVkQueueFamilyQueueRanges;
  pInstance->vkQueueCount = vkQueueCount;
  pInstance->pvkCommandPools = pVkCommandPools;
  pInstance->pvkResetCommandPools = pVkResetCommandPools;

  (*ppInstance) = pInstance;

  return ADK_SUCCESS;
}

void adkInstance_Destroy(AdkInstance *pInstance)
{
  PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(pInstance->vkInstance, "vkDestroyDebugReportCallbackEXT"));
  vkDestroyDebugReportCallbackEXT(pInstance->vkInstance, pInstance->vkDebugReportCallback, pInstance->pVkAllocationCallbacks);

  adkMemoryPool_Destroy(&pInstance->pMemoryPool);
}