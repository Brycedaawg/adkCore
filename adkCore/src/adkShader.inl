#ifndef adkShader_inl__
#define adkShader_inl__

#include "adkShader.h"
#include "vulkan.h"
#include "adkString.h"
#include "adkAssert.h"

typedef enum AdkShaderStage
{
  ADK_SHADER_STAGE_VERTEX,
  ADK_SHADER_STAGE_GEOMETRY,
  ADK_SHADER_STAGE_FRAGMENT,
  ADK_SHADER_STAGE_COMPUTE,
  ADK_SHADER_STAGE_COUNT,
} AdkShaderStage;

typedef struct AdkShader
{
  AdkInstance *pInstance;
  VkShaderModule modules[ADK_SHADER_STAGE_COUNT];
} AdkShader;

#endif // adkShader_inl__
