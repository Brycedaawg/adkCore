#ifndef AdkLineRenderer_h__
#define AdkLineRenderer_h__

#include "adkWindow.h"
#include "AdkQueue.h"

struct AdkLineRenderer;

typedef struct AdkLineRendererCreateInfo
{
  adkWindow *pWindow;
  uint32_t maxLines;
  AdkQueue *pGraphicsQueue;
  AdkQueue *pTransferQueue;
} AdkLineRendererCreateInfo;

void adkLineRenderer_Create(AdkInstance *pInstance, const AdkLineRendererCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkLineRenderer **ppLineRenderer);
void adkLineRenderer_Destroy(AdkLineRenderer **ppLineRenderer);
void adkLineRenderer_Clear(AdkLineRenderer *pLineRenderer);
void adkLineRenderer_Add(AdkLineRenderer *pLineRenderer, const adkVector3<float> &pointA, uint32_t colorA, const adkVector3<float> &pointB, uint32_t colorB);
void adkLineRenderer_Render(AdkLineRenderer *pLineRenderer, const adkMatrix4x4<float> &viewProjection);

#endif // AdkLineRenderer_h__
