#ifndef AdkQueue_h__
#define AdkQueue_h__

#include "adkInstance.h"
#include "adkMemory.h"

struct AdkQueue;

#define ADK_QUEUE_FLAGS_IGNORE 0xFFFFFFFF

typedef enum AdkQueueFlagBits
{
  ADK_QUEUE_GRAPHICS_BIT = 0x00000001,
  ADK_QUEUE_COMPUTE_BIT = 0x00000002,
  ADK_QUEUE_TRANSFER_BIT = 0x00000004,
} AdkQueueFlagBits;
typedef AdkFlags AdkQueueFlags;

typedef struct AdkQueueCreateInfo
{
  AdkQueueFlags flags;
} AdkQueueCreateInfo;

AdkBool32 adkQueue_Create(AdkInstance *pInstance, const AdkQueueCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkQueue **ppQueue);
void adkQueue_Destroy(AdkQueue **ppQueue);

#endif // AdkQueue_h__
