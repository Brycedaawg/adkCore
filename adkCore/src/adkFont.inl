#ifndef adkFont_inl__
#define adkFont_inl__

#include "adkFont.h"
#include "adkInstance.inl"
#include "ft2build.h"
#include FT_FREETYPE_H

typedef struct AdkFont
{
  uint32_t bitmapWidth;
  uint32_t bitmapHeight;
  char *pBitmap;
  uint32_t fontSize;
  uint32_t glyphCount;
  uint32_t glyphsPerRow;
  adkVector2<float> *pUvs;
  adkVector2<float> *pBearings;
  adkVector2<float> *pExtents;
  adkVector2<float> *pAdvances;
  uint32_t *pAsciiGlyphIndices;
} AdkFont;

#endif // adkFont_inl__