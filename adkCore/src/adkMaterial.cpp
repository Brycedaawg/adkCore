#include "AdkMaterial.inl"
#include "adkTexture.h"
#include "adkMemory.h"
#include "adkInstance.inl"
#include "adkString.h"

void adkMaterial_Create(AdkInstance *pInstance, AdkShader *pShader, AdkMaterial **ppMaterial)
{
  AdkMaterial *pMaterial = adkAllocTypeCall(AdkMaterial, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pMaterial->pInstance = pInstance;
  pMaterial->pShader = pShader;

  (*ppMaterial) = pMaterial;
}

void adkMaterial_Destroy(AdkMaterial **ppMaterial)
{
  if (*ppMaterial == nullptr)
    return;

  AdkMaterial *pMaterial = (*ppMaterial);

  adkFreeCall(pMaterial, pMaterial->pInstance->pAllocationCallbacks);
}

void adkMaterial_SetTexture(AdkMaterial *pMaterial, uint32_t binding, AdkTexture *pTexture)
{
  uint32_t textureIndex = ADK_MATERIAL_INVALID_TEXTURE_INDEX;

  for (uint32_t i = 0; i < pMaterial->textureCount; ++i)
  {
    if (pMaterial->textureBindings[i] == binding)
    {
      textureIndex = i;
      break;
    }
  }

  if (textureIndex == ADK_MATERIAL_INVALID_TEXTURE_INDEX)
    textureIndex = pMaterial->textureCount++;

  pMaterial->textures[textureIndex] = pTexture;
  pMaterial->textureBindings[textureIndex] = binding;
}