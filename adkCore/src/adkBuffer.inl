#ifndef adkBuffer_inl__
#define adkBuffer_inl__

#include "adkBuffer.h"
#include "vulkan.h"
#include "adkMemoryPool.inl"

typedef struct AdkBuffer
{
  AdkBufferCreateFlags flags;
  uint32_t count;
  AdkBufferType type;
  VkBuffer buffer;
  VkBuffer stagingBuffer;
  AdkDeviceMemory deviceMemory;
  AdkDeviceMemory stagingDeviceMemory;
  VkCommandBuffer copyToDeviceCommandBuffer;
  VkCommandBuffer copyFromDeviceCommandBuffer;
  VkFence fence;
  VkSemaphore semaphore;
  AdkQueue *pTransferQueue;
} AdkBuffer;

static inline size_t adkBuffer_SizeOfElementType(AdkBufferType type)
{
  switch (type)
  {
  case ADK_BUFFER_TYPE_FLOAT32:
    return 4;
  case ADK_BUFFER_TYPE_FLOAT32_2:
    return 8;
  case ADK_BUFFER_TYPE_FLOAT32_3:
    return 12;
  case ADK_BUFFER_TYPE_FLOAT32_4:
    return 16;
  case ADK_BUFFER_TYPE_FLOAT32_4X4:
    return 64;
  case ADK_BUFFER_TYPE_UINT8:
    return 1;
  case ADK_BUFFER_TYPE_UINT32:
    return 4;
  case ADK_BUFFER_TYPE_UINT32_INDEX:
    return 4;
  default:
    return 0;
  }
}

static inline VkFormat adkBuffer_TypeToVkFormat(AdkBufferType type)
{
  switch (type)
  {
  case ADK_BUFFER_TYPE_FLOAT32:
    return VK_FORMAT_R32_SFLOAT;
  case ADK_BUFFER_TYPE_FLOAT32_2:
    return VK_FORMAT_R32G32_SFLOAT;
  case ADK_BUFFER_TYPE_FLOAT32_3:
    return VK_FORMAT_R32G32B32_SFLOAT;
  case ADK_BUFFER_TYPE_FLOAT32_4:
    return VK_FORMAT_R32G32B32A32_SFLOAT;
  case ADK_BUFFER_TYPE_FLOAT32_4X4:
    return VK_FORMAT_R32G32B32A32_SFLOAT;
  case ADK_BUFFER_TYPE_UINT8:
    return VK_FORMAT_R32_UINT;
  case ADK_BUFFER_TYPE_UINT32:
    return VK_FORMAT_R32_UINT;
  case ADK_BUFFER_TYPE_UINT32_INDEX:
    return VK_FORMAT_R32_UINT;
  default:
    return VK_FORMAT_UNDEFINED;
  }
}

static inline uint32_t adkBuffer_LocationCountOfType(AdkBufferType type)
{
  switch (type)
  {
  case ADK_BUFFER_TYPE_FLOAT32:
    return 1;
  case ADK_BUFFER_TYPE_FLOAT32_2:
    return 1;
  case ADK_BUFFER_TYPE_FLOAT32_3:
    return 1;
  case ADK_BUFFER_TYPE_FLOAT32_4:
    return 1;
  case ADK_BUFFER_TYPE_FLOAT32_4X4:
    return 4;
  case ADK_BUFFER_TYPE_UINT8:
    return 1;
  case ADK_BUFFER_TYPE_UINT32:
    return 1;
  case ADK_BUFFER_TYPE_UINT32_INDEX:
    return 4;
  default:
    return 0;
  }
}

static inline AdkBufferType adkBuffer_LocationTypeOfType(AdkBufferType type)
{
  switch (type)
  {
  case ADK_BUFFER_TYPE_FLOAT32:
    return ADK_BUFFER_TYPE_FLOAT32;
  case ADK_BUFFER_TYPE_FLOAT32_2:
    return ADK_BUFFER_TYPE_FLOAT32_2;
  case ADK_BUFFER_TYPE_FLOAT32_3:
    return ADK_BUFFER_TYPE_FLOAT32_3;
  case ADK_BUFFER_TYPE_FLOAT32_4:
    return ADK_BUFFER_TYPE_FLOAT32_4;
  case ADK_BUFFER_TYPE_FLOAT32_4X4:
    return ADK_BUFFER_TYPE_FLOAT32_4;
  case ADK_BUFFER_TYPE_UINT8:
    return ADK_BUFFER_TYPE_UINT8;
  case ADK_BUFFER_TYPE_UINT32:
    return ADK_BUFFER_TYPE_UINT32;
  case ADK_BUFFER_TYPE_UINT32_INDEX:
    return ADK_BUFFER_TYPE_UINT32_INDEX;
  default:
    return ADK_BUFFER_TYPE_FLOAT32;
  }
}

static inline VkBufferUsageFlags adkBuffer_TypeToVkBufferUsageFlags(AdkBufferType type)
{
  switch (type)
  {
  case ADK_BUFFER_TYPE_FLOAT32:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_FLOAT32_2:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_FLOAT32_3:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_FLOAT32_4:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_FLOAT32_4X4:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_UINT8:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_UINT32:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  case ADK_BUFFER_TYPE_UINT32_INDEX:
    return VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
  default:
    return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
  }
}

#endif // adkBuffer_inl__