#ifndef AdkPhysicsMesh_h__
#define AdkPhysicsMesh_h__

#include "adkMath.h"
#include "adkMemory.h"
#include "AdkOctree.h"
#include "AdkLineRenderer.h"

struct AdkPhysicsMesh;
struct AdkBinaryReader;
struct AdkBinaryWriter;

typedef struct AdkPhysicsMeshCreateInfo
{
  uint32_t maxTriangleCount;
} AdkPhysicsMeshCreateInfo;

void adkPhysicsMesh_Create(const AdkPhysicsMeshCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkPhysicsMesh **ppPhysicsMesh);
void adkPhysicsMesh_Destroy(AdkPhysicsMesh **ppPhysicsMesh);
bool adkPhysicsMesh_Serialize(AdkPhysicsMesh *pPhysicsMesh, AdkBinaryWriter *pBinaryWriter);
bool adkPhysicsMesh_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkPhysicsMesh **ppPhysicsMesh);
void adkPhysicsMesh_Copy(AdkPhysicsMesh *pSource, AdkPhysicsMesh **ppCopy);
void adkPhysicsMesh_SetTriangles(AdkPhysicsMesh *pPhysicsMesh, uint32_t triangleCount, adkVector3<float> *pTriangles);
bool adkPhysicsMesh_SweepSphere(AdkPhysicsMesh *pPhysicsMesh, const adkVector3<float> &sphereStart, const adkVector3<float> &sphereEnd, float sphereRadius, AdkSphereSweepCollision *pCollision);
void adkPhysicsMesh_SetEnabled(AdkPhysicsMesh *pPhysicsMesh, bool enabled);


#endif // AdkPhysicsMesh_h__
