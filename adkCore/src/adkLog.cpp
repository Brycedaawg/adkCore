#include "adkLog.h"
#include <vadefs.h>
#include <stdarg.h>
#include <stdio.h>

int adkLog(const char* format, ...)
{
  va_list pArgs;
  va_start(pArgs, format);
  int written = vfprintf(stderr, format, pArgs);
  va_end(pArgs);
  return written;
}