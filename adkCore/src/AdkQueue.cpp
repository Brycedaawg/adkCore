#include "AdkQueue.inl"
#include "adkInstance.inl"

VkQueueFlags _AdkToVkQueueFlags(AdkQueueFlags flags)
{
  VkQueueFlags output = 0;

  if (flags & ADK_QUEUE_GRAPHICS_BIT)
    output |= VK_QUEUE_GRAPHICS_BIT;

  if (flags & ADK_QUEUE_COMPUTE_BIT)
    output |= VK_QUEUE_COMPUTE_BIT;

  if (flags & ADK_QUEUE_TRANSFER_BIT)
    output |= VK_QUEUE_TRANSFER_BIT;

  return output;
}

AdkBool32 adkQueue_Create(AdkInstance *pInstance, const AdkQueueCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkQueue **ppQueue)
{
  bool success = false;
  uint32_t deviceIndex = pInstance->primaryDeviceIndex;
  bool queueFound = false;
  VkQueueFlags vkQueueflags = _AdkToVkQueueFlags(pCreateInfo->flags);
  uint32_t queueFamilyIndex = 0;
  uint32_t queueIndex = 0;
  AdkQueue *pQueue = nullptr;

  //Find a queue that exactly matches the requirements.
  for (uint32_t i = 0; i < pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].length && !queueFound; ++i)
  {
    queueFamilyIndex = pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].start + i;

    if (vkQueueflags == pInstance->pVkQueueFamilyProperties[queueFamilyIndex].queueFlags)
    {
      uint32_t queueFamilyStart = pInstance->pVkQueueFamilyQueueRange[queueFamilyIndex].start;
      uint32_t queueFamilyEnd = queueFamilyStart + pInstance->pVkQueueFamilyQueueRange[queueFamilyIndex].length;
      for (uint32_t j = queueFamilyStart; j < queueFamilyEnd; ++j)
      {
        if (!pInstance->pVkQueuesConsumed[j])
        {
          pInstance->pVkQueuesConsumed[j] = true;
          queueIndex = j;
          queueFound = true;
          break;
        }
      }
    }
  }

  //Find a queue that broadly matches the requirements.
  for (uint32_t i = 0; i < pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].length && !queueFound; ++i)
  {
    queueFamilyIndex = pInstance->pVkDeviceQueueFamilyRanges[deviceIndex].start + i;

    if ((vkQueueflags & pInstance->pVkQueueFamilyProperties[queueFamilyIndex].queueFlags) == vkQueueflags)
    {
      uint32_t queueFamilyStart = pInstance->pVkQueueFamilyQueueRange[queueFamilyIndex].start;
      uint32_t queueFamilyEnd = queueFamilyStart + pInstance->pVkQueueFamilyQueueRange[queueFamilyIndex].length;
      for (uint32_t j = queueFamilyStart; j < queueFamilyEnd; ++j)
      {
        if (!pInstance->pVkQueuesConsumed[j])
        {
          pInstance->pVkQueuesConsumed[j] = true;
          queueIndex = j;
          queueFound = true;
          break;
        }
      }
    }
  }

  if (!queueFound)
    goto epilogue;

  pQueue = adkAllocTypeCall(AdkQueue, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pQueue->pInstance = pInstance;
  pQueue->pAllocator = pAllocator;
  pQueue->familyIndex = queueFamilyIndex;
  pQueue->index = queueIndex;

  *ppQueue = pQueue;
  success = true;

epilogue:
  if (!success)
    adkFreeCall(pQueue, pAllocator);

  return success;
}

void adkQueue_Destroy(AdkQueue **ppQueue)
{
  AdkQueue *pQueue = *ppQueue;

  pQueue->pInstance->pVkQueuesConsumed[pQueue->index] = false;

  adkFreeCall(*ppQueue, pQueue->pAllocator);
}