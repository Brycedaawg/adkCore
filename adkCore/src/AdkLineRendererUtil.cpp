#include "AdkLineRendererUtil.h"
#include "adkMath.h"

void adkLineRenderer_AddBox(AdkLineRenderer *pLineRenderer, const adkVector3<float> &min, const adkVector3<float> &max, uint32_t color)
{
  adkVector3<float> a = adkVector3<float>::create(min.x, min.y, min.z);
  adkVector3<float> b = adkVector3<float>::create(max.x, min.y, min.z);
  adkVector3<float> c = adkVector3<float>::create(max.x, max.y, min.z);
  adkVector3<float> d = adkVector3<float>::create(min.x, max.y, min.z);

  adkVector3<float> e = adkVector3<float>::create(min.x, min.y, max.z);
  adkVector3<float> f = adkVector3<float>::create(max.x, min.y, max.z);
  adkVector3<float> g = adkVector3<float>::create(max.x, max.y, max.z);
  adkVector3<float> h = adkVector3<float>::create(min.x, max.y, max.z);

  adkLineRenderer_Add(pLineRenderer, a, color, b, color);
  adkLineRenderer_Add(pLineRenderer, b, color, c, color);
  adkLineRenderer_Add(pLineRenderer, c, color, d, color);
  adkLineRenderer_Add(pLineRenderer, d, color, a, color);

  adkLineRenderer_Add(pLineRenderer, a, color, e, color);
  adkLineRenderer_Add(pLineRenderer, b, color, f, color);
  adkLineRenderer_Add(pLineRenderer, c, color, g, color);
  adkLineRenderer_Add(pLineRenderer, d, color, h, color);

  adkLineRenderer_Add(pLineRenderer, e, color, f, color);
  adkLineRenderer_Add(pLineRenderer, f, color, g, color);
  adkLineRenderer_Add(pLineRenderer, g, color, h, color);
  adkLineRenderer_Add(pLineRenderer, h, color, e, color);
}

void adkLineRenderer_AddCircle(AdkLineRenderer *pLineRenderer, const adkVector3<float> &point, float radius, const adkQuaternion<float> &rotation, uint32_t color)
{
  const uint32_t LINE_COUNT = 32;

  adkVector3<float> a = adkVector3<float>::create(radius, 0.f, 0.f);
  a = point + rotation.apply(a);

  for (uint32_t i = 1; i <= LINE_COUNT; ++i)
  {
    adkVector3<float> b = radius * adkVector3<float>::create(adkCosf((float)i / LINE_COUNT * adk2PIf), adkSinf((float)i / LINE_COUNT * adk2PIf), 0.f);
    b = point + rotation.apply(b);
    adkLineRenderer_Add(pLineRenderer, a, color, b, color);
    a = b;
  }
}

void adkLineRenderer_AddSphere(AdkLineRenderer *pLineRenderer, const adkVector3<float> &point, float radius, const adkQuaternion<float> &rotation, uint32_t color)
{
  adkLineRenderer_AddCircle(pLineRenderer, point, radius, rotation, color);
  adkLineRenderer_AddCircle(pLineRenderer, point, radius, adkQuaternion<float>::create(0.5f * adkPIf, adkVector3<float>::create(1.f, 0.f, 0.f)) * rotation, color);
  adkLineRenderer_AddCircle(pLineRenderer, point, radius, adkQuaternion<float>::create(0.5f * adkPIf, adkVector3<float>::create(0.f, 1.f, 0.f)) * rotation, color);
}

void adkLineRenderer_AddTriangle(AdkLineRenderer *pLineRenderer, adkVector3<float> points[3], uint32_t color)
{
  adkLineRenderer_Add(pLineRenderer, points[0], color, points[1], color);
  adkLineRenderer_Add(pLineRenderer, points[1], color, points[2], color);
  adkLineRenderer_Add(pLineRenderer, points[2], color, points[0], color);
}

static inline void _AddCap(AdkLineRenderer *pLineRenderer, const adkVector3<float> &point, float radius, const adkQuaternion<float> &rotation, uint32_t color)
{
  const uint32_t LINE_COUNT = 16;

  adkVector3<float> a0 = adkVector3<float>::create(radius, 0.f, 0.f);
  adkVector3<float> a1 = adkVector3<float>::create(0.f, 0.f, radius);
  a0 = point + rotation.apply(a0);
  a1 = point + rotation.apply(a1);

  for (uint32_t i = 1; i <= LINE_COUNT; ++i)
  {
    adkVector3<float> b0 = radius * adkVector3<float>::create(adkCosf((float)i / LINE_COUNT * adkPIf), adkSinf((float)i / LINE_COUNT * adkPIf), 0.f);
    adkVector3<float> b1 = radius * adkVector3<float>::create(0.f, adkSinf((float)i / LINE_COUNT * adkPIf), adkCosf((float)i / LINE_COUNT * adkPIf));
    b0 = point + rotation.apply(b0);
    b1 = point + rotation.apply(b1);
    adkLineRenderer_Add(pLineRenderer, a0, color, b0, color);
    adkLineRenderer_Add(pLineRenderer, a1, color, b1, color);
    a0 = b0;
    a1 = b1;
  }
}

void adkLineRenderer_AddCapsule(AdkLineRenderer *pLineRenderer, const adkVector3<float> &position, float height, float radius, const adkQuaternion<float> &rotation, uint32_t color)
{
  //adkQuaternion<float> inverse = rotation.inverse();
  adkVector3<float> start = adkVector3<float>::create(0.f, -0.5f * height, 0.f);
  //adkVector3<float> end = adkVector3<float>::create(0.f, 0.5f * height, 0.f);
  start = position + rotation.apply(start);
  //end = position + inverse.apply(end);

  _AddCap(pLineRenderer, start, radius, rotation, color);
  //_AddCap(pLineRenderer, end, radius, rotation, color);
}