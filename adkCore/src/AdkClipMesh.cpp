#include "AdkClipMesh.inl"
#include "adkMemory.h"
#include "AdkLineRenderer.h"
#include "adkAssert.h"
#include "AdkBinaryReader.h"
#include "AdkBinaryWriter.h"

void adkClipMesh_Create(const AdkClipMeshCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkClipMesh **ppClipMesh)
{
  AdkClipMesh *pClipMesh = adkAllocTypeCall(AdkClipMesh, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pClipMesh->maxTriangleCount = pCreateInfo->maxTriangleCount;

  pClipMesh->pTriangles = adkAllocTypeCall(adkVector3<float>, 3 * pClipMesh->maxTriangleCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  adkOctree_Create(pCreateInfo->maxTriangleCount, pAllocator, &pClipMesh->pOctree);

  pClipMesh->pAllocator = pAllocator;

  *ppClipMesh = pClipMesh;
}

void adkClipMesh_Destroy(AdkClipMesh **ppClipMesh)
{
  AdkClipMesh *pClipMesh = *ppClipMesh;

  adkOctree_Destroy(&pClipMesh->pOctree);

  adkFreeCall(pClipMesh->pTriangles, pClipMesh->pAllocator);

  adkFreeCall(*ppClipMesh, pClipMesh->pAllocator);
}

bool adkClipMesh_Serialize(AdkClipMesh *pClipMesh, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, pClipMesh->triangleCount);
  adkBinaryWriter_Write(pBinaryWriter, pClipMesh->maxTriangleCount);

  adkBinaryWriter_Write(pBinaryWriter, 3 * pClipMesh->triangleCount * sizeof(*pClipMesh->pTriangles), pClipMesh->pTriangles);

  adkOctree_Serialize(pClipMesh->pOctree, pBinaryWriter);

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkClipMesh_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkClipMesh **ppClipMesh)
{
  bool success = false;
  AdkClipMesh *pClipMesh = adkAllocTypeCall(AdkClipMesh, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pClipMesh->pAllocator = pAllocator;

  adkBinaryReader_Read(pBinaryReader, &pClipMesh->triangleCount);
  adkBinaryReader_Read(pBinaryReader, &pClipMesh->maxTriangleCount);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  pClipMesh->pTriangles = adkAllocTypeCall(adkVector3<float>, 3 * pClipMesh->triangleCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  adkBinaryReader_Read(pBinaryReader, 3 * pClipMesh->triangleCount * sizeof(*pClipMesh->pTriangles), pClipMesh->pTriangles);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  if (!adkOctree_Deserialize(pBinaryReader, pAllocator, &pClipMesh->pOctree))
    goto epilogue;

  *ppClipMesh = pClipMesh;
  success = true;

epilogue:
  if (!success)
  {
    adkOctree_Destroy(&pClipMesh->pOctree);
    adkFreeCall(pClipMesh->pTriangles, pAllocator);
    adkFreeCall(pClipMesh, pAllocator);
  }
  return success;
}

void adkClipMesh_Copy(AdkClipMesh *pSource, AdkClipMesh **ppCopy)
{
  AdkClipMesh *pCopy = nullptr;
  AdkClipMeshCreateInfo createInfo;
  createInfo.maxTriangleCount = pSource->maxTriangleCount;
  adkClipMesh_Create(&createInfo, pSource->pAllocator, &pCopy);
  adkClipMesh_SetTriangles(pCopy, pSource->triangleCount, pSource->pTriangles);
  *ppCopy = pCopy;
}

void adkClipMesh_SetTriangles(AdkClipMesh *pClipMesh, uint32_t triangleCount, adkVector3<float> *pTriangles)
{
  ADK_ASSERT(triangleCount <= pClipMesh->maxTriangleCount, "triangleCount must be less than or equal to pCreateInfo->maxTriangleCount passed into adkClipMesh_Create when this adkClipMesh was created.");

  pClipMesh->triangleCount = triangleCount;
  adkMemCpy(pClipMesh->pTriangles, pTriangles, triangleCount * 3 * sizeof(*pTriangles));

  adkOctree_Link(pClipMesh->pOctree, triangleCount, (AdkOctreeTriangle*)pTriangles);
}

bool adkClipMesh_SweepSphere(AdkClipMesh *pClipMesh, const adkVector3<float> &sphereStart, const adkVector3<float> &sphereVelocity, float sphereRadius, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  return adkOctree_SweepSphere(pClipMesh->pOctree, sphereStart, sphereVelocity, sphereRadius, pCollision, pLineRenderer);
}