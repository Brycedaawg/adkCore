#ifndef adkTexture_inl__
#define adkTexture_inl__

#include "adkTexture.h"
#include "vulkan.h"
#include "adkVkTools.inl"
#include "AdkQueue.inl"

typedef struct AdkTexture
{
  AdkInstance *pInstance;
  AdkFlags createFlags;
  VkDevice device;
  VkBuffer stagingBuffer;
  VkCommandPool commandPool;
  VkCommandPool resetCommandPool;
  VkCommandBuffer copyCommandBuffer;
  VkCommandBuffer copyFromDeviceCommandBuffer;
  AdkColorType colorType;
  VkImageType imageType;
  VkFormat vkFormat;
  uint32_t mipLevels;
  VkAccessFlags accessMask;
  VkImageLayout layout;
  VkPipelineStageFlags stageMask;
  VkImage image;
  AdkDeviceMemory stagingDeviceMemory;
  AdkDeviceMemory deviceMemory;
  VkSampler sampler;
  VkImageView imageView;
  adkVector3<uint32_t> extent;
  VkSemaphore semaphore;
  VkCommandBuffer transitionCommandBuffer;
  VkFence transitionFence;
  AdkQueue *pGraphicsQueue;
} AdkTexture;

static inline size_t adkTexture_SizeOfColorType(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_R8:
    return 1;
  case ADK_COLOR_TYPE_R8G8:
    return 2;
  case ADK_COLOR_TYPE_R8G8B8:
    return 3;
  case ADK_COLOR_TYPE_R8G8B8A8:
    return 4;
  case ADK_COLOR_TYPE_R32:
    return 4;
  case ADK_COLOR_TYPE_R32_SFLOAT:
    return 4;
  case ADK_COLOR_TYPE_R32G32_SFLOAT:
    return 8;
  case ADK_COLOR_TYPE_R32G32B32_SFLOAT:
    return 12;
  case ADK_COLOR_TYPE_R32G32B32A32_SFLOAT:
    return 16;
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return 4;
  default:
    ADK_ASSERT(false, "Invalide color type");
    return 0;
  }
}

static inline VkFormat adkTexture_ColorTypeToVkFormat(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_R8:
    return VK_FORMAT_R8_UNORM;
  case ADK_COLOR_TYPE_R8G8:
    return VK_FORMAT_R8G8_UNORM;
  case ADK_COLOR_TYPE_R8G8B8:
    return VK_FORMAT_R8G8B8_UNORM;
  case ADK_COLOR_TYPE_R8G8B8A8:
    return VK_FORMAT_R8G8B8A8_UNORM;
  case ADK_COLOR_TYPE_R32:
    return VK_FORMAT_R32_UINT;
  case ADK_COLOR_TYPE_R32_SFLOAT:
    return VK_FORMAT_R32_SFLOAT;
  case ADK_COLOR_TYPE_R32G32_SFLOAT:
    return VK_FORMAT_R32G32_SFLOAT;
  case ADK_COLOR_TYPE_R32G32B32_SFLOAT:
    return VK_FORMAT_R32G32B32_SFLOAT;
  case ADK_COLOR_TYPE_R32G32B32A32_SFLOAT:
    return VK_FORMAT_R32G32B32A32_SFLOAT;
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return VK_FORMAT_D32_SFLOAT;
  default:
    ADK_ASSERT(false, "Invalide color type");
    return VK_FORMAT_UNDEFINED;
  }
}

static inline VkImageAspectFlags adkTexture_ColorTypeToImageAspect(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return VK_IMAGE_ASPECT_DEPTH_BIT;
  default:
    return VK_IMAGE_ASPECT_COLOR_BIT;
  }
}

static inline VkImageLayout adkTexture_ColorTypeToImageLayout(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
  default:
    return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  }
}

static inline VkImageUsageFlags adkTexture_ColorTypeToImageUsage(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
  default:
    return VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT;
  }
}

static inline AdkBool32 adkTexture_ColorTypeIsDepth(AdkColorType type)
{
  switch (type)
  {
  case ADK_COLOR_TYPE_D32_SFLOAT:
    return ADK_TRUE;
  default:
    return ADK_FALSE;
  }
}

static inline void adkTexture_ChangeLayout(AdkTexture *pTexture, VkAccessFlags accessMask, VkImageLayout layout, VkPipelineStageFlags stageMask)
{
  if (accessMask == pTexture->accessMask && layout == pTexture->layout)
    return;

  VkQueue queue = pTexture->pInstance->pVkQueues[pTexture->pGraphicsQueue->index];

  _WaitForFence(pTexture->device, pTexture->transitionFence, true);

  VkCommandBufferBeginInfo beginInfo;
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.pNext = nullptr;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
  beginInfo.pInheritanceInfo = nullptr;
  ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pTexture->transitionCommandBuffer, &beginInfo));

  VkImageMemoryBarrier barrier;
  barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.pNext = nullptr;
  barrier.srcAccessMask = pTexture->accessMask;
  barrier.dstAccessMask = accessMask;
  barrier.oldLayout = pTexture->layout;
  barrier.newLayout = layout;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.image = pTexture->image;
  barrier.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pTexture->colorType);
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = pTexture->mipLevels;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;
  vkCmdPipelineBarrier(pTexture->transitionCommandBuffer, pTexture->stageMask, stageMask, 0, 0, nullptr, 0, nullptr, 1, &barrier);

  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pTexture->transitionCommandBuffer));

  VkPipelineStageFlags waitMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pTexture->semaphore;
  submitInfo.pWaitDstStageMask = &waitMask;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pTexture->transitionCommandBuffer;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &pTexture->semaphore;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pTexture->transitionFence));

  pTexture->accessMask = accessMask;
  pTexture->layout = layout;
  pTexture->stageMask = stageMask;
}

#endif // adkTexture_inl__