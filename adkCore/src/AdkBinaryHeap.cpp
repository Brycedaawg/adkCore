#include "AdkBinaryHeap.h"
#include "adkMemory.h"
#include "adkAssert.h"
#include "adkMath.h"

static const uint32_t SWAP_INDEX = 0;

typedef struct AdkBinaryHeap
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t depth;
  size_t elementSize;
  uint32_t elementCount;
  uint32_t maxElementCount;
  float *pScores;
  char *pElements;
} AdkBinaryHeap;

void adkBinaryHeap_Create(const AdkBinaryHeapCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryHeap **ppBinaryHeap)
{
  AdkBinaryHeap *pBinaryHeap = adkAllocTypeCall(AdkBinaryHeap, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pBinaryHeap->depth = pCreateInfo->depth;
  pBinaryHeap->elementSize = pCreateInfo->elementSize;
  pBinaryHeap->maxElementCount = 1 << pCreateInfo->depth;

  pBinaryHeap->pScores = adkAllocTypeCall(float, pBinaryHeap->maxElementCount + 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pBinaryHeap->pElements = adkAllocTypeCall(char, (pBinaryHeap->maxElementCount + 1) * pCreateInfo->elementSize, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pBinaryHeap->pAllocator = pAllocator;

  *ppBinaryHeap = pBinaryHeap;
}

void adkBinaryHeap_Destroy(AdkBinaryHeap **ppBinaryHeap)
{
  AdkBinaryHeap *pBinaryHeap = *ppBinaryHeap;

  adkFreeCall(pBinaryHeap->pElements, pBinaryHeap->pAllocator);
  adkFreeCall(pBinaryHeap->pScores, pBinaryHeap->pAllocator);

  adkFreeCall(*ppBinaryHeap, pBinaryHeap->pAllocator);
}

static inline void _SwapScoresAndElements(AdkBinaryHeap *pBinaryHeap, uint32_t indexA, uint32_t indexB)
{
  adkSwap(&pBinaryHeap->pScores[indexA], &pBinaryHeap->pScores[indexB]);

  size_t a = indexA * pBinaryHeap->elementSize;
  size_t b = indexB * pBinaryHeap->elementSize;

  memcpy(&pBinaryHeap->pElements[SWAP_INDEX], &pBinaryHeap->pElements[a], pBinaryHeap->elementSize);
  memcpy(&pBinaryHeap->pElements[a], &pBinaryHeap->pElements[b], pBinaryHeap->elementSize);
  memcpy(&pBinaryHeap->pElements[b], &pBinaryHeap->pElements[SWAP_INDEX], pBinaryHeap->elementSize);
}

void adkBinaryHeap_Push(AdkBinaryHeap *pBinaryHeap, float score, const void *pElement)
{
  ADK_ASSERT(pBinaryHeap->elementCount != pBinaryHeap->maxElementCount, "Cannot push because binary heap has reached max capacity of %d", pBinaryHeap->elementCount);

  //Binary heap starts at element 1.
  uint32_t index = pBinaryHeap->elementCount + 1;
  uint32_t nextIndex = index >> 1;

  pBinaryHeap->pScores[index] = score;
  memcpy(&pBinaryHeap->pElements[index * pBinaryHeap->elementSize], pElement, pBinaryHeap->elementSize);

  //Balance upward
  while (index != 1 && pBinaryHeap->pScores[index] < pBinaryHeap->pScores[nextIndex])
  {
    _SwapScoresAndElements(pBinaryHeap, index, nextIndex);

    index = nextIndex;
    nextIndex >>= 1;
  }

  ++pBinaryHeap->elementCount;
}

bool adkBinaryHeap_Pop(AdkBinaryHeap *pBinaryHeap, float *pScore, void *pElement)
{
  if (pBinaryHeap->elementCount == 0)
    return false;

  //Binary heap starts at element 1.
  uint32_t index = 1;
  uint32_t nextIndexA = index << 1;
  uint32_t nextIndexB = nextIndexA + 1;

  *pScore = pBinaryHeap->pScores[index];
  pBinaryHeap->pScores[index] = pBinaryHeap->pScores[pBinaryHeap->elementCount];

  memcpy(pElement, &pBinaryHeap->pElements[index * pBinaryHeap->elementSize], pBinaryHeap->elementSize);
  memcpy(&pBinaryHeap->pElements[index * pBinaryHeap->elementSize], &pBinaryHeap->pElements[pBinaryHeap->elementCount * pBinaryHeap->elementSize], pBinaryHeap->elementSize);

  --pBinaryHeap->elementCount;

  //Balance downward
  while (true)
  {
    uint32_t bestNextIndex = 0;

    bool validA = nextIndexA <= pBinaryHeap->elementCount && pBinaryHeap->pScores[index] > pBinaryHeap->pScores[nextIndexA];
    bool validB = nextIndexB <= pBinaryHeap->elementCount && pBinaryHeap->pScores[index] > pBinaryHeap->pScores[nextIndexB];

    if (validA && validB)
      bestNextIndex = pBinaryHeap->pScores[nextIndexA] < pBinaryHeap->pScores[nextIndexB] ? nextIndexA : nextIndexB;
    else if (validA)
      bestNextIndex = nextIndexA;
    else if (validB)
      bestNextIndex = nextIndexB;
    else
      break;

    _SwapScoresAndElements(pBinaryHeap, index, bestNextIndex);
    index = bestNextIndex;
    nextIndexA = bestNextIndex << 1;
    nextIndexB = nextIndexA + 1;
  }

  return true;
}

void adkBinaryHeap_Peek(AdkBinaryHeap *pBinaryHeap, uint32_t index, void *pElement)
{
  memcpy(pElement, &pBinaryHeap->pElements[index + 1], pBinaryHeap->elementSize);
}

void adkBinaryHeap_Clear(AdkBinaryHeap *pBinaryHeap)
{
  pBinaryHeap->elementCount = 0;
}

uint32_t adkBinaryHeap_GetCount(AdkBinaryHeap *pBinaryHeap)
{
  return pBinaryHeap->elementCount;
}