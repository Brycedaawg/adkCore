#ifndef adkDensity3dNormal_h__
#define adkDensity3dNormal_h__

#include "adkTexture.h"
#include "AdkQueue.h"

struct AdkDensity3dNormal;

typedef struct AdkDensity3dNormalCreateInfo
{
  AdkTexture *pDensity;
  AdkQueue *pTransferQueue;
  AdkQueue *pComputeQueue;
} AdkDensity3dNormalCreateInfo;

void adkDensity3dNormal_Create(AdkInstance *pInstance, AdkTexture *pDensity, AdkQueue *pGraphicsQueue, AdkQueue *pComputeQueue, const adkAllocationCallbacks *pAllocator, AdkDensity3dNormal **ppDensity3dNormal);
void adkDensity3dNormal_Destroy(AdkDensity3dNormal **ppDensity3dNormal);
void adkDensity3dNormal_Generate(AdkDensity3dNormal *pDensity3dNormal);
void adkDensity3dNormal_GetTexture(AdkDensity3dNormal *pDensity3dNormal, AdkTexture **ppTexture);

#endif // adkDensity3dNormal_h__