#ifndef AdkPhysicsUtil_h__
#define AdkPhysicsUtil_h__

template <typename T>
bool adkPhysicsUtil_LineOnPlaneIntersection(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &p0, const adkVector3<T> &pn, T *pLt)
{
  T addendA = pn.x * (p0.x - l0.x);
  T addendB = pn.y * (p0.y - l0.y);
  T addendC = pn.z * (p0.z - l0.z);
  T divisor = pn.x * ln.x + pn.y * ln.y + pn.z * ln.z;

  if (divisor == T(0))
    return false;

  *pLt = (addendA + addendB + addendC) / divisor;

  return true;
}

template <typename T>
bool adkPhysicsUtil_LineOnLineClosestPoints(const adkVector3<T> &p0, const adkVector3<T> &n0, const adkVector3<T> &p1, const adkVector3<T> &n1, T *pT0, T *pT1)
{
  adkVector3<T> n2 = adkVector3<T>::cross(n0, n1);

  adkVector3<T> n2xn0 = adkVector3<T>::cross(n2, n0);
  adkVector3<T> n2xn1 = adkVector3<T>::cross(n2, n1);

  bool successB = adkPhysicsUtil_LineOnPlaneIntersection(p0, n0, p1, n2xn1, pT0);
  bool successA = adkPhysicsUtil_LineOnPlaneIntersection(p1, n1, p0, n2xn0, pT1);

  return successA && successB;
}

template <typename T>
bool adkPhysicsUtil_Quadratic(T &a, T &b, T &c, T *pX0, T *pX1)
{
  bool valid = false;
  T radicand = b * b - T(4) * a * c;
  T twoA = T(2) * a;

  if (radicand < T(0) || twoA == T(0))
    goto epilogue;

  T sr = (T)adkSqrt((double)radicand);

  *pX0 = (-b - sr) / twoA;
  *pX1 = (-b + sr) / twoA;

  valid = true;

epilogue:
  return valid;
}

template <typename T>
T adkPhysicsUtil_PointOnLineClosestToPoint(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &p)
{
  adkVector3<T> l0_p = p - l0;

  return adkVector3<T>::dot(l0_p, ln);
}

template <typename T>
bool adkPhysicsUtil_LineOnSphereIntersection(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &sc, const T &sr, T *pLT0, T *pLT1)
{
  adkVector3<T> a = l0 - sc;
  adkVector3<T> b = ln;
  T r = sr;

  T qa = b.x * b.x + b.y * b.y + b.z * b.z;
  T qb = T(2) * a.x * b.x + T(2) * a.y * b.y + T(2) * a.z * b.z;
  T qc = a.x * a.x + a.y * a.y + a.z * a.z - r * r;

  return adkPhysicsUtil_Quadratic(qa, qb, qc, pLT0, pLT1);
}

template <typename T>
bool adkPhysicsUtil_LineOnInfiniteCylinder(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &c0, const adkVector3<T> &cn, const T &cr, T *pLT0, T *pLT1)
{
  adkVector3<T> d = cn;
  T r = cr;

  adkVector3<T> a = l0 - c0;
  adkVector3<T> b = ln;

  T dzby2 = d.z * b.y * d.z * b.y;
  T dybz2 = d.y * b.z * d.y * b.z;
  T dzbx2 = d.z * b.x * d.z * b.x;
  T dxbz2 = d.x * b.z * d.x * b.z;
  T dybx2 = d.y * b.x * d.y * b.x;
  T dxby2 = d.x * b.y * d.x * b.y;

  T qa = dzby2 - T(2) * d.z * b.y * d.y * b.z + dybz2
    + dzbx2 - T(2) * d.z * b.x * d.x * b.z + dxbz2
    + dybx2 - T(2) * d.y * b.x * d.x * b.y + dxby2;

  T dz2 = d.z * d.z;
  T dy2 = d.y * d.y;
  T dx2 = d.x * d.x;

  T qb = T(2) * a.y * b.y * dz2 - T(2) * d.z * a.y * d.y * b.z - T(2) * d.z * b.y * d.y * a.z + T(2) * a.z * b.z * dy2
    + T(2) * a.x * b.x * dz2 - T(2) * d.z * a.x * d.x * b.z - T(2) * d.z * b.x * d.x * a.z + T(2) * a.z * b.z * dx2
    + T(2) * a.x * b.x * dy2 - T(2) * d.y * a.x * d.x * b.y - T(2) * d.y * b.x * d.x * a.y + T(2) * a.y * b.y * dx2;

  T dzay2 = d.z * a.y * d.z * a.y;
  T dyaz2 = d.y * a.z * d.y * a.z;
  T dzax2 = d.z * a.x * d.z * a.x;
  T dxaz2 = d.x * a.z * d.x * a.z;
  T dyax2 = d.y * a.x * d.y * a.x;
  T dxay2 = d.x * a.y * d.x * a.y;
  T r2 = r * r;

  T qc = dzay2 - T(2) * d.z * a.y * d.y * a.z + dyaz2
    + dzax2 - T(2) * d.z * a.x * d.x * a.z + dxaz2
    + dyax2 - T(2) * d.y * a.x * d.x * a.y + dxay2
    - r2;

  return adkPhysicsUtil_Quadratic(qa, qb, qc, pLT0, pLT1);
}

template <typename T>
T adkPhysicsUtil_Test(const adkVector3<T> &l0, const adkVector3<T> ln, float lt, const adkVector3<T> &c0, const adkVector3<T> &cn)
{
  return adkVector3<T>::cross((l0 + lt * ln) - c0, cn).squareMagnitude();
}

template <typename T>
T adkPhysicsUtil_Test2(const adkVector3<T> &l0, const adkVector3<T> ln, float lt, const adkVector3<T> &c0, const adkVector3<T> &cn)
{
  adkVector3<T> d = cn;

  adkVector3<T> a = l0 - c0;
  adkVector3<T> b = ln;

  T t = lt;

  T dzay2 = d.z * a.y * d.z * a.y;
  T dyaz2 = d.y * a.z * d.y * a.z;
  T dzax2 = d.z * a.x * d.z * a.x;
  T dxaz2 = d.x * a.z * d.x * a.z;
  T dyax2 = d.y * a.x * d.y * a.x;
  T dxay2 = d.x * a.y * d.x * a.y;

  T dztby2 = d.z * t * b.y * d.z * t * b.y;
  T dytbz2 = d.y * t * b.z * d.y * t * b.z;
  T dztbx2 = d.z * t * b.x * d.z * t * b.x;
  T dxtbz2 = d.x * t * b.z * d.x * t * b.z;
  T dytbx2 = d.y * t * b.x * d.y * t * b.x;
  T dxtby2 = d.x * t * b.y * d.x * t * b.y;

  T dz2 = d.z * d.z;
  T dy2 = d.y * d.y;
  T dx2 = d.x * d.x;

  T section0 = dzay2 + T(2) * a.y * b.y * t * dz2 + dztby2 - T(2) * (d.z * a.y + d.z * t * b.y) * (d.y * a.z + d.y * t * b.z) + dyaz2 + T(2) * t * a.z * b.z * dy2 + dytbz2;
  T section1 = dzax2 + T(2) * a.x * b.x * t * dz2 + dztbx2 - T(2) * (d.z * a.x + d.z * t * b.x) * (d.x * a.z + d.x * t * b.z) + dxaz2 + T(2) * a.z * b.z * t * dx2 + dxtbz2;
  T section2 = dyax2 + T(2) * a.x * b.x * t * dy2 + dytbx2 - T(2) * (d.y * a.x + d.y * t * b.x) * (d.x * a.y + d.x * t * b.y) + dxay2 + T(2) * a.y * b.y * t * dx2 + dxtby2;

  return section0 + section1 + section2;
}

template <typename T>
T adkPhysicsUtil_Test3(const adkVector3<T> &l0, const adkVector3<T> ln, float lt, const adkVector3<T> &c0, const adkVector3<T> &cn)
{
  adkVector3<T> d = cn;

  adkVector3<T> a = l0 - c0;
  adkVector3<T> b = ln;

  T t = lt;

  T dzay2 = d.z * a.y * d.z * a.y;
  T dyaz2 = d.y * a.z * d.y * a.z;
  T dzax2 = d.z * a.x * d.z * a.x;
  T dxaz2 = d.x * a.z * d.x * a.z;
  T dyax2 = d.y * a.x * d.y * a.x;
  T dxay2 = d.x * a.y * d.x * a.y;

  T dztby2 = d.z * t * b.y * d.z * t * b.y;
  T dytbz2 = d.y * t * b.z * d.y * t * b.z;
  T dztbx2 = d.z * t * b.x * d.z * t * b.x;
  T dxtbz2 = d.x * t * b.z * d.x * t * b.z;
  T dytbx2 = d.y * t * b.x * d.y * t * b.x;
  T dxtby2 = d.x * t * b.y * d.x * t * b.y;

  T dz2 = d.z * d.z;
  T dy2 = d.y * d.y;
  T dx2 = d.x * d.x;

  T t2 = t * t;

  T section0 = dzay2 + T(2) * a.y * b.y * t * dz2 + dztby2 - T(2) * (d.z * a.y * d.y * a.z + d.z * a.y * d.y * t * b.z + d.z * t * b.y * d.y * a.z + d.z * b.y * d.y * b.z * t2) + dyaz2 + T(2) * t * a.z * b.z * dy2 + dytbz2;
  T section1 = dzax2 + T(2) * a.x * b.x * t * dz2 + dztbx2 - T(2) * (d.z * a.x * d.x * a.z + d.z * a.x * d.x * t * b.z + d.z * t * b.x * d.x * a.z + d.z * b.x * d.x * b.z * t2) + dxaz2 + T(2) * a.z * b.z * t * dx2 + dxtbz2;
  T section2 = dyax2 + T(2) * a.x * b.x * t * dy2 + dytbx2 - T(2) * (d.y * a.x * d.x * a.y + d.y * a.x * d.x * t * b.y + d.y * t * b.x * d.x * a.y + d.y * b.x * d.x * b.y * t2) + dxay2 + T(2) * a.y * b.y * t * dx2 + dxtby2;

  return section0 + section1 + section2;
}

template <typename T>
T adkPhysicsUtil_Test4(const adkVector3<T> &l0, const adkVector3<T> ln, float lt, const adkVector3<T> &c0, const adkVector3<T> &cn)
{
  adkVector3<T> d = cn;

  adkVector3<T> a = l0 - c0;
  adkVector3<T> b = ln;

  T dzby2 = d.z * b.y * d.z * b.y;
  T dybz2 = d.y * b.z * d.y * b.z;
  T dzbx2 = d.z * b.x * d.z * b.x;
  T dxbz2 = d.x * b.z * d.x * b.z;
  T dybx2 = d.y * b.x * d.y * b.x;
  T dxby2 = d.x * b.y * d.x * b.y;

  T qa = dzby2 - T(2) * d.z * b.y * d.y * b.z + dybz2
    + dzbx2 - T(2) * d.z * b.x * d.x * b.z + dxbz2
    + dybx2 - T(2) * d.y * b.x * d.x * b.y + dxby2;

  T dz2 = d.z * d.z;
  T dy2 = d.y * d.y;
  T dx2 = d.x * d.x;

  T qb = T(2) * a.y * b.y * dz2 - T(2) * d.z * a.y * d.y * b.z - T(2) * d.z * b.y * d.y * a.z + T(2) * a.z * b.z * dy2
    + T(2) * a.x * b.x * dz2 - T(2) * d.z * a.x * d.x * b.z - T(2) * d.z * b.x * d.x * a.z + T(2) * a.z * b.z * dx2
    + T(2) * a.x * b.x * dy2 - T(2) * d.y * a.x * d.x * b.y - T(2) * d.y * b.x * d.x * a.y + T(2) * a.y * b.y * dx2;

  T dzay2 = d.z * a.y * d.z * a.y;
  T dyaz2 = d.y * a.z * d.y * a.z;
  T dzax2 = d.z * a.x * d.z * a.x;
  T dxaz2 = d.x * a.z * d.x * a.z;
  T dyax2 = d.y * a.x * d.y * a.x;
  T dxay2 = d.x * a.y * d.x * a.y;

  T qc = dzay2 - T(2) * d.z * a.y * d.y * a.z + dyaz2
    + dzax2 - T(2) * d.z * a.x * d.x * a.z + dxaz2
    + dyax2 - T(2) * d.y * a.x * d.x * a.y + dxay2;

  return lt * lt * qa + lt * qb + qc;
}

template <typename T>
bool adkPhysicsUtil_SweepingSphereOnTriangle(const adkVector3<T> &s0, const adkVector3<T> sv, const T &sr, const adkVector3<T> &t0, const adkVector3<T> &t1, const adkVector3<T> &t2, AdkCollision<T> *pCollision)
{
  bool collided = false;

  //Find point sweeping sphere center line intersects triangle plane.
  adkVector3<T> t2_t0 = t0 - t2;
  adkVector3<T> t0_t1 = t1 - t0;
  adkVector3<T> t1_t2 = t2 - t1;

  T sm = sv.magnitude();

  if (sm == 0.f)
    return false;

  adkVector3<T> sn = sv / sm;
  adkVector3<T> tn = adkVector3<T>::cross(t0_t1, t2_t0).normalized();
  T intersectDist;

  adkVector3<T> tri[4];

  tri[0] = t0;
  tri[1] = t1;
  tri[2] = t2;
  tri[3] = t0;

  //Check if the sphere hits the plane's area inside the triangle.
  if (adkPhysicsUtil_LineOnPlaneIntersection(s0, sn, t0, tn, &intersectDist))
  {
    //Flip triangle if it faces away from sphere sweep direction.
    if (adkVector3<T>::dot(tn, sn) < T(0))
      tn = -tn;

    //Pull intersection point away from plane.
    T sndtn = adkVector3<T>::dot(sn, tn);
    T pullDist = T(1) / sndtn * sr;
    T resDist = intersectDist - pullDist;

    //Don't push further than s1.
    if (resDist <= sm && resDist >= -sr / T(2))
    {
      //Don't pull further back than s0.
      resDist = adkMax(resDist, T(0));

      //If the resolution projected onto the plane is inside the triangle,
      //it means the sphere will collide with the plane before it hits a line or point.
      //Otherwise it means the sphere will collide with a line or a point first.
      adkVector3<T> res = s0 + resDist * sn;
      adkVector3<T> sp = res + sr * tn;

      adkVector3<T> v0 = t2 - t0;
      adkVector3<T> v1 = t1 - t0;
      adkVector3<T> v2 = sp - t0;

      T d00 = adkVector3<T>::dot(v0, v0);
      T d01 = adkVector3<T>::dot(v0, v1);
      T d02 = adkVector3<T>::dot(v0, v2);
      T d11 = adkVector3<T>::dot(v1, v1);
      T d12 = adkVector3<T>::dot(v1, v2);

      T factor = T(1) / (d00 * d11 - d01 * d01);
      T u = (d11 * d02 - d01 * d12) * factor;
      T v = (d00 * d12 - d01 * d02) * factor;

      if (u >= T(0) && v >= T(0) && u + v < T(1))
      {
        collided = true;
        pCollision->resolution = s0 + resDist * sn;
        pCollision->resolutionT = resDist / sm;
        pCollision->contact = sp;
      }
    }
  }

  T closestT = adkFloatMax;

  //Sweep the sphere against each line of the triangle and keep the closest collision.
  if (!collided)
  {
    adkVector3<T> ln[3];

    ln[0] = t0_t1.normalized();
    ln[1] = t1_t2.normalized();
    ln[2] = t2_t0.normalized();

    for (uint32_t i = 0; i < 3; ++i)
    {
      T intersectionT0;
      T intersectionT1;

      //Using the sphere's movement as a line and the triangle's line as a cylinder in a line on cylinder collision test is
      //equivalent to sweeping a sphere through a line.
      if (adkPhysicsUtil_LineOnInfiniteCylinder(s0, sn, tri[i], ln[i], sr, &intersectionT0, &intersectionT1) && intersectionT0 < closestT)
      {
        //Don't push further than s1.
        if (intersectionT0 > sm)
          continue;

        //* Float point error will sometimes mean the sphere gets pushed a little bit inside the triangle.
        //* The resolution that is calculated is likely used as an input next frame.
        //* If intersectionT0 was checked against zero, this would fail the next frame and no collision would happen.
        //* If intersectionT0 was checked against radius, this would succeed if the sphere was moving the opposite direction next frame,
        //  which would be incorrect because it's traveling away from the triangle.
        //* Checking for half radius satisfies both cases and would only break if the error was greater than half the radius.
        //* There are two other places this applies to in the function.
        if (intersectionT0 < -sr / T(2))
          continue;

        //Don't pull further back than s0.
        intersectionT0 = adkMax(intersectionT0, T(0));

        adkVector3<T> resolution = s0 + intersectionT0 * sn;
        T contactTriT = adkPhysicsUtil_PointOnLineClosestToPoint(tri[i], ln[i], resolution);

        adkVector3<T> contact = tri[i] + contactTriT * ln[i];

        T closestPointDistance = adkVector3<float>::distance(resolution, contact);
        (void)closestPointDistance;

        T proofRadius = adkPhysicsUtil_Test(s0, sn, intersectionT0, tri[i], ln[i]);
        (void)proofRadius;

        T proofRadius2 = adkPhysicsUtil_Test2(s0, sn, intersectionT0, tri[i], ln[i]);
        (void)proofRadius2;

        T proofRadius3 = adkPhysicsUtil_Test3(s0, sn, intersectionT0, tri[i], ln[i]);
        (void)proofRadius3;

        T proofRadius4 = adkPhysicsUtil_Test4(s0, sn, intersectionT0, tri[i], ln[i]);
        (void)proofRadius4;

        adkVector3<T> tri_nextTri = tri[i + 1] - tri[i];
        T contactTriTSq = contactTriT * contactTriT;
        T nextTriTSq = adkVector3<T>::dot(tri_nextTri, tri_nextTri);

        if (contactTriT >= T(0) && contactTriTSq <= nextTriTSq)
        {
          closestT = intersectionT0;
          pCollision->contact = contact;
          pCollision->resolution = resolution;
          pCollision->resolutionT = intersectionT0 / sm;
          collided = true;
        }
      }
    }

    //Sweep the sphere with each point of the triangle as well. Use the closest point collision if it is closer
    //than the closest line collision.
    for (uint32_t i = 0; i < 3; ++i)
    {
      T intersectionT0;
      T intersectionT1;

      //Using the sphere's movement as the line and the point as a sphere in a line on sphere intersection test is
      //equivalent to sweeping a sphere through a point.
      if (adkPhysicsUtil_LineOnSphereIntersection(s0, sn, tri[i], sr, &intersectionT0, &intersectionT1) && intersectionT0 < closestT)
      {
        //Don't push further than s1.
        if (intersectionT0 > sm || intersectionT0 < -sr / T(2))
          continue;

        //Don't pull further back than s0.
        intersectionT0 = adkMax(intersectionT0, T(0));

        closestT = intersectionT0;
        pCollision->contact = tri[i];
        pCollision->resolution = s0 + intersectionT0 * sn;
        pCollision->resolutionT = intersectionT0 / sm;
        collided = true;
      }
    }
  }

  if (adkVector3<T>::dot(pCollision->contact - pCollision->resolution, sn) < T(0))
    return false;

  return collided;
}

template <typename T>
bool adkPhysicsUtil_SweepingSphereOnSphere(const adkVector3<T> &sweepPoint, const adkVector3<T> &sweepNormal, const T &sweepMagnitude, const T &sweepRadius, const adkVector3<T> &spherePoint, const T &sphereRadius, T *pResolutionT)
{
  adkVector3<T> sweepDelta = sweepEnd - sweepStart;
  T sweepMag = sweepDelta.magnitude();

  //Sweep isn't moving; don't detect any collisions.
  if (sweepMag == T(0))
    return false;

  adkVector3<T> sweepNorm = deltaB / magB;
  T radii = sweepRadius + sphereRadius;
  T t1 = 0.f;
  return adkPhysicsUtil_LineOnSphereIntersection(sweepStart, sweepNorm, spherePoint, radii, pResolutionT, &t1);
}

template <typename T>
bool adkPhysicsUtil_TestSs(T t, const adkVector3<T> &startA, const adkVector3<T> &endA, const T &radiusA, const adkVector3<T> &startB, const adkVector3<T> &endB, const T &radiusB)
{
  adkVector3<T> deltaA = endA - startA;
  adkVector3<T> deltaB = endB - startB;
  T magA = deltaA.magnitude();
  T magB = deltaB.magnitude();

  //Neither sphere is sweeping. No collision generated.
  if (magA == T(0) && magB == T(0))
    return false;

  if (magA == T(0))
  {
    deltaA = adkVector3<T>::create(T(1), T(0), T(0));
    magA = T(1);
  }

  if (magB == T(0))
  {
    deltaB = adkVector3<T>::create(T(1), T(0), T(0));
    magB = T(1);
  }

  T radii = radiusA + radiusB;

  //adkVector3<float> delta = startA + t * deltaA - startB - t * deltaB;
  //T distSq = adkVector3<float>::dot(delta, delta);

  //adkVector3<T> vDistSq = (startB + t * deltaB - startA - t * deltaA) * (startB + t * deltaB - startA - t * deltaA);

  //adkVector3<T> vDistSq = startB * startB + startB * t * deltaB - startB * startA - startB * t * deltaA
  //                      + startB * t * deltaB + (t * deltaB) * (t * deltaB) - startA * t * deltaB - t * t * deltaB * deltaA
  //                      - startB * startA - startA * t * deltaB + startA * startA + startA * t * deltaA
  //                      - startB * t * deltaA - t * t * deltaB * deltaA + startA * t * deltaA + (t * deltaA) * (t * deltaA);

  //adkVector3<T> vDistSq = t * t * (deltaB * deltaB - deltaB * deltaA - deltaB * deltaA + deltaA * deltaA)
  //                      + t * (startB * deltaB - startB * deltaA + startB * deltaB - startA * deltaB - startA * deltaB + startA * deltaA - startB * deltaA + startA * deltaA)
  //                      + startB * startB - startB * startA - startB * startA + startA * startA;

  //adkVector3<T> vDistSq = t * t * ((deltaB - deltaA) * (deltaB - deltaA))
  //                      + t * (T(2) * startB * deltaB - T(2) * startB * deltaA - T(2) * startA * deltaB + T(2) * startA * deltaA)
  //                      + startB * startB - startB * startA - startB * startA + startA * startA;

  //adkVector3<T> vDistSq = t * t * ((deltaB - deltaA) * (deltaB - deltaA))
  //                      + t * (T(2) * startB * deltaB - T(2) * startB * deltaA - T(2) * startA * deltaB + T(2) * startA * deltaA)
  //                      + (startB - startA) * (startB - startA);

  adkVector3<T> da_db = deltaB - deltaA;
  adkVector3<T> sa_sb = startB - startA;

  adkVector3<T> vDistSq = t * t * da_db * da_db
    + t * T(2) * da_db * sa_sb
    + sa_sb * sa_sb;

  //adkVector3<T> tDeltaB = t * deltaB;
  //adkVector3<T> tDeltaA = t * deltaA;
  //T t2 = t * t;
  //
  //adkVector3<T> vDistSq = startB * startB + startB * t * deltaB - startB * startA - startB * t * deltaA + startB * t * deltaB +
  //                        tDeltaB * tDeltaB - t * deltaB * startA - t2 * deltaB * deltaA - startB * startA - t * deltaB * startA +
  //                        startA * startA + startA * t * deltaA - startB * t * deltaA - t2 * deltaB * deltaA - startA * t * deltaA + 
  //                        tDeltaA * tDeltaA;

  //adkVector3<T> vDistSq = t * t * (deltaB * deltaB - deltaB * deltaA - deltaB * deltaA + deltaA * deltaA) +
  //                        t * (startB * deltaB - startB * deltaA + startB * deltaB - deltaB * startA - deltaB * startA + startA * deltaA - startB * deltaA - startA * deltaA) +
  //                        startB * startB - startB * startA - startB * startA + startA * startA;

  //adkVector3<T> vDistSq = t * t * (deltaB * (deltaB - T(2) * deltaA) + deltaA * deltaA) +
  //                        t * (deltaB * (startB + startB - startA - startA)) + deltaA * (-startB + startA - startB - startA) +
  //                        startB * (startB - startA - startA) + startA * startA;


  //adkVector3<T> vDistSq = t * t * (deltaB * (deltaB - T(2) * deltaA) + deltaA * deltaA) +
  //                        t * (T(-2) * deltaB * startA - T(2) * deltaA * startB) +
  //                        startB * (startB - T(2) * startA) + startA * startA;

  T distSq = vDistSq.x + vDistSq.y + vDistSq.z;

  return distSq < radii * radii;
}

template <typename T>
bool adkPhysicsUtil_SweepingSphereOnSweepingSphere(const adkVector3<T> &startA, const adkVector3<T> &velocityA, const T &radiusA, const adkVector3<T> &startB, const adkVector3<T> &velocityB, const T &radiusB, AdkSweepingSphereOnSweepingSphereCollision<T> *pCollision)
{
  //The following function is rearranged into a quadratic:
  //(B - A) . (B - A) = radii^2
  //where:
  //A = startA + t * deltaA and
  //B = startB + t * deltaB.
  //The aforementioned function is true if the two spheres intersect exactly for a given t.
  //The rearranged quadratic gives the value of t at the time of intersection of the two spheres.

  T radii = radiusA + radiusB;

  adkVector3<T> da_db = velocityB - velocityA;
  adkVector3<T> sa_sb = startB - startA;

  adkVector3<T> va = da_db * da_db;
  adkVector3<T> vb = T(2) * da_db * sa_sb;
  adkVector3<T> vc = sa_sb * sa_sb;

  T a = va.x + va.y + va.z;
  T b = vb.x + vb.y + vb.z;
  T c = vc.x + vc.y + vc.z - radii * radii;

  T t0 = T(0);
  T t1 = T(0);

  bool intersected = adkPhysicsUtil_Quadratic(a, b, c, &t0, &t1);

  bool collided = intersected && t0 <= T(1);

  //Discount collisions where sphere's are traveling in the same direction and front sphere is faster or same speed as behind sphere.
  //adkVector3<T> relativeStart = startB - startA;
  //adkVector3<T> relativeDelta = deltaB - deltaA;
  //adkVector3<T> relativeStartDirection = relativeStart.normalized();
  //T relativeDot = adkVector3<float>::dot(relativeStartDirection, relativeDelta);
  //collided = collided && (relativeDot >= T(0) && relativeDot <= magB);

  //Don't collide if the distance between the two spheres is increasing at the start of the sweep.
  //y' = 2ax + b => 2a(0) + b => b
  T startVelocity = b;

  collided = collided && (startVelocity < T(0));

  if (collided)
  {
    pCollision->resolutionA = startA + t0 * velocityA;
    pCollision->resolutionB = startB + t0 * velocityB;
    pCollision->contact = adkLerp(pCollision->resolutionA, pCollision->resolutionB, radiusA / radii);
    pCollision->resolutionT = t0;
  }

  return collided;
}

#endif // AdkPhysicsUtil_h__
