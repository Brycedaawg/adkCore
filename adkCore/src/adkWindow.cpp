#include "adkWindow.inl"
#include "adkMemory.h"
#include <windows.h>
#include "adkLog.h"
#include "adkVkTools.inl"
#include "adkAssert.h"
#include "adkTexture.inl"

typedef enum AdkRawInputDevice
{
  AdkRawInputDevice_Mouse,
  AdkRawInputDevice_Count,
} AdkRawInputDevice;

static void _DispatchRawInput(adkWindow *pWindow, HWND windowHandle, UINT /*message*/, WPARAM /*wParam*/, LPARAM lParam)
{
  uint32_t size = ADK_ARRAY_SIZE(pWindow->rawInputData);
  if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, pWindow->rawInputData, &size, sizeof(RAWINPUTHEADER)) != -1)
  {
    RAWINPUT *pRawInput = (RAWINPUT*)pWindow->rawInputData;
    POINT cursorPosition;

    switch (pRawInput->header.dwType)
    {
    case RIM_TYPEMOUSE:
      if (pRawInput->data.mouse.lLastX | pRawInput->data.mouse.lLastY)
      {
        pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_MOVE;
        GetCursorPos(&cursorPosition);
        ScreenToClient(windowHandle, &cursorPosition);
        pWindow->inputEvents[pWindow->inputEventCount].mouseMove.position = adkVector2<int32_t>::create(cursorPosition.x, cursorPosition.y);
        pWindow->inputEvents[pWindow->inputEventCount].mouseMove.delta = adkVector2<int32_t>::create(pRawInput->data.mouse.lLastX, pRawInput->data.mouse.lLastY);
        pWindow->inputState.mousePosition = pWindow->inputEvents[pWindow->inputEventCount].mouseMove.position;
        pWindow->inputState.mouseDelta += pWindow->inputEvents[pWindow->inputEventCount].mouseMove.delta;
        ++pWindow->inputEventCount;
      }
      break;

    default:
      break;
    }
  }
}

static void _RegisterRawInputDevices(HWND windowHandle)
{
  RAWINPUTDEVICE rawInputDevices[AdkRawInputDevice_Count];
  rawInputDevices[AdkRawInputDevice_Mouse].usUsagePage = 1;
  rawInputDevices[AdkRawInputDevice_Mouse].usUsage = 2;
  rawInputDevices[AdkRawInputDevice_Mouse].dwFlags = RIDEV_INPUTSINK;
  rawInputDevices[AdkRawInputDevice_Mouse].hwndTarget = windowHandle;
  if (RegisterRawInputDevices(rawInputDevices, AdkRawInputDevice_Count, sizeof(RAWINPUTDEVICE)) != TRUE)
    GetLastError();
}

static void _UnregisterRawInputDevices()
{
  RAWINPUTDEVICE rawInputDevices[AdkRawInputDevice_Count];
  rawInputDevices[AdkRawInputDevice_Mouse].usUsagePage = 1;
  rawInputDevices[AdkRawInputDevice_Mouse].usUsage = 2;
  rawInputDevices[AdkRawInputDevice_Mouse].dwFlags = RIDEV_REMOVE;
  rawInputDevices[AdkRawInputDevice_Mouse].hwndTarget = nullptr;
  if (RegisterRawInputDevices(rawInputDevices, AdkRawInputDevice_Count, sizeof(RAWINPUTDEVICE)) != TRUE)
    GetLastError();
}

static LRESULT WINAPI _WindowProcedure(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{
  adkWindow *pWindow = (adkWindow*)GetWindowLongPtr(windowHandle, GWLP_USERDATA);
  AdkKey key;

  if (pWindow != nullptr && pWindow->inputEventCount < ADK_WINDOW_PARAMETER_MAX_INPUT_EVENTS)
  {
    switch (message)
    {
    case WM_DESTROY:
      PostQuitMessage(0);
      return 0;
      break;

    case WM_CLOSE:
      pWindow->callbacks.pCloseFunction(pWindow->callbacks.pUserData, pWindow);
      return 0;
      break;

    case WM_KEYDOWN:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_KEY_DOWN;
      key = pWindow->systemToAdkKey[wParam];
      pWindow->inputEvents[pWindow->inputEventCount].keyboard.key = key;
      pWindow->inputState.keys[key] = ADK_TRUE;
      ++pWindow->inputEventCount;
      break;

    case WM_KEYUP:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_KEY_UP;
      key = pWindow->systemToAdkKey[wParam];
      pWindow->inputEvents[pWindow->inputEventCount].keyboard.key = key;
      pWindow->inputState.keys[key] = ADK_FALSE;
      ++pWindow->inputEventCount;
      break;

    case WM_LBUTTONDOWN:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_DOWN;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_LEFT;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_LEFT] = ADK_TRUE;
      ++pWindow->inputEventCount;
      break;

    case WM_LBUTTONUP:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_UP;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_LEFT;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_LEFT] = ADK_FALSE;
      ++pWindow->inputEventCount;
      break;

    case WM_RBUTTONDOWN:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_DOWN;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_RIGHT;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_RIGHT] = ADK_TRUE;
      ++pWindow->inputEventCount;
      break;

    case WM_RBUTTONUP:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_UP;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_RIGHT;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_RIGHT] = ADK_FALSE;
      ++pWindow->inputEventCount;
      break;

    case WM_MBUTTONDOWN:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_DOWN;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_MIDDLE;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_MIDDLE] = ADK_TRUE;
      ++pWindow->inputEventCount;
      break;

    case WM_MBUTTONUP:
      pWindow->inputEvents[pWindow->inputEventCount].type = ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_UP;
      pWindow->inputEvents[pWindow->inputEventCount].mouseButton.button = ADK_MOUSE_BUTTON_MIDDLE;
      pWindow->inputState.mouseButtons[ADK_MOUSE_BUTTON_MIDDLE] = ADK_FALSE;
      ++pWindow->inputEventCount;
      break;

    case WM_SETFOCUS:
      _RegisterRawInputDevices(windowHandle);
      break;

    case WM_KILLFOCUS:
      _UnregisterRawInputDevices();
      break;

    case WM_INPUT:
      _DispatchRawInput(pWindow, windowHandle, message, wParam, lParam);
      break;

    default:
      break;
    }
  }

  return DefWindowProc(windowHandle, message, wParam, lParam);
}

static inline void _InitSystemToAdkKey(adkWindow *pWindow)
{
  memset(pWindow->systemToAdkKey, 0, sizeof(pWindow->systemToAdkKey));

  for (uint16_t i = 0; i <= '9' - '0'; ++i)
    pWindow->systemToAdkKey['0' + i] = (AdkKey)(ADK_KEY_0 + i);

  for (uint16_t i = 0; i <= 'Z' - 'A'; ++i)
    pWindow->systemToAdkKey['A' + i] = (AdkKey)(ADK_KEY_A + i);

  pWindow->systemToAdkKey[VK_SHIFT] = ADK_KEY_SHIFT;
  pWindow->systemToAdkKey[VK_CONTROL] = ADK_KEY_CONTROL;
  pWindow->systemToAdkKey[VK_MENU] = ADK_KEY_ALT;
  pWindow->systemToAdkKey[VK_ESCAPE] = ADK_KEY_ESCAPE;
  pWindow->systemToAdkKey[VK_SPACE] = ADK_KEY_SPACE;
}

void adkWindow_Create(AdkInstance *pInstance, const adkWindowCreateInfo *pCreateInfo, adkWindow **ppWindow)
{
  adkWindow *pWindow = adkAllocTypeCall(adkWindow, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pWindow->pInstance = pInstance;
  memcpy(&pWindow->callbacks, &pCreateInfo->callbacks, sizeof(AdkWindowCallbacks));

  _InitSystemToAdkKey(pWindow);

  pWindow->moduleHandle = GetModuleHandle(nullptr);

  const wchar_t pClassName[] = L"adkWindow";

  WNDCLASS windowClass;
  windowClass.style = 0;
  windowClass.lpfnWndProc = _WindowProcedure;
  windowClass.cbClsExtra = 0;
  windowClass.cbWndExtra = 0;
  windowClass.hInstance = pWindow->moduleHandle;
  windowClass.hIcon = nullptr;
  windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
  windowClass.hbrBackground = nullptr;
  windowClass.lpszMenuName = nullptr;
  windowClass.lpszClassName = pClassName;

  RegisterClass(&windowClass);

  wchar_t windowName[256];

  for (uint32_t i = 0; i < 256; ++i)
  {
    windowName[i] = pCreateInfo->pName[i];
    if (windowName[i] == 0)
      break;
  }

  DWORD style = pCreateInfo->flags & ADK_WINDOW_CREATE_BORDERLESS_BIT ? WS_POPUP : WS_OVERLAPPEDWINDOW;
  pWindow->windowHandle = CreateWindowEx(0, (LPCWSTR)pClassName, windowName, style, pCreateInfo->rect.x, pCreateInfo->rect.y, pCreateInfo->rect.width, pCreateInfo->rect.height, nullptr, nullptr, pWindow->moduleHandle, nullptr);
  pWindow->rect = pCreateInfo->rect;
  SetWindowLongPtr(pWindow->windowHandle, GWLP_USERDATA, (LONG_PTR)pWindow);

#ifdef _WIN32
  VkWin32SurfaceCreateInfoKHR vkSurfaceCreateInfo;
  vkSurfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
  vkSurfaceCreateInfo.pNext = nullptr;
  vkSurfaceCreateInfo.flags = 0;
  vkSurfaceCreateInfo.hinstance = pWindow->moduleHandle;
  vkSurfaceCreateInfo.hwnd = pWindow->windowHandle;

  vkCreateWin32SurfaceKHR(pInstance->vkInstance, &vkSurfaceCreateInfo, pInstance->pVkAllocationCallbacks, &pWindow->vkSurface);
#endif

  pWindow->pVkPhysicalDeviceSurfaceCapabilities = adkAllocTypeCall(VkSurfaceCapabilitiesKHR, pInstance->vkDeviceCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pInstance->vkDeviceCount; ++i)
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pInstance->pVkPhysicalDevices[i], pWindow->vkSurface, pWindow->pVkPhysicalDeviceSurfaceCapabilities + i);

  pWindow->extent.x = pWindow->pVkPhysicalDeviceSurfaceCapabilities[pInstance->primaryDeviceIndex].currentExtent.width;
  pWindow->extent.y = pWindow->pVkPhysicalDeviceSurfaceCapabilities[pInstance->primaryDeviceIndex].currentExtent.height;

  pWindow->vkSurfaceFormatCount = 0;
  pWindow->pVkSurfaceFormatDeviceRanges = adkAllocTypeCall(AdkRange<uint32_t>, pInstance->vkDeviceCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pInstance->vkDeviceCount; ++i)
  {
    uint32_t deviceSurfaceFormatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(pInstance->pVkPhysicalDevices[i], pWindow->vkSurface, &deviceSurfaceFormatCount, nullptr);
    pWindow->pVkSurfaceFormatDeviceRanges[i].start = pWindow->vkSurfaceFormatCount;
    pWindow->pVkSurfaceFormatDeviceRanges[i].length = deviceSurfaceFormatCount;
    pWindow->vkSurfaceFormatCount += deviceSurfaceFormatCount;
  }

  pWindow->pVkSurfaceFormats = adkAllocTypeCall(VkSurfaceFormatKHR, pWindow->vkSurfaceFormatCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pInstance->vkDeviceCount; ++i)
  {
    uint32_t deviceSurfaceFormatCount = pWindow->pVkSurfaceFormatDeviceRanges[i].length;
    vkGetPhysicalDeviceSurfaceFormatsKHR(pInstance->pVkPhysicalDevices[i], pWindow->vkSurface, &deviceSurfaceFormatCount, pWindow->pVkSurfaceFormats + pWindow->pVkSurfaceFormatDeviceRanges[i].start);
  }

  pWindow->swapchainFormat = pWindow->pVkSurfaceFormats[pWindow->pVkSurfaceFormatDeviceRanges[pWindow->deviceIndex].start].format;

  pWindow->pVkSurfacePresentModes = 0;
  pWindow->pVkSurfacePresentModeDeviceRanges = adkAllocTypeCall(AdkRange<uint32_t>, pInstance->vkDeviceCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pInstance->vkDeviceCount; ++i)
  {
    uint32_t deviceSurfacePresentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(pInstance->pVkPhysicalDevices[i], pWindow->vkSurface, &deviceSurfacePresentModeCount, nullptr);
    pWindow->pVkSurfacePresentModeDeviceRanges[i].start = pWindow->vkSurfacePresentModeCount;
    pWindow->pVkSurfacePresentModeDeviceRanges[i].length = deviceSurfacePresentModeCount;
    pWindow->vkSurfacePresentModeCount += deviceSurfacePresentModeCount;
  }

  pWindow->pVkSurfacePresentModes = adkAllocTypeCall(VkPresentModeKHR, pWindow->vkSurfacePresentModeCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pInstance->vkDeviceCount; ++i)
  {
    uint32_t deviceSurfacePresentModeCount = pWindow->pVkSurfacePresentModeDeviceRanges[i].length;
    ADK_ASSERT_VK_RESULT(vkGetPhysicalDeviceSurfacePresentModesKHR(pInstance->pVkPhysicalDevices[i], pWindow->vkSurface, &deviceSurfacePresentModeCount, pWindow->pVkSurfacePresentModes + pWindow->pVkSurfaceFormatDeviceRanges[i].start));
  }

  pWindow->deviceIndex = 0;
  VkDevice device = pInstance->pVkDevices[pWindow->deviceIndex];
  VkPhysicalDevice physicalDevice = pInstance->pVkPhysicalDevices[pWindow->deviceIndex];
  VkBool32 physicalDeviceSupportsSurface = VK_FALSE;

  for (uint32_t i = 0; i < pInstance->pVkDeviceQueueFamilyRanges[pWindow->deviceIndex].length; ++i)
  {
    ADK_ASSERT_VK_RESULT(vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, pWindow->vkSurface, &physicalDeviceSupportsSurface));
    
    uint32_t possibleQueueFamilyIndex = pInstance->pVkDeviceQueueFamilyRanges[pWindow->deviceIndex].start + i;
    if (physicalDeviceSupportsSurface && (pInstance->pVkQueueFamilyProperties[possibleQueueFamilyIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT))
    {
      pWindow->queueFamilyIndex = possibleQueueFamilyIndex;
      pWindow->queueIndex = pInstance->pVkQueueFamilyQueueRange[possibleQueueFamilyIndex].start;
      break;
    }
  }

  ADK_ASSERT(physicalDeviceSupportsSurface, "Physical device does not support surface.\n");

  //Setup depth buffer.
  AdkTextureCreateInfo textureCreateInfo;
  textureCreateInfo.flags = 0;
  textureCreateInfo.dimensions = ADK_TEXTURE_DIMENSIONS_2D;
  textureCreateInfo.extent = adkVector3<uint32_t>::create(pWindow->extent.x, pWindow->extent.y, 1);
  textureCreateInfo.pColors = nullptr;
  textureCreateInfo.colorType = ADK_COLOR_TYPE_D32_SFLOAT;
  textureCreateInfo.minFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.magFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.mipmapMode = ADK_TEXTURE_MIPMAP_MODE_NONE;
  textureCreateInfo.addressModeU = ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE;
  textureCreateInfo.addressModeV = ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE;
  textureCreateInfo.anisotropyMode = ADK_TEXTURE_ANISOTROPY_MODE_DISABLED;
  textureCreateInfo.pGraphicsQueue = pCreateInfo->pQueue;
  adkTexture_Create(pInstance, &textureCreateInfo, &pWindow->pDepthTexture);

  VkSwapchainCreateInfoKHR vkSwapChainCreateinfo;
  vkSwapChainCreateinfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  vkSwapChainCreateinfo.pNext = nullptr;
  vkSwapChainCreateinfo.flags = 0;
  vkSwapChainCreateinfo.surface = pWindow->vkSurface;
  vkSwapChainCreateinfo.minImageCount = pWindow->pVkPhysicalDeviceSurfaceCapabilities[pWindow->deviceIndex].minImageCount;
  vkSwapChainCreateinfo.imageFormat = pWindow->swapchainFormat;
  vkSwapChainCreateinfo.imageColorSpace = pWindow->pVkSurfaceFormats[pWindow->pVkSurfaceFormatDeviceRanges[pWindow->deviceIndex].start].colorSpace;
  vkSwapChainCreateinfo.imageExtent = pWindow->pVkPhysicalDeviceSurfaceCapabilities[pWindow->deviceIndex].currentExtent;
  vkSwapChainCreateinfo.imageArrayLayers = 1;
  vkSwapChainCreateinfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
  vkSwapChainCreateinfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  vkSwapChainCreateinfo.queueFamilyIndexCount = 0;
  vkSwapChainCreateinfo.pQueueFamilyIndices = nullptr;
  vkSwapChainCreateinfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
  vkSwapChainCreateinfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  vkSwapChainCreateinfo.presentMode = pWindow->pVkSurfacePresentModes[pWindow->pVkSurfacePresentModeDeviceRanges[pWindow->deviceIndex].start];
  vkSwapChainCreateinfo.clipped = true;
  vkSwapChainCreateinfo.oldSwapchain = VK_NULL_HANDLE;

  ADK_ASSERT_VK_RESULT(vkCreateSwapchainKHR(device, &vkSwapChainCreateinfo, pInstance->pVkAllocationCallbacks, &pWindow->vkSwapchain));

  ADK_ASSERT_VK_RESULT(vkGetSwapchainImagesKHR(device, pWindow->vkSwapchain, &pWindow->swapchainImageCount, nullptr));
  pWindow->pVkSwapchainImages = adkAllocTypeCall(VkImage, pWindow->swapchainImageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  ADK_ASSERT_VK_RESULT(vkGetSwapchainImagesKHR(device, pWindow->vkSwapchain, &pWindow->swapchainImageCount, pWindow->pVkSwapchainImages));

  pWindow->pVkSwapchainImageViews = adkAllocTypeCall(VkImageView, pWindow->swapchainImageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pWindow->pSwapchainCommandBuffers = adkAllocTypeCall(VkCommandBuffer, pWindow->swapchainImageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  VkCommandBufferAllocateInfo vkCommandBufferAllocateInfo;
  vkCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  vkCommandBufferAllocateInfo.pNext = nullptr;
  vkCommandBufferAllocateInfo.commandPool = pInstance->pvkCommandPools[pWindow->queueIndex];
  vkCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  vkCommandBufferAllocateInfo.commandBufferCount = pWindow->swapchainImageCount;

  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &vkCommandBufferAllocateInfo, pWindow->pSwapchainCommandBuffers));

  //Allocate the pre and post present command buffers.
  pWindow->pPrePresentCommandBuffers = adkAllocTypeCall(VkCommandBuffer, pWindow->swapchainImageCount * 2, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pWindow->pPostPresentCommandBuffers = pWindow->pPrePresentCommandBuffers + pWindow->swapchainImageCount;

  VkCommandBufferAllocateInfo preAndPostPresentCommandBufferAllocateInfo;
  preAndPostPresentCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  preAndPostPresentCommandBufferAllocateInfo.pNext = nullptr;
  preAndPostPresentCommandBufferAllocateInfo.commandPool = pInstance->pvkCommandPools[pWindow->queueIndex];
  preAndPostPresentCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  preAndPostPresentCommandBufferAllocateInfo.commandBufferCount = pWindow->swapchainImageCount * 2;

  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &preAndPostPresentCommandBufferAllocateInfo, pWindow->pPrePresentCommandBuffers));

  VkFence *pSwapchainInitFences = adkAllocTypeCall(VkFence, pWindow->swapchainImageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

  VkQueue queue = pInstance->pVkQueues[pWindow->queueIndex];

  for (uint32_t i = 0; i < pWindow->swapchainImageCount; ++i)
  {
    VkImageViewCreateInfo vkSwapchainImageViewCreateInfo;
    vkSwapchainImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    vkSwapchainImageViewCreateInfo.pNext = nullptr;
    vkSwapchainImageViewCreateInfo.flags = 0;
    vkSwapchainImageViewCreateInfo.image = pWindow->pVkSwapchainImages[i];
    vkSwapchainImageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    vkSwapchainImageViewCreateInfo.format = pWindow->swapchainFormat;
    vkSwapchainImageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    vkSwapchainImageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    vkSwapchainImageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    vkSwapchainImageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    vkSwapchainImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    vkSwapchainImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    vkSwapchainImageViewCreateInfo.subresourceRange.levelCount = 1;
    vkSwapchainImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    vkSwapchainImageViewCreateInfo.subresourceRange.layerCount = 1;

    ADK_ASSERT_VK_RESULT(vkCreateImageView(device, &vkSwapchainImageViewCreateInfo, pInstance->pVkAllocationCallbacks, pWindow->pVkSwapchainImageViews + i));

    VkSemaphoreCreateInfo swapchainSemaphoreCreateInfo;
    swapchainSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    swapchainSemaphoreCreateInfo.pNext = nullptr;
    swapchainSemaphoreCreateInfo.flags = 0;
    ADK_ASSERT_VK_RESULT(vkCreateSemaphore(device, &swapchainSemaphoreCreateInfo, pInstance->pVkAllocationCallbacks, &pWindow->swapchainSemaphore));

    VkCommandBufferAllocateInfo swapchainCommandBufferAllocateInfo;
    swapchainCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    swapchainCommandBufferAllocateInfo.pNext = nullptr;
    swapchainCommandBufferAllocateInfo.commandPool = pInstance->pvkCommandPools[pWindow->queueIndex];
    swapchainCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    swapchainCommandBufferAllocateInfo.commandBufferCount = 1;

    //Setup swapchain images for drawing into.
    VkCommandBuffer swapchainCommandBuffer;
    ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &swapchainCommandBufferAllocateInfo, &swapchainCommandBuffer));

    VkCommandBufferBeginInfo swapchainCommandBufferBeginInfo;
    swapchainCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    swapchainCommandBufferBeginInfo.pNext = nullptr;
    swapchainCommandBufferBeginInfo.flags = 0;
    swapchainCommandBufferBeginInfo.pInheritanceInfo = nullptr;
    ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(swapchainCommandBuffer, &swapchainCommandBufferBeginInfo));

    VkImageMemoryBarrier swapchainMemoryBarrier;
    swapchainMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    swapchainMemoryBarrier.pNext = nullptr;
    swapchainMemoryBarrier.srcAccessMask = 0;
    swapchainMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    swapchainMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    swapchainMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    swapchainMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    swapchainMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    swapchainMemoryBarrier.image = pWindow->pVkSwapchainImages[i];
    swapchainMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    swapchainMemoryBarrier.subresourceRange.baseMipLevel = 0;
    swapchainMemoryBarrier.subresourceRange.levelCount = 1;
    swapchainMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    swapchainMemoryBarrier.subresourceRange.layerCount = 1;

    vkCmdPipelineBarrier(swapchainCommandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &swapchainMemoryBarrier);

    VkClearColorValue vkSwapchainClearColorValue;
    vkSwapchainClearColorValue.float32[0] = 0.f;
    vkSwapchainClearColorValue.float32[1] = 0.f;
    vkSwapchainClearColorValue.float32[2] = 0.f;
    vkSwapchainClearColorValue.float32[3] = 1.f;
    
    VkImageSubresourceRange swapchainSubresourceRange;
    swapchainSubresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    swapchainSubresourceRange.baseMipLevel = 0;
    swapchainSubresourceRange.levelCount = 1;
    swapchainSubresourceRange.baseArrayLayer = 0;
    swapchainSubresourceRange.layerCount = 1;
    
    vkCmdClearColorImage(swapchainCommandBuffer, pWindow->pVkSwapchainImages[i], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &vkSwapchainClearColorValue, 1, &swapchainSubresourceRange);
    
    swapchainMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    swapchainMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    swapchainMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    swapchainMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    
    vkCmdPipelineBarrier(swapchainCommandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0, 0, nullptr, 0, nullptr, 1, &swapchainMemoryBarrier);

    ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(swapchainCommandBuffer));

    VkFenceCreateInfo fenceCreateInfo;
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.pNext = nullptr;
    fenceCreateInfo.flags = 0;

    vkCreateFence(device, &fenceCreateInfo, pInstance->pVkAllocationCallbacks, pSwapchainInitFences + i);

    VkSubmitInfo swapchainSubmitInfo;
    swapchainSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    swapchainSubmitInfo.pNext = nullptr;
    swapchainSubmitInfo.waitSemaphoreCount = 0;
    swapchainSubmitInfo.pWaitSemaphores = nullptr;
    swapchainSubmitInfo.pWaitDstStageMask = nullptr;
    swapchainSubmitInfo.commandBufferCount = 1;
    swapchainSubmitInfo.pCommandBuffers = &swapchainCommandBuffer;
    swapchainSubmitInfo.signalSemaphoreCount = 0;
    swapchainSubmitInfo.pSignalSemaphores = nullptr;

    ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &swapchainSubmitInfo, pSwapchainInitFences[i]));

    //Setup pre and post present command buffers.
    VkCommandBufferBeginInfo presentCommandBufferBeginInfo;
    presentCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    presentCommandBufferBeginInfo.pNext = nullptr;
    presentCommandBufferBeginInfo.flags = 0;
    presentCommandBufferBeginInfo.pInheritanceInfo = nullptr;

    VkImageMemoryBarrier presentMemoryBarrier;
    presentMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    presentMemoryBarrier.pNext = nullptr;
    presentMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    presentMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    presentMemoryBarrier.image = pWindow->pVkSwapchainImages[i];
    presentMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    presentMemoryBarrier.subresourceRange.baseMipLevel = 0;
    presentMemoryBarrier.subresourceRange.levelCount = 1;
    presentMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    presentMemoryBarrier.subresourceRange.layerCount = 1;

    //Setup pre.
    ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pWindow->pPrePresentCommandBuffers[i], &presentCommandBufferBeginInfo));
    presentMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    presentMemoryBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    presentMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    presentMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    vkCmdPipelineBarrier(pWindow->pPrePresentCommandBuffers[i], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr, 1, &presentMemoryBarrier);
    ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pWindow->pPrePresentCommandBuffers[i]));

    //Setup post.
    ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pWindow->pPostPresentCommandBuffers[i], &presentCommandBufferBeginInfo));
    presentMemoryBarrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
    presentMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    presentMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    presentMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    vkCmdPipelineBarrier(pWindow->pPostPresentCommandBuffers[i], VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, nullptr, 0, nullptr, 1, &presentMemoryBarrier);
    ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pWindow->pPostPresentCommandBuffers[i]));
  }

  //Wait for all the swapchain images to be initialized.
  VkResult result;
  do
  {
    result = vkWaitForFences(device, pWindow->swapchainImageCount, pSwapchainInitFences, true, 1000000000);
  } while (result != VK_SUCCESS);

  //Free the swapchain initialization fences.
  for (uint32_t i = 0; i < pWindow->swapchainImageCount; ++i)
    vkDestroyFence(device, pSwapchainInitFences[i], pInstance->pVkAllocationCallbacks);

  adkFreeCall(pSwapchainInitFences, pInstance->pAllocationCallbacks);

  //Get the first image from the swapchain.
  do
  {
    result = vkAcquireNextImageKHR(device, pWindow->vkSwapchain, 1000000000, pWindow->swapchainSemaphore, VK_NULL_HANDLE, &pWindow->swapchainImageIndex);
  } while (result != VK_SUCCESS);

  VkFenceCreateInfo fenceCreateInfo;
  fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceCreateInfo.pNext = nullptr;
  fenceCreateInfo.flags = 0;
  vkCreateFence(device, &fenceCreateInfo, pInstance->pVkAllocationCallbacks, &pWindow->fence);

  //Wait for the first image to be acquired before doing more with the swapchain.
  VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;
  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitDstStageMask = &pipelineStageFlags;
  submitInfo.pWaitSemaphores = &pWindow->swapchainSemaphore;
  submitInfo.commandBufferCount = 0;
  submitInfo.pCommandBuffers = nullptr;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &pWindow->swapchainSemaphore;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pWindow->fence));

  ShowWindow(pWindow->windowHandle, SW_SHOW);

  (*ppWindow) = pWindow;
}

void adkWindow_Destroy(adkWindow *pWindow, const adkAllocationCallbacks *pAllocationCallbacks)
{
  VkDevice device = pWindow->pInstance->pVkDevices[pWindow->deviceIndex];

  _WaitForFence(device, pWindow->fence, true);

  vkFreeCommandBuffers(device, pWindow->pInstance->pvkCommandPools[pWindow->queueIndex], pWindow->swapchainImageCount, pWindow->pSwapchainCommandBuffers);
  vkFreeCommandBuffers(device, pWindow->pInstance->pvkCommandPools[pWindow->queueIndex], pWindow->swapchainImageCount, pWindow->pPrePresentCommandBuffers);
  vkFreeCommandBuffers(device, pWindow->pInstance->pvkCommandPools[pWindow->queueIndex], pWindow->swapchainImageCount, pWindow->pPostPresentCommandBuffers);
  vkDestroySemaphore(device, pWindow->swapchainSemaphore, pWindow->pInstance->pVkAllocationCallbacks);
  vkDestroyFence(device, pWindow->fence, pWindow->pInstance->pVkAllocationCallbacks);
  adkTexture_Destroy(pWindow->pInstance, &pWindow->pDepthTexture);

  DestroyWindow(pWindow->windowHandle);

  adkFreeCall(pWindow, pAllocationCallbacks);
}

void adkWindow_Update(adkWindow *pWindow)
{
  VkResult result;
  VkDevice device = pWindow->pInstance->pVkDevices[pWindow->deviceIndex];
  VkQueue queue = pWindow->pInstance->pVkQueues[pWindow->queueIndex];
  VkPipelineStageFlags submitInfoPipelineStageFlags = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT;

  _WaitForFence(device, pWindow->fence, true);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pWindow->swapchainSemaphore;
  submitInfo.pWaitDstStageMask = &submitInfoPipelineStageFlags;
  submitInfo.commandBufferCount = 1;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &pWindow->swapchainSemaphore;

  //Transition swapchain image to present.
  submitInfo.pCommandBuffers = pWindow->pPrePresentCommandBuffers + pWindow->swapchainImageIndex;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE));

  //Present swapchain image.
  VkPresentInfoKHR vkPresentInfo;
  vkPresentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  vkPresentInfo.pNext = nullptr;
  vkPresentInfo.waitSemaphoreCount = 1;
  vkPresentInfo.pWaitSemaphores = &pWindow->swapchainSemaphore;
  vkPresentInfo.swapchainCount = 1;
  vkPresentInfo.pSwapchains = &pWindow->vkSwapchain;
  vkPresentInfo.pImageIndices = &pWindow->swapchainImageIndex;
  vkPresentInfo.pResults = &result;
  ADK_ASSERT_VK_RESULT(vkQueuePresentKHR(queue, &vkPresentInfo));

  //Wait until presentation is complete before continuing.
  //ADK_ASSERT_VK_RESULT(vkQueueWaitIdle(queue));

  //Transition swapchain image to color.
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pCommandBuffers = pWindow->pPostPresentCommandBuffers + pWindow->swapchainImageIndex;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pWindow->fence));

  //Wait for the transition to complete before acquiring the next image in the swapchain.
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pWindow->swapchainSemaphore;
  submitInfo.commandBufferCount = 0;
  submitInfo.pCommandBuffers = nullptr;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE));

  //Get the next image in the swapchain.
  do
  {
    result = vkAcquireNextImageKHR(device, pWindow->vkSwapchain, 1000000000, pWindow->swapchainSemaphore, VK_NULL_HANDLE, &pWindow->swapchainImageIndex);
  } while (result != VK_SUCCESS);

  //Reinit input values.
  pWindow->inputEventCount = 0;
  pWindow->inputState.mouseDelta = adkVector2<int32_t>::zero();

  //Handle window messages.
  MSG message;

  while (pWindow->inputEventCount < ADK_WINDOW_PARAMETER_MAX_INPUT_EVENTS && PeekMessage(&message, pWindow->windowHandle, 0, 0, PM_REMOVE))
    DispatchMessage(&message);
}

adkVector2<uint32_t> adkWindow_GetContentSize(adkWindow *pWindow)
{
  return pWindow->extent;
}

void adkWindow_GetInputEvents(adkWindow *pWindow, uint32_t *pInputEventCount, AdkInputEvent *pInputEvents)
{
  if (pInputEvents == nullptr)
    (*pInputEventCount) = pWindow->inputEventCount;
  else
    memcpy(pInputEvents, pWindow->inputEvents, (*pInputEventCount) * sizeof(AdkInputEvent));
}

void adkWindow_GetInputState(adkWindow *pWindow, AdkInputState *pInputState)
{
  memcpy(pInputState, &pWindow->inputState, sizeof(AdkInputState));
}

void adkWindow_MapKeys(char *pCharacters)
{
  for (char i = (char)ADK_KEY_0; i <= (char)ADK_KEY_9; ++i)
    pCharacters[i] = '0' + i;

  for (char i = (char)ADK_KEY_A; i <= (char)ADK_KEY_Z; ++i)
    pCharacters[i] = 'a' + i;
}

void adkWindow_GetScreenSize(AdkInstance * /*pInstance*/, adkVector2<uint32_t> *pScreenSize)
{
  RECT screenRectangle;
  HWND desktopHandle = GetDesktopWindow();
  GetWindowRect(desktopHandle, &screenRectangle);
  pScreenSize->x = screenRectangle.right;
  pScreenSize->y = screenRectangle.bottom;
}