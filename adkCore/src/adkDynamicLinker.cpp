#include "adkDynamicLinker.h"
#include "adkMemory.h"
#include "adkInstance.inl"

#ifdef _WIN32
#include <windows.h>
#endif

typedef struct AdkDynamicLinker
{
#ifdef _WIN32
  HINSTANCE library;
#endif
} AdkDynamicLinker;

#ifdef _WIN32
static inline AdkResult _CreateWindows(AdkDynamicLinker *pDynamicLinker, const AdkDynamicLinkerCreateInfo *pCreateInfo)
{
  AdkResult result = ADK_ERROR;

  pDynamicLinker->library = LoadLibraryA(pCreateInfo->pFilename);

  if (pDynamicLinker->library == 0)
  {
    FreeLibrary(pDynamicLinker->library);
    goto epilogue;
  }

  result = ADK_SUCCESS;

epilogue:
  return result;
}
#endif

AdkResult adkDynamicLinker_Create(AdkInstance *pInstance, const AdkDynamicLinkerCreateInfo *pCreateInfo, AdkDynamicLinker **ppDynamicLinker)
{
  AdkResult result;
  AdkDynamicLinker *pDynamicLinker = adkAllocTypeCall(AdkDynamicLinker, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

#ifdef _WIN32
  result = _CreateWindows(pDynamicLinker, pCreateInfo);

  if (result != ADK_SUCCESS)
    goto epilogue;
#endif

  result = ADK_SUCCESS;

epilogue:
  if (result == ADK_SUCCESS)
    (*ppDynamicLinker) = pDynamicLinker;
  else
    adkFreeCall(pDynamicLinker, pInstance->pAllocationCallbacks);

  return result;
}

void adkDynamicLinker_Destroy(AdkInstance *pInstance, AdkDynamicLinker **ppDynamicLinker)
{
  AdkDynamicLinker *pDynamicLinker = (*ppDynamicLinker);

  adkFreeCall(pDynamicLinker, pInstance->pAllocationCallbacks);

  (*ppDynamicLinker) = nullptr;
}

static inline AdkResult _GetSymbolAddressWindows(AdkDynamicLinker *pDynamicLinker, const char *pSymbol, void *pAddress)
{
  void *pAddressBuffer = GetProcAddress(pDynamicLinker->library, pSymbol);

  if (pAddressBuffer == nullptr)
    return ADK_ERROR;

  pAddress = pAddressBuffer;
  return ADK_SUCCESS;
}

AdkResult adkDynamicLinker_GetSymbolAddress(AdkDynamicLinker *pDynamicLinker, const char *pSymbol, void *pAddress)
{
#ifdef _WIN32
  return _GetSymbolAddressWindows(pDynamicLinker, pSymbol, pAddress);
#endif
}