#ifndef AdkClipBody_h__
#define AdkClipBody_h__

#include "adkTypedefs.h"
#include "adkMath.h"
#include "adkMemory.h"

struct AdkClipBody;
struct adkAllocationCallbacks;

typedef struct AdkSphereCollider
{
  float radius;
  adkVector3<float> offset;
} AdkSphereCollider;

typedef struct AdkClipBodyInstance
{
  adkVector3<float> displacement;
  adkVector3<float> smoothPosition;
  adkVector3<float> realPosition;
  adkQuaternion<float> rotation;
  adkVector3<float> velocity;
  float mass;
  float drag;
  float bounce;
  float slide;
  uint32_t collisionLayers;
  void *pUserData;
} AdkClipBodyInstance;

typedef struct AdkClipResolveParameters
{
  adkVector3<float> position;
  adkVector3<float> displacement;
  adkVector3<float> velocity;
  adkVector3<float> collisionNormal;
  float mass;
  float bounce;
  float slide;
} AdkClipResolveParameters;

typedef struct AdkClipResolveBodyParameters
{
  uint32_t layer;
  const AdkClipBodyInstance *pInstance;
  uint32_t otherLayer;
  const AdkClipBodyInstance *pOtherInstance;
} AdkClipResolveBodyParameters;

typedef struct AdkClipResolveFeedback
{
  bool resolved;
  adkVector3<float> displacement;
  adkVector3<float> velocity;
} AdkClipResolveFeedback;

typedef void (ADKAPI_CALL AdkClipResolveFunction)(void *pUserData, const AdkClipResolveParameters *pParameters, AdkClipResolveFeedback *pFeedback);
typedef void (ADKAPI_CALL AdkClipResolveBodyFunction)(void *pUserData, const AdkClipResolveBodyParameters *pParameters, AdkClipResolveFeedback *pFeedback);

typedef struct AdkClipBodyCreateInfo
{
  uint32_t maxInstanceCount;
  uint32_t sphereCollidersPerInstance;
  void *pResolverUserData;
  AdkClipResolveFunction *pResolver;
  AdkClipResolveBodyFunction *pBodyResolver;
} AdkClipBodyCreateInfo;

void adkClipBody_Create(const AdkClipBodyCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkClipBody **ppClipBody);
void adkClipBody_Destroy(AdkClipBody **ppClipBody);
uint32_t adkClipBody_GetInstanceCount(AdkClipBody *pClipBody);
void adkClipBody_SetInstanceCount(AdkClipBody *pClipBody, uint32_t instanceCount);
void adkClipBody_Map(AdkClipBody *pClipBody, AdkClipBodyInstance **ppInstances);
void adkClipBody_Unmap(AdkClipBody *pClipBody, AdkClipBodyInstance **ppInstances);
void adkClipBody_MapSphereColliders(AdkClipBody *pClipBody, AdkSphereCollider **ppSphereColliders);
void adkClipBody_UnmapSphereColliders(AdkClipBody *pClipBody, AdkSphereCollider **ppSphereColliders);
void adkClipBody_RemoveSwapLast(AdkClipBody *pClipBody, uint32_t instance);

#endif // AdkClipBody_h__
