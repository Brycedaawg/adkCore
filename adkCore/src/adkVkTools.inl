#ifndef adkVkTools_inl__
#define adkVkTools_inl__

#include "adkAssert.h"
#include "vulkan.h"
#include "adkMemory.h"
#include "adkInstance.inl"

#define ADK_ASSERT_VK_RESULT(f)\
{\
  VkResult adkAssertVkResult = (f);\
  if (adkAssertVkResult != VK_SUCCESS)\
  {\
    adkLog("Vulkan error: VkResult is \" %d \" in %s at line %d.\n", adkAssertVkResult, __FILE__, __LINE__);\
    assert(adkAssertVkResult == VK_SUCCESS);\
  }\
}

static inline void _GetMemoryTypeIndex(VkPhysicalDevice physicalDevice, const VkMemoryRequirements *pMemoryRequirements, VkMemoryPropertyFlags requirementFlags, uint32_t *pMemoryTypeIndex)
{
  VkPhysicalDeviceMemoryProperties memoryProperties;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);

  uint32_t memoryTypeBits = pMemoryRequirements->memoryTypeBits;

  for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
  {
    if ((memoryTypeBits & 1) && ((requirementFlags & memoryProperties.memoryTypes[i].propertyFlags) == requirementFlags))
    {
      (*pMemoryTypeIndex) = i;
      return;
    }

    memoryTypeBits >>= 1;
  }

  ADK_ASSERT(false, "Couldn't find memory type index with requirement flags %d", requirementFlags);
}

static inline void _BeginCommandBuffer(VkCommandBuffer commandBuffer)
{
  VkCommandBufferBeginInfo vkCommandBufferBeginInfo;
  vkCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  vkCommandBufferBeginInfo.pNext = nullptr;
  vkCommandBufferBeginInfo.flags = 0;
  vkCommandBufferBeginInfo.pInheritanceInfo = nullptr;

  vkBeginCommandBuffer(commandBuffer, &vkCommandBufferBeginInfo);
}

static inline void _CreateBuffer(AdkInstance *pInstance, VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, const adkAllocationCallbacks *pAllocator, VkBufferUsageFlags usage, VkMemoryPropertyFlags propertyFlags, size_t size, VkBuffer *pBuffer, AdkDeviceMemory *pDeviceMemory)
{
  //Create the buffer.
  VkBufferCreateInfo bufferCreateInfo;
  bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  bufferCreateInfo.pNext = nullptr;
  bufferCreateInfo.flags = 0;
  bufferCreateInfo.size = size;
  bufferCreateInfo.usage = usage;
  bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  bufferCreateInfo.queueFamilyIndexCount = 0;
  bufferCreateInfo.pQueueFamilyIndices = nullptr;

  VkBuffer buffer;
  ADK_ASSERT_VK_RESULT(vkCreateBuffer(device, &bufferCreateInfo, pVkAllocator, &buffer));

  VkMemoryRequirements memoryRequirements;
  vkGetBufferMemoryRequirements(device, buffer, &memoryRequirements);

  uint32_t memoryTypeIndex;
  _GetMemoryTypeIndex(physicalDevice, &memoryRequirements, propertyFlags, &memoryTypeIndex);

  AdkDeviceMemory deviceMemory;
  adkMemoryPool_AllocateDeviceMemory(pInstance->pMemoryPool, physicalDevice, device, pVkAllocator, pAllocator, memoryRequirements.size, memoryRequirements.alignment, memoryTypeIndex, &deviceMemory);

  //Bind the staging buffer to the allocated device memory.
  ADK_ASSERT_VK_RESULT(vkBindBufferMemory(device, buffer, deviceMemory.vkDeviceMemory, deviceMemory.offset));

  (*pBuffer) = buffer;
  (*pDeviceMemory) = deviceMemory;
}

static inline void _CreateImage(AdkInstance *pInstance, VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, const adkAllocationCallbacks *pAllocator, VkImageUsageFlags usage, VkMemoryPropertyFlagBits properties, uint32_t width, uint32_t height, uint32_t depth, uint32_t mipLevels, VkImageType imageType, VkFormat format, VkImage *pImage, AdkDeviceMemory *pDeviceMemory)
{
  VkFormatProperties formatProperties;
  VkImageTiling tiling;
  vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &formatProperties);

  ADK_ASSERT(formatProperties.optimalTilingFeatures != 0 || formatProperties.linearTilingFeatures != 0, "Format has no optimal tiling features or linear tiling features");

  if (formatProperties.optimalTilingFeatures != 0)
    tiling = VK_IMAGE_TILING_OPTIMAL;
  else
    tiling = VK_IMAGE_TILING_LINEAR;

  //Create image.
  VkImageCreateInfo imageCreateInfo;
  imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  imageCreateInfo.pNext = nullptr;
  imageCreateInfo.flags = 0;
  imageCreateInfo.imageType = imageType;
  imageCreateInfo.format = format;
  imageCreateInfo.extent.width = width;
  imageCreateInfo.extent.height = height;
  imageCreateInfo.extent.depth = depth;
  imageCreateInfo.mipLevels = mipLevels;
  imageCreateInfo.arrayLayers = 1;
  imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
  imageCreateInfo.tiling = tiling;
  imageCreateInfo.usage = usage;
  imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  imageCreateInfo.queueFamilyIndexCount = 0;
  imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  VkImage image;
  ADK_ASSERT_VK_RESULT(vkCreateImage(device, &imageCreateInfo, pVkAllocator, &image));

  //Allocate memory.
  VkMemoryRequirements memoryRequirements;
  vkGetImageMemoryRequirements(device, image, &memoryRequirements);

  uint32_t memoryTypeIndex;
  _GetMemoryTypeIndex(physicalDevice, &memoryRequirements, properties, &memoryTypeIndex);
  AdkDeviceMemory deviceMemory;
  adkMemoryPool_AllocateDeviceMemory(pInstance->pMemoryPool, physicalDevice, device, pVkAllocator, pAllocator, memoryRequirements.size, memoryRequirements.alignment, memoryTypeIndex, &deviceMemory);

  //Bind image to memory.
  ADK_ASSERT_VK_RESULT(vkBindImageMemory(device, image, deviceMemory.vkDeviceMemory, deviceMemory.offset));

  (*pImage) = image;
  (*pDeviceMemory) = deviceMemory;
}

static inline void _FreeDeviceMemory(AdkInstance *pInstance, AdkDeviceMemory *pDeviceMemory)
{
  adkMemoryPool_FreeDeviceMemory(pInstance->pMemoryPool, pDeviceMemory);
}

static inline void _CreateAndBeginCommandBuffer(VkDevice device, VkCommandPool commandPool, VkCommandBuffer *pCommandBuffer)
{
  VkCommandBufferAllocateInfo allocateInfo;
  allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  allocateInfo.pNext = nullptr;
  allocateInfo.commandPool = commandPool;
  allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  allocateInfo.commandBufferCount = 1;
  VkCommandBuffer commandBuffer;
  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &allocateInfo, &commandBuffer));

  VkCommandBufferBeginInfo beginInfo;
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.pNext = nullptr;
  beginInfo.flags = 0;
  beginInfo.pInheritanceInfo = nullptr;
  ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(commandBuffer, &beginInfo));

  (*pCommandBuffer) = commandBuffer;
}

static inline void _CreateFence(VkDevice device, const VkAllocationCallbacks *pAllocationCallbacks, VkFence *pFence, bool createSignaled)
{
  VkFenceCreateInfo createInfo;
  createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  createInfo.pNext = nullptr;
  createInfo.flags = createSignaled ? VK_FENCE_CREATE_SIGNALED_BIT : 0;
  ADK_ASSERT_VK_RESULT(vkCreateFence(device, &createInfo, pAllocationCallbacks, pFence));
}

static inline void _WaitForFence(VkDevice device, VkFence fence, bool reset)
{
  VkResult result;
  do
  {
    result = vkWaitForFences(device, 1, &fence, false, 1000000000);
  } while (result == VK_TIMEOUT);

  ADK_ASSERT_VK_RESULT(result);

  if (reset)
    vkResetFences(device, 1, &fence);
}

static inline void _CreateSemaphore(VkDevice device, VkQueue queue, const VkAllocationCallbacks *pAllocationCallbacks, VkSemaphore *pSemaphore)
{
  VkSemaphoreCreateInfo createInfo;
  createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
  createInfo.pNext = nullptr;
  createInfo.flags = 0;
  ADK_ASSERT_VK_RESULT(vkCreateSemaphore(device, &createInfo, pAllocationCallbacks, pSemaphore));

  //Signal the semaphore
  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pWaitDstStageMask = 0;
  submitInfo.commandBufferCount = 0;
  submitInfo.pCommandBuffers = nullptr;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = pSemaphore;
  vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
}

static inline void _EndAndSubmitCommandBuffer(VkCommandBuffer commandBuffer, VkQueue queue, VkFence fence)
{
  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(commandBuffer));

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pWaitDstStageMask = nullptr;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, fence));
}

static inline void _CreateCopyCommandBuffer(VkDevice device, VkBuffer src, VkBuffer dst, VkDeviceSize size, VkCommandPool commandPool, VkCommandBuffer *pCommandBuffer)
{
  //Allocate copy command buffer.
  VkCommandBufferAllocateInfo commandBufferAllocateInfo;
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.pNext = nullptr;
  commandBufferAllocateInfo.commandPool = commandPool;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = 1;

  VkCommandBuffer commandBuffer;
  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer));

  //Record copy commands.
  VkCommandBufferBeginInfo commandBufferBeginInfo;
  commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  commandBufferBeginInfo.pNext = nullptr;
  commandBufferBeginInfo.flags = 0;
  commandBufferBeginInfo.pInheritanceInfo = nullptr;

  ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo));

  VkBufferCopy bufferCopy;
  bufferCopy.srcOffset = 0;
  bufferCopy.dstOffset = 0;
  bufferCopy.size = size;
  vkCmdCopyBuffer(commandBuffer, src, dst, 1, &bufferCopy);

  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(commandBuffer));

  (*pCommandBuffer) = commandBuffer;
}

static inline bool _GetQueueFamily(AdkInstance *pInstance, VkQueueFlags flags, uint32_t *pQueueFamilyIndex)
{
  for (uint32_t i = 0; i < pInstance->pVkDeviceQueueFamilyRanges[pInstance->primaryDeviceIndex].length; ++i)
  {
    uint32_t queueFamilyIndex = pInstance->pVkDeviceQueueFamilyRanges[pInstance->primaryDeviceIndex].start + i;
    if ((pInstance->pVkQueueFamilyProperties[queueFamilyIndex].queueFlags & flags) == flags)
    {
      *pQueueFamilyIndex = queueFamilyIndex;
      return true;
    }
  }

  return false;
}

static inline VkClearColorValue _CreateVkClearColorValueFloat(uint32_t color)
{
  VkClearColorValue clearColorValue;
  clearColorValue.float32[0] = (color >> 24) / 255.f;
  clearColorValue.float32[1] = ((color >> 16) & 0x000000FF) / 255.f;
  clearColorValue.float32[2] = ((color >> 8) & 0x000000FF) / 255.f;
  clearColorValue.float32[3] = (color & 0x000000FF) / 255.f;
  return clearColorValue;
}

static inline uint32_t _ForceLeftLSB(uint32_t value)
{
  uint8_t *pValue = (uint8_t*)&value;
  return ((uint32_t)pValue[0] << 24) | ((uint32_t)pValue[1] << 16) | ((uint32_t)pValue[2] << 8) | (uint32_t)pValue[3];
}

static inline AdkAllocationScope _VkToAdkAllocationScope(VkSystemAllocationScope allocationScope)
{
  if (allocationScope == VK_SYSTEM_ALLOCATION_SCOPE_COMMAND)
    return ADK_ALLOCATION_SCOPE_COMMAND;

  return ADK_ALLOCATION_SCOPE_DYNAMIC;
}

static inline void* VKAPI_PTR _AllocationCallback(void *pUserData, size_t size, size_t /*alignment*/, VkSystemAllocationScope allocationScope)
{
  adkAllocationCallbacks *pAllocator = (adkAllocationCallbacks*)pUserData;
  return pAllocator->pAllocationFunction(pAllocator->pUserData, size, _VkToAdkAllocationScope(allocationScope));

}

static inline void* VKAPI_PTR _ReallocationCallback(void *pUserData, void *pOriginal, size_t size, size_t /*alignment*/, VkSystemAllocationScope allocationScope)
{
  adkAllocationCallbacks *pAllocator = (adkAllocationCallbacks*)pUserData;
  return pAllocator->pReallocationFunction(pAllocator->pUserData, pOriginal, size, _VkToAdkAllocationScope(allocationScope));
}

static inline void VKAPI_PTR _FreeCallback(void *pUserData, void *pMemory)
{
  adkAllocationCallbacks *pAllocator = (adkAllocationCallbacks*)pUserData;
  pAllocator->pFreeFunction(pAllocator->pUserData, pMemory);
}

static inline void VKAPI_PTR _InternalAllocationCallback(void * /*pUserData*/, size_t /*size*/, VkInternalAllocationType /*allocationType*/, VkSystemAllocationScope /*allocationScope*/)
{

}

static inline void VKAPI_PTR _InternalFreeCallback(void * /*pUserData*/, size_t /*size*/, VkInternalAllocationType /*allocationType*/, VkSystemAllocationScope /*allocationScore*/)
{

}

static inline VkAllocationCallbacks* _AdkToVkAllocationCallbacks(const adkAllocationCallbacks *pAdkAllocationCallbacks, VkAllocationCallbacks *pVkAllocationCallbacks)
{
  if (pAdkAllocationCallbacks == nullptr)
    return nullptr;

  pVkAllocationCallbacks->pUserData = (void*)pAdkAllocationCallbacks;
  pVkAllocationCallbacks->pfnAllocation = _AllocationCallback;
  pVkAllocationCallbacks->pfnReallocation = _ReallocationCallback;
  pVkAllocationCallbacks->pfnFree = _FreeCallback;
  pVkAllocationCallbacks->pfnInternalAllocation = _InternalAllocationCallback;
  pVkAllocationCallbacks->pfnInternalFree = _InternalFreeCallback;

  return pVkAllocationCallbacks;
}

#endif // adkVkTools_inl__
