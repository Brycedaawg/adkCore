#ifndef AdkRandom_h__
#define AdkRandom_h__

#include "adkMemory.h"

struct AdkRandom;

void adkRandom_Create(uint32_t seed, const adkAllocationCallbacks *pAllocator, AdkRandom **ppRandom);
void adkRandom_Destroy(AdkRandom **ppRandom);
uint16_t adkRandom_NextUint16(AdkRandom *pRandom);
uint16_t adkRandom_NextUint16(AdkRandom *pRandom, uint16_t max);
uint32_t adkRandom_NextUint32(AdkRandom *pRandom);
uint32_t adkRandom_NextUint32(AdkRandom *pRandom, uint32_t max);
float adkRandom_NextFloat32(AdkRandom *pRandom);

#endif // AdkRandom_h__
