#include "AdkClip.h"
#include "adkMemory.h"
#include "adkAssert.h"
#include "AdkClipBody.inl"
#include "AdkClipMesh.inl"
#include "AdkPhysicsUtil.h"
#include "AdkRTree.h"
#include "AdkBinaryHeap.h"
#include "AdkLineRendererUtil.h"

static const float ADK_CLIP_MIN_BOUNCE = 0.002f;
static const float ADK_CLIP_MAX_SLEEP_SPEED = 0.002f;
static const uint32_t MAX_PASS_COUNT = 8;

typedef struct AdkClipLayer
{
  bool collides;
  uint32_t mask;
  AdkClipBody *pFirstBody; //The first clip body in the linked list.
  AdkClipMesh *pFirstMesh; //The first clip mesh in the linked list.
} AdkClipLayer;

typedef struct AdkClip
{
  const adkAllocationCallbacks *pAllocator;
  AdkClipLayer layers[8 * sizeof(AdkFlags)];
  uint64_t timeSinceLastStep;
  float lastStepT;
} AdkClip;

typedef enum AdkClipCollisionType
{
  ADK_CLIP_COLLISION_TYPE_MESH,
  ADK_CLIP_COLLISION_TYPE_BODY,
} AdkClipCollisionType;

void adkClip_Create(const adkAllocationCallbacks *pAllocator, AdkClip **ppClip)
{
  AdkClip *pClip = adkAllocTypeCall(AdkClip, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pClip->pAllocator = pAllocator;

  for (uint32_t l = 0; l < ADK_ARRAY_SIZE(pClip->layers); ++l)
  {
    pClip->layers[l].mask = 0xFFFFFFFF;
    pClip->layers[l].collides = true;
  }

  *ppClip = pClip;
}

void adkClip_Destroy(AdkClip **ppClip)
{
  AdkClip *pClip = *ppClip;

  adkFreeCall(*ppClip, pClip->pAllocator);
}

void adkClip_SetLayer(AdkClip *pClip, uint32_t layer, bool collides, uint32_t mask)
{
  uint32_t layerIndex = adkLog2(layer);
  AdkClipLayer *pLayer = &pClip->layers[layerIndex];

  pLayer->collides = collides;
  pLayer->mask = mask;
}

template<typename T>
static inline void _AddElement(T **ppFirst, T *pElement)
{
  //Add the element to the end of the linked list.
  T **ppPrevious = ppFirst;

  while (*ppPrevious != nullptr)
    ppPrevious = &(*ppPrevious)->pNext;

  (*ppPrevious) = pElement;
  pElement->pNext = nullptr;
}

void adkClip_AddBody(AdkClip *pClip, AdkClipBody *pBody, uint32_t layer)
{
  ADK_ASSERT(layer != 0, "layer cannot be zero");
  ADK_ASSERT((layer & (layer - 1)) == 0, "layer must be a power of two");
  ADK_ASSERT(!pBody->added, "pBody has already been added to a clip");

  uint32_t layerIndex = adkLog2(layer);
  AdkClipLayer *pLayer = &pClip->layers[layerIndex];
  pBody->layerIndex = layerIndex;

  _AddElement(&pLayer->pFirstBody, pBody);
  pBody->added = true;
}

void adkClip_AddMesh(AdkClip *pClip, AdkClipMesh *pMesh, uint32_t layer)
{
  ADK_ASSERT(layer != 0, "layer cannot be zero");
  ADK_ASSERT((layer & (layer - 1)) == 0, "layer must be a power of two");
  ADK_ASSERT(!pMesh->added, "pMesh has already been added to a clip");

  uint32_t layerIndex = adkLog2(layer);
  AdkClipLayer *pLayer = &pClip->layers[layerIndex];
  pMesh->layerIndex = layerIndex;

  _AddElement(&pLayer->pFirstMesh, pMesh);
  pMesh->added = true;
}

template<typename T>
static inline void _RemoveElement(T **ppFirst, T *pElement)
{
  //Remove the element from the linked list. Link the previous pointer (element or start) to the element after (could be nullptr).
  T **ppPrevious = ppFirst;

  while (*ppPrevious != pElement)
    ppPrevious = &(*ppPrevious)->pNext;

  (*ppPrevious) = pElement->pNext;
  pElement->pNext = nullptr;
}

void adkClip_RemoveBody(AdkClip *pClip, AdkClipBody *pBody)
{
  ADK_ASSERT(pBody->added, "pBody has not been added to a clip");

  AdkClipLayer *pLayer = &pClip->layers[pBody->layerIndex];

  _RemoveElement(&pLayer->pFirstBody, pBody);
  pBody->added = false;
}

void adkClip_RemoveMesh(AdkClip *pClip, AdkClipMesh *pMesh)
{
  ADK_ASSERT(pMesh->added, "pMesh has not been added to a clip");

  AdkClipLayer *pLayer = &pClip->layers[pMesh->layerIndex];

  _RemoveElement(&pLayer->pFirstMesh, pMesh);
  pMesh->added = false;
}

//Reflects incident off normal.
//Parallel is the portion of the resulting reflection that is parallel to normal.
//Perpendicular is the portion of the resulting reflection that is perpendicular to normal.
//Parallel is multiplied by slide.
//Perpendicular is multiplied by bounce.
static inline void _Reflect(const adkVector3<float> &incident, const adkVector3<float> &normal, float bounce, float slide, adkVector3<float> *pParallel, adkVector3<float> *pPerpendicular)
{
  float dot = adkVector3<float>::dot(-incident, normal);

  adkVector3<float> projection = dot * normal;
  adkVector3<float> rejection = incident + projection;

  *pPerpendicular = projection * bounce;
  *pParallel = rejection * slide;
}

static inline void _GetVectorComponents(const adkVector3<float> &incident, const adkVector3<float> &normal, adkVector3<float> *pParallel, adkVector3<float> *pPerpendicular)
{
  float dot = adkVector3<float>::dot(incident, normal);

  *pPerpendicular = dot * normal;
  *pParallel = incident - *pPerpendicular;
}

static inline void _ResolveCollision(const adkVector3<float> &resolutionA, adkVector3<float> *pVelocityA, adkVector3<float> *pDisplacementA, const adkVector3<float> &resolutionB, adkVector3<float> *pVelocityB, adkVector3<float> *pDisplacementB)
{
  adkVector3<float> normal = (resolutionA - resolutionB).normalized();

  adkVector3<float> parallelVelocityA, parallelVelocityB, perpendicularVelocityA, perpendicularVelocityB;
  _GetVectorComponents(*pVelocityA, normal, &parallelVelocityA, &perpendicularVelocityA);
  _GetVectorComponents(*pVelocityB, -normal, &parallelVelocityB, &perpendicularVelocityB);

  //perpendicularVelocityA *= 0.9f;
  //perpendicularVelocityB *= 0.9f;

  *pVelocityA = parallelVelocityA + perpendicularVelocityB;
  *pVelocityB = parallelVelocityB + perpendicularVelocityA;

  //adkVector3<float> resolutionDisplacementA = resolutionA - *pPositionA;
  //adkVector3<float> remainingDisplacementA = *pDisplacementA - resolutionDisplacementA;

  //adkVector3<float> resolutionDisplacementB = resolutionB - *pPositionB;
  //adkVector3<float> remainingDisplacementB = *pDisplacementB - resolutionDisplacementB;

  adkVector3<float> parallelDisplacementA, parallelDisplacementB, perpendicularDisplacementA, perpendicularDisplacementB;
  _GetVectorComponents(*pDisplacementA, normal, &parallelDisplacementA, &perpendicularDisplacementA);
  _GetVectorComponents(*pDisplacementB, -normal, &parallelDisplacementB, &perpendicularDisplacementB);

  //perpendicularDisplacementA *= 0.9f;
  //perpendicularDisplacementB *= 0.9f;

  //Push bodies away from each other slightly if they're not moving toward or away from each other.
  if (adkVector3<float>::squareDistance(perpendicularDisplacementA, perpendicularDisplacementB) <= ADK_CLIP_MIN_BOUNCE * ADK_CLIP_MIN_BOUNCE)
  {
    perpendicularDisplacementA += ADK_CLIP_MIN_BOUNCE * normal;
    perpendicularDisplacementB -= ADK_CLIP_MIN_BOUNCE * normal;
  }

  *pDisplacementA = parallelDisplacementA + perpendicularDisplacementB;
  *pDisplacementB = parallelDisplacementB + perpendicularDisplacementA;
}

static inline void _ResolveElasticCollision(float massA, float massB, const adkVector3<float> &velocityA, const adkVector3<float> &velocityB, adkVector3<float> *pResolvedA, adkVector3<float> *pResolvedB)
{
  float sumMass = massA + massB;
  *pResolvedA = (massA - massB) / sumMass * velocityA + 2.f * massB / sumMass * velocityB;
  *pResolvedB = (massB - massA) / sumMass * velocityB + 2.f * massA / sumMass * velocityA;
}

//Try to move the given body by its displacement, resolving the soonest collision if one occurs.
static inline bool _CollideWithMeshes(AdkClip *pClip, AdkClipBody *pBody, uint32_t in, AdkLineRenderer *pLineRenderer)
{
  AdkClipBodyInstance *pInstance = &pBody->pInstances[in];

  //Find soonest body on mesh collision.
  float bestT = adkFloatMax;
  adkVector3<float> bestResolution = adkVector3<float>::zero();
  adkVector3<float> bestContact = adkVector3<float>::zero();
  for (uint32_t otLa = 0; otLa < ADK_ARRAY_SIZE(pClip->layers); ++otLa)
  {
    AdkClipLayer *pOtherLayer = &pClip->layers[otLa];

    for (AdkClipMesh *pMesh = pOtherLayer->pFirstMesh; pMesh != nullptr; pMesh = pMesh->pNext)
    {
      for (uint32_t colliderIndex = 0; colliderIndex < pBody->sphereCollidersPerInstance; ++colliderIndex)
      {
        AdkSphereCollider *pCollider = &pBody->pSphereColliders[in * pBody->sphereCollidersPerInstance + colliderIndex];
        adkVector3<float> colliderPosition = pInstance->realPosition + pCollider->offset;

        AdkSphereSweepCollision collision;
        if (adkClipMesh_SweepSphere(pMesh, colliderPosition, pInstance->displacement, pCollider->radius, &collision, pLineRenderer) && collision.resolutionT < bestT)
        {
          bestT = collision.resolutionT;
          bestResolution = collision.resolution;
          bestContact = collision.contact;
        }
      }
    }
  }

  if (bestT == adkFloatMax)
  {
    //No mesh collision detected; move body to end.
    pInstance->realPosition += pInstance->displacement;
    pInstance->displacement = adkVector3<float>::zero();
  }
  else
  {
    //A mesh collision has been detected; resolve.
    pInstance->realPosition += bestT * pInstance->displacement;
    pInstance->displacement *= 1.f - bestT;

    adkVector3<float> collisionNormal = (bestResolution - bestContact).normalized();

    //Run custom resolver if one has been provided.
    bool resolved = false;
    if (pBody->pResolver != nullptr)
    {
      AdkClipResolveParameters parameters;
      parameters.position = pInstance->realPosition;
      parameters.displacement = pInstance->displacement;
      parameters.velocity = pInstance->velocity;
      parameters.collisionNormal = collisionNormal;
      parameters.mass = pInstance->mass;
      parameters.bounce = pInstance->bounce;
      parameters.slide = pInstance->slide;

      AdkClipResolveFeedback feedback;
      memset(&feedback, 0, sizeof(feedback));

      pBody->pResolver(pBody->pResolverUserData, &parameters, &feedback);

      if (feedback.resolved)
      {
        resolved = feedback.resolved;
        pInstance->velocity = feedback.velocity;
        pInstance->displacement = feedback.displacement;
      }
    }

    //Fall back to default collision resolution if there's no custom resolver or the custom resolver didn't resolve the collision.
    if (!resolved)
    {
      //Reflect velocity off the wall.
      adkVector3<float> parallelVelocity, perpendicularVelocity;
      _Reflect(pInstance->velocity, collisionNormal, pInstance->bounce, pInstance->slide, &parallelVelocity, &perpendicularVelocity);
      pInstance->velocity = perpendicularVelocity + parallelVelocity;

      //Reflect displacement off the wall.
      adkVector3<float> parallelDisplacement, perpendicularDisplacement;
      _Reflect(pInstance->displacement, collisionNormal, pInstance->bounce, pInstance->slide, &parallelDisplacement, &perpendicularDisplacement);
      pInstance->displacement = parallelDisplacement + perpendicularDisplacement;
    }

    //Prevent body from continuously colliding with mesh when body's perpendicular displacement is too low (it's sliding).
    float signedPerpMag = adkVector3<float>::dot(pInstance->displacement, collisionNormal);
    if (pInstance->displacement != adkVector3<float>::zero() && signedPerpMag <= ADK_CLIP_MIN_BOUNCE)
      pInstance->displacement += (ADK_CLIP_MIN_BOUNCE - signedPerpMag) * collisionNormal;
  }

  return bestT != adkFloatMax;
}

//Gets soonest collision for a given body instance and pushes it to the binary heap.
static inline bool _CollideWithBodies(AdkClip *pClip, AdkClipBody *pBody, uint32_t in, AdkLineRenderer *pLineRenderer)
{
  bool collided = false;
  AdkClipBodyInstance *pInstance = &pBody->pInstances[in];
  AdkRTreeEntity *pRTreeEntity = &pBody->pRTreeEntities[in];

  for (uint32_t colliderIndex = 0; colliderIndex < pBody->sphereCollidersPerInstance; ++colliderIndex)
  {
    AdkSphereCollider *pCollider = &pBody->pSphereColliders[in * pBody->sphereCollidersPerInstance + colliderIndex];
    adkVector3<float> colliderPosition = pInstance->realPosition + pCollider->offset;

    if (pLineRenderer)
      adkLineRenderer_AddSphere(pLineRenderer, colliderPosition + pInstance->displacement, pCollider->radius, adkQuaternion<float>::identity(), 0x00FF00FF);

    for (uint32_t otLa = 0; otLa < ADK_ARRAY_SIZE(pClip->layers); ++otLa)
    {
      AdkClipLayer *pOtherLayer = &pClip->layers[otLa];

      //Check for body on body collisions.
      for (AdkClipBody *pOtherBody = pOtherLayer->pFirstBody; pOtherBody != nullptr; pOtherBody = pOtherBody->pNext)
      {
        //Broad-phase test.
        uint32_t broadCollisionCount = 0;
        uint32_t ignoreEntity = ADK_R_TREE_ENTITY_NONE;
        adkRTree_GetCollisions(pOtherBody->pRTree, pRTreeEntity->position, pRTreeEntity->radius, pOtherBody->instanceCount, &broadCollisionCount, pOtherBody->pRTreeCollisions, ignoreEntity);

        for (uint32_t col = 0; col < broadCollisionCount; ++col)
        {
          AdkRTreeCollision *pBroadCollision = &pOtherBody->pRTreeCollisions[col];
          AdkClipBodyInstance *pOtherInstance = &pOtherBody->pInstances[pBroadCollision->entity / pOtherBody->sphereCollidersPerInstance];
          AdkSphereCollider *pOtherCollider = &pOtherBody->pSphereColliders[pBroadCollision->entity];
          adkVector3<float> otherColliderPosition = pOtherInstance->realPosition + pOtherCollider->offset;
          uint32_t otIn = pBroadCollision->entity / pOtherBody->sphereCollidersPerInstance;

          //Don't collide with self.
          if (pBody == pOtherBody && in == otIn)
            continue;

          //Don't collide with ignored layers.
          bool aCollidesWithB = (pInstance->collisionLayers & (1 << pOtherBody->layerIndex)) != 0;
          bool bCollidesWithA = (pOtherInstance->collisionLayers & (1 << pBody->layerIndex)) != 0;

          if (!aCollidesWithB || !bCollidesWithA)
            continue;

          //Exact check.
          adkVector3<float> endA = colliderPosition + pInstance->displacement;
          adkVector3<float> endB = otherColliderPosition + pOtherInstance->displacement;
          float distance = adkVector3<float>::distance(endA, endB);
          float radii = pCollider->radius + pOtherCollider->radius;
          float penetration = radii - distance;

          //Only collide if the bodies are moving toward each other. Prevents collisions happening twice because the resolution wasn't quite enough to put the two bodies out of range.
          adkVector3<float> da_db = pOtherInstance->displacement - pInstance->displacement;
          adkVector3<float> sa_sb = otherColliderPosition - colliderPosition;
          adkVector3<float> vDistVel = 2.f * da_db * sa_sb;
          float distVel = vDistVel.x + vDistVel.y + vDistVel.z; //How fast the bodies are moving toward or away from each other.
          bool movingCloser = distVel < 0.f;

          if (penetration > 0.f && movingCloser)
          {
            collided = true;

            adkVector3<float> tangent = distance > 0.005f ? (endB - endA) / distance : adkVector3<float>::create(1.f, 0.f, 0.f);

            pInstance->displacement -= 0.5f * penetration * tangent;
            pOtherInstance->displacement += 0.5f * penetration * tangent;

            adkVector3<float> parallelVelocityA, parallelVelocityB, perpendicularVelocityA, perpendicularVelocityB;
            _GetVectorComponents(pInstance->velocity, tangent, &parallelVelocityA, &perpendicularVelocityA);
            _GetVectorComponents(pOtherInstance->velocity, -tangent, &parallelVelocityB, &perpendicularVelocityB);

            adkVector3<float> resolvedPerpVelA, resolvedPerpVelB;
            _ResolveElasticCollision(pInstance->mass, pOtherInstance->mass, perpendicularVelocityA, perpendicularVelocityB, &resolvedPerpVelA, &resolvedPerpVelB);

            AdkClipResolveBodyParameters parametersA;
            parametersA.layer = 1 << pBody->layerIndex;
            parametersA.pInstance = pInstance;
            parametersA.otherLayer = 1 << pOtherBody->layerIndex;
            parametersA.pOtherInstance = pOtherInstance;

            AdkClipResolveBodyParameters parametersB;
            parametersB.layer = 1 << pOtherBody->layerIndex;
            parametersB.pInstance = pOtherInstance;
            parametersB.otherLayer = 1 << pBody->layerIndex;
            parametersB.pOtherInstance = pInstance;

            AdkClipResolveFeedback feedbackA;
            memset(&feedbackA, 0, sizeof(feedbackA));

            AdkClipResolveFeedback feedbackB;
            memset(&feedbackB, 0, sizeof(feedbackB));

            if (pBody->pBodyResolver != nullptr)
              pBody->pBodyResolver(pBody->pResolverUserData, &parametersA, &feedbackA);

            if (pOtherBody->pBodyResolver != nullptr)
              pOtherBody->pBodyResolver(pOtherBody->pResolverUserData, &parametersB, &feedbackB);

            if (feedbackA.resolved)
            {
              pInstance->displacement = feedbackA.displacement;
              pInstance->velocity = feedbackA.velocity;
            }
            else
            {
              pInstance->velocity = parallelVelocityA + resolvedPerpVelA;
            }

            if (feedbackB.resolved)
            {
              pOtherInstance->displacement = feedbackB.displacement;
              pOtherInstance->velocity = feedbackB.velocity;
            }
            else
            {
              pOtherInstance->velocity = parallelVelocityB + resolvedPerpVelB;
            }
          }
        }
      }
    }
  }

  return collided;
}

static inline void _Step(AdkClip *pClip, AdkLineRenderer *pLineRenderer)
{
  if (pLineRenderer != nullptr)
    adkLineRenderer_Clear(pLineRenderer);

  float deltaTime = ADK_CLIP_MILLISECONDS_PER_STEP / 1000.f;
 float gravity = 9.8f * deltaTime;

  //Prepare body's for physics step.
  for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
  {
    AdkClipLayer *pLayer = &pClip->layers[la];

    if (!pLayer->collides)
      continue;

    for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
    {
      for (uint32_t in = 0; in < pBody->instanceCount; ++in)
      {
        //Apply gravity and drag.
        AdkClipBodyInstance *pInstance = &pBody->pInstances[in];
        ADK_ASSERT(pInstance->mass != 0.f, "Instance has zero mass during physics step. This is not allowed.");
        pInstance->velocity.y -= gravity;
        pInstance->velocity /= 1.f + deltaTime * pInstance->drag / pInstance->mass;

        //Body displacement accumulates one step's worth on velocity.
        pBody->pInstances[in].smoothPosition = pBody->pInstances[in].realPosition;
        pBody->pInstances[in].displacement += deltaTime * pBody->pInstances[in].velocity;
      }
    }
  }

  for (uint32_t pass = 0; pass < MAX_PASS_COUNT; ++pass)
  {
    //Update R-trees with positions and radii.
    for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
    {
      AdkClipLayer *pLayer = &pClip->layers[la];

      if (!pLayer->collides)
        continue;

      for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
      {
        for (uint32_t in = 0; in < pBody->instanceCount; ++in)
        {
          AdkClipBodyInstance *pInstance = &pBody->pInstances[in];

          for (uint32_t colliderIndex = 0; colliderIndex < pBody->sphereCollidersPerInstance; ++colliderIndex)
          {
            AdkSphereCollider *pCollider = &pBody->pSphereColliders[in * pBody->sphereCollidersPerInstance + colliderIndex];

            pBody->pRTreeEntities[in].position = pInstance->realPosition + 0.5f * pInstance->displacement + pCollider->offset;

            //TODO: This value is arbitrary and works for the use case.
            pBody->pRTreeEntities[in].radius = 2.f * pCollider->radius + pInstance->displacement.magnitude();
          }
        }

        //Link R-Tree.
        adkRTree_Link(pBody->pRTree, pBody->instanceCount, pBody->pRTreeEntities);
      }
    }

    bool collided = false;

    //Detect body collisions and resolve by adding to displacement.
    for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
    {
      AdkClipLayer *pLayer = &pClip->layers[la];

      if (!pLayer->collides)
        continue;

      for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
      {
        for (uint32_t in = 0; in < pBody->instanceCount; ++in)
        {
          if (_CollideWithBodies(pClip, pBody, in, pLineRenderer))
            collided = true;
        }
      }
    }

    //Detect mesh collisions and resolve by moving body instances.
    for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
    {
      AdkClipLayer *pLayer = &pClip->layers[la];

      if (!pLayer->collides)
        continue;

      for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
      {
        for (uint32_t in = 0; in < pBody->instanceCount; ++in)
          if (_CollideWithMeshes(pClip, pBody, in, pLineRenderer))
            collided = true;
      }
    }

    //Skip the remaining passes if there are no more collisions to resolve.
    if (!collided)
      break;
  }
}

static inline void _InterpolateBodies(AdkClip *pClip)
{
  float stepT = (float)pClip->timeSinceLastStep / ADK_CLIP_MILLISECONDS_PER_STEP;
  float deltaT = stepT - pClip->lastStepT;
  float remainingT = 1.f - pClip->lastStepT;
  float scaledT = deltaT / remainingT;

  for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
  {
    AdkClipLayer *pLayer = &pClip->layers[la];

    for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
    {
      for (uint32_t in = 0; in < pBody->instanceCount; ++in)
      {
        AdkClipBodyInstance *pInstance = &pBody->pInstances[in];
        pInstance->smoothPosition = adkLerp(pInstance->smoothPosition, pInstance->realPosition, scaledT);
      }
    }
  }

  pClip->lastStepT = stepT;
}

void adkClip_Update(AdkClip *pClip, uint64_t deltaTime, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  pClip->timeSinceLastStep += deltaTime;

  while (pClip->timeSinceLastStep >= ADK_CLIP_MILLISECONDS_PER_STEP)
  {
    pClip->timeSinceLastStep -= ADK_CLIP_MILLISECONDS_PER_STEP;
    _Step(pClip, pLineRenderer);

    pClip->lastStepT = 0.f;
  }

  _InterpolateBodies(pClip);
}

void adkClip_Find(AdkClip *pClip, const adkVector3<float> &position, float radius, uint32_t mask, uint32_t foundPositionsLength, uint32_t *pFoundCount, AdkClipFindProperties *pFound)
{
  *pFoundCount = 0;

  for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
  {
    if ((mask & (1 << la)) == 0)
      continue;

    AdkClipLayer *pLayer = &pClip->layers[la];

    for (AdkClipBody *pBody = pLayer->pFirstBody; pBody != nullptr; pBody = pBody->pNext)
    {
      for (uint32_t in = 0; in < pBody->instanceCount; ++in)
      {
        AdkClipBodyInstance *pInstance = &pBody->pInstances[in];
        for (uint32_t colliderIndex = 0; colliderIndex < pBody->sphereCollidersPerInstance; ++colliderIndex)
        {
          AdkSphereCollider *pCollder = &pBody->pSphereColliders[in * pBody->sphereCollidersPerInstance + colliderIndex];

          float radii = radius + pCollder->radius;
          if (adkVector3<float>::squareDistance(pInstance->realPosition, position) <= radii * radii && *pFoundCount < foundPositionsLength)
          {
            AdkClipFindProperties *pFoundElement = &pFound[*pFoundCount];
            memset(pFoundElement, 0, sizeof(*pFoundElement));
            pFoundElement->pInstance = pInstance;
            ++(*pFoundCount);
          }
        }
      }
    }
  }
}

bool adkClip_Spherecast(AdkClip *pClip, const adkVector3<float>& start, const adkVector3<float> &velocity, float radius, uint32_t mask, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  AdkSphereSweepCollision bestCollision;
  bestCollision.resolutionT = adkFloatMax;

  for (uint32_t la = 0; la < ADK_ARRAY_SIZE(pClip->layers); ++la)
  {
    if ((mask & (1 << la)) == 0)
      continue;

    AdkClipLayer *pLayer = &pClip->layers[la];

    for (AdkClipMesh *pMesh = pLayer->pFirstMesh; pMesh != nullptr; pMesh = pMesh->pNext)
    {
      AdkSphereSweepCollision collision;
      if (adkClipMesh_SweepSphere(pMesh, start, velocity, radius, &collision, pLineRenderer) && collision.resolutionT < bestCollision.resolutionT)
        bestCollision = collision;
    }
  }

  if (bestCollision.resolutionT != adkFloatMax)
    *pCollision = bestCollision;

  return bestCollision.resolutionT != adkFloatMax;
}