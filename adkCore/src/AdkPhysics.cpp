#include "AdkPhysics.h"
#include "AdkRigidbody.inl"

static const uint32_t ADK_PHYSICS_MAX_COLLISION_PASS_COUNT = 8;
static const float ADK_PHYSICS_APPROXIMATE_POSITION_LERP_FACTOR = 10.f;
static const float ADK_PHYSICS_COLLISION_PERPENDICULAR_DISPLACEMENT_MIN_FACTOR = 0.002f;

typedef struct AdkPhysicsRigidbodyData
{
  bool collided;
  adkVector3<float> stepOriginalStart;
  adkVector3<float> stepOriginalEnd;
  AdkRigidbodyCollisionResolveFlags resolveFlags;
  adkVector3<float> positionLastUpdate;
  adkVector3<float> positionThisUpdate;
  adkVector3<float> storedVelocityDisplacement;
  AdkSphereSweepCollision collision;
} AdkPhysicsRigidbodyData;

typedef struct AdkPhysicsUnresolvedCollision
{
  AdkRigidbodyProperties *pProperties;
  AdkPhysicsRigidbodyData *pData;
} AdkPhysicsUnresolvedCollision;

typedef struct AdkRigidbodyBufferData
{
  AdkRigidbodyBuffer *pRigidbodyBuffer;
  AdkPhysicsRigidbodyData *pRigidbodyData;
  uint32_t unresolvedCollisionCount;
  AdkPhysicsUnresolvedCollision *pUnresolveCollisions;
  AdkRigidbodyCollisionResolveData *pCollisionResolveData;
} AdkRigidbodyBufferData;

typedef struct AdkPhysics
{
  const adkAllocationCallbacks *pAllocator;
  AdkTime *pTime;
  uint64_t timeStep;
  uint64_t timeLastStep;
  uint32_t rigidbodyBufferCount;
  AdkRigidbodyBufferData *pRigidbodyBufferData;
  uint32_t physicsMeshCount;
  AdkPhysicsMesh **ppPhysicsMeshes;
} AdkPhysics;

void adkPhysics_Create(const AdkPhysicsCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkPhysics **ppPhysics)
{
  AdkPhysics *pPhysics = adkAllocTypeCall(AdkPhysics, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pPhysics->pAllocator = pAllocator;
  pPhysics->timeStep = pCreateInfo->timeStep;
  pPhysics->rigidbodyBufferCount = pCreateInfo->rigidbodyBufferCount;
  pPhysics->physicsMeshCount = pCreateInfo->physicsMeshCount;

  pPhysics->pRigidbodyBufferData = adkAllocTypeCall(AdkRigidbodyBufferData, pCreateInfo->rigidbodyBufferCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pPhysics->ppPhysicsMeshes = adkAllocTypeCall(AdkPhysicsMesh*, pCreateInfo->physicsMeshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pCreateInfo->rigidbodyBufferCount; ++i)
  {
    uint32_t rigidbodyCount = adkRigidbodyBuffer_GetCount(pCreateInfo->ppRigidbodyBuffers[i]);

    pPhysics->pRigidbodyBufferData[i].pRigidbodyBuffer = pCreateInfo->ppRigidbodyBuffers[i];
    pPhysics->pRigidbodyBufferData[i].pRigidbodyData = adkAllocTypeCall(AdkPhysicsRigidbodyData, rigidbodyCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pPhysics->pRigidbodyBufferData[i].pUnresolveCollisions = adkAllocTypeCall(AdkPhysicsUnresolvedCollision, rigidbodyCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pPhysics->pRigidbodyBufferData[i].pCollisionResolveData = adkAllocTypeCall(AdkRigidbodyCollisionResolveData, rigidbodyCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

    AdkRigidbodyProperties *pRigidbodyProperties = nullptr;

    adkRigidbodyBuffer_Map(pCreateInfo->ppRigidbodyBuffers[i], &pRigidbodyProperties);

    for (uint32_t j = 0; j < rigidbodyCount; ++j)
    {
      AdkPhysicsRigidbodyData *pData = &pPhysics->pRigidbodyBufferData[i].pRigidbodyData[j];
      AdkRigidbodyProperties *pProperties = &pRigidbodyProperties[j];

      pProperties->realPosition = pProperties->position;
      pData->positionLastUpdate = pProperties->position;
      pData->positionThisUpdate = pProperties->position;


    }

    adkRigidbodyBuffer_Unmap(pCreateInfo->ppRigidbodyBuffers[i], &pRigidbodyProperties);
  }

  adkMemCpy(pPhysics->ppPhysicsMeshes, pCreateInfo->ppPhysicsMeshes, pCreateInfo->physicsMeshCount * sizeof(AdkPhysicsMesh*));

  AdkTimeCreateInfo timeCreateInfo;
  timeCreateInfo.flags = 0;

  adkTime_Create(&timeCreateInfo, pAllocator, &pPhysics->pTime);

  *ppPhysics = pPhysics;
}

void adkPhysics_Destroy(AdkPhysics **ppPhysics)
{
  AdkPhysics *pPhysics = *ppPhysics;

  for (uint32_t i = 0; i < pPhysics->rigidbodyBufferCount; ++i)
  {
    adkFreeCall(pPhysics->pRigidbodyBufferData[i].pRigidbodyData, pPhysics->pAllocator);
    adkFreeCall(pPhysics->pRigidbodyBufferData[i].pUnresolveCollisions, pPhysics->pAllocator);
    adkFreeCall(pPhysics->pRigidbodyBufferData[i].pCollisionResolveData, pPhysics->pAllocator);
  }

  adkFreeCall(pPhysics->ppPhysicsMeshes, pPhysics->pAllocator);
  adkFreeCall(pPhysics->pRigidbodyBufferData, pPhysics->pAllocator);

  adkFreeCall(*ppPhysics, pPhysics->pAllocator);
}

void adkPhysics_ResetTime(AdkPhysics *pPhysics)
{
  pPhysics->timeLastStep = adkTime_GetMilliseconds(pPhysics->pTime);
}

static bool _SpherecastAll(AdkPhysics *pPhysics, const adkVector3<float> &start, const adkVector3<float> &end, float radius, AdkSphereSweepCollision *pCollision, AdkLineRenderer * /*pLineRenderer*/ = nullptr)
{
  bool collided = false;
  AdkSphereSweepCollision collision;
  memset(&collision, 0, sizeof(collision));

  collision.resolutionT = adkFloatMax;

  //Find the closest collision that the rigidbody encounters while being swept over all meshes.
  //This is hence the first in time and only collision that occurred.
  for (uint32_t i = 0; i < pPhysics->physicsMeshCount; ++i)
  {
    AdkSphereSweepCollision pendingCollision;

    if (adkPhysicsMesh_SweepSphere(pPhysics->ppPhysicsMeshes[i], start, end, radius, &pendingCollision) && pendingCollision.resolutionT < collision.resolutionT)
    {
      collided = true;
      collision = pendingCollision;
    }
  }

  *pCollision = collision;
  return collided;
}

static void adkPhysics_Step(AdkPhysics *pPhysics, AdkLineRenderer *pLineRenderer)
{
  float deltaTime = pPhysics->timeStep / 1000.f;

  for (uint32_t i = 0; i < pPhysics->rigidbodyBufferCount; ++i)
  {
    AdkRigidbodyBufferData *pRigidbodyBufferData = &pPhysics->pRigidbodyBufferData[i];
    AdkRigidbodyProperties *pRigidbodyBufferProperties = nullptr;
    uint32_t rigidbodyCount = adkRigidbodyBuffer_GetCount(pRigidbodyBufferData->pRigidbodyBuffer);

    adkRigidbodyBuffer_Map(pRigidbodyBufferData->pRigidbodyBuffer, &pRigidbodyBufferProperties);

    for (uint32_t j = 0; j < rigidbodyCount; ++j)
    {
      AdkRigidbodyProperties *pProperties = &pRigidbodyBufferProperties[j];
      AdkPhysicsRigidbodyData *pData = &pRigidbodyBufferData->pRigidbodyData[j];

      if (!pProperties->enabled)
        continue;

      //Gravity
      pProperties->velocity.y -= deltaTime * 9.8f;

      //Accumulate some displacement from this rigidbody's velocity.
      pData->storedVelocityDisplacement += deltaTime * pRigidbodyBufferProperties[j].velocity;

      //Move the accumulated displacement to the displacement buffer that will be resolved this frame if there's not already some there.
      if (pProperties->storedDisplacement == adkVector3<float>::zero())
      {
        pProperties->storedDisplacement = pData->storedVelocityDisplacement;
        pData->storedVelocityDisplacement = adkVector3<float>::zero();
      }
    }
  }

  for (uint32_t i = 0; i < pPhysics->rigidbodyBufferCount; ++i)
  {
    AdkRigidbodyBufferData *pRigidbodyBufferData = &pPhysics->pRigidbodyBufferData[i];
    AdkRigidbodyBuffer *pRigidbodyBuffer = pRigidbodyBufferData->pRigidbodyBuffer;
    AdkRigidbodyProperties *pRigidbodyBufferProperties = nullptr;
    uint32_t rigidbodyCount = adkRigidbodyBuffer_GetCount(pRigidbodyBuffer);

    adkRigidbodyBuffer_Map(pRigidbodyBuffer, &pRigidbodyBufferProperties);

    for (uint32_t pass = 0; pass < ADK_PHYSICS_MAX_COLLISION_PASS_COUNT; ++pass)
    {
      bool doNextPass = false;
      pRigidbodyBufferData->unresolvedCollisionCount = 0;

      for (uint32_t j = 0; j < rigidbodyCount; ++j)
      {
        AdkRigidbodyProperties *pProperties = &pRigidbodyBufferProperties[j];
        AdkPhysicsRigidbodyData *pData = &pRigidbodyBufferData->pRigidbodyData[j];

        if (!pProperties->enabled)
          continue;

        pData->resolveFlags = ADK_RIGIDBODY_COLLISION_RESOLVE_NONE_BIT;

        pData->stepOriginalStart = pProperties->realPosition;
        pData->stepOriginalEnd = pProperties->realPosition + pProperties->storedDisplacement;

        if (pProperties->storedDisplacement == adkVector3<float>::zero())
          continue;

        pProperties->storedDisplacement = adkVector3<float>::zero();

        AdkSphereSweepCollision collision;

        //Resolve the collision by moving the sphere only to where it collided with the mesh.
        //Extra unused displacement is stored and will be used next pass.
        pData->collided = _SpherecastAll(pPhysics, pData->stepOriginalStart, pData->stepOriginalEnd, pProperties->radius, &collision, pLineRenderer);

        if (pData->collided)
        {
          AdkPhysicsUnresolvedCollision *pUnresolvedCollision = &pRigidbodyBufferData->pUnresolveCollisions[pRigidbodyBufferData->unresolvedCollisionCount];
          AdkRigidbodyCollisionResolveData *pCollisionResolveData = &pRigidbodyBufferData->pCollisionResolveData[pRigidbodyBufferData->unresolvedCollisionCount];

          pData->resolveFlags = ADK_RIGIDBODY_COLLISION_RESOLVE_ALL_BIT;
          pData->collision = collision;

          pUnresolvedCollision->pData = pData;
          pUnresolvedCollision->pProperties = pProperties;

          pCollisionResolveData->pCollision = &pData->collision;
          pCollisionResolveData->pRigidbodyProperties = pProperties;
          pCollisionResolveData->resolvedFlags = ADK_RIGIDBODY_COLLISION_RESOLVE_NONE_BIT;

          ++pRigidbodyBufferData->unresolvedCollisionCount;
        }
      }

      if (pRigidbodyBuffer->pCollisionResolveFunction != nullptr && pRigidbodyBufferData->unresolvedCollisionCount != 0)
      {
        pRigidbodyBuffer->pCollisionResolveFunction(pRigidbodyBuffer->pCollisionResolveUserData, pRigidbodyBufferData->unresolvedCollisionCount, pRigidbodyBufferData->pCollisionResolveData);

        for (uint32_t j = 0; j < pRigidbodyBufferData->unresolvedCollisionCount; ++j)
        {
          AdkRigidbodyCollisionResolveFlags unresolvedFlags;

          unresolvedFlags = ~pRigidbodyBufferData->pCollisionResolveData[j].resolvedFlags;
          unresolvedFlags = unresolvedFlags & ADK_RIGIDBODY_COLLISION_RESOLVE_ALL_BIT;

          pRigidbodyBufferData->pUnresolveCollisions[j].pData->resolveFlags = unresolvedFlags;
        }
      }

      for (uint32_t j = 0; j < rigidbodyCount; ++j)
      {
        AdkRigidbodyProperties *pProperties = &pRigidbodyBufferProperties[j];
        AdkPhysicsRigidbodyData *pData = &pRigidbodyBufferData->pRigidbodyData[j];
        AdkSphereSweepCollision *pCollision = &pData->collision;

        if (!pProperties->enabled)
          continue;

        if (pData->collided)
        {
          adkVector3<float> collisionNormal = (pCollision->resolution - pCollision->contact).normalized();
          adkVector3<float> velocity = pProperties->velocity;

          float slide = pProperties->slide;
          float bounce = pProperties->bounce;

          float originalDistance = adkVector3<float>::distance(pData->stepOriginalStart, pData->stepOriginalEnd);
          float travelledDistance = adkVector3<float>::distance(pData->stepOriginalStart, pProperties->realPosition);
          float unusedDistance = originalDistance - travelledDistance;
          float unusedFraction = unusedDistance / originalDistance;
          float dot = adkVector3<float>::dot(-velocity, collisionNormal);

          adkVector3<float> perpendicular = dot * collisionNormal;
          adkVector3<float> parallel = perpendicular + velocity;

          perpendicular *= bounce;
          parallel *= slide;

          adkVector3<float> perpendicularDisplacement = deltaTime * unusedFraction * perpendicular;
          adkVector3<float> parallelDisplacement = deltaTime * unusedFraction * parallel;

          //The rigidbody needs to be displaced at least a little away from the surface so that sliding works correctly.
          //A minimum amount of perpendicular displacement is added to the rigidbody if it hasn't enough from this collision.
          adkVector3<float> perpendicularOffset = adkVector3<float>::zero();

          if (perpendicularDisplacement.magnitude() < ADK_PHYSICS_COLLISION_PERPENDICULAR_DISPLACEMENT_MIN_FACTOR)
            perpendicularOffset = (ADK_PHYSICS_COLLISION_PERPENDICULAR_DISPLACEMENT_MIN_FACTOR - perpendicularDisplacement.magnitude()) * collisionNormal;

          if ((pData->resolveFlags & ADK_RIGIDBODY_COLLISION_RESOLVE_POSITION_BIT) != 0)
            pProperties->realPosition = pCollision->resolution;

          if ((pData->resolveFlags & ADK_RIGIDBODY_COLLISION_RESOLVE_VELOCITY_BIT) != 0)
            pProperties->velocity = parallel + perpendicular;

          //This is unresolved displacement that should've been added to the rigidbody had no collision occurred.
          //This extra displacement will be resolved next frame.
          if ((pData->resolveFlags & ADK_RIGIDBODY_COLLISION_RESOLVE_PERPENDICULAR_OFFSET_BIT) != 0)
            pProperties->storedDisplacement += perpendicularOffset;

          if ((pData->resolveFlags & ADK_RIGIDBODY_COLLISION_RESOLVE_REMAINING_DISPLACEMENT_BIT) != 0)
            pProperties->storedDisplacement += parallelDisplacement + perpendicularDisplacement;

          pData->collided = false;

          doNextPass = true;
        }
        else
        {
          pProperties->realPosition = pData->stepOriginalEnd;
          pProperties->storedDisplacement = adkVector3<float>::zero();
        }
      }

      if (!doNextPass)
        break;
    }
  }

  for (uint32_t i = 0; i < pPhysics->rigidbodyBufferCount; ++i)
  {
    AdkRigidbodyBuffer *pRigidbodyBuffer = pPhysics->pRigidbodyBufferData[i].pRigidbodyBuffer;

    AdkRigidbodyProperties *pRigidbodyBufferProperties = nullptr;

    uint32_t rigidbodyCount = adkRigidbodyBuffer_GetCount(pRigidbodyBuffer);

    adkRigidbodyBuffer_Map(pRigidbodyBuffer, &pRigidbodyBufferProperties);

    for (uint32_t j = 0; j < rigidbodyCount; ++j)
      pRigidbodyBufferProperties[j].velocity /= 1.f + deltaTime * pRigidbodyBufferProperties[j].drag / pRigidbodyBufferProperties[j].mass;
  }
}

void adkPhysics_Update(AdkPhysics *pPhysics, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  uint64_t timeThisUpdate = adkTime_GetMilliseconds(pPhysics->pTime);
  bool stepped = false;

  //Physics runs on a fixed framerate. Step as many times as needed for physics to catch up to real time.
  while (pPhysics->timeLastStep + pPhysics->timeStep < timeThisUpdate)
  {
    pPhysics->timeLastStep += pPhysics->timeStep;
    stepped = true;

    adkPhysics_Step(pPhysics, pLineRenderer);
  }

  float stepT = (float)(timeThisUpdate - pPhysics->timeLastStep) / pPhysics->timeStep;

  //Each rigidbody is smoothly interpolated between steps if the game's framerate is higher than the physics framerate.
  for (uint32_t i = 0; i < pPhysics->rigidbodyBufferCount; ++i)
  {
    uint32_t rigidbodyCount = adkRigidbodyBuffer_GetCount(pPhysics->pRigidbodyBufferData[i].pRigidbodyBuffer);

    AdkRigidbodyProperties *pRigidbodyProperties = nullptr;

    adkRigidbodyBuffer_Map(pPhysics->pRigidbodyBufferData[i].pRigidbodyBuffer, &pRigidbodyProperties);

    for (uint32_t j = 0; j < rigidbodyCount; ++j)
    {
      AdkPhysicsRigidbodyData *pData = &pPhysics->pRigidbodyBufferData[i].pRigidbodyData[j];
      AdkRigidbodyProperties *pProperties = &pRigidbodyProperties[j];

      if (stepped)
      {
        pData->positionLastUpdate = pData->positionThisUpdate;
        pData->positionThisUpdate = pProperties->realPosition;
      }

      pProperties->position = adkLerp(pData->positionLastUpdate, pData->positionThisUpdate, stepT);
    }

    adkRigidbodyBuffer_Unmap(pPhysics->pRigidbodyBufferData[i].pRigidbodyBuffer, &pRigidbodyProperties);
  }
}

bool adkPhysics_Spherecast(AdkPhysics *pPhysics, const adkVector3<float> &start, const adkVector3<float> &end, float radius, AdkSphereSweepCollision *pCollision)
{
  return _SpherecastAll(pPhysics, start, end, radius, pCollision);
}