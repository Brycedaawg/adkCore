#include "adkMesh.h"
#include "adkMemory.h"
#include "adkBuffer.h"
#include "adkRenderPipeline.h"
#include "adkWindow.h"
#include "adkMaterial.inl"
#include <inttypes.h>
#include "adkString.h"
#include "AdkBinaryReader.h"
#include "AdkBinaryWriter.h"
#include "adkAssert.h"

static const uint32_t VERSION = 1000;

typedef struct AdkMesh
{
  AdkInstance *pInstance;
  uint32_t positionCount;
  adkVector3<float> *pPositions;
  uint32_t normalCount;
  adkVector3<float> *pNormals;
  uint32_t uvCount;
  adkVector2<float> *pUvs;
  uint32_t skinWeightCount;
  uint32_t bonesPerVertex;
  uint32_t boneCount;
  adkMatrix4x4<float> *pBindPoses;
  float *pSkinWeights;
  uint32_t *pSkinBones;
  uint32_t indexCount;
  uint32_t *pIndices;
  adkAllocationCallbacks *pAllocator;
  AdkBuffer *pPositionBuffer;
  AdkBuffer *pNormalBuffer;
  AdkBuffer *pUvBuffer;
  AdkBuffer *pSkinWeightBuffer;
  AdkBuffer *pSkinBoneBuffer;
  AdkBuffer *pBonesPerVertexBuffer;
  AdkBuffer *pIndexBuffer;
  AdkMeshShaderInfo shaderInfo;
  AdkQueue *pGraphicsQueue;
  AdkQueue *pTransferQueue;
} AdkMesh;

typedef struct AdkMeshRenderer
{
  const AdkMesh *pMesh;
  AdkRenderPipeline *pRenderPipeline;
  AdkBuffer *pModelBuffer;
  AdkBuffer *pBoneBuffer;
  AdkBuffer *pViewBuffer;
  AdkBuffer *pProjectionBuffer;
  uint32_t modelCount;
  AdkBuffer *pLightDirectionBuffer;
  AdkBuffer *pLightColorBuffer;
} AdkMeshRenderer;

static inline adkVector4<float> _Color32ToColor4f(uint32_t color)
{
  adkVector4<float> c;
  c.x = (float)((color >> 24) & 0x000000FF) / 0x000000FF;
  c.y = (float)((color >> 16) & 0x000000FF) / 0x000000FF;
  c.z = (float)((color >> 8) & 0x000000FF) / 0x000000FF;
  c.w = (float)((color >> 0) & 0x000000FF) / 0x000000FF;
  return c;
}

void adkMesh_Create(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkMesh **ppMesh)
{
  AdkMesh *pMesh = adkAllocTypeCall(AdkMesh, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pMesh->pInstance = pInstance;
  pMesh->pGraphicsQueue = pGraphicsQueue;
  pMesh->pTransferQueue = pTransferQueue;

  if (pAllocator == nullptr)
  {
    pMesh->pAllocator = nullptr;
  }
  else
  {
    pMesh->pAllocator = adkAllocTypeCall(adkAllocationCallbacks, 1, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    (*pMesh->pAllocator) = (*pAllocator);
  }

  (*ppMesh) = pMesh;
}

void adkMesh_Destroy(AdkMesh **ppMesh)
{
  if (*ppMesh == nullptr)
    return;

  AdkMesh *pMesh = (*ppMesh);

  adkBuffer_Destroy(pMesh->pInstance, &pMesh->pPositionBuffer);
  adkBuffer_Destroy(pMesh->pInstance, &pMesh->pNormalBuffer);
  adkBuffer_Destroy(pMesh->pInstance, &pMesh->pUvBuffer);

  if (pMesh->pSkinWeightBuffer != nullptr)
  {
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pSkinWeightBuffer);
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pSkinBoneBuffer);
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pBonesPerVertexBuffer);
  }

  adkBuffer_Destroy(pMesh->pInstance, &pMesh->pIndexBuffer);

  adkFreeCall(pMesh->pPositions, pMesh->pAllocator);
  adkFreeCall(pMesh->pNormals, pMesh->pAllocator);
  adkFreeCall(pMesh->pBindPoses, pMesh->pAllocator);
  adkFreeCall(pMesh->pSkinWeights, pMesh->pAllocator);
  adkFreeCall(pMesh->pSkinBones, pMesh->pAllocator);
  adkFreeCall(pMesh->pIndices, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalAttributes, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalBuffers, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalSamplers, pMesh->pAllocator);
  adkFreeCall(pMesh, pMesh->pAllocator);

  (*ppMesh) = nullptr;
}

void adkMesh_Copy(AdkMesh *pSource, AdkMesh **ppCopy)
{
  AdkMesh *pCopy;
  adkMesh_Create(pSource->pInstance, pSource->pGraphicsQueue, pSource->pTransferQueue, pSource->pAllocator, &pCopy);
  adkMesh_SetPositions(pCopy, pSource->positionCount, pSource->pPositions);
  adkMesh_SetNormals(pCopy, pSource->normalCount, pSource->pNormals);
  adkMesh_SetUvs(pCopy, pSource->uvCount, pSource->pUvs);
  adkMesh_SetSkin(pCopy, pSource->boneCount, pSource->pBindPoses, pSource->bonesPerVertex, pSource->skinWeightCount, pSource->pSkinWeights, pSource->pSkinBones);
  adkMesh_SetIndices(pCopy, pSource->indexCount, pSource->pIndices);
  adkMesh_SetShader(pCopy, &pSource->shaderInfo);
  (*ppCopy) = pCopy;
}

void adkMesh_SetPositions(AdkMesh *pMesh, uint32_t count, adkVector3<float> *pPositions)
{
  adkFreeCall(pMesh->pPositions, pMesh->pAllocator);
  pMesh->positionCount = count;
  pMesh->pPositions = adkAllocTypeCall(adkVector3<float>, count, adkAF_None, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pMesh->pPositions, pPositions, count * sizeof(adkVector3<float>));

  if (pMesh->pPositionBuffer != nullptr)
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pPositionBuffer);

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = count * sizeof(adkVector3<float>);
  bufferCopy.pData = pPositions;

  AdkBufferCreateInfo bufferCreateInfo;
  bufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  bufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_3;
  bufferCreateInfo.elementCount = count;
  bufferCreateInfo.copyCount = 1;
  bufferCreateInfo.pCopies = &bufferCopy;
  bufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &bufferCreateInfo, &pMesh->pPositionBuffer);
}

void adkMesh_SetNormals(AdkMesh *pMesh, uint32_t count, adkVector3<float> *pNormals)
{
  adkFreeCall(pMesh->pNormals, pMesh->pAllocator);
  pMesh->normalCount = count;
  pMesh->pNormals = adkAllocTypeCall(adkVector3<float>, count, adkAF_None, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pMesh->pNormals, pNormals, count * sizeof(adkVector3<float>));

  if (pMesh->pNormalBuffer != nullptr)
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pNormalBuffer);

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = count * sizeof(adkVector3<float>);
  bufferCopy.pData = pNormals;

  AdkBufferCreateInfo bufferCreateInfo;
  bufferCreateInfo.flags = 0;
  bufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_3;
  bufferCreateInfo.elementCount = count;
  bufferCreateInfo.copyCount = 1;
  bufferCreateInfo.pCopies = &bufferCopy;
  bufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &bufferCreateInfo, &pMesh->pNormalBuffer);
}

void adkMesh_SetUvs(AdkMesh *pMesh, uint32_t count, adkVector2<float> *pUvs)
{
  adkFreeCall(pMesh->pUvs, pMesh->pAllocator);
  pMesh->uvCount = count;
  pMesh->pUvs = adkAllocTypeCall(adkVector2<float>, count, adkAF_None, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pMesh->pUvs, pUvs, count * sizeof(adkVector2<float>));

  if (pMesh->pUvBuffer != nullptr)
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pUvBuffer);

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = count * sizeof(adkVector2<float>);
  bufferCopy.pData = pUvs;

  AdkBufferCreateInfo bufferCreateInfo;
  bufferCreateInfo.flags = 0;
  bufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_2;
  bufferCreateInfo.elementCount = count;
  bufferCreateInfo.copyCount = 1;
  bufferCreateInfo.pCopies = &bufferCopy;
  bufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &bufferCreateInfo, &pMesh->pUvBuffer);
}

void adkMesh_SetSkin(AdkMesh *pMesh, uint32_t boneCount, adkMatrix4x4<float> *pBindPoses, uint32_t bonesPerSkinVertex, uint32_t skinVertexCount, float *pSkinVertexWeights, uint32_t *pSkinVertexBones)
{
  adkFreeCall(pMesh->pBindPoses, pMesh->pAllocator);
  adkFreeCall(pMesh->pSkinWeights, pMesh->pAllocator);
  adkFreeCall(pMesh->pSkinBones, pMesh->pAllocator);

  if (pMesh->pSkinWeightBuffer != nullptr)
  {
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pSkinWeightBuffer);
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pSkinBoneBuffer);
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pBonesPerVertexBuffer);
  }

  pMesh->boneCount = 0;

  if (boneCount != 0 && bonesPerSkinVertex != 0 && skinVertexCount != 0)
  {
    uint32_t totalSkinVertexCount = skinVertexCount * bonesPerSkinVertex;

    pMesh->skinWeightCount = skinVertexCount;
    pMesh->bonesPerVertex = bonesPerSkinVertex;
    pMesh->pBindPoses = adkMemDupTypeCall(pBindPoses, boneCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pMesh->pSkinWeights = adkMemDupTypeCall(pSkinVertexWeights, totalSkinVertexCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pMesh->pSkinBones = adkMemDupTypeCall(pSkinVertexBones, totalSkinVertexCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pMesh->boneCount = boneCount;

    AdkBufferCopy skinWeightBufferCopy;
    skinWeightBufferCopy.offset = 0;
    skinWeightBufferCopy.size = totalSkinVertexCount * sizeof(float);
    skinWeightBufferCopy.pData = pMesh->pSkinWeights;

    AdkBufferCreateInfo skinWeightBufferCreateInfo;
    skinWeightBufferCreateInfo.flags = 0;
    skinWeightBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32;
    skinWeightBufferCreateInfo.elementCount = totalSkinVertexCount;
    skinWeightBufferCreateInfo.copyCount = 1;
    skinWeightBufferCreateInfo.pCopies = &skinWeightBufferCopy;
    skinWeightBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
    adkBuffer_Create(pMesh->pInstance, &skinWeightBufferCreateInfo, &pMesh->pSkinWeightBuffer);

    AdkBufferCopy skinBoneBufferCopy;
    skinBoneBufferCopy.offset = 0;
    skinBoneBufferCopy.size = totalSkinVertexCount * sizeof(uint32_t);
    skinBoneBufferCopy.pData = pMesh->pSkinBones;

    AdkBufferCreateInfo skinBoneBufferCreateInfo;
    skinBoneBufferCreateInfo.flags = 0;
    skinBoneBufferCreateInfo.type = ADK_BUFFER_TYPE_UINT32;
    skinBoneBufferCreateInfo.elementCount = totalSkinVertexCount;
    skinBoneBufferCreateInfo.copyCount = 1;
    skinBoneBufferCreateInfo.pCopies = &skinBoneBufferCopy;
    skinBoneBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
    adkBuffer_Create(pMesh->pInstance, &skinBoneBufferCreateInfo, &pMesh->pSkinBoneBuffer);

    AdkBufferCopy bonesPerVertexBufferCopy;
    bonesPerVertexBufferCopy.offset = 0;
    bonesPerVertexBufferCopy.size = sizeof(uint32_t);
    bonesPerVertexBufferCopy.pData = &pMesh->bonesPerVertex;

    AdkBufferCreateInfo bonesPerVertexCreateInfo;
    bonesPerVertexCreateInfo.flags = 0;
    bonesPerVertexCreateInfo.type = ADK_BUFFER_TYPE_UINT32;
    bonesPerVertexCreateInfo.elementCount = 1;
    bonesPerVertexCreateInfo.copyCount = 1;
    bonesPerVertexCreateInfo.pCopies = &bonesPerVertexBufferCopy;
    bonesPerVertexCreateInfo.pTransferQueue = pMesh->pTransferQueue;
    adkBuffer_Create(pMesh->pInstance, &bonesPerVertexCreateInfo, &pMesh->pBonesPerVertexBuffer);
  }
}

void adkMesh_SetIndices(AdkMesh *pMesh, uint32_t count, uint32_t *pIndices)
{
  adkFreeCall(pMesh->pIndices, pMesh->pAllocator);
  pMesh->indexCount = count;
  pMesh->pIndices = adkAllocTypeCall(uint32_t, count, adkAF_None, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pMesh->pIndices, pIndices, count * sizeof(uint32_t));

  if (pMesh->pIndexBuffer != nullptr)
    adkBuffer_Destroy(pMesh->pInstance, &pMesh->pIndexBuffer);

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = count * sizeof(uint32_t);
  bufferCopy.pData = pIndices;

  AdkBufferCreateInfo bufferCreateInfo;
  bufferCreateInfo.flags = 0;
  bufferCreateInfo.type = ADK_BUFFER_TYPE_UINT32_INDEX;
  bufferCreateInfo.elementCount = count;
  bufferCreateInfo.copyCount = 1;
  bufferCreateInfo.pCopies = &bufferCopy;
  bufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &bufferCreateInfo, &pMesh->pIndexBuffer);
}

void adkMesh_SetShader(AdkMesh *pMesh, const AdkMeshShaderInfo *pShaderInfo)
{
  //Free old arrays.
  adkFreeCall(pMesh->shaderInfo.pAttributes, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pUniforms, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalAttributes, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalBuffers, pMesh->pAllocator);
  adkFreeCall(pMesh->shaderInfo.pAdditionalSamplers, pMesh->pAllocator);

  //Copy all value types.
  pMesh->shaderInfo = *pShaderInfo;

  //Deep copy arrays.
  pMesh->shaderInfo.pAttributes = adkMemDupTypeCall(pShaderInfo->pAttributes, pShaderInfo->attributeCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMesh->shaderInfo.pUniforms = adkMemDupTypeCall(pShaderInfo->pUniforms, pShaderInfo->uniformCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMesh->shaderInfo.pAdditionalAttributes = adkMemDupTypeCall(pShaderInfo->pAdditionalAttributes, pShaderInfo->additionalAttributeCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMesh->shaderInfo.pAdditionalBuffers = adkMemDupTypeCall(pShaderInfo->pAdditionalBuffers, pMesh->shaderInfo.additionalBufferCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMesh->shaderInfo.pAdditionalSamplers = adkMemDupTypeCall(pShaderInfo->pAdditionalSamplers, pMesh->shaderInfo.additionalSamplerCount, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
}

uint32_t adkMesh_GetPositionCount(AdkMesh *pMesh)
{
  return pMesh->positionCount;
}

bool adkMesh_Serialize(AdkMesh *pMesh, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, VERSION);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->positionCount);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->normalCount);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->uvCount);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->skinWeightCount);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->bonesPerVertex);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->boneCount);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->indexCount);

  uint32_t skinVertexBoneCount = pMesh->skinWeightCount * pMesh->bonesPerVertex;
  
  adkBinaryWriter_Write(pBinaryWriter, pMesh->positionCount * sizeof(*pMesh->pPositions), pMesh->pPositions);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->normalCount * sizeof(*pMesh->pNormals), pMesh->pNormals);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->uvCount * sizeof(*pMesh->pUvs), pMesh->pUvs);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->boneCount * sizeof(*pMesh->pBindPoses), pMesh->pBindPoses);
  adkBinaryWriter_Write(pBinaryWriter, skinVertexBoneCount * sizeof(*pMesh->pSkinWeights), pMesh->pSkinWeights);
  adkBinaryWriter_Write(pBinaryWriter, skinVertexBoneCount * sizeof(*pMesh->pSkinBones), pMesh->pSkinBones);
  adkBinaryWriter_Write(pBinaryWriter, pMesh->indexCount * sizeof(*pMesh->pIndices), pMesh->pIndices);

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkMesh_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkMesh **ppMesh)
{
  bool success = false;
  AdkMesh *pMesh = nullptr;

  adkMesh_Create(pInstance, pGraphicsQueue, pTransferQueue, pAllocator, &pMesh);

  uint32_t version = 0;
  uint32_t positionCount = 0;
  uint32_t normalCount = 0;
  uint32_t uvCount = 0;
  uint32_t skinWeightCount = 0;
  uint32_t bonesPerVertex = 0;
  uint32_t boneCount = 0;
  uint32_t indexCount = 0;

  adkBinaryReader_Read(pBinaryReader, &version);
  adkBinaryReader_Read(pBinaryReader, &positionCount);
  adkBinaryReader_Read(pBinaryReader, &normalCount);
  adkBinaryReader_Read(pBinaryReader, &uvCount);
  adkBinaryReader_Read(pBinaryReader, &skinWeightCount);
  adkBinaryReader_Read(pBinaryReader, &bonesPerVertex);
  adkBinaryReader_Read(pBinaryReader, &boneCount);
  adkBinaryReader_Read(pBinaryReader, &indexCount);

  adkVector3<float> *pPositions = nullptr;
  adkVector3<float> *pNormals = nullptr;
  adkVector2<float> *pUvs = nullptr;
  adkMatrix4x4<float> *pBindPoses = nullptr;
  float *pSkinWeights = nullptr;
  uint32_t *pSkinBones = nullptr;
  uint32_t *pIndices = nullptr;

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  uint32_t skinVertexBoneCount = skinWeightCount * bonesPerVertex;

  pPositions = adkAllocTypeCall(adkVector3<float>, positionCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pNormals = adkAllocTypeCall(adkVector3<float>, normalCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pUvs = adkAllocTypeCall(adkVector2<float>, uvCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pBindPoses = adkAllocTypeCall(adkMatrix4x4<float>, boneCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pSkinWeights = adkAllocTypeCall(float, skinVertexBoneCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pSkinBones = adkAllocTypeCall(uint32_t, skinVertexBoneCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pIndices = adkAllocTypeCall(uint32_t, indexCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  adkBinaryReader_Read(pBinaryReader, positionCount * sizeof(*pPositions), pPositions);
  adkBinaryReader_Read(pBinaryReader, normalCount * sizeof(*pNormals), pNormals);
  adkBinaryReader_Read(pBinaryReader, uvCount * sizeof(*pUvs), pUvs);
  adkBinaryReader_Read(pBinaryReader, boneCount * sizeof(*pBindPoses), pBindPoses);
  adkBinaryReader_Read(pBinaryReader, skinVertexBoneCount * sizeof(*pSkinWeights), pSkinWeights);
  adkBinaryReader_Read(pBinaryReader, skinVertexBoneCount * sizeof(*pSkinBones), pSkinBones);
  adkBinaryReader_Read(pBinaryReader, indexCount * sizeof(*pIndices), pIndices);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  adkMesh_SetPositions(pMesh, positionCount, pPositions);
  adkMesh_SetNormals(pMesh, normalCount, pNormals);
  adkMesh_SetUvs(pMesh, uvCount, pUvs);
  adkMesh_SetSkin(pMesh, boneCount, pBindPoses, bonesPerVertex, skinWeightCount, pSkinWeights, pSkinBones);
  adkMesh_SetIndices(pMesh, indexCount, pIndices);

  *ppMesh = pMesh;
  success = true;

epilogue:
  adkFreeCall(pSkinBones, pAllocator);
  adkFreeCall(pSkinWeights, pAllocator);
  adkFreeCall(pBindPoses, pAllocator);
  adkFreeCall(pUvs, pAllocator);
  adkFreeCall(pNormals, pAllocator);
  adkFreeCall(pPositions, pAllocator);
  adkFreeCall(pIndices, pAllocator);

  if (!success)
    adkMesh_Destroy(&pMesh);

  return success;
}

void adkMeshRenderer_Create(AdkMesh *pMesh, adkWindow *pWindow, uint32_t modelCount, AdkMeshRenderer **ppMeshRenderer)
{
  AdkMeshRenderer *pMeshRenderer = adkAllocTypeCall(AdkMeshRenderer, 1, adkAF_Zero, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMeshRenderer->pMesh = pMesh;

  AdkBufferCreateInfo modelBufferCreateInfo;
  modelBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  modelBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4X4;
  modelBufferCreateInfo.elementCount = modelCount;
  modelBufferCreateInfo.copyCount = 0;
  modelBufferCreateInfo.pCopies = nullptr;
  modelBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &modelBufferCreateInfo, &pMeshRenderer->pModelBuffer);

  modelBufferCreateInfo.elementCount = 1;
  adkBuffer_Create(pMesh->pInstance, &modelBufferCreateInfo, &pMeshRenderer->pViewBuffer);
  adkBuffer_Create(pMesh->pInstance, &modelBufferCreateInfo, &pMeshRenderer->pProjectionBuffer);

  AdkBufferCreateInfo lightColorBufferCreateInfo;
  lightColorBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  lightColorBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4;
  lightColorBufferCreateInfo.elementCount = 1;
  lightColorBufferCreateInfo.copyCount = 0;
  lightColorBufferCreateInfo.pCopies = nullptr;
  lightColorBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &lightColorBufferCreateInfo, &pMeshRenderer->pLightColorBuffer);

  AdkBufferCreateInfo lightDirectionBufferCreateInfo;
  lightDirectionBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  lightDirectionBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_3;
  lightDirectionBufferCreateInfo.elementCount = 1;
  lightDirectionBufferCreateInfo.copyCount = 0;
  lightDirectionBufferCreateInfo.pCopies = nullptr;
  lightDirectionBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
  adkBuffer_Create(pMesh->pInstance, &lightDirectionBufferCreateInfo, &pMeshRenderer->pLightDirectionBuffer);

  //Initialize model matrices.
  adkMatrix4x4<float> *pMappedModels = nullptr;
  adkBuffer_Map(pMesh->pInstance, pMeshRenderer->pModelBuffer, (void**)&pMappedModels);
  for (uint32_t i = 0; i < modelCount; ++i)
    pMappedModels[i] = adkMatrix4x4<float>::identity();
  adkBuffer_Unmap(pMesh->pInstance, pMeshRenderer->pModelBuffer, (void**)&pMappedModels);

  //Initialize view matrix.
  adkMatrix4x4<float> *pMappedView = nullptr;
  adkBuffer_Map(pMesh->pInstance, pMeshRenderer->pViewBuffer, (void**)&pMappedView);
  *pMappedView = adkMatrix4x4<float>::identity();
  adkBuffer_Unmap(pMesh->pInstance, pMeshRenderer->pViewBuffer, (void**)&pMappedView);

  //Initialize projection matrix.
  adkMatrix4x4<float> *pMappedProjection = nullptr;
  adkBuffer_Map(pMesh->pInstance, pMeshRenderer->pProjectionBuffer, (void**)&pMappedProjection);
  *pMappedProjection = adkMatrix4x4<float>::identity();
  adkBuffer_Unmap(pMesh->pInstance, pMeshRenderer->pProjectionBuffer, (void**)&pMappedProjection);

  //Initialize light color.
  adkVector4<float> *pMappedLightColor = nullptr;
  adkBuffer_Map(pMesh->pInstance, pMeshRenderer->pLightColorBuffer, (void**)&pMappedLightColor);
  *pMappedLightColor = adkVector4<float>::create(1.f, 1.f, 1.f, 1.f);
  adkBuffer_Unmap(pMesh->pInstance, pMeshRenderer->pLightColorBuffer, (void**)&pMappedLightColor);

  //Initialize light direction.
  adkVector3<float> *pMappedLightDirection = nullptr;
  adkBuffer_Map(pMesh->pInstance, pMeshRenderer->pLightDirectionBuffer, (void**)&pMappedLightDirection);
  *pMappedLightDirection = adkVector3<float>::create(0.25f, -1.f, 0.125f).normalized();
  adkBuffer_Unmap(pMesh->pInstance, pMeshRenderer->pLightDirectionBuffer, (void**)&pMappedLightDirection);

  if (pMesh->pSkinWeightBuffer != nullptr)
  {
    AdkBufferCreateInfo boneBufferCreateInfo;
    boneBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
    boneBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4X4;
    boneBufferCreateInfo.elementCount = modelCount * pMesh->boneCount;
    boneBufferCreateInfo.copyCount = 0;
    boneBufferCreateInfo.pCopies = nullptr;
    boneBufferCreateInfo.pTransferQueue = pMesh->pTransferQueue;
    adkBuffer_Create(pMesh->pInstance, &boneBufferCreateInfo, &pMeshRenderer->pBoneBuffer);
  }

  uint32_t maxAttributeCount = pMesh->shaderInfo.attributeCount + pMesh->shaderInfo.additionalAttributeCount;
  AdkAttributeInfo *pAttributeInfos = adkAllocTypeCall(AdkAttributeInfo, maxAttributeCount, adkAF_Zero, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  uint32_t attributeCount = 0;

  //Assign pre-defined attributes.
  for (uint32_t i = 0; i < pMesh->shaderInfo.attributeCount; ++i)
  {
    switch (pMesh->shaderInfo.pAttributes[i].type)
    {
    case ADK_MESH_ATTRIBUTE_TYPE_POSITION:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
      pAttributeInfos[attributeCount].elementStride = 1;
      pAttributeInfos[attributeCount].pBuffer = pMesh->pPositionBuffer;
      break;

    case ADK_MESH_ATTRIBUTE_TYPE_NORMAL:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
      pAttributeInfos[attributeCount].elementStride = 1;
      pAttributeInfos[attributeCount].pBuffer = pMesh->pNormalBuffer;
      break;

    case ADK_MESH_ATTRIBUTE_TYPE_UV:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
      pAttributeInfos[attributeCount].elementStride = 1;
      pAttributeInfos[attributeCount].pBuffer = pMesh->pUvBuffer;
      break;

    case ADK_MESH_ATTRIBUTE_TYPE_SKIN_WEIGHT:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
      pAttributeInfos[attributeCount].elementStride = pMesh->bonesPerVertex;
      pAttributeInfos[attributeCount].pBuffer = pMesh->pSkinWeightBuffer;
      break;

    case ADK_MESH_ATTRIBUTE_TYPE_SKIN_BONE:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
      pAttributeInfos[attributeCount].elementStride = pMesh->bonesPerVertex;
      pAttributeInfos[attributeCount].pBuffer = pMesh->pSkinBoneBuffer;
      break;

    case ADK_MESH_ATTRIBUTE_TYPE_MODEL:
      pAttributeInfos[attributeCount].location = pMesh->shaderInfo.pAttributes[i].location;
      pAttributeInfos[attributeCount].inputRate = ADK_ATTRIBUTE_INPUT_RATE_INSTANCE;
      pAttributeInfos[attributeCount].elementStride = 1;
      pAttributeInfos[attributeCount].pBuffer = pMeshRenderer->pModelBuffer;
      break;
    }

    //Keep the attribute if the corresponding buffer has been set. discard the attribute otherwise.
    if (pAttributeInfos[attributeCount].pBuffer != nullptr)
      ++attributeCount;
  }

  //Assign additional attributes.
  for (uint32_t i = 0; i < pMesh->shaderInfo.additionalAttributeCount; ++i)
  {
    pAttributeInfos[attributeCount] = pMesh->shaderInfo.pAdditionalAttributes[i];
    ++attributeCount;
  }

  uint32_t maxBufferUniformCount = pMesh->shaderInfo.uniformCount + pMesh->shaderInfo.additionalBufferCount;
  AdkBufferUniformInfo *pBufferUniformInfos = adkAllocTypeCall(AdkBufferUniformInfo, maxBufferUniformCount, adkAF_Zero, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  uint32_t bufferUniformCount = 0;

  //Assign pre-defined uniforms.
  for (uint32_t i = 0; i < pMesh->shaderInfo.uniformCount; ++i)
  {
    switch (pMesh->shaderInfo.pUniforms[i].type)
    {
    case ADK_MESH_UNIFORM_TYPE_VIEW:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMeshRenderer->pViewBuffer;
      break;

    case ADK_MESH_UNIFORM_TYPE_PROJECTION:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMeshRenderer->pProjectionBuffer;
      break;

    case ADK_MESH_UNIFORM_TYPE_BONES:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMeshRenderer->pBoneBuffer;
      break;

    case ADK_MESH_UNIFORM_TYPE_BONES_PER_VERTEX:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMesh->pBonesPerVertexBuffer;
      break;

    case ADK_MESH_UNIFORM_TYPE_LIGHT_COLOR:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMeshRenderer->pLightColorBuffer;
      break;

    case ADK_MESH_UNIFORM_TYPE_LIGHT_DIRECTION:
      pBufferUniformInfos[bufferUniformCount].binding = pMesh->shaderInfo.pUniforms[i].binding;
      pBufferUniformInfos[bufferUniformCount].pBuffer = pMeshRenderer->pLightDirectionBuffer;
      break;
    }

    //Keep the uniform if the corresponding buffer has been set. discard the attribute otherwise.
    if (pBufferUniformInfos[bufferUniformCount].pBuffer != nullptr)
      ++bufferUniformCount;
  }

  //Assign additional buffers.
  for (uint32_t i = 0; i < pMesh->shaderInfo.additionalBufferCount; ++i)
  {
    pBufferUniformInfos[bufferUniformCount] = pMesh->shaderInfo.pAdditionalBuffers[i];
    ++bufferUniformCount;
  }

  AdkSpecializationInfo *pSpecializationInfos = adkAllocTypeCall(AdkSpecializationInfo, pMesh->shaderInfo.specializationCount, adkAF_Zero, pMesh->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  //Assign pre-defined specializations.
  for (uint32_t i = 0; i < pMesh->shaderInfo.specializationCount; ++i)
  {
    switch (pMesh->shaderInfo.pSpecializations[i].type)
    {
    case ADK_MESH_SPECIALIZATION_TYPE_BONE_COUNT:
      pSpecializationInfos[i].constantId = pMesh->shaderInfo.pSpecializations[i].constantId;
      pSpecializationInfos[i].type = ADK_SPECIALIZATION_TYPE_UINT32;
      pSpecializationInfos[i].value.uint32 = pMesh->boneCount;
      break;

    default:
      ADK_ASSERT(false, "Invalid mesh specialization type %d", pMesh->shaderInfo.pSpecializations[i].type);
      break;
    }
  }

  adkVector2<uint32_t> windowSize = adkWindow_GetContentSize(pWindow);

  AdkRenderPipelineCreateInfo renderPipelineCreateInfo;
  renderPipelineCreateInfo.flags = ADK_RENDER_PIPELINE_DEPTH_READ_BIT | ADK_RENDER_PIPELINE_DEPTH_WRITE_BIT | ADK_RENDER_PIPELINE_INDEXED_BIT;
  renderPipelineCreateInfo.primitiveTopology = ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  renderPipelineCreateInfo.polygonMode = ADK_POLYGON_MODE_FILL;
  renderPipelineCreateInfo.blending.enable = false;
  renderPipelineCreateInfo.attributeCount = attributeCount;
  renderPipelineCreateInfo.pAttributes = pAttributeInfos;
  renderPipelineCreateInfo.bufferUniformCount = bufferUniformCount;
  renderPipelineCreateInfo.pBufferUniforms = pBufferUniformInfos;
  renderPipelineCreateInfo.samplerUniformCount = pMesh->shaderInfo.additionalSamplerCount;
  renderPipelineCreateInfo.pSamplerUniforms = pMesh->shaderInfo.pAdditionalSamplers;
  renderPipelineCreateInfo.specializationCount = pMesh->shaderInfo.specializationCount;
  renderPipelineCreateInfo.pSpecializations = pSpecializationInfos;
  renderPipelineCreateInfo.pIndices = pMesh->pIndexBuffer;
  renderPipelineCreateInfo.pShader = pMesh->shaderInfo.pShader;
  renderPipelineCreateInfo.colorTargetType = ADK_RENDER_TARGET_TYPE_WINDOW;
  renderPipelineCreateInfo.colorTarget.pWindow = pWindow;
  renderPipelineCreateInfo.depthTargetType = ADK_RENDER_TARGET_TYPE_WINDOW;
  renderPipelineCreateInfo.depthTarget.pWindow = pWindow;
  renderPipelineCreateInfo.viewport = adkRect<uint32_t>::create(0, 0, windowSize.x, windowSize.y);
  renderPipelineCreateInfo.clearColor = 0;
  renderPipelineCreateInfo.pGraphicsQueue = pMesh->pGraphicsQueue;
  adkRenderPipeline_Create(pMesh->pInstance, &renderPipelineCreateInfo, &pMeshRenderer->pRenderPipeline);

  pMeshRenderer->modelCount = modelCount;

  adkFreeCall(pSpecializationInfos, pMesh->pAllocator);
  adkFreeCall(pBufferUniformInfos, pMesh->pAllocator);
  adkFreeCall(pAttributeInfos, pMesh->pAllocator);

  (*ppMeshRenderer) = pMeshRenderer;
}

void adkMeshRenderer_Destroy(AdkMeshRenderer **ppMeshRenderer)
{
  AdkMeshRenderer *pMeshRenderer = (*ppMeshRenderer);

  adkRenderPipeline_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pRenderPipeline);
  adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pLightColorBuffer);
  adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pLightDirectionBuffer);
  adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pProjectionBuffer);
  adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pViewBuffer);

  if (pMeshRenderer->pBoneBuffer != nullptr)
    adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pBoneBuffer);

  adkBuffer_Destroy(pMeshRenderer->pMesh->pInstance, &pMeshRenderer->pModelBuffer);

  adkFreeCall(pMeshRenderer, pMeshRenderer->pMesh->pAllocator);

  (*ppMeshRenderer) = nullptr;
}

AdkBool32 adkMeshRenderer_HasSkin(AdkMeshRenderer *pMeshRenderer)
{
  return pMeshRenderer->pMesh->pSkinWeights != nullptr;
}

void adkMeshRenderer_MapModels(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppModels)
{
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pModelBuffer, (void**)ppModels);
}

void adkMeshRenderer_UnmapModels(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppModels)
{
  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pModelBuffer, (void**)ppModels);
}

void adkMeshRenderer_MapBones(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppBones)
{
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pBoneBuffer, (void**)ppBones);
}

void adkMeshRenderer_UnmapBones(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppBones)
{
  uint32_t mappedBoneCount = pMeshRenderer->modelCount * pMeshRenderer->pMesh->boneCount;
  for (uint32_t mappedBoneIndex = 0; mappedBoneIndex < mappedBoneCount; ++mappedBoneIndex)
  {
    uint32_t bindPoseIndex = mappedBoneIndex % pMeshRenderer->pMesh->boneCount;
    (*ppBones)[mappedBoneIndex] = (*ppBones)[mappedBoneIndex] * pMeshRenderer->pMesh->pBindPoses[bindPoseIndex].inverse();
  }

  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pBoneBuffer, (void**)ppBones);
}

void adkMeshRenderer_SetDirectionalLight(AdkMeshRenderer *pMeshRenderer, uint32_t color, const adkVector3<float> *pDirection)
{
  adkVector3<float> *pMappedLightDirection = nullptr;
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pLightDirectionBuffer, (void**)pMappedLightDirection);
  *pMappedLightDirection = *pDirection;
  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pLightDirectionBuffer, (void**)pMappedLightDirection);

  adkVector4<float> *pMappedLightColor = nullptr;
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pLightColorBuffer, (void**)pMappedLightColor);
  *pMappedLightColor = _Color32ToColor4f(color);
  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pLightColorBuffer, (void**)pMappedLightColor);
}

void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount, const adkMatrix4x4<float> *pModels)
{
  if (modelCount == 0)
    return;

  uint32_t batchCount = pMeshRenderer->modelCount;
  AdkBufferCopy bufferCopy;
  uint32_t finalRender = modelCount / (batchCount + 1);

  //Upload view buffer.
  bufferCopy.offset = 0;
  bufferCopy.size = sizeof(adkMatrix4x4<float>);
  bufferCopy.pData = pView;
  adkBuffer_Set(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pViewBuffer, 1, &bufferCopy);

  //Upload projection buffer.
  bufferCopy.offset = 0;
  bufferCopy.size = sizeof(adkMatrix4x4<float>);
  bufferCopy.pData = pProjection;
  adkBuffer_Set(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pProjectionBuffer, 1, &bufferCopy);

  //Upload Model matrix batches.
  for (uint32_t i = 0; i < finalRender; ++i)
  {
    bufferCopy.offset = 0;
    bufferCopy.size = batchCount * sizeof(adkMatrix4x4<float>);
    bufferCopy.pData = pModels + i * batchCount;
    adkBuffer_Set(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pModelBuffer, 1, &bufferCopy);
    adkRenderPipeline_Render(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pRenderPipeline, pMeshRenderer->pMesh->indexCount, batchCount);
  }

  //Upload final model matrix batch.
  bufferCopy.offset = 0;
  bufferCopy.size = (modelCount % (batchCount + 1)) * sizeof(adkMatrix4x4<float>);
  bufferCopy.pData = pModels + finalRender * batchCount;
  adkBuffer_Set(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pModelBuffer, 1, &bufferCopy);
  adkRenderPipeline_Render(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pRenderPipeline, pMeshRenderer->pMesh->indexCount, modelCount % (batchCount + 1));
}

void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount)
{
  //Upload view buffer.
  adkMatrix4x4<float> *pMappedView;
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pViewBuffer, (void**)&pMappedView);
  *pMappedView = *pView;
  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pViewBuffer, (void**)&pMappedView);

  //Upload projection buffer.
  adkMatrix4x4<float> *pMappedProjection;
  adkBuffer_Map(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pProjectionBuffer, (void**)&pMappedProjection);
  *pMappedProjection = *pProjection;
  adkBuffer_Unmap(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pProjectionBuffer, (void**)&pMappedProjection);

  adkRenderPipeline_Render(pMeshRenderer->pMesh->pInstance, pMeshRenderer->pRenderPipeline, pMeshRenderer->pMesh->indexCount, modelCount);
}

void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection)
{
  adkMeshRenderer_Render(pMeshRenderer, pView, pProjection, pMeshRenderer->modelCount);
}