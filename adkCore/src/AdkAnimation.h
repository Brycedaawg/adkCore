#ifndef AdkAnimation_h__
#define AdkAnimation_h__

#include <stdint.h>
#include "adkMath.h"
#include "adkMesh.h"

struct AdkAnimation;
struct AdkInstance;
struct adkAllocationCallbacks;
struct AdkMesh;
struct AdkMaterial;
struct AdkAnimationRenderer;
struct adkWindow;
struct AdkAnimationNode;
struct AdkQueue;

#define ADK_ANIMATION_NODE_NAME_MAX_LENGTH 256

typedef enum AdkAnimationGraphType
{
  ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_X,
  ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Y,
  ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Z,
  ADK_ANIMATION_GRAPH_TYPE_ROTATION_X,
  ADK_ANIMATION_GRAPH_TYPE_ROTATION_Y,
  ADK_ANIMATION_GRAPH_TYPE_ROTATION_Z,
  ADK_ANIMATION_GRAPH_TYPE_SCALE_X,
  ADK_ANIMATION_GRAPH_TYPE_SCALE_Y,
  ADK_ANIMATION_GRAPH_TYPE_SCALE_Z,
  ADK_ANIMATION_GRAPH_TYPE_COUNT,
} AdkAnimationGraphType;

typedef struct AdkKeyFrame
{
  float time;
  adkCubicBezier<float> curve;
} AdkKeyFrame;

typedef struct AdkAnimationGraph
{
  AdkAnimationGraphType type;
  uint32_t keyFrameCount;
  AdkKeyFrame *pKeyFrames;
} AdkAnimationGraph;

typedef struct AdkAnimationNode
{
  const char *pName;
  bool isBone;
  AdkMesh *pMesh;
  uint32_t graphCount;
  AdkAnimationGraph *pGraphs;
  uint32_t childCount;
  AdkAnimationNode *pChildren;
} AdkAnimationNode;

typedef struct AdkAnimationBlendLayer
{
  AdkAnimation *pAnimation;
  float time;
  float weight;
} AdkAnimationBlendLayer;

typedef struct AdkAnimationTransform
{
  uint32_t blendLayerCount;
  AdkAnimationBlendLayer *pBlendLayers;
} AdkAnimationTransform;

void adkAnimation_Create(AdkInstance *pInstance, AdkAnimationNode *pHierarchy, const adkAllocationCallbacks *pAllocator, AdkAnimation **ppAnimation);
void adkAnimation_Destroy(AdkAnimation **ppAnimation);
bool adkAnimation_Serialize(AdkAnimation *pAnimation, AdkBinaryWriter *pBinaryWriter);
bool adkAnimation_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, const adkAllocationCallbacks *pAllocator, AdkAnimation **ppAnimation);
void adkAnimation_Copy(AdkAnimation *pSource, AdkAnimation **ppCopy);
uint32_t adkAnimation_GetMeshCount(AdkAnimation *pAnimation);
void adkAnimation_SetShader(AdkAnimation *pAnimation, uint32_t index, const AdkMeshShaderInfo *pMeshShaderInfo);
void adkAnimation_SetMesh(AdkAnimation *pAnimation, uint32_t index, AdkMesh *pMesh);
bool adkAnimation_GetNodeByName(AdkAnimation *pAnimation, const char *pName, uint32_t *pNodeIndex);
void adkAnimationRenderer_Create(AdkAnimation *pAnimation, adkWindow *pWindow, uint32_t modelCount, AdkAnimationRenderer **ppAnimationRenderer);
void adkAnimationRenderer_Destroy(AdkAnimationRenderer **ppAnimationRenderer);
adkMatrix4x4<float> adkAnimationRenderer_GetNodeTransform(AdkAnimationRenderer *pAnimationRenderer, uint32_t nodeIndex);
void adkAnimationRenderer_Transform(AdkAnimationRenderer *pAnimationRenderer, float time);
void adkAnimationRenderer_Transform(AdkAnimationRenderer *pAnimationRenderer, uint32_t transformCount, AdkAnimationTransform *pTransforms);
void adkAnimationRenderer_Render(AdkAnimationRenderer *pAnimationRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount, const adkMatrix4x4<float> *pModels);

#endif // AdkAnimation_h__
