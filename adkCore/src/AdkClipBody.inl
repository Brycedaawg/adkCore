#include "AdkClipBody.h"
#include "adkMath.h"
#include "AdkRTree.h"

typedef struct AdkClipBodyInstanceInternal
{
  bool collided;
  uint64_t collisionUid;
  uint32_t unresolvedCollisionCount;
} AdkClipBodyInstanceInternal;

typedef struct AdkClipBody
{
  const adkAllocationCallbacks *pAllocator;
  void *pResolverUserData;
  AdkClipResolveFunction *pResolver;
  AdkClipResolveBodyFunction *pBodyResolver;
  uint32_t instanceCount;
  uint32_t maxInstanceCount;
  AdkClipBodyInstance *pInstances;
  AdkClipBodyInstanceInternal *pInternalInstances;
  uint32_t sphereCollidersPerInstance;
  AdkSphereCollider *pSphereColliders;
  uint32_t layerIndex; //The layer in which this clip body exists.
  AdkClipBody *pNext; //The next clip body in the linked list.
  bool added; // True if the clip body has been added to a clip.
  AdkRTree *pRTree;
  AdkRTreeEntity *pRTreeEntities;
  AdkRTreeCollision *pRTreeCollisions;
} AdkClipBody;