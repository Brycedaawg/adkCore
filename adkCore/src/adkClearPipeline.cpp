#include "adkClearPipeline.h"
#include "adkInstance.inl"
#include "adkVkTools.inl"
#include "adkTexture.inl"
#include "adkWindow.inl"
#include "AdkQueue.inl"

typedef struct AdkClearPipeline
{
  AdkClearTargetType targetType;
  AdkClearTarget target;
  VkCommandPool targetCommandPool;
  VkFence fence;
  AdkQueue *pQueue;
  union
  {
    VkCommandBuffer textureCommandBuffer;
    VkCommandBuffer *pWindowCommandBuffers;
  };
} AdkClearPipeline;

static inline void _RecordCommandBuffer(AdkClearPipeline *pClearPipeline, const void *pValue)
{
  if (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR)
  {
    //Create command buffers for each image in the windows' swapchain.
    for (uint32_t i = 0; i < pClearPipeline->target.pWindow->swapchainImageCount; ++i)
    {
      VkImage image = pClearPipeline->target.pWindow->pVkSwapchainImages[i];

      //Begin the command buffer.
      VkCommandBufferBeginInfo commandBufferBeginInfo;
      commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
      commandBufferBeginInfo.pNext = nullptr;
      commandBufferBeginInfo.flags = 0;
      commandBufferBeginInfo.pInheritanceInfo = nullptr;
      ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pClearPipeline->pWindowCommandBuffers[i], &commandBufferBeginInfo));

      //Change image layout to transfer dst optimal.
      VkImageMemoryBarrier imageMemoryBarrier;
      imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
      imageMemoryBarrier.pNext = nullptr;
      imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
      imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      imageMemoryBarrier.image = image;
      imageMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
      imageMemoryBarrier.subresourceRange.levelCount = 1;
      imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
      imageMemoryBarrier.subresourceRange.layerCount = 1;
      vkCmdPipelineBarrier(pClearPipeline->pWindowCommandBuffers[i], VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

      //Clear the image.
      VkClearColorValue clearColorValue = _CreateVkClearColorValueFloat(*(uint32_t*)pValue);
      VkImageSubresourceRange imageSubresourceRange;
      imageSubresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      imageSubresourceRange.baseMipLevel = 0;
      imageSubresourceRange.levelCount = 1;
      imageSubresourceRange.baseArrayLayer = 0;
      imageSubresourceRange.layerCount = 1;
      vkCmdClearColorImage(pClearPipeline->pWindowCommandBuffers[i], image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearColorValue, 1, &imageSubresourceRange);

      //Change image layout to color attachment optimal for color and depth stencil attachment optimal for depth.
      imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      imageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
      vkCmdPipelineBarrier(pClearPipeline->pWindowCommandBuffers[i], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

      //End the command buffer.
      ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pClearPipeline->pWindowCommandBuffers[i]));
    }
  }
  else
  {
    AdkTexture *pTexture = (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_DEPTH ? pClearPipeline->target.pWindow->pDepthTexture : pClearPipeline->target.pTexture);

    //Begin the command buffer.
    VkCommandBufferBeginInfo commandBufferBeginInfo;
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.pNext = nullptr;
    commandBufferBeginInfo.flags = 0;
    commandBufferBeginInfo.pInheritanceInfo = nullptr;
    ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pClearPipeline->textureCommandBuffer, &commandBufferBeginInfo));

    if (adkTexture_ColorTypeIsDepth(pTexture->colorType))
    {
      //Change image layout to transfer dst optimal.
      VkImageMemoryBarrier imageMemoryBarrier;
      imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
      imageMemoryBarrier.pNext = nullptr;
      imageMemoryBarrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
      imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
      imageMemoryBarrier.image = pTexture->image;
      imageMemoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
      imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
      imageMemoryBarrier.subresourceRange.levelCount = 1;
      imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
      imageMemoryBarrier.subresourceRange.layerCount = 1;
      vkCmdPipelineBarrier(pClearPipeline->textureCommandBuffer, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

      //Clear the image.
      VkClearDepthStencilValue value;
      memcpy(&value.depth, pValue, adkTexture_SizeOfColorType(pTexture->colorType));
      value.stencil = 0;
      VkImageSubresourceRange range;
      range.aspectMask = adkTexture_ColorTypeToImageAspect(pTexture->colorType);
      range.baseMipLevel = 0;
      range.levelCount = 1;
      range.baseArrayLayer = 0;
      range.layerCount = 1;
      vkCmdClearDepthStencilImage(pClearPipeline->textureCommandBuffer, pTexture->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &value, 1, &range);

      //Change image layout to depth stencil attachment optimal.
      imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      imageMemoryBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
      imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
      vkCmdPipelineBarrier(pClearPipeline->textureCommandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

      //End the command buffer.
      ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pClearPipeline->textureCommandBuffer));
    }
    else
    {

    }
  }
}

void adkClearPipeline_Create(AdkInstance *pInstance, const AdkClearPipelineCreateInfo *pCreateInfo, AdkClearPipeline **ppClearPipeline)
{
  AdkClearPipeline *pClearPipeline = adkAllocTypeCall(AdkClearPipeline, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pClearPipeline->targetType = pCreateInfo->targetType;
  pClearPipeline->target = pCreateInfo->target;
  pClearPipeline->pQueue = pCreateInfo->pQueue;

  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  pClearPipeline->targetCommandPool = (pCreateInfo->flags & ADK_CLEAR_PIPELINE_CREATE_MUTABLE_BIT) ? pInstance->pvkResetCommandPools[pCreateInfo->pQueue->index] : pInstance->pvkCommandPools[pCreateInfo->pQueue->index];

  _CreateFence(device, pInstance->pVkAllocationCallbacks, &pClearPipeline->fence, true);

  VkCommandBufferAllocateInfo commandBufferAllocateInfo;
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.pNext = nullptr;
  commandBufferAllocateInfo.commandPool = pClearPipeline->targetCommandPool;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

  if (pCreateInfo->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR)
  {
    commandBufferAllocateInfo.commandBufferCount = pCreateInfo->target.pWindow->swapchainImageCount;
    pClearPipeline->pWindowCommandBuffers = adkAllocTypeCall(VkCommandBuffer, pCreateInfo->target.pWindow->swapchainImageCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
    vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, pClearPipeline->pWindowCommandBuffers);
  }
  else
  {
    commandBufferAllocateInfo.commandBufferCount = 1;
    vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &pClearPipeline->textureCommandBuffer);
  }

  _RecordCommandBuffer(pClearPipeline, pCreateInfo->pValue);

  (*ppClearPipeline) = pClearPipeline;
}

void adkClearPipeline_Destroy(AdkInstance *pInstance, AdkClearPipeline **ppClearPipeline)
{
  AdkClearPipeline *pClearPipeline = (*ppClearPipeline);
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];

  if (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR)
  {
    vkFreeCommandBuffers(device, pClearPipeline->targetCommandPool, pClearPipeline->target.pWindow->swapchainImageCount, pClearPipeline->pWindowCommandBuffers);
    adkFreeCall(pClearPipeline->pWindowCommandBuffers, pInstance->pAllocationCallbacks);
  }
  else
  {
    vkFreeCommandBuffers(device, pClearPipeline->targetCommandPool, 1, &pClearPipeline->textureCommandBuffer);
  }

  vkDestroyFence(device, pClearPipeline->fence, pInstance->pVkAllocationCallbacks);

  adkFreeCall(*ppClearPipeline, pInstance->pAllocationCallbacks);
}

void adkClearPipeline_SetValue(AdkInstance * /*pInstance*/, AdkClearPipeline *pClearPipeline, void *pValue)
{
  _RecordCommandBuffer(pClearPipeline, pValue);
}

void adkClearPipeline_Clear(AdkInstance *pInstance, AdkClearPipeline *pClearPipeline)
{
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkQueue queue = pInstance->pVkQueues[pClearPipeline->pQueue->index];

  _WaitForFence(device, pClearPipeline->fence, true);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;

  VkPipelineStageFlags submitWaitMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
  VkSemaphore semaphore;

  if (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR)
    semaphore = pClearPipeline->target.pWindow->swapchainSemaphore;
  else if (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_DEPTH)
    semaphore = pClearPipeline->target.pWindow->pDepthTexture->semaphore;
  else
    semaphore = pClearPipeline->target.pTexture->semaphore;

  VkCommandBuffer commandBuffer;

  if (pClearPipeline->targetType == ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR)
    commandBuffer = pClearPipeline->pWindowCommandBuffers[pClearPipeline->target.pWindow->swapchainImageIndex];
  else
    commandBuffer = pClearPipeline->textureCommandBuffer;

  //Submit render.
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &semaphore;
  submitInfo.pWaitDstStageMask = &submitWaitMask;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &commandBuffer;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &semaphore;

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pClearPipeline->fence/*VK_NULL_HANDLE*/));
}