#ifndef AdkComputePipeline_h__
#define AdkComputePipeline_h__

#include "adkShader.h"
#include "adkMemory.h"
#include "adkBuffer.h"
#include "adkTexture.h"
#include "AdkQueue.h"

struct AdkComputePipeline;

typedef struct AdkComputeStorageBuffer
{
  uint32_t binding;
  AdkBuffer *pBuffer;
} AdkComputeStorageBuffer;

typedef struct AdkComputeStorageImage
{
  uint32_t binding;
  AdkTexture *pTexture;
} AdkComputeStorageImage;

typedef struct AdkComputePipelineCreateInfo
{
  uint32_t storageBufferCount;
  AdkComputeStorageBuffer *pStorageBuffers;
  uint32_t storageImageCount;
  AdkComputeStorageImage *pStorageImages;
  AdkShader *pShader;
  AdkQueue *pComputeQueue;
} AdkComputePipelineCreateInfo;

void adkComputePipeline_Create(AdkInstance *pInstance, const AdkComputePipelineCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkComputePipeline **ppComputePipeline);
void adkComputePipeline_Destroy(AdkComputePipeline **ppComputePipeline);
void adkComputePipeline_Dispatch(AdkComputePipeline *pComputePipeline, const adkVector3<uint32_t> &groupCount);
AdkBool32 adkComputePipeline_Wait(AdkComputePipeline *pComputePipeline, uint64_t timeout);

#endif // AdkComputePipeline_h__
