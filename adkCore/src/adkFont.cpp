#include "adkFont.inl"
#include "adkFontReader.inl"
#include "adkWindow.h"
#include "adkString.h"

AdkResult adkFont_Create(AdkInstance *pInstance, const AdkFontCreateInfo *pCreateInfo, AdkFont **ppFont)
{
  FT_Face face;

  if (FT_New_Face(pCreateInfo->pFontReader->freeTypeLibrary, pCreateInfo->pFilename, 0, &face) != 0)
    return ADK_ERROR;

  FT_Set_Pixel_Sizes(face, 0, pCreateInfo->fontSize);

  uint32_t glyphsPerRow = 1;
  while (glyphsPerRow * glyphsPerRow < (uint32_t)face->num_glyphs)
    glyphsPerRow <<= 1;

  size_t textureSize = (size_t)glyphsPerRow * glyphsPerRow * pCreateInfo->fontSize * pCreateInfo->fontSize;
  size_t allocSize = sizeof(AdkFont) + textureSize + face->num_glyphs * 5 * sizeof(adkVector2<float>) + 256 * sizeof(uint32_t);
  AdkFont *pFont = (AdkFont*)adkAllocCall(allocSize, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memset(pFont, 0, allocSize);
  pFont->bitmapWidth = glyphsPerRow * pCreateInfo->fontSize;
  pFont->bitmapHeight = glyphsPerRow * pCreateInfo->fontSize;
  pFont->pBitmap = (char*)pFont + sizeof(AdkFont);
  pFont->fontSize = pCreateInfo->fontSize;
  pFont->glyphCount = face->num_glyphs;
  pFont->glyphsPerRow = glyphsPerRow;
  pFont->pUvs = (adkVector2<float>*)((char*)pFont + sizeof(AdkFont) + textureSize);
  pFont->pBearings = (adkVector2<float>*)((char*)pFont + sizeof(AdkFont) + textureSize + face->num_glyphs * 2 * sizeof(adkVector2<float>));
  pFont->pExtents = (adkVector2<float>*)((char*)pFont + sizeof(AdkFont) + textureSize + face->num_glyphs * 3 * sizeof(adkVector2<float>));
  pFont->pAdvances = (adkVector2<float>*)((char*)pFont + sizeof(AdkFont) + textureSize + face->num_glyphs * 4 * sizeof(adkVector2<float>));
  pFont->pAsciiGlyphIndices = (uint32_t*)((char*)pFont + sizeof(AdkFont) + textureSize + face->num_glyphs * 5 * sizeof(adkVector2<float>));

  FT_Select_Charmap(face, FT_ENCODING_UNICODE);

  for (uint32_t i = 0; i < 256; ++i)
    pFont->pAsciiGlyphIndices[i] = (uint8_t)FT_Get_Char_Index(face, i);

  size_t glyphRowSize = pFont->bitmapWidth * pCreateInfo->fontSize;
  float glyphsPerRowReciprocal = 1.f / glyphsPerRow;

  for (uint32_t i = 0, uvI = 0; i < (uint32_t)face->num_glyphs; ++i, uvI += 2)
  {
    if (FT_Load_Glyph(face, i, FT_LOAD_RENDER) == 0)
    {
      uint32_t row = i / glyphsPerRow;
      uint32_t column = i % glyphsPerRow;

      pFont->pUvs[uvI + 0].x = column * glyphsPerRowReciprocal;
      pFont->pUvs[uvI + 0].y = row * glyphsPerRowReciprocal;
      pFont->pUvs[uvI + 1].x = pFont->pUvs[uvI + 0].x + glyphsPerRowReciprocal * face->glyph->bitmap.width / pCreateInfo->fontSize;
      pFont->pUvs[uvI + 1].y = pFont->pUvs[uvI + 0].y + glyphsPerRowReciprocal * face->glyph->bitmap.rows / pCreateInfo->fontSize;

      pFont->pBearings[i] = adkVector2<float>::create((float)face->glyph->bitmap_left, (float)face->glyph->bitmap_top);
      pFont->pExtents[i] = adkVector2<float>::create((float)face->glyph->bitmap.width, (float)face->glyph->bitmap.rows);
      pFont->pAdvances[i] = adkVector2<float>::create((float)(face->glyph->advance.x >> 6), (float)(face->glyph->advance.y >> 6));

      for (uint32_t j = 0; j < face->glyph->bitmap.rows; ++j)
      {
        size_t dstOffset = row * glyphRowSize + column * pCreateInfo->fontSize + j * pFont->bitmapWidth;
        size_t srcOffset = j * face->glyph->bitmap.width;
        memcpy(pFont->pBitmap + dstOffset, face->glyph->bitmap.buffer + srcOffset, face->glyph->bitmap.width);
      }
    }
  }

  FT_Done_Face(face);

  (*ppFont) = pFont;
  return ADK_SUCCESS;
}

void adkFont_Destroy(AdkInstance *pInstance, AdkFont **ppFont)
{
  AdkFont *pFont = (*ppFont);

  adkFreeCall(pFont, pInstance->pAllocationCallbacks);

  (*ppFont) = nullptr;
}

void adkFont_GetTextMesh(AdkFont *pFont, const AdkFontTextMeshInfo *pTextMeshInfo, uint32_t *pVertexCount, adkVector3<float> *pPositions, adkVector2<float> *pUvs)
{
  if (pPositions == nullptr || pUvs == nullptr)
  {
    (*pVertexCount) = (uint32_t)adkStrLen(pTextMeshInfo->pCharacters) * 6;
  }
  else
  {
    bool wordWrap = (pTextMeshInfo->flags & ADK_FONT_TEXT_MESH_WORD_WRAP_BIT) != 0;
    uint32_t characterIndex = 0;
    char character = pTextMeshInfo->pCharacters[characterIndex];
    uint32_t vertexIndex = 0;
    uint32_t lineIndex = 0;
    uint32_t wordEndIndex = 0;
    float glyphUvWidth = 1.f / pFont->glyphsPerRow;
    adkVector3<float> zero = (pTextMeshInfo->matrix * adkVector4<float>::identity()).homogenize().toVector3();
    adkVector3<float> right = (pTextMeshInfo->matrix * adkVector4<float>::create(1.f, 0.f, 0.f, 1.f)).homogenize().toVector3() - zero;
    adkVector3<float> down = (pTextMeshInfo->matrix * adkVector4<float>::create(0.f, -1.f, 0.f, 1.f)).homogenize().toVector3() - zero;
    zero -= (float)pFont->fontSize * down;
    adkVector3<float> position = zero;

    adkVector2<float> position2d = adkVector2<float>::create(0.f, -(float)pFont->fontSize);
    adkVector2<float> boundsStart2d = adkVector2<float>::create((float)pTextMeshInfo->bounds.x, (float)-pTextMeshInfo->bounds.y);
    adkVector2<float> boundsEnd2d = adkVector2<float>::create((float)(pTextMeshInfo->bounds.x + pTextMeshInfo->bounds.width), (float)(-pTextMeshInfo->bounds.y - pTextMeshInfo->bounds.height));

    while (character != 0 && vertexIndex + 6 <= (*pVertexCount))
    {
      if (wordWrap && characterIndex == wordEndIndex) //Word wrap.
      {
        ++wordEndIndex;
        char wordCharacter = pTextMeshInfo->pCharacters[wordEndIndex];
        uint32_t wordGlyphIndex = pFont->pAsciiGlyphIndices[wordCharacter];
        float wordX = position2d.x;

        while (wordCharacter != 0 && wordCharacter != '\n' && wordCharacter != ' ')
        {
          ++wordEndIndex;
          wordX += pFont->pAdvances[wordGlyphIndex].x;
          wordCharacter = pTextMeshInfo->pCharacters[wordEndIndex];
          wordGlyphIndex = pFont->pAsciiGlyphIndices[wordCharacter];
        }

        float wordEndX = wordX + pFont->pBearings[wordGlyphIndex].x + pFont->pAdvances[wordGlyphIndex].x;

        if (wordEndX > boundsEnd2d.x)
          character = '\n';

        continue;
      }

      if (character == '\n')
      {
        ++lineIndex;
        position2d = adkVector2<float>::create(0.f, -(float)lineIndex * pFont->fontSize);
        position = zero + position2d.y * down;
      }
      else
      {
        uint32_t glyphIndex = pFont->pAsciiGlyphIndices[character];

        adkVector2<float> glyphStart2d = adkVector2<float>::create(position2d.x + pFont->pBearings[glyphIndex].x, position2d.y + pFont->pBearings[glyphIndex].y);
        adkVector3<float> glyphStart = position + right * pFont->pBearings[glyphIndex].x + down * pFont->pBearings[glyphIndex].y;
        adkVector3<float> deltaRight = right * pFont->pExtents[glyphIndex].x;
        adkVector3<float> deltaUp = -down * pFont->pExtents[glyphIndex].y;
        adkVector2<float> glyphEnd2d = adkVector2<float>::create(glyphStart2d.x + pFont->pExtents[glyphIndex].x, glyphStart2d.y - pFont->pExtents[glyphIndex].y);
        adkVector2<float> uvOffsetAddend = adkVector2<float>::zero();
        adkVector2<float> uvExtentAddend = adkVector2<float>::zero();

        if (glyphEnd2d.y > boundsStart2d.y) //Character entirely above bounds; skip rest of line.
        {
          do
          {
            ++characterIndex;
            character = pTextMeshInfo->pCharacters[characterIndex];
          } while (character != '\n' && character != 0);

          continue;
        }
        else if (glyphStart2d.y > boundsStart2d.y) //Character partially above bounds; clip.
        {
          float overshot2dy = glyphStart2d.y - boundsStart2d.y;
          glyphStart -= overshot2dy * down;
          deltaUp += overshot2dy * down;
          uvOffsetAddend.y = overshot2dy / pFont->fontSize * glyphUvWidth;
        }

        if (glyphStart2d.y < boundsEnd2d.y) //Character entirely below bounds; ignore.
        {
          position2d.x += pFont->pAdvances[glyphIndex].x;
          position += right * pFont->pAdvances[glyphIndex].x;
          ++characterIndex;
          character = pTextMeshInfo->pCharacters[characterIndex];
          continue;
        }
        else if (glyphEnd2d.y < boundsEnd2d.y) //Character partially below bounds; clip.
        {
          float overshot2dy = boundsEnd2d.y - glyphEnd2d.y;
          deltaUp += overshot2dy * down;
          uvExtentAddend.y = -(overshot2dy / pFont->fontSize * glyphUvWidth);
        }

        if (glyphEnd2d.x < boundsStart2d.x) //Character entirely left of bounds; skip to first character within left bound.
        {
          do
          {
            position2d.x += pFont->pAdvances[glyphIndex].x;
            position += right * pFont->pAdvances[glyphIndex].x;
            ++characterIndex;
            character = pTextMeshInfo->pCharacters[characterIndex];
            glyphIndex = pFont->pAsciiGlyphIndices[character];
            glyphStart2d.x = position2d.x + pFont->pBearings[glyphIndex].x;
            glyphEnd2d.x = glyphStart2d.x + pFont->pExtents[glyphIndex].x;
          } while (glyphEnd2d.x <boundsStart2d.x && character != '\n' && character != 0);

          continue;
        }
        else if (glyphStart2d.x < boundsStart2d.x) //Character partially left of bounds; clip.
        {
          float overshot2dx = boundsStart2d.x - glyphStart2d.x;
          glyphStart += overshot2dx * right;
          deltaRight -= overshot2dx * right;
          uvOffsetAddend.x = overshot2dx / pFont->fontSize * glyphUvWidth;
        }

        if (glyphStart2d.x > boundsEnd2d.x) //Character entirely right of bounds; skip rest of line.
        {
          if (wordWrap) //word too big for line; break it up.
          {
            character = '\n';
            --characterIndex;
            continue;
          }
          else
          {
            do
            {
              ++characterIndex;
              character = pTextMeshInfo->pCharacters[characterIndex];
            } while (character != '\n' && character != 0);

            continue;
          }
        }
        else if (glyphEnd2d.x > boundsEnd2d.x) //Character partially right of bounds; clip.
        {
          if (wordWrap) //word too big for line; break it up.
          {
            character = '\n';
            --characterIndex;
            continue;
          }
          {
            float overshot2dx = glyphEnd2d.x - boundsEnd2d.x;
            deltaRight -= overshot2dx * right;
            uvExtentAddend.x = -(overshot2dx / pFont->fontSize * glyphUvWidth);
          }
        }

        //Add character.
        pPositions[vertexIndex + 0] = glyphStart;
        pPositions[vertexIndex + 1] = glyphStart + deltaUp;
        pPositions[vertexIndex + 2] = glyphStart + deltaRight;
        pPositions[vertexIndex + 3] = glyphStart + deltaRight;
        pPositions[vertexIndex + 4] = glyphStart + deltaUp;
        pPositions[vertexIndex + 5] = glyphStart + deltaRight + deltaUp;

        uint32_t uvIndex = glyphIndex * 2;
        pUvs[vertexIndex + 0] = adkVector2<float>::create(pFont->pUvs[uvIndex + 0].x, pFont->pUvs[uvIndex + 0].y);
        pUvs[vertexIndex + 1] = adkVector2<float>::create(pFont->pUvs[uvIndex + 0].x, pFont->pUvs[uvIndex + 1].y);
        pUvs[vertexIndex + 2] = adkVector2<float>::create(pFont->pUvs[uvIndex + 1].x, pFont->pUvs[uvIndex + 0].y);
        pUvs[vertexIndex + 3] = adkVector2<float>::create(pFont->pUvs[uvIndex + 1].x, pFont->pUvs[uvIndex + 0].y);
        pUvs[vertexIndex + 4] = adkVector2<float>::create(pFont->pUvs[uvIndex + 0].x, pFont->pUvs[uvIndex + 1].y);
        pUvs[vertexIndex + 5] = adkVector2<float>::create(pFont->pUvs[uvIndex + 1].x, pFont->pUvs[uvIndex + 1].y);

        pUvs[vertexIndex + 0].x += uvOffsetAddend.x;
        pUvs[vertexIndex + 1].x += uvOffsetAddend.x;
        pUvs[vertexIndex + 4].x += uvOffsetAddend.x;

        pUvs[vertexIndex + 0].y += uvOffsetAddend.y;
        pUvs[vertexIndex + 2].y += uvOffsetAddend.y;
        pUvs[vertexIndex + 3].y += uvOffsetAddend.y;

        pUvs[vertexIndex + 2].x += uvExtentAddend.x;
        pUvs[vertexIndex + 3].x += uvExtentAddend.x;
        pUvs[vertexIndex + 5].x += uvExtentAddend.x;

        pUvs[vertexIndex + 1].y += uvExtentAddend.y;
        pUvs[vertexIndex + 4].y += uvExtentAddend.y;
        pUvs[vertexIndex + 5].y += uvExtentAddend.y;

        position2d.x += pFont->pAdvances[glyphIndex].x;
        position += right * pFont->pAdvances[glyphIndex].x;
        vertexIndex += 6;
      }

      ++characterIndex;
      character = pTextMeshInfo->pCharacters[characterIndex];
    }

    (*pVertexCount) = vertexIndex;
  }
}

void adkFont_GetBitmap(AdkFont *pFont, adkVector2<uint32_t> *pExtent, char *pBitmap)
{
  (*pExtent) = adkVector2<uint32_t>::create(pFont->bitmapWidth, pFont->bitmapHeight);
  
  if (pBitmap != nullptr)
    memcpy(pBitmap, pFont->pBitmap, pFont->bitmapWidth * pFont->bitmapHeight);
}