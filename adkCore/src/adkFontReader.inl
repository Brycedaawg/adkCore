#ifndef adkFontReader_inl__
#define adkFontReader_inl__

#include "adkFontReader.h"
#include "ft2build.h"
#include FT_FREETYPE_H

typedef struct AdkFontReader
{
  FT_Library freeTypeLibrary;
} AdkFontReader;

#endif // adkFontReader_inl__
