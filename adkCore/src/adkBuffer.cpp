#include "adkBuffer.inl"
#include "adkMemory.h"
#include "adkInstance.inl"
#include "adkVkTools.inl"
#include "AdkQueue.inl"

static inline void _CopyToDevice(VkDevice device, VkQueue queue, AdkBuffer *pBuffer, uint32_t copyCount, const AdkBufferCopy *pCopies)
{
  for (uint32_t i = 0; i < copyCount; ++i)
    memcpy((char*)pBuffer->stagingDeviceMemory.pMapped + pCopies[i].offset, pCopies[i].pData, pCopies[i].size);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pWaitDstStageMask = nullptr;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pBuffer->copyToDeviceCommandBuffer;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pBuffer->fence));

  _WaitForFence(device, pBuffer->fence, true);
}

void adkBuffer_Create(AdkInstance *pInstance, const AdkBufferCreateInfo *pCreateInfo, AdkBuffer **ppBuffer)
{
  AdkBuffer *pBuffer = adkAllocTypeCall(AdkBuffer, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pBuffer->flags = pCreateInfo->flags;
  pBuffer->pTransferQueue = pCreateInfo->pTransferQueue;

  VkPhysicalDevice physicalDevice = pInstance->pVkPhysicalDevices[pInstance->primaryDeviceIndex];
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkCommandPool commandPool = pInstance->pvkCommandPools[pCreateInfo->pTransferQueue->index];
  VkQueue queue = pInstance->pVkQueues[pCreateInfo->pTransferQueue->index];

  pBuffer->type = pCreateInfo->type;
  pBuffer->count = pCreateInfo->elementCount;
  size_t bufferSize = pCreateInfo->elementCount * adkBuffer_SizeOfElementType(pCreateInfo->type);

  VkBufferUsageFlags stagingBufferUsageFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
  VkBufferUsageFlags deviceBufferUsageFlags = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | adkBuffer_TypeToVkBufferUsageFlags(pCreateInfo->type);

  _CreateBuffer(pInstance, physicalDevice, device, pInstance->pVkAllocationCallbacks, pInstance->pAllocationCallbacks, stagingBufferUsageFlags, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, bufferSize, &pBuffer->stagingBuffer, &pBuffer->stagingDeviceMemory);
  _CreateBuffer(pInstance, physicalDevice, device, pInstance->pVkAllocationCallbacks, pInstance->pAllocationCallbacks, deviceBufferUsageFlags, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, bufferSize, &pBuffer->buffer, &pBuffer->deviceMemory);
  _CreateFence(device, pInstance->pVkAllocationCallbacks, &pBuffer->fence, false);
  _CreateSemaphore(device, queue, pInstance->pVkAllocationCallbacks, &pBuffer->semaphore);
  _CreateCopyCommandBuffer(device, pBuffer->stagingBuffer, pBuffer->buffer, bufferSize, commandPool, &pBuffer->copyToDeviceCommandBuffer);
  _CreateCopyCommandBuffer(device, pBuffer->buffer, pBuffer->stagingBuffer, bufferSize, commandPool, &pBuffer->copyFromDeviceCommandBuffer);

  _CopyToDevice(device, queue, pBuffer, pCreateInfo->copyCount, pCreateInfo->pCopies);

  if ((pCreateInfo->flags & ADK_BUFFER_CREATE_MUTABLE_BIT) == 0)
  {
    vkFreeCommandBuffers(device, commandPool, 1, &pBuffer->copyToDeviceCommandBuffer);
    vkDestroyBuffer(device, pBuffer->stagingBuffer, pInstance->pVkAllocationCallbacks);
    _FreeDeviceMemory(pInstance, &pBuffer->stagingDeviceMemory);

    pBuffer->copyToDeviceCommandBuffer = VK_NULL_HANDLE;
    pBuffer->stagingBuffer = VK_NULL_HANDLE;
  }

  (*ppBuffer) = pBuffer;
}

void adkBuffer_Destroy(AdkInstance *pInstance, AdkBuffer **ppBuffer)
{
  AdkBuffer *pBuffer = (*ppBuffer);
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkCommandPool commandPool = pInstance->pvkCommandPools[pBuffer->pTransferQueue->index];

  if (pBuffer->flags & ADK_BUFFER_CREATE_MUTABLE_BIT)
  {
    vkFreeCommandBuffers(device, commandPool, 1, &pBuffer->copyToDeviceCommandBuffer);
    vkDestroyBuffer(device, pBuffer->stagingBuffer, pInstance->pVkAllocationCallbacks);
    _FreeDeviceMemory(pInstance, &pBuffer->stagingDeviceMemory);
  }

  vkDestroySemaphore(device, pBuffer->semaphore, pInstance->pVkAllocationCallbacks);
  vkDestroyFence(device, pBuffer->fence, pInstance->pVkAllocationCallbacks);
  vkDestroyBuffer(device, pBuffer->buffer, pInstance->pVkAllocationCallbacks);
  _FreeDeviceMemory(pInstance, &pBuffer->deviceMemory);
  adkFreeCall(pBuffer, pInstance->pAllocationCallbacks);

  (*ppBuffer) = nullptr;
}

void adkBuffer_Set(AdkInstance *pInstance, AdkBuffer *pBuffer, uint32_t copyCount, const AdkBufferCopy *pCopies)
{
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkQueue queue = pInstance->pVkQueues[pBuffer->pTransferQueue->index];

  _CopyToDevice(device, queue, pBuffer, copyCount, pCopies);
}

void adkBuffer_Map(AdkInstance *pInstance, AdkBuffer *pBuffer, void **ppData)
{
  //Copy device buffer to staging buffer
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  
  VkQueue queue = pInstance->pVkQueues[pBuffer->pTransferQueue->index];
  
  VkPipelineStageFlags waitFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pBuffer->semaphore;
  submitInfo.pWaitDstStageMask = &waitFlags;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pBuffer->copyFromDeviceCommandBuffer;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &pBuffer->semaphore;
  
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pBuffer->fence));
  
  _WaitForFence(device, pBuffer->fence, true);

  *ppData = pBuffer->stagingDeviceMemory.pMapped;
}

void adkBuffer_Unmap(AdkInstance *pInstance, AdkBuffer *pBuffer, void **ppData)
{
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];

  VkQueue queue = pInstance->pVkQueues[pBuffer->pTransferQueue->index];

  VkPipelineStageFlags waitFlags = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pBuffer->semaphore;
  submitInfo.pWaitDstStageMask = &waitFlags;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pBuffer->copyToDeviceCommandBuffer;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &pBuffer->semaphore;

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pBuffer->fence));

  _WaitForFence(device, pBuffer->fence, true);

  *ppData = nullptr;
}