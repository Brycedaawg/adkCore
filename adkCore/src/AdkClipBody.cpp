#include "AdkClipBody.inl"
#include "adkMemory.h"
#include "adkAssert.h"

void adkClipBody_Create(const AdkClipBodyCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkClipBody **ppClipBody)
{
  AdkClipBody *pClipBody = adkAllocTypeCall(AdkClipBody, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pClipBody->pAllocator = pAllocator;
  pClipBody->pResolverUserData = pCreateInfo->pResolverUserData;
  pClipBody->pResolver = pCreateInfo->pResolver;
  pClipBody->pBodyResolver = pCreateInfo->pBodyResolver;
  pClipBody->maxInstanceCount = pCreateInfo->maxInstanceCount;
  pClipBody->sphereCollidersPerInstance = pCreateInfo->sphereCollidersPerInstance;

  pClipBody->pInstances = adkAllocTypeCall(AdkClipBodyInstance, pCreateInfo->maxInstanceCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pClipBody->pInternalInstances = adkAllocTypeCall(AdkClipBodyInstanceInternal, pCreateInfo->maxInstanceCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pClipBody->pSphereColliders = adkAllocTypeCall(AdkSphereCollider, pCreateInfo->sphereCollidersPerInstance * pCreateInfo->maxInstanceCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  adkRTree_Create(pClipBody->maxInstanceCount, pAllocator, &pClipBody->pRTree);

  pClipBody->pRTreeEntities = adkAllocTypeCall(AdkRTreeEntity, pCreateInfo->maxInstanceCount * pCreateInfo->sphereCollidersPerInstance, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pClipBody->pRTreeCollisions = adkAllocTypeCall(AdkRTreeCollision, pCreateInfo->maxInstanceCount * pCreateInfo->sphereCollidersPerInstance, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  *ppClipBody = pClipBody;
}

void adkClipBody_Destroy(AdkClipBody **ppClipBody)
{
  AdkClipBody *pClipBody = *ppClipBody;

  adkFreeCall(pClipBody->pRTreeCollisions, pClipBody->pAllocator);
  adkFreeCall(pClipBody->pRTreeEntities, pClipBody->pAllocator);

  adkRTree_Destroy(&pClipBody->pRTree);

  adkFreeCall(pClipBody->pSphereColliders, pClipBody->pAllocator);
  adkFreeCall(pClipBody->pInternalInstances, pClipBody->pAllocator);
  adkFreeCall(pClipBody->pInstances, pClipBody->pAllocator);

  adkFreeCall(*ppClipBody, pClipBody->pAllocator);
}

uint32_t adkClipBody_GetInstanceCount(AdkClipBody *pClipBody)
{
  return pClipBody->instanceCount;
}

void adkClipBody_SetInstanceCount(AdkClipBody *pClipBody, uint32_t instanceCount)
{
  pClipBody->instanceCount = instanceCount;
}

void adkClipBody_Map(AdkClipBody *pClipBody, AdkClipBodyInstance **ppInstances)
{
  *ppInstances = pClipBody->pInstances;
}

void adkClipBody_Unmap(AdkClipBody * /*pClipBody*/, AdkClipBodyInstance **ppInstances)
{
  *ppInstances = nullptr;
}

void adkClipBody_MapSphereColliders(AdkClipBody *pClipBody, AdkSphereCollider **ppSphereColliders)
{
  *ppSphereColliders = pClipBody->pSphereColliders;
}

void adkClipBody_UnmapSphereColliders(AdkClipBody * /*pClipBody*/, AdkSphereCollider **ppSphereColliders)
{
  *ppSphereColliders = nullptr;
}

void adkClipBody_RemoveSwapLast(AdkClipBody *pClipBody, uint32_t instance)
{
  ADK_ASSERT(pClipBody->instanceCount != 0, "Cannot remove swap last because instanceCount is zero");
  ADK_ASSERT(instance < pClipBody->instanceCount, "Cannot remove swap last instance %d because it exceeds instance count %d", instance, pClipBody->instanceCount);

  --pClipBody->instanceCount;
  adkSwap(&pClipBody->pInstances[instance], &pClipBody->pInstances[pClipBody->instanceCount]);
  adkSwap(&pClipBody->pInternalInstances[instance], &pClipBody->pInternalInstances[pClipBody->instanceCount]);
}