#ifndef AdkRTree_h__
#define AdkRTree_h__

#include "adkTypedefs.h"
#include "adkMath.h"

#define ADK_R_TREE_ENTITY_NONE 0xFFFFFFFF

struct AdkRTree;
struct adkAllocationCallbacks;
struct AdkLineRenderer;

typedef struct AdkRTreeEntity
{
  adkVector3<float> position;
  float radius;
} AdkRTreeEntity;

typedef struct AdkRTreeCollision
{
  uint32_t entity;
} AdkRTreeCollision;

void adkRTree_Create(uint32_t maxEntityCount, const adkAllocationCallbacks *pAllocator, AdkRTree **ppRTree);
void adkRTree_Destroy(AdkRTree **ppRTree);
void adkRTree_Link(AdkRTree *pRTree, uint32_t entityCount, AdkRTreeEntity *pEntities, AdkLineRenderer *pLineRenderer = nullptr);
void adkRTree_GetCollisions(AdkRTree *pRTree, uint32_t entity, uint32_t collisionsLength, uint32_t *pCollisionCount, AdkRTreeCollision *pCollisions, AdkLineRenderer *pLineRenderer = nullptr);
void adkRTree_GetCollisions(AdkRTree *pRTree, const adkVector3<float> &position, float radius, uint32_t collisionsLength, uint32_t *pCollisionCount, AdkRTreeCollision *pCollisions, uint32_t ignoreEntity = ADK_R_TREE_ENTITY_NONE, AdkLineRenderer *pLineRenderer = nullptr);
void adkRTree_Draw(AdkRTree *pRTree, AdkLineRenderer *pLineRenderer);

#endif // AdkRTree_h__
