#include "adkMemory.h"
#include "adkFile.h"
#include "stdio.h"

typedef struct AdkFile
{
  FILE *pFile;
} AdkFile;

AdkBool32 adkFile_Load(const char *pFilename, size_t *pBufferSize, void** ppBuffer)
{
  AdkBool32 success = ADK_FALSE;
  AdkFile *pFile = nullptr;
  size_t size = 0;
  void *pBuffer = nullptr;

  if (!adkFile_Open(pFilename, ADK_FILE_OPEN_READ_BIT, &size, &pFile))
    goto epilogue;

  pBuffer = adkAlloc(size + 1);

  if (!adkFile_Read(pFile, size, pBuffer))
    goto epilogue;

  adkFile_Close(&pFile);

  ((char*)pBuffer)[size] = 0;

  if (pBufferSize != nullptr)
    (*pBufferSize) = size;

  (*ppBuffer) = pBuffer;

  success = ADK_TRUE;

epilogue:
  if (pFile != nullptr)
    adkFile_Close(&pFile);

  if (!success)
  {
    if (pBuffer != nullptr)
      adkFree(pBuffer);
  }

  return success;
}

AdkBool32 adkFile_Open(const char *pFilename, AdkFileOpenFlags flags, size_t *pFileSize, AdkFile **ppFile)
{
  AdkBool32 success = ADK_FALSE;
  AdkFile *pFile = adkAllocType(AdkFile, 1, adkAF_Zero);

  char *pMode = nullptr;

  switch (flags & (ADK_FILE_OPEN_READ_BIT | ADK_FILE_OPEN_WRITE_BIT | ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_APPEND_BIT))
  {
  case ADK_FILE_OPEN_READ_BIT:
    pMode = "rb";
    break;

  case ADK_FILE_OPEN_WRITE_BIT:
    pMode = "wb";
    break;

  case ADK_FILE_OPEN_WRITE_BIT | ADK_FILE_OPEN_READ_BIT:
    pMode = "r+b";
    break;

  case ADK_FILE_OPEN_CREATE_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_READ_BIT:
    pMode = "w+b";
    break;

  case ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_WRITE_BIT:
    pMode = "w+b";
    break;

  case ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_WRITE_BIT | ADK_FILE_OPEN_READ_BIT:
    pMode = "w+b";
    break;

  case ADK_FILE_OPEN_APPEND_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_READ_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_WRITE_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_WRITE_BIT | ADK_FILE_OPEN_READ_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_CREATE_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_READ_BIT:
    goto epilogue;
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_WRITE_BIT:
    pMode = "ab";
    break;

  case ADK_FILE_OPEN_APPEND_BIT | ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_WRITE_BIT | ADK_FILE_OPEN_READ_BIT:
    pMode = "a+b";
    break;

  default:
    goto epilogue;
    break;
  }

  errno_t error = fopen_s(&pFile->pFile, pFilename, pMode);



  //Open the file.
  if (error != 0)
    goto epilogue;

  //Get the size.
  int size;
  if (fseek(pFile->pFile, 0, SEEK_END) != 0)
    goto epilogue;

  size = ftell(pFile->pFile);

  if (size == -1)
    goto epilogue;

  //Rewind the file.
  if (fseek(pFile->pFile, 0, SEEK_SET) != 0)
    goto epilogue;

  (*pFileSize) = (uint32_t)size;
  (*ppFile) = pFile;

  success = ADK_TRUE;

epilogue:
  if (!success)
  {
    if (pFile != nullptr && pFile->pFile != nullptr)
      fclose(pFile->pFile);

    adkFree(pFile);
  }

  return success;
}

void adkFile_Close(AdkFile **ppFile)
{
  AdkFile *pFile = (*ppFile);

  fclose(pFile->pFile);

  adkFree(pFile);

  (*ppFile) = nullptr;
}

AdkBool32 adkFile_Read(AdkFile *pFile, size_t bufferSize, void *pBuffer)
{
  return fread(pBuffer, sizeof(char), bufferSize, pFile->pFile) == bufferSize;
}

AdkBool32 adkFile_Write(AdkFile *pFile, size_t bufferSize, void *pBuffer)
{
  return fwrite(pBuffer, sizeof(char), bufferSize, pFile->pFile) == bufferSize;
}

AdkBool32 adkFile_Seek(AdkFile *pFile, AdkFileOrigin origin, int32_t offset)
{
  int originI = 0;

  switch (origin)
  {
  case ADK_FILE_ORIGIN_START:
    originI = SEEK_SET;
    break;
  case ADK_FILE_ORIGIN_CURRENT:
    originI = SEEK_CUR;
    break;
  case ADK_FILE_ORIGIN_END:
    originI = SEEK_END;
    break;
  default:
    return ADK_FALSE;
  }

  return fseek(pFile->pFile, offset, originI) == 0;
}