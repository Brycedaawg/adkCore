#include "adkShader.inl"
#include "adkMemory.h"
#include "adkInstance.inl"
#include "adkString.h"
//#include "ShaderLang.h"
//#include <vector>
#include "adkVkTools.inl"
#include "adkFile.h"
//#include "..\..\SPIRV\GlslangToSpv.h"

typedef struct AdkShaderConversion
{
  uint32_t uniformCount;
  char **ppUniforms;
  uint32_t attributeCount;
  char **ppAttributes;
  size_t spirvSize;
  uint32_t *pSpirv;
} AdkShaderConversion;

static void _CreateShaderModules(AdkShader *pShader, size_t *pShaderSizes, const char **pShaderCodes)
{
  VkDevice device = pShader->pInstance->pVkDevices[pShader->pInstance->primaryDeviceIndex];

  for (uint32_t i = 0; i < ADK_SHADER_STAGE_COUNT; ++i)
  {
    if (pShaderCodes[i] == nullptr)
      continue;
  
    //Create the Vulkan shader module.
    VkShaderModuleCreateInfo shaderModuleCreateInfo;
    shaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderModuleCreateInfo.pNext = nullptr;
    shaderModuleCreateInfo.flags = 0;
    shaderModuleCreateInfo.codeSize = pShaderSizes[i];
    shaderModuleCreateInfo.pCode = (uint32_t*)pShaderCodes[i];
    ADK_ASSERT_VK_RESULT(vkCreateShaderModule(device, &shaderModuleCreateInfo, pShader->pInstance->pVkAllocationCallbacks, &pShader->modules[i]));
  }
}

AdkBool32 adkShader_Create(AdkInstance *pInstance, const AdkShaderCreateInfo *pCreateInfo, AdkShader **ppShader)
{
  AdkBool32 success = ADK_FALSE;
  AdkShader *pShader = adkAllocTypeCall(AdkShader, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pShader->pInstance = pInstance;

  const char *shaders[ADK_SHADER_STAGE_COUNT];
  shaders[ADK_SHADER_STAGE_VERTEX] = pCreateInfo->pVertexShader;
  shaders[ADK_SHADER_STAGE_GEOMETRY] = pCreateInfo->pGeometryShader;
  shaders[ADK_SHADER_STAGE_FRAGMENT] = pCreateInfo->pFragmentShader;
  shaders[ADK_SHADER_STAGE_COMPUTE] = pCreateInfo->pComputeShader;

  size_t shaderSizes[ADK_SHADER_STAGE_COUNT];
  shaderSizes[ADK_SHADER_STAGE_VERTEX] = pCreateInfo->vertexShaderSize;
  shaderSizes[ADK_SHADER_STAGE_GEOMETRY] = pCreateInfo->geometryShaderSize;
  shaderSizes[ADK_SHADER_STAGE_FRAGMENT] = pCreateInfo->fragmentShaderSize;
  shaderSizes[ADK_SHADER_STAGE_COMPUTE] = pCreateInfo->computeShaderSize;

  if ((shaders[ADK_SHADER_STAGE_VERTEX] == nullptr || shaders[ADK_SHADER_STAGE_FRAGMENT] == nullptr) && shaders[ADK_SHADER_STAGE_COMPUTE] == nullptr)
    goto epilogue;

  if (pCreateInfo->flags & ADK_SHADER_CREATE_LOAD_FROM_FILE_BIT)
  {
    for (uint32_t i = 0; i < ADK_SHADER_STAGE_COUNT; ++i)
    {
      if (shaders[i] == nullptr)
        continue;

      if (!adkFile_Load(shaders[i], &shaderSizes[i], (void**)&shaders[i]))
        goto epilogue;
    }
  }

  _CreateShaderModules(pShader, shaderSizes, shaders);

  (*ppShader) = pShader;

  success = ADK_TRUE;

epilogue:
  if (!success)
  {
    adkFreeCall(pShader, pInstance->pAllocationCallbacks);
  }

  return success;
}

void adkShader_Destroy(AdkShader **ppShader)
{
  if (*ppShader == nullptr)
    return;

  AdkShader *pShader = (*ppShader);
  VkDevice device = pShader->pInstance->pVkDevices[pShader->pInstance->primaryDeviceIndex];

  for (uint32_t i = 0; i < ADK_SHADER_STAGE_COUNT; ++i)
    vkDestroyShaderModule(device, pShader->modules[i], pShader->pInstance->pVkAllocationCallbacks);
}