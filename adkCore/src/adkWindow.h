#ifndef adkWindow_h__
#define adkWindow_h__

#include "adkMath.h"
#include "adkMemory.h"

struct adkWindow;
struct AdkInstance;
struct AdkQueue;

typedef enum AdkMouseButton
{
  ADK_MOUSE_BUTTON_LEFT,
  ADK_MOUSE_BUTTON_RIGHT,
  ADK_MOUSE_BUTTON_MIDDLE,
  ADK_MOUSE_BUTTON_COUNT,
} AdkMouseButton;

typedef enum AdkKey
{
  ADK_KEY_0,
  ADK_KEY_1,
  ADK_KEY_2,
  ADK_KEY_3,
  ADK_KEY_4,
  ADK_KEY_5,
  ADK_KEY_6,
  ADK_KEY_7,
  ADK_KEY_8,
  ADK_KEY_9,
  ADK_KEY_A,
  ADK_KEY_B,
  ADK_KEY_C,
  ADK_KEY_D,
  ADK_KEY_E,
  ADK_KEY_F,
  ADK_KEY_G,
  ADK_KEY_H,
  ADK_KEY_I,
  ADK_KEY_J,
  ADK_KEY_K,
  ADK_KEY_L,
  ADK_KEY_M,
  ADK_KEY_N,
  ADK_KEY_O,
  ADK_KEY_P,
  ADK_KEY_Q,
  ADK_KEY_R,
  ADK_KEY_S,
  ADK_KEY_T,
  ADK_KEY_U,
  ADK_KEY_V,
  ADK_KEY_W,
  ADK_KEY_X,
  ADK_KEY_Y,
  ADK_KEY_Z,
  ADK_KEY_SHIFT,
  ADK_KEY_CONTROL,
  ADK_KEY_ALT,
  ADK_KEY_ESCAPE,
  ADK_KEY_SPACE,
  ADK_KEY_COUNT = 256,
} AdkKey;

typedef void (ADKAPI_CALL AdkWindowFunction)(void *pUserData, adkWindow *pWindow);

typedef enum AdkWindowCreateFlagBits
{
  ADK_WINDOW_CREATE_BORDERLESS_BIT = 0x00000001,
} AdkWindowFlagBits;
typedef AdkFlags AdkWindowCreateFlags;

typedef struct AdkWindowCallbacks
{
  void *pUserData;
  AdkWindowFunction *pCloseFunction;
} AdkWindowCallbacks;

typedef struct adkWindowCreateInfo
{
  AdkWindowCreateFlags flags;
  const char *pName;
  adkRect<uint32_t> rect;
  AdkWindowCallbacks callbacks;
  AdkQueue *pQueue;
} adkWindowCreateInfo;

typedef enum AdkInputEventType
{
  ADK_INPUT_EVENT_TYPE_KEY_DOWN,
  ADK_INPUT_EVENT_TYPE_KEY_UP,
  ADK_INPUT_EVENT_TYPE_MOUSE_MOVE,
  ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_DOWN,
  ADK_INPUT_EVENT_TYPE_MOUSE_BUTTON_UP,
} AdkInputEventType;

typedef struct AdkKeyboardInput
{
  AdkKey key;
} AdkKeyboardInput;

typedef struct AdkMouseMoveInput
{
  adkVector2<int32_t> position;
  adkVector2<int32_t> delta;
} AdkMouseMoveInput;

typedef struct AdkMouseButtonInput
{
  AdkMouseButton button;
} AdkMouseButtonInput;

typedef struct AdkInputEvent
{
  AdkInputEventType type;
  union
  {
    AdkKeyboardInput keyboard;
    AdkMouseMoveInput mouseMove;
    AdkMouseButtonInput mouseButton;
  };
} AdkInputEvent;

typedef struct AdkInputState
{
  AdkBool32 keys[ADK_KEY_COUNT];
  AdkBool32 mouseButtons[ADK_MOUSE_BUTTON_COUNT];
  adkVector2<int32_t> mousePosition;
  adkVector2<int32_t> mouseDelta;
} AdkInputState;

void adkWindow_Create(AdkInstance *pInstance, const adkWindowCreateInfo *pCreateInfo, adkWindow **ppWindow);
void adkWindow_Destroy(adkWindow *pWindow, const adkAllocationCallbacks *pAllocationCallbacks);
void adkWindow_Update(adkWindow *pWindow);
adkVector2<uint32_t> adkWindow_GetContentSize(adkWindow *pWindow);
void adkWindow_GetInputEvents(adkWindow *pWindow, uint32_t *pInputEventCount, AdkInputEvent *pInputEvents);
void adkWindow_GetInputState(adkWindow *pWindow, AdkInputState *pInputState);
void adkWindow_MapKeys(char *pCharacters);
void adkWindow_GetScreenSize(AdkInstance *pInstance, adkVector2<uint32_t> *pScreenSize);

#endif // adkWindow_h__
