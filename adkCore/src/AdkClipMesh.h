#ifndef AdkClipMesh_h__
#define AdkClipMesh_h__

#include "adkTypedefs.h"
#include "AdkOctree.h"

struct AdkClipMesh;
struct adkAllocationCallbacks;
template<typename T> struct adkVector3;

typedef struct AdkClipMeshCreateInfo
{
  uint32_t maxTriangleCount;
} AdkClipMeshCreateInfo;

void adkClipMesh_Create(const AdkClipMeshCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkClipMesh **ppClipMesh);
void adkClipMesh_Destroy(AdkClipMesh **ppClipMesh);
bool adkClipMesh_Serialize(AdkClipMesh *pClipMesh, AdkBinaryWriter *pBinaryWriter);
bool adkClipMesh_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkClipMesh **ppClipMesh);
void adkClipMesh_Copy(AdkClipMesh *pSource, AdkClipMesh **ppCopy);
void adkClipMesh_SetTriangles(AdkClipMesh *pClipMesh, uint32_t triangleCount, adkVector3<float> *pTriangles);
bool adkClipMesh_SweepSphere(AdkClipMesh *pClipMesh, const adkVector3<float> &sphereStart, const adkVector3<float> &sphereVelocity, float sphereRadius, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer = nullptr);

#endif // AdkClipMesh_h__
