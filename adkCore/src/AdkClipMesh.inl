#include "AdkClipMesh.h"
#include "adkMath.h"
#include "AdkOctree.h"

typedef struct AdkClipMesh
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t triangleCount;
  uint32_t maxTriangleCount;
  adkVector3<float> *pTriangles;
  AdkOctree *pOctree;
  uint32_t layerIndex; //The layer in which this clip mesh exists.
  AdkClipMesh *pNext; //The next clip mesh in the linked list.
  bool added; //True if the clip mesh has been added to a clip body
} AdkClipMesh;