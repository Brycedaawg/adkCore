#ifndef adkBuffer_h__
#define adkBuffer_h__

#include "adkTypedefs.h"

struct AdkBuffer;
struct AdkInstance;
struct AdkQueue;

typedef enum AdkBufferCreateFlagBits
{
  ADK_BUFFER_CREATE_MUTABLE_BIT = 0x00000001,
} AdkBufferCreateFlagBits;
typedef AdkFlags AdkBufferCreateFlags;

typedef enum AdkBufferType
{
  ADK_BUFFER_TYPE_FLOAT32,
  ADK_BUFFER_TYPE_FLOAT32_2,
  ADK_BUFFER_TYPE_FLOAT32_3,
  ADK_BUFFER_TYPE_FLOAT32_4,
  ADK_BUFFER_TYPE_FLOAT32_4X4,
  ADK_BUFFER_TYPE_UINT8,
  ADK_BUFFER_TYPE_UINT32,
  ADK_BUFFER_TYPE_UINT32_INDEX,
} AdkBufferType;

typedef struct AdkBufferCopy
{
  size_t offset;
  size_t size;
  const void *pData;
} AdkBufferCopy;

typedef struct AdkBufferCreateInfo
{
  AdkBufferCreateFlags flags;
  AdkBufferType type;
  uint32_t elementCount;
  uint32_t copyCount;
  AdkBufferCopy *pCopies;
  AdkQueue *pTransferQueue;
} AdkBufferCreateInfo;

void adkBuffer_Create(AdkInstance *pInstance, const AdkBufferCreateInfo *pCreateInfo, AdkBuffer **ppBuffer);
void adkBuffer_Destroy(AdkInstance *pInstance, AdkBuffer **ppBuffer);
void adkBuffer_Set(AdkInstance *pInstance, AdkBuffer *pBuffer, uint32_t copyCount, const AdkBufferCopy *pCopies);
void adkBuffer_Map(AdkInstance *pInstance, AdkBuffer *pBuffer, void **ppData);
void adkBuffer_Unmap(AdkInstance *pInstance, AdkBuffer *pBuffer, void **ppData);

#endif // adkBuffer_h__
