#include "AdkRandom.h"
#include <stdint.h>

typedef struct AdkRandom
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t next;
} AdkRandom;

void adkRandom_Create(uint32_t seed, const adkAllocationCallbacks *pAllocator, AdkRandom **ppRandom)
{
  AdkRandom *pRandom = adkAllocTypeCall(AdkRandom, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pRandom->pAllocator = pAllocator;
  pRandom->next = (uint16_t)(0xACE1 + seed);

  *ppRandom = pRandom;
}

void adkRandom_Destroy(AdkRandom **ppRandom)
{
  AdkRandom *pRandom = *ppRandom;
  adkFreeCall(*ppRandom, pRandom->pAllocator);
}

uint16_t adkRandom_NextUint16(AdkRandom *pRandom)
{
  return (uint16_t)(adkRandom_NextUint32(pRandom) & 0x0000FFFF);
}

uint16_t adkRandom_NextUint16(AdkRandom *pRandom, uint16_t max)
{
  return adkRandom_NextUint16(pRandom) % max;
}

uint32_t adkRandom_NextUint32(AdkRandom *pRandom)
{
  const uint32_t a = 22695477;
  const uint32_t c = 1;
  const uint32_t m = 0xFFFFFFFF;
  pRandom->next = a * pRandom->next + m;

  //uint16_t bit = ((pRandom->next >> 0) ^ (pRandom->next >> 2) ^ (pRandom->next >> 3) ^ (pRandom->next >> 5)) & 1;
  //pRandom->next = (pRandom->next >> 1) | (bit << 15);
  return pRandom->next;

  //return (adkRandom_NextUint16(pRandom) << 16) | (adkRandom_NextUint16(pRandom) << 0);
}

uint32_t adkRandom_NextUint32(AdkRandom *pRandom, uint32_t max)
{
  return adkRandom_NextUint32(pRandom) % max;
}

float adkRandom_NextFloat32(AdkRandom *pRandom)
{
  return (float)adkRandom_NextUint32(pRandom) / 0xFFFFFFFF;
}