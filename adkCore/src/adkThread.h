#ifndef adkThread_h__
#define adkThread_h__

#include "adkMemory.h"

typedef uint32_t (ADKAPI_CALL AdkThreadFunction)(void *pData);

struct AdkThread;

void adkThread_Create(AdkThreadFunction *pBegin, void *pData, const adkAllocationCallbacks *pAllocator, AdkThread **ppThread);
void adkThread_Destroy(const adkAllocationCallbacks *pAllocator, AdkThread **ppThread);
void adkThread_Join(AdkThread *pThread);
void adkSleep(uint64_t timeout);
uint32_t adkCompareAndSwap(volatile uint32_t *pDestination, uint32_t compare, uint32_t swap);
uint64_t adkCompareAndSwap(volatile uint64_t *pDestination, uint64_t compare, uint64_t swap);

#endif // adkThread_h__
