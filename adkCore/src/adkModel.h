#ifndef adkModel_h__
#define adkModel_h__

#include "adkInstance.h"
#include "adkMath.h"

struct AdkModel;
struct AdkRenderPipeline;
struct AdkBuffer;
struct AdkMesh;
struct AdkAnimation;
struct AdkQueue;
struct AdkPhysicsMesh;
struct AdkClipMesh;
struct AdkBinaryReader;
struct AdkBinaryWriter;

typedef enum AdkModelCreateFlagBits
{
  ADK_MODEL_CREATE_PHYSICS_MESH_BIT = 0x00000001,
  ADK_MODEL_CREATE_CLIP_MESH_BIT = 0x00000002,
} AdkModelCreateFlagBits;
typedef AdkFlags AdkModelCreateFlags;

AdkBool32 adkModel_Create(AdkInstance *pInstance, AdkModelCreateFlags flags, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const char *pFilename, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel);
void adkModel_Destroy(AdkInstance *pInstance, AdkModel **ppModel);
bool adkModel_Serialize(AdkModel *pModel, AdkBinaryWriter *pBinaryWriter);
bool adkModel_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel);
bool adkModel_Deserialize(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const char *pFilename, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel);
uint32_t adkModel_GetMeshCount(AdkModel *pModel);
void adkModel_CreateMesh(AdkModel *pModel, uint32_t index, AdkMesh **ppMesh);
void adkModel_CreatePhysicsMesh(AdkModel *pModel, uint32_t index, AdkPhysicsMesh **ppPhysicsMesh);
void adkModel_CreateClipMesh(AdkModel *pModel, uint32_t index, AdkClipMesh **ppClipMesh);
void adkModel_CreateAnimation(AdkModel *pModel, AdkAnimation **ppAnimation);

#endif // adkModel_h__
