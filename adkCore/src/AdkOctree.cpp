#include "AdkOctree.h"
#include "adkMemory.h"
#include "adkAssert.h"
#include "AdkLineRendererUtil.h"
#include "AdkPhysicsUtil.h"
#include "AdkBinaryReader.h"
#include "AdkBinaryWriter.h"

static const uint32_t ADK_OCTREE_SERIALIZE_NODE_NULL = 0xFFFFFFFF;

typedef struct AdkOctreeTriangleInternal
{
  adkVector3<float> points[3]; //The three points that make up the triangle.
  adkVector3<float> min; //Bounding box min
  adkVector3<float> max; //Bounding box max
  AdkOctreeTriangleInternal *pNext; //The next node in the linked list of triangles.
} AdkOctreeTriangleInternal;

typedef struct AdkOctreeNode
{
  AdkOctreeNode *children[8];
  uint8_t childCount;
  uint32_t triangleCount;
  AdkOctreeTriangleInternal *pFirstTriangle; //A linked list of triangles.
  AdkOctreeTriangleInternal *pLastTriangle; //The end of the linked list of triangles.
  adkVector3<float> min;
  adkVector3<float> max;
} AdkOctreeNode;

typedef struct AdkOctree
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t maxTriCount;
  uint32_t depth;
  uint32_t shortestAxis;
  uint32_t nodeCount;
  AdkOctreeNode *pNodes;
  uint32_t triangleCount;
  AdkOctreeTriangleInternal *pTriangles;
} AdkOctree;

static inline uint32_t _GetMaxNodesForTriangles(uint32_t triangles)
{
  ADK_ASSERT(triangles != 0, "Triangle count must be greater than zero");
  uint32_t nodes = 0;

  while (true)
  {
    nodes += triangles;

    if (triangles == 1)
      break;

    triangles = triangles / 8 + (triangles % 8 != 0);
  }

  return nodes;
}

static inline uint32_t _GetOctreeDepthForTriangles(uint32_t triangles)
{
  uint32_t nodes = 1;
  uint32_t depth = 0;
  while (nodes < triangles)
  {
    nodes <<= 3;
    ++depth;
  }
  return depth;
}

void adkOctree_Create(uint32_t maxTriangleCount, const adkAllocationCallbacks *pAllocator, AdkOctree **ppOctree)
{
  AdkOctree *pOctree = adkAllocTypeCall(AdkOctree, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pOctree->pAllocator = pAllocator;
  pOctree->maxTriCount = maxTriangleCount;

  uint32_t maxDepth = _GetOctreeDepthForTriangles(maxTriangleCount);
  uint32_t maxNodeCount = 1 << (3 * maxDepth);
  pOctree->pNodes = adkAllocTypeCall(AdkOctreeNode, maxNodeCount, adkAF_Zero, pOctree->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pOctree->pTriangles = adkAllocTypeCall(AdkOctreeTriangleInternal, maxTriangleCount, adkAF_Zero, pOctree->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  *ppOctree = pOctree;
}

void adkOctree_Destroy(AdkOctree **ppOctree)
{
  AdkOctree *pOctree = *ppOctree;

  adkFreeCall(pOctree->pNodes, pOctree->pAllocator);
  adkFreeCall(pOctree->pTriangles, pOctree->pAllocator);

  adkFreeCall(*ppOctree, pOctree->pAllocator);
}

bool adkOctree_Serialize(AdkOctree *pOctree, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, pOctree->maxTriCount);
  adkBinaryWriter_Write(pBinaryWriter, pOctree->depth);
  adkBinaryWriter_Write(pBinaryWriter, pOctree->shortestAxis);
  adkBinaryWriter_Write(pBinaryWriter, pOctree->nodeCount);
  adkBinaryWriter_Write(pBinaryWriter, pOctree->triangleCount);

  for (uint32_t n = 0; n < pOctree->nodeCount; ++n)
  {
    AdkOctreeNode *pNode = &pOctree->pNodes[n];

    for (uint32_t c = 0; c < ADK_ARRAY_SIZE(pNode->children); ++c)
    {
      if (pNode->children[c] == nullptr)
        adkBinaryWriter_Write(pBinaryWriter, ADK_OCTREE_SERIALIZE_NODE_NULL);
      else
        adkBinaryWriter_Write(pBinaryWriter, (uint32_t)(pNode - pOctree->pNodes));
    }

    adkBinaryWriter_Write(pBinaryWriter, pNode->childCount);
    adkBinaryWriter_Write(pBinaryWriter, pNode->triangleCount);

    adkBinaryWriter_Write(pBinaryWriter, (uint32_t)(pNode->pFirstTriangle - pOctree->pTriangles));
    adkBinaryWriter_Write(pBinaryWriter, (uint32_t)(pNode->pLastTriangle - pOctree->pTriangles));

    adkBinaryWriter_Write(pBinaryWriter, pNode->min.x);
    adkBinaryWriter_Write(pBinaryWriter, pNode->min.y);
    adkBinaryWriter_Write(pBinaryWriter, pNode->min.z);

    adkBinaryWriter_Write(pBinaryWriter, pNode->max.x);
    adkBinaryWriter_Write(pBinaryWriter, pNode->max.y);
    adkBinaryWriter_Write(pBinaryWriter, pNode->max.z);
  }

  for (uint32_t tri = 0; tri < pOctree->triangleCount; ++tri)
  {
    AdkOctreeTriangleInternal *pTri = &pOctree->pTriangles[tri];

    for (uint32_t p = 0; p < ADK_ARRAY_SIZE(pTri->points); ++p)
    {
      adkBinaryWriter_Write(pBinaryWriter, pTri->points[p].x);
      adkBinaryWriter_Write(pBinaryWriter, pTri->points[p].y);
      adkBinaryWriter_Write(pBinaryWriter, pTri->points[p].z);

      adkBinaryWriter_Write(pBinaryWriter, (uint32_t)(pTri->pNext - pOctree->pTriangles));
    }
  }

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkOctree_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkOctree **ppOctree)
{
  bool success = false;
  AdkOctree *pOctree = adkAllocTypeCall(AdkOctree, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pOctree->pAllocator = pAllocator;

  adkBinaryReader_Read(pBinaryReader, &pOctree->maxTriCount);
  adkBinaryReader_Read(pBinaryReader, &pOctree->depth);
  adkBinaryReader_Read(pBinaryReader, &pOctree->shortestAxis);
  adkBinaryReader_Read(pBinaryReader, &pOctree->nodeCount);
  adkBinaryReader_Read(pBinaryReader, &pOctree->triangleCount);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  uint32_t maxNodeCount = 1 << (3 * pOctree->depth);
  pOctree->pNodes = adkAllocTypeCall(AdkOctreeNode, maxNodeCount, adkAF_Zero, pOctree->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pOctree->pTriangles = adkAllocTypeCall(AdkOctreeTriangleInternal, pOctree->maxTriCount, adkAF_Zero, pOctree->pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  for (uint32_t n = 0; n < pOctree->nodeCount; ++n)
  {
    AdkOctreeNode *pNode = &pOctree->pNodes[n];

    for (uint32_t c = 0; c < ADK_ARRAY_SIZE(pNode->children); ++c)
    {
      uint32_t nodeIndex = 0;
      adkBinaryReader_Read(pBinaryReader, &nodeIndex);

      if (nodeIndex == ADK_OCTREE_SERIALIZE_NODE_NULL)
        pNode->children[c] = nullptr;
      else
        pNode->children[c] = &pOctree->pNodes[nodeIndex];
    }

    adkBinaryReader_Read(pBinaryReader, &pNode->childCount);
    adkBinaryReader_Read(pBinaryReader, &pNode->triangleCount);

    uint32_t firstTriIndex = 0;
    uint32_t lastTriIndex = 0;
    adkBinaryReader_Read(pBinaryReader, &firstTriIndex);
    adkBinaryReader_Read(pBinaryReader, &lastTriIndex);
    pNode->pFirstTriangle = &pOctree->pTriangles[firstTriIndex];
    pNode->pLastTriangle = &pOctree->pTriangles[lastTriIndex];

    adkBinaryReader_Read(pBinaryReader, &pNode->min.x);
    adkBinaryReader_Read(pBinaryReader, &pNode->min.y);
    adkBinaryReader_Read(pBinaryReader, &pNode->min.z);

    adkBinaryReader_Read(pBinaryReader, &pNode->max.x);
    adkBinaryReader_Read(pBinaryReader, &pNode->max.y);
    adkBinaryReader_Read(pBinaryReader, &pNode->max.z);
  }

  for (uint32_t tri = 0; tri < pOctree->triangleCount; ++tri)
  {
    AdkOctreeTriangleInternal *pTri = &pOctree->pTriangles[tri];

    for (uint32_t p = 0; p < ADK_ARRAY_SIZE(pTri->points); ++p)
    {
      adkBinaryReader_Read(pBinaryReader, &pTri->points[p].x);
      adkBinaryReader_Read(pBinaryReader, &pTri->points[p].y);
      adkBinaryReader_Read(pBinaryReader, &pTri->points[p].z);

      uint32_t nextTriIndex = 0;
      adkBinaryReader_Read(pBinaryReader, &nextTriIndex);
      pTri->pNext = &pOctree->pTriangles[nextTriIndex];
    }
  }

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  *ppOctree = pOctree;
  success = true;

epilogue:
  if (!success)
  {
    adkFreeCall(pOctree->pNodes, pOctree->pAllocator);
    adkFreeCall(pOctree->pTriangles, pOctree->pAllocator);
    adkFreeCall(pOctree, pOctree->pAllocator);
  }

  return success;
}

//Calculates the min and max bounds of a set of points
static void _CalculateBounds(uint32_t pointCount, const adkVector3<float> *pPoints, adkVector3<float> *pMin, adkVector3<float> *pMax)
{
  *pMin = adkVector3<float>::create(+adkFloatMax);
  *pMax = adkVector3<float>::create(-adkFloatMax);

  for (uint32_t pt = 0; pt < pointCount; ++pt)
  {
    pMin->x = adkMin(pMin->x, pPoints[pt].x);
    pMin->y = adkMin(pMin->y, pPoints[pt].y);
    pMin->z = adkMin(pMin->z, pPoints[pt].z);

    pMax->x = adkMax(pMax->x, pPoints[pt].x);
    pMax->y = adkMax(pMax->y, pPoints[pt].y);
    pMax->z = adkMax(pMax->z, pPoints[pt].z);
  }
}

void adkOctree_Link(AdkOctree *pOctree, uint32_t triangleCount, const AdkOctreeTriangle *pTriangles, AdkLineRenderer * /*pLineRenderer*/ /*= nullptr*/)
{
  ADK_ASSERT(triangleCount <= pOctree->maxTriCount, "adkOctree_Link: triangle count must be <= maxTriangles, passed in during adkOctree_Create");

  //Calculate min and max bounds of the octree.
  adkVector3<float> boundsMin = adkVector3<float>::zero();
  adkVector3<float> boundsMax = adkVector3<float>::zero();
  _CalculateBounds(ADK_ARRAY_SIZE(pTriangles->points) * triangleCount, (adkVector3<float>*)pTriangles, &boundsMin, &boundsMax);

  //Reset the node hierarchy.
  pOctree->nodeCount = 0;
  pOctree->triangleCount = 0;
  pOctree->depth = _GetOctreeDepthForTriangles(triangleCount);

  adkVector3<float> extents = boundsMax - boundsMin;

  //The shortest axis of the octree's bounds is used to determine the depth of each node.
  uint32_t shortestAxis = 0;
  for (uint32_t ax = 1; ax < 3; ++ax)
    shortestAxis = extents[ax] < extents[shortestAxis] ? ax : shortestAxis;

  pOctree->shortestAxis = shortestAxis;

  //Initialize the root node of the hierarchy.
  AdkOctreeNode *pRootNode = &pOctree->pNodes[0];
  memset(pRootNode, 0, sizeof(*pRootNode));
  pRootNode->min = boundsMin;
  pRootNode->max = boundsMax;
  pOctree->nodeCount = 1;

  for (uint32_t i = 0; i < triangleCount; ++i)
  {
    AdkOctreeTriangleInternal *pTriangle = &pOctree->pTriangles[i];
    memcpy(pTriangle->points, pTriangles[i].points, sizeof(pTriangle->points));
    _CalculateBounds(ADK_ARRAY_SIZE(pTriangle->points), pTriangle->points, &pTriangle->min, &pTriangle->max);

    //The triangle belongs to the lowest octree layer who's nodes are twice as wide as the triangle's bounding sphere.
    //This means that a sphere testing against the layer, chosen by the same logic, will hit all the correct nodes provided the neighbor nodes are checked, too.
    uint32_t depth = 0;
    AdkOctreeNode *pNode = pRootNode;

    //Iterate to the bottom of the octree, adding nodes as needed.
    while (depth != pOctree->depth)
    {
      //Stop if the triangle's bounding sphere won't fit inside a child.
      adkVector3<float> triExtent = pTriangle->max - pTriangle->min;
      adkVector3<float> childExtent = 0.5f * (pNode->max - pNode->min);
      if (triExtent.x > childExtent.x || triExtent.y > childExtent.y || triExtent.z > childExtent.z)
        break;

      //Get the index of the child this tri should be added to.
      adkVector3<float> nodeCenter = 0.5f * (pNode->min + pNode->max);
      adkVector3<float> triCenter = 0.5f * (pTriangle->min + pTriangle->max);

      uint8_t childIndex = 0;
      childIndex = (childIndex << 1) | (triCenter.z > nodeCenter.z);
      childIndex = (childIndex << 1) | (triCenter.y > nodeCenter.y);
      childIndex = (childIndex << 1) | (triCenter.x > nodeCenter.x);

      //Get or add the child.
      if (pNode->children[childIndex] == nullptr)
      {
        AdkOctreeNode *pChild = &pOctree->pNodes[pOctree->nodeCount];
        memset(pChild, 0, sizeof(*pChild));

        //Calculate the child's bounds.
        pChild->min = pNode->min;

        if ((childIndex & 1) != 0)
          pChild->min.x += childExtent.x;
        if ((childIndex & 2) != 0)
          pChild->min.y += childExtent.y;
        if ((childIndex & 4) != 0)
          pChild->min.z += childExtent.z;

        pChild->max = pChild->min + childExtent;

        //Assign child to node and update metrics.
        pNode->children[childIndex] = pChild;
        ++pNode->childCount;
        ++pOctree->nodeCount;
      }

      //Move to the child.
      pNode = pNode->children[childIndex];
      ++depth;
    }

    //Add the triangle to the node's linked list of triangles.
    if (pNode->triangleCount == 0)
    {
      pNode->pFirstTriangle = &pOctree->pTriangles[pOctree->triangleCount];
      pNode->pLastTriangle = pNode->pFirstTriangle;
    }
    else
    {
      pNode->pLastTriangle->pNext = &pOctree->pTriangles[pOctree->triangleCount];
      pNode->pLastTriangle = pNode->pLastTriangle->pNext;
    }

    ++pNode->triangleCount;
    ++pOctree->triangleCount;
  }
}

static bool _PlaneOnLine1d(float plane, float point, float direction, float *pDistance)
{
  if (adkFabsf(direction) <= 0.005f)
    return false;

  *pDistance = (plane - point) / direction;
  return true;
}

static bool _IsPointInsideBox(const adkVector3<float> &point, const adkVector3<float>& min, const adkVector3<float>& max)
{
  if (point.x < min.x || point.x > max.x)
    return false;

  if (point.y < min.y || point.y > max.y)
    return false;

  if (point.z < min.z || point.z > max.z)
    return false;

  return true;
}

static bool _DoesSweepingSphereIntersectBox(const adkVector3<float> &spherePoint, const adkVector3<float> &sphereDirection, float sphereDistance, float sphereRadius, const adkVector3<float> &min, const adkVector3<float> &max)
{
  //Sphere sweep on box = line on box with sides extended by sphere radius.
  adkVector3<float> extendMin = min - sphereRadius;
  adkVector3<float> extendMax = max + sphereRadius;

  if (_IsPointInsideBox(spherePoint, extendMin, extendMax))
    return true;

  const uint32_t AXIS_COUNT = 3;
  const uint32_t SIDES_PER_AXIS = 2;
  const uint32_t ITERATION_COUNT = AXIS_COUNT * SIDES_PER_AXIS;

  //Check if any of the six sides intersect line segment.
  for (uint32_t i = 0; i < ITERATION_COUNT; ++i)
  {
    uint32_t axis = i / SIDES_PER_AXIS;
    uint32_t side = i % SIDES_PER_AXIS;

    //Check both sides of box on each axis.
    float plane = side == 0 ? extendMin[axis] : extendMax[axis];
    float distance;

    //Find where line intersects plane.
    if (!_PlaneOnLine1d(plane, spherePoint[axis], sphereDirection[axis], &distance))
      continue;

    //Check intersection is within line segment.
    if (distance < 0.f || distance > sphereDistance)
      continue;

    //Check intersection is within plane extents.
    adkVector3<float> intersect = spherePoint + distance * sphereDirection;
    uint32_t axisB = (axis + 1) % AXIS_COUNT;
    uint32_t axisC = (axis + 2) % AXIS_COUNT;

    if (intersect[axisB] < extendMin[axisB] || intersect[axisB] > extendMax[axisB])
      continue;

    if (intersect[axisC] < extendMin[axisC] || intersect[axisC] > extendMax[axisC])
      continue;

    return true;
  }

  return false;
}

//Recursively descend the octree, doing sphere collision checks on each node. Discontinue recursion on nodes that don't touch the sphere. returns the index of the closest triangle hit.
//bsPoint is bounding sphere point and bsRadius is bounding sphere radius.
static bool _SweepSphereOnNode(const adkVector3<float> &start, const adkVector3<float> &direction, float distance, float radius, AdkOctreeNode *pNode, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer, uint32_t layer)
{
  bool collided = false;

  //Check to see if a sphere is within one half node extent of the given node. This works because a bounding sphere's diameter will never exceed its node's
  //shortest extent. Consequently, the sphere will never overflow to a neighbor node more than half the sphere's radius.
  adkVector3<float> halfExtent = 0.5f * (pNode->max - pNode->min);
  if (!_DoesSweepingSphereIntersectBox(start, direction, distance, radius, pNode->min - halfExtent, pNode->max + halfExtent))
    return false;

  //Check triangles in this node.
  for (AdkOctreeTriangleInternal *pTri = pNode->pFirstTriangle; pTri != nullptr; pTri = pTri->pNext)
  {
    if (!_DoesSweepingSphereIntersectBox(start, direction, distance, radius, pTri->min, pTri->max))
      continue;

    AdkCollision<float> collision;
    if (adkPhysicsUtil_SweepingSphereOnTriangle(start, distance * direction, radius, pTri->points[0], pTri->points[1], pTri->points[2], &collision) && collision.resolutionT < pCollision->resolutionT)
    {
      pCollision->contact = collision.contact;
      pCollision->resolution = collision.resolution;
      pCollision->resolutionT = collision.resolutionT;
      memcpy(pCollision->triangle, pTri->points, sizeof(pCollision->triangle));
      collided = true;
    }
  }

  //Check children.
  for (uint32_t c = 0; c < ADK_ARRAY_SIZE(pNode->children); ++c)
  {
    if (pNode->children[c] == nullptr)
      continue;

    if (_SweepSphereOnNode(start, direction, distance, radius, pNode->children[c], pCollision, pLineRenderer, layer + 1))
      collided = true;
  }

  return collided;
}

bool adkOctree_SweepSphere(AdkOctree *pOctree, const adkVector3<float> &start, const adkVector3<float> &velocity, float radius, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  memset(pCollision, 0, sizeof(*pCollision));
  pCollision->resolutionT = adkFloatMax;

  float distance = velocity.magnitude();
  adkVector3<float> direction;

  if (distance > 0.005f)
    direction = velocity / distance;
  else
    direction = adkVector3<float>::create(1.f, 0.f, 0.f);

  bool collided = _SweepSphereOnNode(start, direction, distance, radius, &pOctree->pNodes[0], pCollision, pLineRenderer, 0);

  return collided;
}