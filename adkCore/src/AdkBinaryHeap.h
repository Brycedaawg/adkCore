#ifndef AdkBinaryHeap_h__
#define AdkBinaryHeap_h__

#include "adkTypedefs.h"

struct AdkBinaryHeap;
struct adkAllocationCallbacks;

typedef struct AdkBinaryHeapCreateInfo
{
  uint32_t depth;
  size_t elementSize;
} AdkBinaryHeapCreateInfo;

void adkBinaryHeap_Create(const AdkBinaryHeapCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryHeap **ppBinaryHeap);
void adkBinaryHeap_Destroy(AdkBinaryHeap **ppBinaryHeap);
void adkBinaryHeap_Push(AdkBinaryHeap *pBinaryHeap, float score, const void *pElement);
bool adkBinaryHeap_Pop(AdkBinaryHeap *pBinaryHeap, float *pScore, void *pElement);
void adkBinaryHeap_Peek(AdkBinaryHeap *pBinaryHeap, uint32_t index, void *pElement);
void adkBinaryHeap_Clear(AdkBinaryHeap *pBinaryHeap);
uint32_t adkBinaryHeap_GetCount(AdkBinaryHeap *pBinaryHeap);

#endif // AdkBinaryHeap_h__
