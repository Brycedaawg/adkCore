#include "adkTextureReader.inl"
#include "lodePng.h"

AdkBool32 adkTextureReader_Read(const char *pFilename, adkVector2<uint32_t> *pExtent, void **ppColors)
{
  unsigned char *pColors;
  if (lodepng_decode32_file(&pColors, &pExtent->x, &pExtent->y, pFilename) != 0)
    return ADK_FALSE;

  (*ppColors) = pColors;
  return ADK_TRUE;
}