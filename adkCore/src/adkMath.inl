#ifndef adkMath_h__
#define adkMath_h__

#include <math.h>

#include "adkTypedefs.h"
#include "adkLog.h"
#include <float.h>

#define adkPI 3.14159265358979323846
#define adkPIf 3.14159265358979323846f
#define adk2PI 6.28318530717958647693
#define adk2PIf 6.28318530717958647693f
#define adkDeg2Rad(degrees) ((degrees) * adkPI / 180.0)
#define adkRad2Deg(radians) ((radians) * 180.0 / adkPI)
#define adkDeg2Radf(degrees) ((float)((degrees) * float(adkPI) / 180.f))
#define adkRad2Degf(radians) ((float)((radians) * 180.f / float(adkPI)))
#define adkSinf(x) sinf(x)
#define adkCosf(x) cosf(x)
#define adkTanf(x) tanf(x)
#define adkAsinf(x) asinf(x)
#define adkAcosf(x) acosf(x)
#define adkAtanf(x) atanf(x)
#define adkAtan2f(x, y) atan2f(x, y)
#define adkSqrt(x) sqrt(x)
#define adkSqrtf(x) sqrtf(x)
#define adkFmodf(x, y) fmodf(x, y)
#define adkFModf(x, y) fmodf(x, y)
#define adkFloor(x) floorf(x)
#define adkCeil(x) ceilf(x)
#define adkRound(x) roundf(x)
#define adkAbs(x) abs(x)
#define adkFabs(x) fabs(x)
#define adkFabsf(x) fabsf(x)
#define adkFloatMax FLT_MAX
#define adkFloatMin FLT_MIN
#define adkDoubleMax FLT_MAX
#define adkDoubleMin FLT_MIN

template <typename T>
struct AdkRange
{
  T start;
  T length;

  static inline AdkRange<T> create(T start, T length) { return { start, length }; }

  template <typename U>
  static inline AdkRange<T> create(AdkRange<U> range) { return { T(range.start), T(range.length) }; }
};

template <typename T>
struct adkRect
{
  T x, y, width, height;

  static inline adkRect<T> create(T x, T y, T width, T height) { return { x, y, width, height }; }

  template <typename U>
  static inline adkRect<T> create(const adkRect<U> &r) { return { T(r.x), T(r.y), T(r.width), T(r.height) }; }

  static inline adkRect zero() { return { T(0), T(0), T(0), T(0) }; }
};

template <typename T>
struct adkBox
{
  T x, y, z, width, height, depth;

  static inline adkBox<T> create(T x, T y, T z, T width, T height, T depth) { return{ x, y, z, width, height, depth }; }

  template <typename U>
  static inline adkBox<T> create(const adkBox<U> &b) { return{ T(b.x), T(b.y), T(b.z), T(b.width), T(b.height), T(b.depth) }; }

  static inline adkBox zero() { return { T(0), T(0), T(0), T(0), T(0), T(0) }; }
};

template <typename T>
struct adkVector2
{
  T x, y;

  static inline adkVector2<T> create(T s) { return { s, s }; }
  static inline adkVector2<T> create(T x, T y) { adkVector2<T> o = { x, y }; return o; }

  template<typename U>
  inline T operator[](const U &i) const { return ((T*)this)[i]; }
  template<typename U>
  inline T& operator[](const U &i) { return ((T*)this)[i]; }
  template <typename U>
  static inline adkVector2<T> create(const adkVector2<U> &v) { return { T(v.x), T(v.y) }; }
  static inline adkVector2<T> zero() { return { T(0), T(0) }; }
  static inline adkVector2<T> one() { return{ T(1), T(1) }; }
  static inline T dot(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return lhs.x * rhs.x + lhs.y * rhs.y; }
  static inline T distance(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return (lhs - rhs).magnitude(); }
  static inline T squareDistance(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return (lhs - rhs).squareMagnitude(); }
  inline T squareMagnitude() const { return x * x + y * y; }
  inline T magnitude() const { return T(adkSqrt(double(squareMagnitude()))); }
  inline adkVector2<T> normalized() const { T mi = T(1) / magnitude(); return { mi * x, mi * y }; }
  
};

template<typename T> static inline bool operator==(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
template <typename T> static inline bool operator!=(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return !(lhs == rhs); }
template<typename T> static inline adkVector2<T> operator+(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x + rhs.x, lhs.y + rhs.y }; }
template<typename T> static inline adkVector2<T> operator+(const adkVector2<T> &v) { return{ +v.x, +v.y }; }
template<typename T> static inline adkVector2<T> operator+(const adkVector2<T> &lhs, const T &rhs) { return { lhs.x + rhs, lhs.y + rhs }; }
template<typename T> static inline adkVector2<T> operator+(const T &lhs, const adkVector2<T> &rhs) { return { lhs + rhs.x, lhs + rhs.y }; }
template<typename T> static inline adkVector2<T> operator+=(adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x += rhs.x, lhs.y += rhs.y }; }
template<typename T> static inline adkVector2<T> operator+=(adkVector2<T> &lhs, const T &rhs) { return { lhs.x += rhs, lhs.y += rhs, lhs.z += rhs }; }
template<typename T> static inline adkVector2<T> operator-(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x - rhs.x, lhs.y - rhs.y }; }
template<typename T> static inline adkVector2<T> operator-(const adkVector2<T> &v) { return { -v.x, -v.y }; }
template<typename T> static inline adkVector2<T> operator-(const adkVector2<T> &lhs, const T &rhs) { return { lhs.x - rhs, lhs.y - rhs }; }
template<typename T> static inline adkVector2<T> operator-(const T &lhs, const adkVector2<T> &rhs) { return { lhs - rhs.x, lhs - rhs.y }; }
template<typename T> static inline adkVector2<T> operator-=(adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x -= rhs.x, lhs.y -= rhs.y }; }
template<typename T> static inline adkVector2<T> operator-=(adkVector2<T> &lhs, const T &rhs) { return { lhs.x -= rhs, lhs -= rhs }; }
template<typename T> static inline adkVector2<T> operator*(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x * rhs.x, lhs.y * rhs.y }; }
template<typename T> static inline adkVector2<T> operator*(const adkVector2<T> &v, const T s) { return { v.x * s, v.y * s }; }
template<typename T> static inline adkVector2<T> operator*(const T s, const adkVector2<T> &v) { return { s * v.x, s * v.y }; }
template<typename T> static inline adkVector2<T> operator*=(adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x *= rhs.x, lhs.y *= rhs.y }; }
template<typename T> static inline adkVector2<T> operator*=(adkVector2<T> &lhs, const T &rhs) { return { lhs.x *= rhs, lhs.y *= rhs }; }
template<typename T> static inline adkVector2<T> operator/(const adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x / rhs.x, lhs.y / rhs.y }; }
template<typename T> static inline adkVector2<T> operator/(const adkVector2<T> &v, const T s) { return { v.x / s, v.y / s }; }
template<typename T> static inline adkVector2<T> operator/(const T s, const adkVector2<T> &v) { return { s / v.x, s / v.y }; }
template<typename T> static inline adkVector2<T> operator/=(adkVector2<T> &lhs, const adkVector2<T> &rhs) { return { lhs.x /= rhs.x, lhs.y /= rhs.y }; }
template<typename T> static inline adkVector2<T> operator/=(adkVector2<T> &lhs, const T &rhs) { return { lhs.x /= rhs, lhs.y /= rhs}; }

template <typename T>
struct adkVector3
{
  T x, y, z;

  static inline adkVector3<T> create(T s) { return { s, s, s }; }
  static inline adkVector3<T> create(T x, T y, T z) { return { x, y, z }; }

  template<typename U>
  inline T operator[](const U &i) const { return ((T*)this)[i]; }
  template<typename U>
  inline T& operator[](const U &i) { return ((T*)this)[i]; }
  template <typename U>
  static inline adkVector3<T> create(const adkVector3<U> &v) { return { T(v.x), T(v.y), T(v.z) }; }
  static inline adkVector3<T> zero() { return { T(0), T(0), T(0) }; }
  static inline adkVector3<T> one() { return{ T(1), T(1), T(1) }; }
  static inline T dot(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z; }
  static inline adkVector3<T> cross(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.y * rhs.z - lhs.z * rhs.y, -(lhs.x * rhs.z - lhs.z * rhs.x), lhs.x * rhs.y - lhs.y * rhs.x }; }
  static inline adkVector3<T> reflect(const adkVector3<T> &incident, const adkVector3<T> &normal) { return incident + T(2) * adkVector3<T>::dot(-incident, normal) * normal; }
  static inline T distance(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return (lhs - rhs).magnitude(); }
  static inline T squareDistance(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return (lhs - rhs).squareMagnitude(); }
  inline adkVector2<T> toVector2() const { return { x, y }; }
  inline T squareMagnitude() const { return x * x + y * y + z * z; }
  inline T magnitude() const { return T(adkSqrt(double(squareMagnitude()))); }
  inline adkVector3<T> normalized() const { T mi = T(1) / magnitude(); return { mi * x, mi * y, mi * z }; }
};

inline float adkVector3<float>::magnitude() const { return adkSqrtf(squareMagnitude()); }

template<typename T> static inline bool operator==(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z; }
template <typename T> static inline bool operator!=(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return !(lhs == rhs); }
template<typename T> static inline adkVector3<T> operator+(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z }; }
template<typename T> static inline adkVector3<T> operator+(const adkVector3<T> &lhs, const T &rhs) { return { lhs.x + rhs, lhs.y + rhs, lhs.z + rhs }; }
template<typename T> static inline adkVector3<T> operator+(const T &lhs, const adkVector3<T> &rhs) { return { lhs + rhs.x, lhs + rhs.y, lhs + rhs.z }; }
template<typename T> static inline adkVector3<T> operator+=(adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x += rhs.x, lhs.y += rhs.y, lhs.z += rhs.z }; }
template<typename T> static inline adkVector3<T> operator+=(adkVector3<T> &lhs, const T &rhs) { return { lhs.x += rhs, lhs.y += rhs, lhs.z += rhs }; }
template<typename T> static inline adkVector3<T> operator-(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z }; }
template<typename T> static inline adkVector3<T> operator-(const adkVector3<T> &lhs, const T &rhs) { return { lhs.x - rhs, lhs.y - rhs, lhs.z - rhs }; }
template<typename T> static inline adkVector3<T> operator-(const T &lhs, const adkVector3<T> &rhs) { return { lhs - rhs.x, lhs - rhs.y, lhs - rhs.z }; }
template<typename T> static inline adkVector3<T> operator-(const adkVector3<T> &v) { return { -v.x, -v.y, -v.z }; }
template<typename T> static inline adkVector3<T> operator-=(adkVector3<T> &lhs, const adkVector3<T> &rhs) { { return { lhs.x -= rhs.x, lhs.y -= rhs.y, lhs.z -= rhs.z }; } }
template<typename T> static inline adkVector3<T> operator-=(adkVector3<T> &lhs, const T &rhs) { return { lhs.x -= rhs, lhs.y -= rhs, lhs.z -= rhs}; }
template<typename T> static inline adkVector3<T> operator*(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z }; }
template<typename T> static inline adkVector3<T> operator*(const adkVector3<T> &v, const T s) { return { v.x * s, v.y * s, v.z * s }; }
template<typename T> static inline adkVector3<T> operator*(const T s, const adkVector3<T> &v) { return { s * v.x, s * v.y, s * v.z }; }
template<typename T> static inline adkVector3<T> operator*=(adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x *= rhs.x, lhs.y *= rhs.y, lhs.z *= rhs.z }; }
template<typename T> static inline adkVector3<T> operator*=(adkVector3<T> &lhs, const T &rhs) { return { lhs.x *= rhs, lhs.y *= rhs, lhs.z *= rhs }; }
template<typename T> static inline adkVector3<T> operator/(const adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z }; }
template<typename T> static inline adkVector3<T> operator/(const adkVector3<T> &v, const T s) { return { v.x / s, v.y / s, v.z / s }; }
template<typename T> static inline adkVector3<T> operator/(const T s, const adkVector3<T> &v) { return { s / v.x, s / v.y, s / v.z }; }
template<typename T> static inline adkVector3<T> operator/=(adkVector3<T> &lhs, const adkVector3<T> &rhs) { return { lhs.x /= rhs.x, lhs.y /= rhs.y, lhs.z /= rhs.z }; }
template<typename T> static inline adkVector3<T> operator/=(adkVector3<T> &lhs, const T &rhs) { return { lhs.x /= rhs, lhs.y /= rhs, lhs.z /= rhs }; }

template <typename T>
struct adkVector4
{
  T x, y, z, w;

  static inline adkVector4<T> create(T s) { return { s, s, s, s }; }
  static inline adkVector4<T> create(T x, T y, T z, T w) { adkVector4<T> o = { x, y, z, w }; return o; }

  template<typename U>
  inline T operator[](const U &i) const { return ((T*)this)[i]; }
  template<typename U>
  inline T& operator[](const U &i) { return ((T*)this)[i]; }
  template <typename U>
  static inline adkVector4<T> create(const adkVector4<U> &v) { return { T(v.x), T(v.y), T(v.z), T(v.w) }; }
  static inline adkVector4<T> zero() { return { T(0), T(0), T(0), T(0) }; }
  static inline adkVector4<T> one() { return{ T(1), T(1), T(1), T(1) }; }
  static inline adkVector4<T> identity() { return { T(0), T(0), T(0), T(1) }; }
  static inline T dot(adkVector4<T> &lhs, adkVector4<T> &rhs) { return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w; }
  static inline T distance(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return (lhs - rhs).magnitude(); }
  static inline T squareDistance(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return (lhs - rhs).squareMagnitude(); }
  inline adkVector3<T> toVector3() const { return { x, y, z }; }
  inline adkVector4<T> homogenize() const { T r = T(1) / w; return { r * x, r * y, r * z, T(1) }; }
  inline T squareMagnitude() const { return x * x + y * y + z * z + w * w; }
  inline T magnitude() const { return T(adkSqrt(double(squareMagnitude()))); }
  inline adkVector4<T> normalized() const { T mi = T(1) / magnitude(); return { mi * x, mi * y, mi * z, mi * w }; }
};

inline float adkVector4<float>::magnitude() const { return adkSqrtf(squareMagnitude()); }

template<typename T> static inline bool operator==(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.w == rhs.w; }
template<typename T> static inline bool operator!=(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return !(lhs == rhs); }
template<typename T> static inline adkVector4<T> operator+(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w }; }
template<typename T> static inline adkVector4<T> operator+(const adkVector4<T> &lhs, const T &rhs) { return { lhs.x + rhs, lhs.y + rhs, lhs.z + rhs, lhs.w + rhs }; }
template<typename T> static inline adkVector4<T> operator+(const T &lhs, const adkVector4<T> &rhs) { return { lhs + rhs.x, lhs + rhs.y, lhs + rhs.z, lhs + rhs.w }; }
template<typename T> static inline adkVector4<T> operator+=(adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x += rhs.x, lhs.y += rhs.y, lhs.z += rhs.z, lhs.w += rhs.w }; }
template<typename T> static inline adkVector4<T> operator+=(adkVector4<T> &lhs, const T &rhs) { return { lhs.x += rhs, lhs.y += rhs, lhs.z += rhs, lhs.w += rhs }; }
template<typename T> static inline adkVector4<T> operator-(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w }; }
template<typename T> static inline adkVector4<T> operator-(const adkVector4<T> &lhs, const T &rhs) { return { lhs.x - rhs, lhs.y - rhs, lhs.z - rhs, lhs.w - rhs }; }
template<typename T> static inline adkVector4<T> operator-(const T &lhs, const adkVector4<T> &rhs) { return { lhs - rhs.x, lhs - rhs.y, lhs - rhs.z, lhs - rhs.w }; }
template<typename T> static inline adkVector4<T> operator-(const adkVector4<T> &v) { return { -v.x, -v.y, -v.z, -v.w }; }
template<typename T> static inline adkVector4<T> operator-=(adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x -= rhs.x, lhs.y -= rhs.y, lhs.z -= rhs.z, lhs.w -= rhs.w }; }
template<typename T> static inline adkVector4<T> operator-=(adkVector4<T> &lhs, const T &rhs) { return { lhs.x -= rhs, lhs.y -= rhs, lhs.z -= rhs, lhs.w -= rhs }; }
template<typename T> static inline adkVector4<T> operator*(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w }; }
template<typename T> static inline adkVector4<T> operator*(const adkVector4<T> &v, const T s) { return { v.x * s, v.y * s, v.z * s, v.w * s }; }
template<typename T> static inline adkVector4<T> operator*(const T s, const adkVector4<T> &v) { return { s * v.x, s * v.y, s * v.z, s * v.w }; }
template<typename T> static inline adkVector4<T> operator*=(adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x *= rhs.x, lhs.y *= rhs.y, lhs.z *= rhs.z, lhs.w *= rhs.w }; }
template<typename T> static inline adkVector4<T> operator*=(adkVector4<T> &lhs, const T &rhs) { return { lhs.x *= rhs, lhs.y *= rhs, lhs.z *= rhs, lhs.w *= rhs }; }
template<typename T> static inline adkVector4<T> operator/(const adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z, lhs.w / rhs.w }; }
template<typename T> static inline adkVector4<T> operator/(const adkVector4<T> &v, const T s) { return { v.x / s, v.y / s, v.z / s, v.w / s }; }
template<typename T> static inline adkVector4<T> operator/(const T s, const adkVector4<T> &v) { return { s / v.x, s / v.y, s / v.z, s / v.w }; }
template<typename T> static inline adkVector4<T> operator/=(adkVector4<T> &lhs, const adkVector4<T> &rhs) { return { lhs.x /= rhs.x, lhs.y /= rhs.y, lhs.z /= rhs.z, lhs.w /= rhs.w }; }
template<typename T> static inline adkVector4<T> operator/=(adkVector4<T> &lhs, const T &rhs) { return { lhs.x /= rhs, lhs.y /= rhs, lhs.z /= rhs, lhs.w /= rhs }; }

template <typename T>
struct adkQuaternion
{
  T x, y, z, w;

  template <typename U>
  static inline adkQuaternion<T> create(const adkQuaternion<U> &q) { adkQuaternion o = { T(q.x), T(q.y), T(q.z), T(q.w) }; return o; }

  static inline adkQuaternion<T> create(const T &angle, const adkVector3<T> &axis)
  {
    T aDiv2 = angle / T(2);
    T sinADiv2 = adkSinf(aDiv2);
    adkQuaternion<T> o = { axis.x * sinADiv2, axis.y * sinADiv2, axis.z * sinADiv2, adkCosf(aDiv2) };
    return o;
  }

  static inline adkQuaternion<T> create(const T &x, const T &y, const T &z)
  {
    T xDiv2 = x / T(2);
    T yDiv2 = y / T(2);
    T zDiv2 = z / T(2);
    T cx = T(adkCosf(xDiv2));
    T cy = T(adkCosf(yDiv2));
    T cz = T(adkCosf(zDiv2));
    T sx = T(adkSinf(xDiv2));
    T sy = T(adkSinf(yDiv2));
    T sz = T(adkSinf(zDiv2));

    adkQuaternion<T> o =
    {
      sx * cy * cz - cx * sy * sz,
      cx * sy * cz + sx * cy * sz,
      cx * cy * sz - sx * sy * cz,
      cx * cy * cz + sx * sy * sz,
    };

    return o;
  }

  static inline adkQuaternion<T> create(const adkVector3<T> &euler) { return adkQuaternion::create(euler.x, euler.y, euler.z); }

  static inline adkQuaternion<T> identity() { return { T(0), T(0), T(0), T(1) }; }

  inline T squareMagnitude() const { return x * x + y * y + z * z + w * w; }
  inline T magnitude() const { return T(adkSqrt(double(squareMagnitude()))); }
  inline adkQuaternion<T> normalized() const { T mi = T(1) / magnitude(); return { mi * x, mi * y, mi * z, mi * w }; }

  static inline adkQuaternion<T> create(const adkVector3<T> &fromDirection, const adkVector3<T> &toDirection)
  {
    T dot, angle;
    adkVector3<T> axis;

    dot = adkVector3<T>::dot(fromDirection, toDirection);

    if (dot > T(0.999999))
      return adkQuaternion<T>::identity();

    if (dot < T(-0.999999))
    {
      axis = adkVector3<T>::cross(fromDirection, adkVector3<T>::create(T(0), T(1), T(0)));
      axis = adkVector3<T>::cross(axis, fromDirection);
      axis.normalized();
      return adkQuaternion<T>::create(adkPIf, axis);
    }
    
    axis = adkVector3<T>::cross(fromDirection, toDirection).normalized();
    angle = adkAcosf(dot);
    return adkQuaternion<T>::create(angle, axis);
  }

  static inline adkQuaternion<T> lookAt(const adkVector3<T> &lookAt, const adkVector3<T> &up)
  {
    adkVector3<T> forward = adkVector3<T>::create(T(0), T(0), T(1));
    adkVector3<T> direction = lookAt.normalized();
    adkQuaternion<T> rotationA = adkQuaternion<T>::create(forward, direction);

    adkVector3<T> right = adkVector3<T>::cross(direction, up).normalized();
    adkVector3<T> upPerpendicularToDirection = adkVector3<T>::cross(right, direction).normalized();
    adkVector3<T> newUp = rotationA.apply(adkVector3<T>::create(T(0), T(1), T(0)));

    adkQuaternion<T> rotationB = adkQuaternion<T>::create(newUp, upPerpendicularToDirection);
    return rotationA * rotationB;
  }

  static inline T dot(const adkQuaternion<T> &lhs, const adkQuaternion &rhs)
  {
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
  }

  static inline adkQuaternion<T> slerp(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs, const T &t)
  {
    T dot = adkQuaternion<T>::dot(lhs, rhs);
    adkQuaternion<T> v0 = lhs;
    adkQuaternion<T> v1 = rhs;

    if (dot < T(0))
    {
      v1 = -v1;
      dot = -dot;
    }

    if (dot > T(0.999995))
      return (v0 + t * (v1 - v0)).normalized();

    T lhsToRhsTheta = adkAcosf(dot);
    T lhsToResultTheta = lhsToRhsTheta * t;
    T sinLhsToRhsTheta = adkSinf(lhsToRhsTheta);
    T sinLhsToResultTheta = adkSinf(lhsToResultTheta);

    T s0 = adkCosf(lhsToResultTheta) - dot * sinLhsToResultTheta / sinLhsToRhsTheta;
    T s1 = sinLhsToResultTheta / sinLhsToRhsTheta;

    return s0 * v0 + s1 * v1;
  }

  static inline adkQuaternion<T> rotateTowards(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs, const T &angle)
  {
    T dot = adkQuaternion<T>::dot(lhs, rhs);
    adkQuaternion<T> v0 = lhs;
    adkQuaternion<T> v1 = rhs;

    if (dot < T(0))
    {
      v1 = -v1;
      dot = -dot;
    }

    if (dot > T(0.999995))
    {
      return v1;
    }

    T lhsToRhsTheta = adkAcosf(dot);
    T lhsToResultTheta = adkMin(angle, lhsToRhsTheta);
    T sinLhsToRhsTheta = adkSinf(lhsToRhsTheta);
    T sinLhsToResultTheta = adkSinf(lhsToResultTheta);

    T s0 = adkCosf(lhsToResultTheta) - dot * sinLhsToResultTheta / sinLhsToRhsTheta;
    T s1 = sinLhsToResultTheta / sinLhsToRhsTheta;

    return s0 * v0 + s1 * v1;
  }

  inline adkVector3<T> apply(const T &vX, const T &vY, const T &vZ) const
  {
    adkVector3<T> v = adkVector3<T>::create(vX, vY, vZ);
    adkVector3<T> qv = adkVector3<T>::create(this->x, this->y, this->z);

    T qvDotV = adkVector3<T>::dot(qv, v);
    T qvDotQv = adkVector3<T>::dot(qv, qv);
    adkVector3<T> qvCrossV = adkVector3<T>::cross(qv, v);

    return T(2) * qvDotV * qv
      + (w * w - qvDotQv) * v
      + T(2) * w * qvCrossV;
  }

  inline adkVector3<T> apply(const adkVector3<T> &v) const { return apply(v.x, v.y, v.z); }

  inline adkVector3<T> eulerAngles() const
  {
    adkVector3<T> output;
    T ySquared = y * y;

    T t0 = T(2) * (w * x + y * z);
    T t1 = T(1) - T(2) * (x * x + ySquared);
    output.x = adkAtan2f(t0, t1);

    T t2 = T(2) * (w * y - z * x);
    t2 = adkClamp(t2, T(-1), T(1));
    output.y = adkAsinf(t2);

    T t3 = T(2) * (w * z + x * y);
    T t4 = T(1) - T(2) * (ySquared + z * z);
    output.z = adkAtan2f(t3, t4);

    return output;
  }
};

inline float adkQuaternion<float>::magnitude() const { return adkSqrtf(squareMagnitude()); }

template <typename T> static inline bool operator==(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs) { return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.w == rhs.w; }
template <typename T> static inline bool operator!=(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs) { return !(lhs == rhs); }
template <typename T> static inline adkQuaternion<T> operator+(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs) { return{ lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w }; }
template <typename T> static inline adkQuaternion<T> operator-(const adkQuaternion<T> &q) { return { -q.x, -q.y, -q.z, -q.w }; }
template <typename T> static inline adkQuaternion<T> operator-(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs) { return { lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w }; }
template <typename T> static inline adkQuaternion<T> operator*(const adkQuaternion<T> &lhs, const T &rhs) { return { lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs }; }
template <typename T> static inline adkQuaternion<T> operator*(const T &lhs, const adkQuaternion<T> &rhs) { return { lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w }; }

template <typename T>
inline adkQuaternion<T> operator*(const adkQuaternion<T> &lhs, const adkQuaternion<T> &rhs)
{
  adkQuaternion<T> o;
  o.w = lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z;
  o.x = lhs.x * rhs.w + lhs.w * rhs.x + lhs.z * rhs.y - lhs.y * rhs.z;
  o.y = lhs.y * rhs.w - lhs.z * rhs.x + lhs.w * rhs.y + lhs.x * rhs.z;
  o.z = lhs.z * rhs.w + lhs.y * rhs.x - lhs.x * rhs.y + lhs.w * rhs.z;
  return o;
}

//Matrices ordered like this:
//
//	0	4	8	12
//
//	1	5	9	13
//
//	2	6	10	14
//
//	3	7	11	15
//
//Matrices use row major ordering

//Left handed coordinate system
//
// |Y|
//	
//	|            |Z|
//	|
//	|           /
//	|          /
//	|         /
//	|        /
//	|       /
//	|      /
//	|     /
//	|    /
//	|   /
//	|  /
//	| /
//	|/______________________________	|X|

template <typename T>
struct adkMatrix4x4
{
  T a[16];

  inline adkMatrix4x4<T> operator*(const adkMatrix4x4<T> &m) const
  {
    adkMatrix4x4 o =
    {
      a[0] * m.a[0] + a[4] * m.a[1] + a[8] * m.a[2] + a[12] * m.a[3],
      a[1] * m.a[0] + a[5] * m.a[1] + a[9] * m.a[2] + a[13] * m.a[3],
      a[2] * m.a[0] + a[6] * m.a[1] + a[10] * m.a[2] + a[14] * m.a[3],
      a[3] * m.a[0] + a[7] * m.a[1] + a[11] * m.a[2] + a[15] * m.a[3],
      a[0] * m.a[4] + a[4] * m.a[5] + a[8] * m.a[6] + a[12] * m.a[7],
      a[1] * m.a[4] + a[5] * m.a[5] + a[9] * m.a[6] + a[13] * m.a[7],
      a[2] * m.a[4] + a[6] * m.a[5] + a[10] * m.a[6] + a[14] * m.a[7],
      a[3] * m.a[4] + a[7] * m.a[5] + a[11] * m.a[6] + a[15] * m.a[7],
      a[0] * m.a[8] + a[4] * m.a[9] + a[8] * m.a[10] + a[12] * m.a[11],
      a[1] * m.a[8] + a[5] * m.a[9] + a[9] * m.a[10] + a[13] * m.a[11],
      a[2] * m.a[8] + a[6] * m.a[9] + a[10] * m.a[10] + a[14] * m.a[11],
      a[3] * m.a[8] + a[7] * m.a[9] + a[11] * m.a[10] + a[15] * m.a[11],
      a[0] * m.a[12] + a[4] * m.a[13] + a[8] * m.a[14] + a[12] * m.a[15],
      a[1] * m.a[12] + a[5] * m.a[13] + a[9] * m.a[14] + a[13] * m.a[15],
      a[2] * m.a[12] + a[6] * m.a[13] + a[10] * m.a[14] + a[14] * m.a[15],
      a[3] * m.a[12] + a[7] * m.a[13] + a[11] * m.a[14] + a[15] * m.a[15],
    };

    return o;
  }

  inline adkVector4<T> operator*(const adkVector4<T> &v) const
  {
    adkVector4<T> o =
    {
      a[0] * v.x + a[4] * v.y + a[8] * v.z + a[12] * v.w,
      a[1] * v.x + a[5] * v.y + a[9] * v.z + a[13] * v.w,
      a[2] * v.x + a[6] * v.y + a[10] * v.z + a[14] * v.w,
      a[3] * v.x + a[7] * v.y + a[11] * v.z + a[15] * v.w,
    };

    return o;
  }

  inline adkMatrix4x4<T> inverse() const
  {
    //Determinants of all 2x2 matrix combinations.
    T d1_6_5_2 = a[1] * a[6] - a[5] * a[2];
    T d1_7_5_3 = a[1] * a[7] - a[5] * a[3];
    T d1_10_9_2 = a[1] * a[10] - a[9] * a[2];
    T d1_11_9_3 = a[1] * a[11] - a[9] * a[3];
    T d1_14_13_2 = a[1] * a[14] - a[13] * a[2];
    T d1_15_13_3 = a[1] * a[15] - a[13] * a[3];

    T d2_7_6_3 = a[2] * a[7] - a[6] * a[3];
    T d2_11_10_3 = a[2] * a[11] - a[10] * a[3];
    T d2_15_14_3 = a[2] * a[15] - a[14] * a[3];

    T d5_10_9_6 = a[5] * a[10] - a[9] * a[6];
    T d5_11_9_7 = a[5] * a[11] - a[9] * a[7];
    T d5_14_13_6 = a[5] * a[14] - a[13] * a[6];
    T d5_15_13_7 = a[5] * a[15] - a[13] * a[7];

    T d6_11_10_7 = a[6] * a[11] - a[10] * a[7];
    T d6_15_14_7 = a[6] * a[15] - a[14] * a[7];

    T d9_14_13_10 = a[9] * a[14] - a[13] * a[10];
    T d9_15_13_11 = a[9] * a[15] - a[13] * a[11];

    T d10_15_14_11 = a[10] * a[15] - a[14] * a[11];

    adkMatrix4x4<T> o;

    //Determinants of minor matrices made from excluding column i and row j.
    o.a[0] = a[5] * d10_15_14_11 - a[9] * d6_15_14_7 + a[13] * d6_11_10_7;
    o.a[1] = a[4] * d10_15_14_11 - a[8] * d6_15_14_7 + a[12] * d6_11_10_7;
    o.a[2] = a[4] * d9_15_13_11 - a[8] * d5_15_13_7 + a[12] * d5_11_9_7;
    o.a[3] = a[4] * d9_14_13_10 - a[8] * d5_14_13_6 + a[12] * d5_10_9_6;
    o.a[4] = a[1] * d10_15_14_11 - a[9] * d2_15_14_3 + a[13] * d2_11_10_3;
    o.a[5] = a[0] * d10_15_14_11 - a[8] * d2_15_14_3 + a[12] * d2_11_10_3;
    o.a[6] = a[0] * d9_15_13_11 - a[8] * d1_15_13_3 + a[12] * d1_11_9_3;
    o.a[7] = a[0] * d9_14_13_10 - a[8] * d1_14_13_2 + a[12] * d1_10_9_2;
    o.a[8] = a[1] * d6_15_14_7 - a[5] * d2_15_14_3 + a[13] * d2_7_6_3;
    o.a[9] = a[0] * d6_15_14_7 - a[4] * d2_15_14_3 + a[12] * d2_7_6_3;
    o.a[10] = a[0] * d5_15_13_7 - a[4] * d1_15_13_3 + a[12] * d1_7_5_3;
    o.a[11] = a[0] * d5_14_13_6 - a[4] * d1_14_13_2 + a[12] * d1_6_5_2;
    o.a[12] = a[1] * d6_11_10_7 - a[5] * d2_11_10_3 + a[9] * d2_7_6_3;
    o.a[13] = a[0] * d6_11_10_7 - a[4] * d2_11_10_3 + a[8] * d2_7_6_3;
    o.a[14] = a[0] * d5_11_9_7 - a[4] * d1_11_9_3 + a[8] * d1_7_5_3;
    o.a[15] = a[0] * d5_10_9_6 - a[4] * d1_10_9_2 + a[8] * d1_6_5_2;

    //Coefficients of previous matrix.
    o.a[1] = -o.a[1];
    o.a[3] = -o.a[3];
    o.a[4] = -o.a[4];
    o.a[6] = -o.a[6];
    o.a[9] = -o.a[9];
    o.a[11] = -o.a[11];
    o.a[12] = -o.a[12];
    o.a[14] = -o.a[14];

    //Determinant
    T d = a[0] * o.a[0] + a[4] * o.a[4] + a[8] * o.a[8] + a[12] * o.a[12];

    //Transposition of previous matrix.
    T c1 = o.a[1];
    T c2 = o.a[2];
    T c3 = o.a[3];
    T c6 = o.a[6];
    T c7 = o.a[7];
    T c11 = o.a[11];

    o.a[1] = o.a[4];
    o.a[2] = o.a[8];
    o.a[3] = o.a[12];
    o.a[6] = o.a[9];
    o.a[7] = o.a[13];
    o.a[11] = o.a[14];

    o.a[4] = c1;
    o.a[8] = c2;
    o.a[12] = c3;
    o.a[9] = c6;
    o.a[13] = c7;
    o.a[14] = c11;

    //determinant reciprical * previous matrix.
    T dr = 1 / d;

    o.a[0] *= dr;
    o.a[1] *= dr;
    o.a[2] *= dr;
    o.a[3] *= dr;
    o.a[4] *= dr;
    o.a[5] *= dr;
    o.a[6] *= dr;
    o.a[7] *= dr;
    o.a[8] *= dr;
    o.a[9] *= dr;
    o.a[10] *= dr;
    o.a[11] *= dr;
    o.a[12] *= dr;
    o.a[13] *= dr;
    o.a[14] *= dr;
    o.a[15] *= dr;
    
    return o;
  }

  template <typename U>
  static inline adkMatrix4x4<T> create(const adkMatrix4x4<U> &m)
  {
    adkMatrix4x4 o =
    {
      T(m.a[0]), T(m.a[1]), T(m.a[2]), T(m.a[3]),
      T(m.a[4]), T(m.a[5]), T(m.a[6]), T(m.a[7]),
      T(m.a[8]), T(m.a[9]), T(m.a[10]), T(m.a[11]),
      T(m.a[12]), T(m.a[13]), T(m.a[14]), T(m.a[15]),
    };

    return o;
  }

  static inline adkMatrix4x4<T> identity()
  {
    adkMatrix4x4 o =
    {
      T(1), T(0), T(0), T(0),
      T(0), T(1), T(0), T(0),
      T(0), T(0), T(1), T(0),
      T(0), T(0), T(0), T(1),
    };

    return o;
  }

  static inline adkMatrix4x4<T> translate(T x, T y, T z)
  {
    adkMatrix4x4 o =
    {
      T(1), T(0), T(0), 0,
      T(0), T(1), T(0), 0,
      T(0), T(0), T(1), 0,
      T(x), T(y), T(z), T(1),
    };

    return o;
  }

  static inline adkMatrix4x4<T> translate(adkVector3<T> v)
  {
    return adkMatrix4x4<T>::translate(v.x, v.y, v.z);
  }

  static inline adkMatrix4x4<T> rotateQuaternion(const adkQuaternion<T> &q)
  {
    T xy = q.x * q.y;
    T xz = q.x * q.z;
    T yz = q.y * q.z;

    T wx = q.w * q.x;
    T wy = q.w * q.y;
    T wz = q.w * q.z;

    T xx = q.x * q.x;
    T yy = q.y * q.y;
    T zz = q.z * q.z;

    adkMatrix4x4<float> o;

    o.a[0]  = T(1) - T(2) * yy - T(2) * zz;
    o.a[1]  =        T(2) * xy + T(2) * wz;
    o.a[2]  =        T(2) * xz - T(2) * wy;
    o.a[3]  = 0.f;

    o.a[4]  =        T(2) * xy - T(2) * wz;
    o.a[5]  = T(1) - T(2) * xx - T(2) * zz;
    o.a[6]  =        T(2) * yz + T(2) * wx;
    o.a[7]  = 0.f;

    o.a[8]  =        T(2) * xz + T(2) * wy;
    o.a[9]  =        T(2) * yz - T(2) * wx;
    o.a[10] = T(1) - T(2) * xx - T(2) * yy;
    o.a[11] = 0.f;

    o.a[12] = 0.f;
    o.a[13] = 0.f;
    o.a[14] = 0.f;
    o.a[15] = 1.f;

    return o;
  }

  static inline adkMatrix4x4<T> rotateEulerX(T x)
  {
    T s = T(adkSinf(x));
    T c = T(adkCosf(x));
    return
    {
      1, 0, 0, 0,
      0, c, s, 0,
      0, -s, c, 0,
      0, 0, 0, 1,
    };
  }

  static inline adkMatrix4x4<T> rotateEulerY(T y)
  {
    T s = T(adkSinf(y));
    T c = T(adkCosf(y));
    return
    {
      c, 0, -s, 0,
      0, 1, 0, 0,
      s, 0, c, 0,
      0, 0, 0, 1,
    };
  }

  static inline adkMatrix4x4<T> rotateEulerZ(T z)
  {
    T s = T(adkSinf(z));
    T c = T(adkCosf(z));
    return
    {
      c, s, 0, 0,
      -s, c, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1,
    };
  }

  static inline adkMatrix4x4<T> rotateEuler(T x, T y, T z)
  {
    (void)x;
    (void)y;
    (void)z;
    return adkMatrix4x4<T>::rotateEulerZ(z) * adkMatrix4x4<T>::rotateEulerY(y) * adkMatrix4x4<T>::rotateEulerX(x);
    /*T sX = T(adkSinf(x));
    T sY = T(adkSinf(y));
    T sZ = T(adkSinf(z));
    T cX = T(adkCosf(x));
    T cY = T(adkCosf(y));
    T cZ = T(adkCosf(z));

    adkMatrix4x4 o =
    {
      cZ * cY,					sZ,			cZ * -sY,					T(0),
      sZ * -cY * cX + sY * sX,	cZ * cX,	sZ * sY * cX + cY * sX,		T(0),
      sZ * cY * sX + sY * cX,		-cZ * sX,	sZ * -sY * sX + cY * cX,	T(0),
      T(0),						T(0),		T(0),						T(1),
    };

    //adkMatrix4x4 o =
    //{
    //  cY * cZ, -cX * sZ + sX * sY * cZ, sX * sZ + cX * sY * cZ, 0,
    //  cY * sZ, cX * -cZ + sX * sY * sZ, -sX * cZ + cX * sY * sZ, 0,
    //  -sY, sX * cY, cX * cY, 0,
    //  0, 0, 0, 1,
    //};

    return o;*/
  }

  static inline adkMatrix4x4<T> rotateEuler(adkVector3<T> r)
  {
    return adkMatrix4x4<T>::rotateEuler(r.x, r.y, r.z);
  }

  static inline adkMatrix4x4<T> scaleUniform(T s)
  {
    adkMatrix4x4 o =
    {
      T(s), T(0), T(0), T(0),
      T(0), T(s), T(0), T(0),
      T(0), T(0), T(s), T(0),
      T(0), T(0), T(0), T(1),
    };

    return o;
  }

  static inline adkMatrix4x4<T> scaleNonUniform(T x, T y, T z)
  {
    adkMatrix4x4 o =
    {
      T(x), T(0), T(0), T(0),
      T(0), T(y), T(0), T(0),
      T(0), T(0), T(z), T(0),
      T(0), T(0), T(0), T(1),
    };

    return o;
  };

  static inline adkMatrix4x4<T> scaleNonUniform(const adkVector3<T> &s) { return adkMatrix4x4<T>::scaleNonUniform(s.x, s.y, s.z); }

  static inline adkMatrix4x4<T> perspective(T fov, T aspect, T zNear, T zFar)
  {
    T zRange = zNear - zFar;
    T tanHalfFov = adkTanf(fov * T(0.5));

    adkMatrix4x4 o =
    {
      T(1) / (aspect * tanHalfFov), T(0), T(0), T(0),
      T(0), T(1) / tanHalfFov, T(0), T(0),
      T(0), T(0), -zFar / zRange, T(1),
      T(0), T(0), zFar * zNear / zRange, T(0),
    };

    return o;
  }

  static inline adkMatrix4x4<T> lookAt(const adkVector3<T> &lookAt, const adkVector3<T> &up)
  {
    adkVector3<T> d = lookAt.normalized();
    adkVector3<T> s = adkVector3<T>::cross(d, up).normalized();

    adkMatrix4x4 o =
    {
      T(s.x), T(s.y), T(s.z), T(0),
      T(up.x), T(up.y), T(up.z), T(0),
      T(d.x), T(d.y), T(d.z), T(0),
      T(0), T(0), T(0), T(1),
    };

    return o;
  }

  static inline adkMatrix4x4<T> transpose(const adkMatrix4x4<T> &m)
  {
    return
    {
      m.a[0], m.a[4], m.a[8], m.a[12],
      m.a[1], m.a[5], m.a[9], m.a[13],
      m.a[2], m.a[6], m.a[10], m.a[14],
      m.a[3], m.a[7], m.a[11], m.a[15],
    };
  }
};

template<typename T>
struct adkCubicBezier
{
  T a, b, c, d;
  T p0, p1, p2, p3;

  static inline adkCubicBezier<T> create(const T &p0, const T &p1, const T &p2, const T &p3)
  {
    adkCubicBezier<T> o;
    o.a = -p0 + T(3) * p1 - T(3) * p2 + p3;
    o.b = T(3) * p0 - T(6) * p1 + T(3) * p2;
    o.c = -T(3) * p0 + T(3) * p1;
    o.d = p0;

    o.p0 = p0;
    o.p1 = p1;
    o.p2 = p2;
    o.p3 = p3;

    return o;
  }

  inline T at(const T &t)
  {
    T t2 = t * t;
    T t3 = t2 * t;
    return t3 * a + t2 * b + t * c + d;
  }
};

template<typename T>
struct adkCubicBezier2
{
  adkVector2<T> a, b, c, d;

  static inline adkCubicBezier2<T> create(const adkVector2<T> &p0, const adkVector2<T> &p1, const adkVector2<T> &p2, const adkVector2<T> &p3)
  {
    adkCubicBezier2<T> o;
    o.a = -p0 + T(3) * p1 - T(3) * p2 + p3;
    o.b = T(3) * p0 - T(6) * p1 + T(3) * p2;
    o.c = -p0 + T(3) * p1;
    o.d = p0;
    return o;
  }

  inline adkVector2<T> at(const T &t)
  {
    T t2 = t * t;
    T t3 = t2 * t;
    return t3 * a + t2 * b + t * c + d;
  }
};

template<typename T>
struct adkCubicBezier3
{
  adkVector3<T> a, b, c, d;

  static inline adkCubicBezier3<T> create(const adkVector3<T> &p0, const adkVector3<T> &p1, const adkVector3<T> &p2, const adkVector3<T> &p3)
  {
    adkCubicBezier3<T> o;
    o.a = -p0 + T(3) * p1 - T(3) * p2 + p3;
    o.b = T(3) * p0 - T(6) * p1 + T(3) * p2;
    o.c = -p0 + T(3) * p1;
    o.d = p0;
    return o;
  }

  inline adkVector3<T> at(const T &t)
  {
    T t2 = t * t;
    T t3 = t2 * t;
    return t3 * a + t2 * b + t * c + d;
  }
};

template<typename T> static inline T adkMin(const T &a, const T &b) { return a < b ? a : b; }
template<typename T> static inline T adkMax(const T &a, const T &b) { return a < b ? b : a; }
template<typename T> static inline T adkFmod(const T &lhs, const T &rhs) { return lhs - rhs * adkFloor(lhs / rhs); }
template<typename T> static inline T adkRepeat(const T &value, const T &lowerLimit, const T &upperLimit)
{
  T range = upperLimit - lowerLimit;
  return lowerLimit + (value % range + range) % range;
}
static inline float adkRepeat(const float &value, const float &lowerLimit, const float &upperLimit) { return lowerLimit + adkFmod((value - lowerLimit), (upperLimit - lowerLimit)); }
template<typename T> static inline T adkClamp(const T &value, const T &min, const T &max) { return adkMin(max, adkMax(value, min)); }
template<typename T, typename U> static inline T adkLerp(const T &a, const T &b, const U &t) { return (U(1) - t) * a + t * b; }
template<typename T, typename U> static inline T adkInverseLerp(const T &a, const T &b, const U &value)
{
  return (value - a) / (b - a);
}

template<typename T> static inline void adkSwap(T *pA, T *pB)
{
  T buffer = *pA;
  *pA = *pB;
  *pB = buffer;
}

template<typename T> static inline T adkLog2(T x)
{
  T o = 0;
  while (x >>= 1)
    ++o;
  return o;
}

template<typename T> static inline int32_t adkRoundToInt(T x)
{
  return (int32_t)(adkFloor(x) + T(0.5));
}

#endif // adkMath_h__