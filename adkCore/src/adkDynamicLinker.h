#ifndef adkDynamicLinker_h__
#define adkDynamicLinker_h__

#include "adkTypedefs.h"

struct AdkDynamicLinker;
struct AdkInstance;

typedef struct AdkDynamicLinkerCreateInfo
{
  char *pFilename;
} AdkDynamicLinkerCreateInfo;

AdkResult adkDynamicLinker_Create(AdkInstance *pInstance, const AdkDynamicLinkerCreateInfo *pCreateInfo, AdkDynamicLinker **ppDynamicLinker);
void adkDynamicLinker_Destroy(AdkInstance *pInstance, AdkDynamicLinker **ppDynamicLinker);
AdkResult adkDynamicLinker_GetSymbolAddress(AdkDynamicLinker *pDynamicLinker, const char *pSymbol, void *pAddress);

#endif // adkDynamicLinker_h__
