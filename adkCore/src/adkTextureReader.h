#ifndef adkTextureReader_h__
#define adkTextureReader_h__

#include "adkMath.h"

AdkBool32 adkTextureReader_Read(const char *pFilename, adkVector2<uint32_t> *pExtent, void **ppColors);

#endif // adkTextureReader_h__
