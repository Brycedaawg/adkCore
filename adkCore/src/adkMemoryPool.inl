#ifndef adkMemoryPool_h__
#define adkMemoryPool_h__

#include "adkMemory.h"
#include "vulkan.h"

struct AdkMemoryPool;

typedef struct AdkDeviceMemory
{
  VkDeviceMemory vkDeviceMemory;
  size_t offset;
  size_t size;
  void *pMapped;
} AdkDeviceMemory;

void adkMemoryPool_Create(const adkAllocationCallbacks *pAllocator, AdkMemoryPool **ppMemoryPool);
void adkMemoryPool_Destroy(AdkMemoryPool **ppMemoryPool);
void adkMemoryPool_AllocateDeviceMemory(AdkMemoryPool *pMemoryPool, VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, const adkAllocationCallbacks *pAllocator, size_t allocationSize, size_t alignment, uint32_t memoryTypeIndex, AdkDeviceMemory *pDeviceMemory);
void adkMemoryPool_FreeDeviceMemory(AdkMemoryPool *pMemoryPool, AdkDeviceMemory *pDeviceMemory);
void adkMemoryPool_FlushDeviceMemory(AdkDeviceMemory *pMemory, VkDevice device);

#endif // adkMemoryPool_h__