#include "adkFontReader.inl"
#include "adkMemory.h"
#include "adkInstance.inl"

void adkFontReader_Create(AdkInstance *pInstance, const AdkFontReaderCreateInfo * /*pCreateInfo*/, AdkFontReader **ppFontReader)
{
  AdkFontReader *pFontReader = adkAllocTypeCall(AdkFontReader, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  FT_Init_FreeType(&pFontReader->freeTypeLibrary);

  (*ppFontReader) = pFontReader;
}

void adkFontReader_Destroy(AdkInstance *pInstance, AdkFontReader **ppFontReader)
{
  AdkFontReader *pFontReader = (*ppFontReader);

  FT_Done_FreeType(pFontReader->freeTypeLibrary);
  adkFreeCall(pFontReader, pInstance->pAllocationCallbacks);

  (*ppFontReader) = nullptr;
}