#include "AdkTime.h"
#include "adkMemory.h"
#include "adkInstance.inl"
#include <time.h>
#include "sys/timeb.h"
#include <profileapi.h>

typedef struct AdkTime
{
  const adkAllocationCallbacks *pAllocator;
  clock_t startTime;
} AdkTime;

void adkTime_Create(const AdkTimeCreateInfo * /*pCreateInfo*/, const adkAllocationCallbacks *pAllocator, AdkTime **ppTime)
{
  AdkTime *pTime = adkAllocTypeCall(AdkTime, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pTime->pAllocator = pAllocator;

  pTime->startTime = clock();

  (*ppTime) = pTime;
}

void adkTime_Destroy(AdkTime **ppTime)
{
  AdkTime *pTime = (*ppTime);

  adkFreeCall(*ppTime, pTime->pAllocator);
  (*ppTime) = nullptr;
}

void adkTime_GetSeconds(AdkTime *pTime, float *pSeconds)
{
  (*pSeconds) = (float)(clock() - pTime->startTime) / CLOCKS_PER_SEC;
}

uint64_t adkTime_GetMilliseconds(AdkTime *pTime)
{
  return (int64_t)(clock() - pTime->startTime) * 1000 / CLOCKS_PER_SEC;
}

void adkTime_Reset(AdkTime *pTime)
{
  pTime->startTime = clock();
}