#ifndef adkPerlin3d_h__
#define adkPerlin3d_h__

#include "adkInstance.h"
#include "adkMath.h"
#include "adkMemory.h"
#include "adkTexture.h"

struct AdkPerlin3d;

void adkPerlin3d_Create(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pComputeQueue, AdkQueue *pTransferQueue, const adkVector3<uint32_t> &extent, const adkAllocationCallbacks *pAllocator, AdkPerlin3d **ppPerlin3d);
void adkPerlin3d_Destroy(AdkPerlin3d **ppPerlin3d);
void adkPerlin3d_Generate(AdkPerlin3d *pPerlin3d, uint32_t seed, uint32_t octaves, float frequency, float persistence, const adkVector3<float> &sampleOffset, const adkVector3<uint32_t> &storeOffset, const adkVector3<uint32_t> &storeExtent);
void adkPerlin3d_GetTexture(AdkPerlin3d *pPerlin3d, AdkTexture **ppTexture);

#endif // adkPerlin3d_h__