#ifndef AdkOctree_h__
#define AdkOctree_h__

#include <stdint.h>
#include "adkMath.h"

template<typename T> struct adkVector3;

struct AdkOctree;
struct adkAllocationCallbacks;
struct AdkLineRenderer;
struct AdkBinaryReader;
struct AdkBinaryWriter;

typedef struct AdkOctreeTriangle
{
  adkVector3<float> points[3];
} AdkOctreeTriangle;

typedef struct AdkSphereSweepCollision
{
  adkVector3<float> contact;
  adkVector3<float> resolution;
  float resolutionT;
  adkVector3<float> triangle[3];
} AdkSphereSweepCollision;

void adkOctree_Create(uint32_t maxTriangleCount, const adkAllocationCallbacks *pAllocator, AdkOctree **ppOctree);
void adkOctree_Destroy(AdkOctree **ppOctree);
bool adkOctree_Serialize(AdkOctree *pOctree, AdkBinaryWriter *pBinaryWriter);
bool adkOctree_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkOctree **ppOctree);
void adkOctree_Link(AdkOctree *pOctree, uint32_t triangleCount, const AdkOctreeTriangle *pTriangles, AdkLineRenderer *pLineRenderer = nullptr);
bool adkOctree_SweepSphere(AdkOctree *pOctree, const adkVector3<float> &start, const adkVector3<float> &velocity, float radius, AdkSphereSweepCollision *pCollision, AdkLineRenderer *pLineRenderer = nullptr);

#endif // AdkOctree_h__
