#include "adkModel.h"
#include "fbxsdk.h"
#include "adkLog.h"
#include "adkFile.h"
#include "adkRenderPipeline.h"
#include "adkBuffer.h"
#include "adkMesh.h"
#include "AdkAnimation.h"
#include "adkString.h"
#include <inttypes.h>
#include "AdkPhysicsMesh.h"
#include "adkAssert.h"
#include "AdkClipMesh.h"
#include "AdkBinaryWriter.h"
#include "AdkBinaryReader.h"

static const uint32_t PRE_ROTATION_ANIMATION_INDEX_UNUSED = 0xFFFFFFFF;
static const size_t ANIMATION_NODE_NAMES_LENGTH = 1024 * 1024;
static const uint32_t EXTRA_ANIMATION_GRAPHS_PER_NODE = 3; //X, Y, and Z graphs for pre-rotation
static const uint32_t EXTRA_ANIMATION_KEY_FRAMES_PER_NODE = 3; //X, Y, and Z graphs for pre-rotation

typedef struct AdkModel
{
  AdkInstance *pInstance;
  AdkQueue *pGraphicsQueue;
  AdkQueue *pTransferQueue;
  const adkAllocationCallbacks *pAllocator;
  uint32_t meshCount;
  AdkMesh **ppMeshes;
  AdkPhysicsMesh **ppPhysicsMeshes;
  AdkClipMesh **ppClipMeshes;
  AdkAnimation *pAnimation;
} AdkModel;

typedef struct AdkModelFbxNodeData
{
  fbxsdk::FbxNode *pFbxNode;
  bool isMesh;
  bool isBone;
  uint32_t index;
  uint32_t boneIndex;
  AdkAnimationNode preRotationNode; //Pre-rotation
  AdkAnimationNode translationAndScaleNode; //Animated translation and animated scale
} AdkModelFbxNodeData;

typedef struct AdkModelFbxInfo
{
  AdkInstance *pInstance;
  AdkModelCreateFlags flags;
  uint32_t nodeCount;
  uint32_t graphCount;
  uint32_t keyFrameCount;
  uint32_t meshCount;
  uint32_t boneCount;
  float frameRate;
  AdkAnimationNode *pAnimationNodes;
  AdkAnimationGraph *pAnimationGraphs;
  AdkKeyFrame *pAnimationKeyFrames;
  AdkModelFbxNodeData *pNodes;
  uint32_t animationNodeNamesIndex;
  char *pAnimationNodeNames;
  fbxsdk::FbxAnimLayer *pAnimLayer;
  fbxsdk::FbxMesh **ppFbxMeshes;
} AdkModelFbxInfo;

typedef enum AdkModelTransformPropertyType
{
  ADK_MODEL_TRANSFORM_PROPERTY_TYPE_TRANSLATION,
  ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION,
  ADK_MODEL_TRANSFORM_PROPERTY_TYPE_SCALE,
  ADK_MODEL_TRANSFORM_PROPERTY_TYPE_COUNT,
} AdkModelTransformPropertyType;

static void _InitializeFbxNodeData(AdkModelFbxInfo *pFbxInfo, uint32_t nodeIndex, fbxsdk::FbxNode *pFbxNode)
{
  AdkModelFbxNodeData *pNode = &pFbxInfo->pNodes[nodeIndex];
  pNode->index = nodeIndex;
  pNode->pFbxNode = pFbxNode;
  pFbxNode->SetUserDataPtr(pNode);
}

static void _CreateFbxInfo(fbxsdk::FbxNode *pRootNode, fbxsdk::FbxAnimLayer *pAnimLayer, const adkAllocationCallbacks *pAllocator, AdkModelFbxInfo **ppFbxInfo)
{
  AdkModelFbxInfo *pFbxInfo = adkAllocTypeCall(AdkModelFbxInfo, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pFbxInfo->pAnimLayer = pAnimLayer;
  pFbxInfo->nodeCount = pRootNode->GetChildCount(true) + 1;
  pFbxInfo->pNodes = adkAllocTypeCall(AdkModelFbxNodeData, pFbxInfo->nodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pFbxInfo->pAnimationNodes = adkAllocTypeCall(AdkAnimationNode, pFbxInfo->nodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  _InitializeFbxNodeData(pFbxInfo, 0, pRootNode);

  for (uint32_t start = 0, end = 1; start != end; ++start)
  {
    AdkModelFbxNodeData *pNode = &pFbxInfo->pNodes[start];
    int childCount = pNode->pFbxNode->GetChildCount();

    for (int i = 0; i < childCount; ++i)
    {
      _InitializeFbxNodeData(pFbxInfo, end, pNode->pFbxNode->GetChild(i));
      ++end;
    }
  }

  pFbxInfo->pAnimationNodeNames = adkAllocTypeCall(char, ANIMATION_NODE_NAMES_LENGTH, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  *ppFbxInfo = pFbxInfo;
}

static void _DestroyFbxInfo(AdkModelFbxInfo **ppFbxInfo)
{
  adkFree((*ppFbxInfo)->pNodes);
  adkFree((*ppFbxInfo)->pAnimationNodeNames);
  adkFree((*ppFbxInfo)->pAnimationGraphs);
  adkFree((*ppFbxInfo)->pAnimationKeyFrames);
  adkFree((*ppFbxInfo)->ppFbxMeshes);
  adkFree(*ppFbxInfo);
  *ppFbxInfo = nullptr;
}

static const char* _StoreAnimationNodeName(AdkModelFbxInfo *pFbxInfo, const char *pPrepend, const char *pName)
{
  char *pNameBuffer = &pFbxInfo->pAnimationNodeNames[pFbxInfo->animationNodeNamesIndex];
  pFbxInfo->animationNodeNamesIndex += adkSprintf(pNameBuffer, (size_t)(ANIMATION_NODE_NAMES_LENGTH - pFbxInfo->animationNodeNamesIndex), "%s%s", pPrepend, pName) + 1;
  return pNameBuffer;
}

static void _DetectBonesAndGetMeshCount(AdkModelFbxInfo *pFbxInfo)
{
  for (uint32_t i = 0; i < pFbxInfo->nodeCount; ++i)
  {
    fbxsdk::FbxNode *pFbxNode = pFbxInfo->pNodes[i].pFbxNode;
    int attributeCount = pFbxNode->GetNodeAttributeCount();
    for (int j = 0; j < attributeCount; ++j)
    {
      fbxsdk::FbxNodeAttribute *pAttribute = pFbxNode->GetNodeAttributeByIndex(j);
      fbxsdk::FbxNodeAttribute::EType type = pAttribute->GetAttributeType();

      if (type == fbxsdk::FbxNodeAttribute::EType::eMesh)
      {
        ++pFbxInfo->meshCount;

        fbxsdk::FbxMesh *pMesh = (fbxsdk::FbxMesh*)pAttribute;
        int deformerCount = pMesh->GetDeformerCount();
        for (int k = 0; k < deformerCount; ++k)
        {
          fbxsdk::FbxDeformer *pDeformer = pMesh->GetDeformer(k);
          if (pDeformer->GetDeformerType() == fbxsdk::FbxDeformer::EDeformerType::eSkin)
          {
            fbxsdk::FbxSkin *pSkin = (fbxsdk::FbxSkin*)pDeformer;
            int clusterCount = pSkin->GetClusterCount();
            for (int l = 0; l < clusterCount; ++l)
            {
              fbxsdk::FbxCluster *pCluster = pSkin->GetCluster(l);
              AdkModelFbxNodeData *pBoneFbxNodeData = (AdkModelFbxNodeData*)pCluster->GetLink()->GetUserDataPtr();
              pBoneFbxNodeData->isBone = true;
            }
          }
        }
      }
    }
  }
}

static void _IndexBonesAndGetBoneCount(AdkModelFbxInfo *pFbxInfo)
{
  uint32_t boneIndex = 0;

  for (uint32_t i = 0; i < pFbxInfo->nodeCount; ++i)
  {
    AdkModelFbxNodeData *pNode = &pFbxInfo->pNodes[i];
    if (pNode->isBone)
    {
      pNode->boneIndex = boneIndex;
      ++boneIndex;
    }
  }

  pFbxInfo->boneCount = boneIndex;
}

static void _DetectGraphAndKeyFrameCounts(AdkModelFbxInfo *pFbxInfo)
{
  for (uint32_t i = 0; i < pFbxInfo->nodeCount; ++i)
  {
    fbxsdk::FbxNode *pFbxNode = pFbxInfo->pNodes[i].pFbxNode;
    const char *ANIMATION_COMPONENTS[] = { FBXSDK_CURVENODE_COMPONENT_X, FBXSDK_CURVENODE_COMPONENT_Y, FBXSDK_CURVENODE_COMPONENT_Z };
    fbxsdk::FbxPropertyT<FbxDouble3> *PROPERTIES[] = { &pFbxNode->LclTranslation, &pFbxNode->LclRotation, &pFbxNode->LclScaling };

    for (uint32_t j = 0; j < ADK_ARRAY_SIZE(PROPERTIES); ++j)
    {
      for (uint32_t k = 0; k < ADK_ARRAY_SIZE(ANIMATION_COMPONENTS); ++k)
      {
        fbxsdk::FbxAnimCurve *pAnimCurve = PROPERTIES[j]->GetCurve(pFbxInfo->pAnimLayer, ANIMATION_COMPONENTS[k]);
        ++pFbxInfo->graphCount;
        if (pAnimCurve != nullptr)
          pFbxInfo->keyFrameCount += pAnimCurve->KeyGetCount();
        else
          ++pFbxInfo->keyFrameCount; //Add a key frame for the stationary component.
      }
    }
  }
}

static adkMatrix4x4<float> _GetFbxNodePreRotation(fbxsdk::FbxNode *pNode)
{
  typedef enum FbxTransform
  {
    FBX_TRANSFORM_TRANSLATION,
    FBX_TRANSFORM_ROTATION_OFFSET,
    FBX_TRANSFORM_ROTATION_PIVOT,
    FBX_TRANSFORM_PRE_ROTATION,
    FBX_TRANSFORM_ROTATION,
    FBX_TRANSFORM_POST_ROTATION,
    FBX_TRANSFORM_SCALING_OFFSET,
    FBX_TRANSFORM_SCALING_PIVOT,
    FBX_TRANSFORM_SCALING,
    FBX_TRANSFORM_GEOMETRIC_TRANSLATION,
    FBX_TRANSFORM_GEOMETRIC_ROTATION,
    FBX_TRANSFORM_GEOMETRIC_SCALING,
    FBX_TRANSFORM_COUNT,
  } FbxTransform;

  fbxsdk::FbxDouble3 transforms[FBX_TRANSFORM_COUNT];

  transforms[FBX_TRANSFORM_TRANSLATION] = pNode->LclTranslation.Get();
  transforms[FBX_TRANSFORM_ROTATION_OFFSET] = pNode->RotationOffset.Get();
  transforms[FBX_TRANSFORM_ROTATION_PIVOT] = pNode->RotationPivot.Get();
  transforms[FBX_TRANSFORM_PRE_ROTATION] = pNode->PreRotation.Get();
  transforms[FBX_TRANSFORM_ROTATION] = pNode->LclRotation.Get();
  transforms[FBX_TRANSFORM_POST_ROTATION] = pNode->PostRotation.Get();
  transforms[FBX_TRANSFORM_SCALING_OFFSET] = pNode->ScalingOffset.Get();
  transforms[FBX_TRANSFORM_SCALING_PIVOT] = pNode->ScalingPivot.Get();
  transforms[FBX_TRANSFORM_SCALING] = pNode->LclScaling.Get();
  transforms[FBX_TRANSFORM_GEOMETRIC_TRANSLATION] = pNode->GeometricTranslation.Get();
  transforms[FBX_TRANSFORM_GEOMETRIC_ROTATION] = pNode->GeometricRotation.Get();
  transforms[FBX_TRANSFORM_GEOMETRIC_SCALING] = pNode->GeometricScaling.Get();

  adkMatrix4x4<float> transformMatrices[FBX_TRANSFORM_COUNT];

  transformMatrices[FBX_TRANSFORM_TRANSLATION] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_TRANSLATION].mData[0], (float)transforms[FBX_TRANSFORM_TRANSLATION].mData[1], (float)transforms[FBX_TRANSFORM_TRANSLATION].mData[2]);
  transformMatrices[FBX_TRANSFORM_ROTATION_OFFSET] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_ROTATION_OFFSET].mData[0], (float)transforms[FBX_TRANSFORM_ROTATION_OFFSET].mData[1], (float)transforms[FBX_TRANSFORM_ROTATION_OFFSET].mData[2]);
  transformMatrices[FBX_TRANSFORM_ROTATION_PIVOT] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_ROTATION_PIVOT].mData[0], (float)transforms[FBX_TRANSFORM_ROTATION_PIVOT].mData[1], (float)transforms[FBX_TRANSFORM_ROTATION_PIVOT].mData[2]);
  transformMatrices[FBX_TRANSFORM_PRE_ROTATION] = adkMatrix4x4<float>::rotateEuler(adkDeg2Radf((float)transforms[FBX_TRANSFORM_PRE_ROTATION].mData[0]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_PRE_ROTATION].mData[1]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_PRE_ROTATION].mData[2]));
  transformMatrices[FBX_TRANSFORM_ROTATION] = adkMatrix4x4<float>::rotateEuler(adkDeg2Radf((float)transforms[FBX_TRANSFORM_ROTATION].mData[0]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_ROTATION].mData[1]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_ROTATION].mData[2]));
  transformMatrices[FBX_TRANSFORM_POST_ROTATION] = adkMatrix4x4<float>::rotateEuler(adkDeg2Radf((float)transforms[FBX_TRANSFORM_POST_ROTATION].mData[0]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_POST_ROTATION].mData[1]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_POST_ROTATION].mData[2]));
  transformMatrices[FBX_TRANSFORM_SCALING_OFFSET] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_SCALING_OFFSET].mData[0], (float)transforms[FBX_TRANSFORM_SCALING_OFFSET].mData[1], (float)transforms[FBX_TRANSFORM_SCALING_OFFSET].mData[2]);
  transformMatrices[FBX_TRANSFORM_SCALING_PIVOT] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_SCALING_PIVOT].mData[0], (float)transforms[FBX_TRANSFORM_SCALING_PIVOT].mData[1], (float)transforms[FBX_TRANSFORM_SCALING_PIVOT].mData[2]);
  transformMatrices[FBX_TRANSFORM_SCALING] = adkMatrix4x4<float>::scaleNonUniform((float)transforms[FBX_TRANSFORM_SCALING].mData[0], (float)transforms[FBX_TRANSFORM_SCALING].mData[1], (float)transforms[FBX_TRANSFORM_SCALING].mData[2]);
  transformMatrices[FBX_TRANSFORM_GEOMETRIC_TRANSLATION] = adkMatrix4x4<float>::translate((float)transforms[FBX_TRANSFORM_GEOMETRIC_TRANSLATION].mData[0], (float)transforms[FBX_TRANSFORM_GEOMETRIC_TRANSLATION].mData[1], (float)transforms[FBX_TRANSFORM_GEOMETRIC_TRANSLATION].mData[2]);
  transformMatrices[FBX_TRANSFORM_GEOMETRIC_ROTATION] = adkMatrix4x4<float>::rotateEuler(adkDeg2Radf((float)transforms[FBX_TRANSFORM_GEOMETRIC_ROTATION].mData[0]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_GEOMETRIC_ROTATION].mData[1]), adkDeg2Radf((float)transforms[FBX_TRANSFORM_GEOMETRIC_ROTATION].mData[2]));
  transformMatrices[FBX_TRANSFORM_GEOMETRIC_SCALING] = adkMatrix4x4<float>::scaleNonUniform((float)transforms[FBX_TRANSFORM_GEOMETRIC_SCALING].mData[0], (float)transforms[FBX_TRANSFORM_GEOMETRIC_SCALING].mData[1], (float)transforms[FBX_TRANSFORM_GEOMETRIC_SCALING].mData[2]);

  const char *transformNames[FBX_TRANSFORM_COUNT];

  transformNames[FBX_TRANSFORM_TRANSLATION] = "Translation";
  transformNames[FBX_TRANSFORM_ROTATION_OFFSET] = "Rotation Offset";
  transformNames[FBX_TRANSFORM_ROTATION_PIVOT] = "Rotation Pivot";
  transformNames[FBX_TRANSFORM_PRE_ROTATION] = "Pre Rotation";
  transformNames[FBX_TRANSFORM_ROTATION] = "Rotation";
  transformNames[FBX_TRANSFORM_POST_ROTATION] = "Post Rotation";
  transformNames[FBX_TRANSFORM_SCALING_OFFSET] = "Scaling Offset";
  transformNames[FBX_TRANSFORM_SCALING_PIVOT] = "Scaling Pivot";
  transformNames[FBX_TRANSFORM_SCALING] = "Scaling";
  transformNames[FBX_TRANSFORM_GEOMETRIC_TRANSLATION] = "Geometric Translation";
  transformNames[FBX_TRANSFORM_GEOMETRIC_ROTATION] = "Geometric Rotation";
  transformNames[FBX_TRANSFORM_GEOMETRIC_SCALING] = "Geometric Scaling";

  adkMatrix4x4<float> transform = adkMatrix4x4<float>::identity();
  transform = transformMatrices[FBX_TRANSFORM_TRANSLATION] * transform;
  transform = transformMatrices[FBX_TRANSFORM_ROTATION_OFFSET] * transform;
  transform = transformMatrices[FBX_TRANSFORM_ROTATION_PIVOT] * transform;
  transform = transformMatrices[FBX_TRANSFORM_PRE_ROTATION] * transform;
  transform = transformMatrices[FBX_TRANSFORM_ROTATION] * transform;
  transform = transformMatrices[FBX_TRANSFORM_POST_ROTATION].inverse() * transform;
  transform = transformMatrices[FBX_TRANSFORM_ROTATION_PIVOT].inverse() * transform;
  transform = transformMatrices[FBX_TRANSFORM_SCALING_OFFSET] * transform;
  transform = transformMatrices[FBX_TRANSFORM_SCALING_PIVOT] * transform;
  transform = transformMatrices[FBX_TRANSFORM_SCALING] * transform;
  transform = transformMatrices[FBX_TRANSFORM_SCALING_PIVOT].inverse() * transform;

  return transformMatrices[FBX_TRANSFORM_PRE_ROTATION];
}

static bool _PushMesh(AdkModelFbxInfo *pFbxInfo, AdkModel *pModel, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, fbxsdk::FbxMesh *pFbxMesh, const adkAllocationCallbacks *pAllocator, uint32_t *pMeshIterator)
{
  //Triangulate the mesh if it's isn't a triangle mesh.
  if (!pFbxMesh->IsTriangleMesh())
  {
    FbxGeometryConverter fbxGeometryConverter(pFbxMesh->GetNode()->GetFbxManager());
    pFbxMesh = (fbxsdk::FbxMesh*)fbxGeometryConverter.Triangulate(pFbxMesh, true);

    if (pFbxMesh == nullptr)
      return false;
  }

  int vertexCount = 0;
  int polygonCount = pFbxMesh->GetPolygonCount();
  fbxsdk::FbxStringList uvNames;
  pFbxMesh->GetUVSetNames(uvNames);
  int uvSetCount = uvNames.GetCount();
  int clusterCount = 0;

  for (int polygon = 0; polygon < polygonCount; ++polygon)
    vertexCount += pFbxMesh->GetPolygonSize(polygon);

  adkVector3<float> *pPositions = adkAllocTypeCall(adkVector3<float>, vertexCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  adkVector3<float> *pNormals = adkAllocTypeCall(adkVector3<float>, vertexCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  adkVector2<float> **ppUvs = adkAllocTypeCall(adkVector2<float>*, uvSetCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  uint32_t *pIndices = adkAllocTypeCall(uint32_t, vertexCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  adkMatrix4x4<float> *pBindPoses = adkAllocTypeCall(adkMatrix4x4<float>, pFbxInfo->boneCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  for (int j = 0; j < uvNames.GetCount(); ++j)
    ppUvs[j] = adkAllocTypeCall(adkVector2<float>, vertexCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  int controlPointCount = pFbxMesh->GetControlPointsCount();
  //fbxsdk::FbxVector4 *pControlPoints = pFbxMesh->GetControlPoints();

  int deformerCount = pFbxMesh->GetDeformerCount();

  uint32_t *pVertexBoneCounts = adkAllocTypeCall(uint32_t, vertexCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  uint32_t maxBonesPerVertex = 0;

  for (int i = 0; i < deformerCount; ++i)
  {
    fbxsdk::FbxDeformer *pDeformer = pFbxMesh->GetDeformer(i);
    if (pDeformer->GetDeformerType() == fbxsdk::FbxDeformer::EDeformerType::eSkin)
    {
      FbxSkin *pSkin = (FbxSkin*)pDeformer;
      clusterCount = pSkin->GetClusterCount();

      //Get vertex skin weights and bones.
      for (int j = 0; j < clusterCount; ++j)
      {
        fbxsdk::FbxCluster *pCluster = pSkin->GetCluster(j);
        int controlPointIndicesCount = pCluster->GetControlPointIndicesCount();
        int *pControlPointIndices = pCluster->GetControlPointIndices();

        for (int k = 0; k < controlPointIndicesCount; ++k)
        {
          ++pVertexBoneCounts[pControlPointIndices[k]];
          maxBonesPerVertex = adkMax(maxBonesPerVertex, pVertexBoneCounts[pControlPointIndices[k]]);
        }
      }
    }
  }

  uint32_t *pControlPointBones = adkAllocTypeCall(uint32_t, maxBonesPerVertex * controlPointCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  float *pControlPointWeights = adkAllocTypeCall(float, maxBonesPerVertex * controlPointCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  uint32_t *pBones = adkAllocTypeCall(uint32_t, maxBonesPerVertex * vertexCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  float *pWeights = adkAllocTypeCall(float, maxBonesPerVertex * vertexCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  // Reuse pVertexBoneCounts as a running total of how many bones have been attributed to a vertex
  memset(pVertexBoneCounts, 0, vertexCount * sizeof(*pVertexBoneCounts));

  //Get skin weights per control point.
  for (int i = 0; i < deformerCount; ++i)
  {
    fbxsdk::FbxDeformer *pDeformer = pFbxMesh->GetDeformer(i);
    if (pDeformer->GetDeformerType() == fbxsdk::FbxDeformer::EDeformerType::eSkin)
    {
      FbxSkin *pSkin = (FbxSkin*)pDeformer;
      clusterCount = pSkin->GetClusterCount();

      //Get vertex skin weights and bones.
      for (int j = 0; j < clusterCount; ++j)
      {
        fbxsdk::FbxCluster *pCluster = pSkin->GetCluster(j);
        AdkModelFbxNodeData *pBoneNode = (AdkModelFbxNodeData*)pCluster->GetLink()->GetUserDataPtr();

        int clusterIndicesCount = pCluster->GetControlPointIndicesCount();
        int *pClusterIndices = pCluster->GetControlPointIndices();
        double *pClusterWeights = pCluster->GetControlPointWeights();

        for (int k = 0; k < clusterIndicesCount; ++k)
        {
          uint32_t controlPointIndex = pClusterIndices[k];
          ADK_ASSERT(pVertexBoneCounts[controlPointIndex] <= maxBonesPerVertex, "Too many bones for this vertex");
          uint32_t controlPointBonesIndex = maxBonesPerVertex * controlPointIndex + pVertexBoneCounts[controlPointIndex];
          ++pVertexBoneCounts[controlPointIndex];

          pControlPointBones[controlPointBonesIndex] = pBoneNode->boneIndex;
          pControlPointWeights[controlPointBonesIndex] = (float)pClusterWeights[k];
        }

        fbxsdk::FbxAMatrix linkMatrix;
        pCluster->GetTransformLinkMatrix(linkMatrix);

        for (uint32_t k = 0; k < 16; ++k)
          pBindPoses[pBoneNode->boneIndex].a[k] = (float)linkMatrix.Get(k / 4, k % 4);
      }

      break;
    }
  }

  //Get vertex positions, normals, uvs, and indices.
  vertexCount = 0;
  for (int polygon = 0; polygon < polygonCount; ++polygon)
  {
    int polygonSize = pFbxMesh->GetPolygonSize(polygon);

    for (int vertexIndex = 0; vertexIndex < polygonSize; ++vertexIndex)
    {
      int vertex = pFbxMesh->GetPolygonVertex(polygon, vertexIndex);
      fbxsdk::FbxVector4 position = pFbxMesh->GetControlPointAt(vertex);
      fbxsdk::FbxVector4 normal;
      pFbxMesh->GetPolygonVertexNormal(polygon, vertexIndex, normal);

      for (int uvSet = 0; uvSet < uvSetCount; ++uvSet)
      {
        fbxsdk::FbxVector2 uv;
        bool unmapped;

        if (pFbxMesh->GetPolygonVertexUV(polygon, vertexIndex, uvNames[uvSet], uv, unmapped))
          ppUvs[uvSet][vertexCount] = adkVector2<float>::create((float)uv[0], (float)uv[1]);
        else
          ppUvs[uvSet][vertexCount] = adkVector2<float>::zero();
      }

      pPositions[vertexCount] = adkVector3<float>::create((float)position[0], (float)position[1], (float)position[2]);
      pNormals[vertexCount] = adkVector3<float>::create((float)normal[0], (float)normal[1], (float)normal[2]);
      pIndices[vertexCount] = vertexCount;

      for (uint32_t vertexBone = 0; vertexBone < maxBonesPerVertex; ++vertexBone)
      {
        uint32_t srcIndex = maxBonesPerVertex * vertex + vertexBone;
        uint32_t dstIndex = maxBonesPerVertex * vertexCount + vertexBone;
        pBones[dstIndex] = pControlPointBones[srcIndex];
        pWeights[dstIndex] = pControlPointWeights[srcIndex];
      }

      ++vertexCount;
    }
  }

  //Get mesh pointer from the back of the ADK mesh array. Push the FBX mesh pointer onto the back of the FBX mesh array. Increment the mesh iterator.
  AdkMesh **ppAdkMesh = pModel->ppMeshes + *pMeshIterator;
  AdkPhysicsMesh **ppPhysicsMesh = pModel->ppPhysicsMeshes + *pMeshIterator;
  AdkClipMesh **ppClipMesh = pModel->ppClipMeshes + *pMeshIterator;
  pFbxInfo->ppFbxMeshes[*pMeshIterator] = pFbxMesh;
  ++*pMeshIterator;

  //Create the mesh and push it onto the back of the ADK mesh array.
  adkMesh_Create(pFbxInfo->pInstance, pGraphicsQueue, pTransferQueue, pAllocator, ppAdkMesh);
  adkMesh_SetPositions(*ppAdkMesh, vertexCount, pPositions);
  adkMesh_SetNormals(*ppAdkMesh, vertexCount, pNormals);
  adkMesh_SetSkin(*ppAdkMesh, pFbxInfo->boneCount, pBindPoses, maxBonesPerVertex, vertexCount, pWeights, pBones);
  adkMesh_SetIndices(*ppAdkMesh, vertexCount, pIndices);

  //Create the physics mesh and push it onto the back of the ADK mesh array.
  if (pFbxInfo->flags & ADK_MODEL_CREATE_PHYSICS_MESH_BIT)
  {
    AdkPhysicsMeshCreateInfo physicsMeshCreateInfo;
    physicsMeshCreateInfo.maxTriangleCount = vertexCount / 3;
    adkPhysicsMesh_Create(&physicsMeshCreateInfo, pAllocator, ppPhysicsMesh);
    adkPhysicsMesh_SetTriangles(*ppPhysicsMesh, vertexCount / 3, pPositions);
  }

  //Create the clip mesh and push it onto the back of the ADK mesh array.
  if (pFbxInfo->flags & ADK_MODEL_CREATE_CLIP_MESH_BIT)
  {
    AdkClipMeshCreateInfo clipMeshCreateInfo;
    clipMeshCreateInfo.maxTriangleCount = vertexCount / 3;
    adkClipMesh_Create(&clipMeshCreateInfo, pAllocator, ppClipMesh);
    adkClipMesh_SetTriangles(*ppClipMesh, vertexCount / 3, pPositions);
  }

  if (uvSetCount > 0)
    adkMesh_SetUvs((*ppAdkMesh), vertexCount, ppUvs[0]);

  for (int i = 0; i < uvSetCount; ++i)
    adkFreeCall(ppUvs[i], pAllocator);

  adkFreeCall(pPositions, pAllocator);
  adkFreeCall(pNormals, pAllocator);
  adkFreeCall(pIndices, pAllocator);
  adkFreeCall(pBindPoses, pAllocator);
  adkFreeCall(ppUvs, pAllocator);

  adkFreeCall(pVertexBoneCounts, pAllocator);

  adkFreeCall(pControlPointBones, pAllocator);
  adkFreeCall(pControlPointWeights, pAllocator);
  adkFreeCall(pBones, pAllocator);
  adkFreeCall(pWeights, pAllocator);

  return true;
}

static bool _GetArrayIndexOfMesh(fbxsdk::FbxMesh *pMesh, uint32_t arrayLength, fbxsdk::FbxMesh **ppArray, uint32_t *pArrayIndex)
{
  for (uint32_t i = 0; i < arrayLength; ++i)
  {
    if (pMesh == ppArray[i])
    {
      *pArrayIndex = i;
      return true;
    }
  }
  return false;
}

static void _SetKeyFrame(AdkKeyFrame *pKeyFrame, float time, float p0, float p1, float p2, float p3, float /*w0*/, float /*w1*/, AdkModelTransformPropertyType propertyType)
{
  pKeyFrame->time = time;

  if (propertyType == ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION)
  {
    p0 = adkDeg2Radf(p0);
    p1 = adkDeg2Radf(p1);
    p2 = adkDeg2Radf(p2);
    p3 = adkDeg2Radf(p3);
  }

  pKeyFrame->curve = adkCubicBezier<float>::create(p0, p1, p2, p3);
}

static void _PushAnimationGraphsForProperty(AdkModelFbxInfo *pFbxInfo, FbxNode *pFbxNode, AdkModelTransformPropertyType type, uint32_t *pGraphIterator, uint32_t *pKeyFrameIterator, uint32_t *pOutputGraphCount, AdkAnimationGraph **ppOutputGraphs)
{
  const char *ANIMATION_COMPONENTS[] = { FBXSDK_CURVENODE_COMPONENT_X, FBXSDK_CURVENODE_COMPONENT_Y, FBXSDK_CURVENODE_COMPONENT_Z };
  fbxsdk::FbxPropertyT<FbxDouble3> *properties[] = { &pFbxNode->LclTranslation, &pFbxNode->LclRotation, &pFbxNode->LclScaling };

  *pOutputGraphCount = 0;
  *ppOutputGraphs = &pFbxInfo->pAnimationGraphs[*pGraphIterator];

  for (uint32_t j = 0; j < ADK_ARRAY_SIZE(ANIMATION_COMPONENTS); ++j)
  {
    pFbxInfo->pAnimationGraphs[*pGraphIterator].type = AdkAnimationGraphType(type * ADK_MODEL_TRANSFORM_PROPERTY_TYPE_COUNT + j);
    pFbxInfo->pAnimationGraphs[*pGraphIterator].pKeyFrames = pFbxInfo->pAnimationKeyFrames + *pKeyFrameIterator;
    fbxsdk::FbxAnimCurve *pAnimCurve = properties[type]->GetCurve(pFbxInfo->pAnimLayer, ANIMATION_COMPONENTS[j]);
    if (pAnimCurve != nullptr)
    {
      pFbxInfo->pAnimationGraphs[*pGraphIterator].keyFrameCount = pAnimCurve->KeyGetCount();
      for (int k = 0; (uint32_t)k < pFbxInfo->pAnimationGraphs[*pGraphIterator].keyFrameCount; ++k)
      {
        fbxsdk::FbxAnimCurveKey key = pAnimCurve->KeyGet(k);
        fbxsdk::FbxAnimCurveKey nextKey = pAnimCurve->KeyGet(adkClamp(k + 1, 0, (int)pFbxInfo->pAnimationGraphs[*pGraphIterator].keyFrameCount - 1));

        float startSlope = key.GetDataFloat(fbxsdk::FbxAnimCurveDef::EDataIndex::eRightSlope);
        float endSlope = key.GetDataFloat(fbxsdk::FbxAnimCurveDef::EDataIndex::eNextLeftSlope);
        float startWeight = key.GetDataFloat(fbxsdk::FbxAnimCurveDef::EDataIndex::eRightWeight);
        float endWeight = key.GetDataFloat(fbxsdk::FbxAnimCurveDef::EDataIndex::eNextLeftWeight);
        float startValue = key.GetValue();
        float endValue = nextKey.GetValue();

        float startTime = (float)key.GetTime().GetSecondDouble();
        float endTime = (float)nextKey.GetTime().GetSecondDouble();
        float timeDelta = endTime - startTime;

        float startOffset = startSlope * timeDelta * startWeight;
        float endOffset = endSlope * timeDelta * endWeight;

        //TODO: This currently only works when both weights are one third. To fix this to use proper weights, a two-dimensional bezier curve must be used.
        //Time must be used in conjunction with the x-curve and the cubic formula to get the value t on the curve.
        //Then that t must be plugged into the y-curve to get the value y for a given time (time is x in this procedure).
        //This should be done in adkAnimation.
        _SetKeyFrame(pFbxInfo->pAnimationKeyFrames + *pKeyFrameIterator, startTime, startValue, startValue + startOffset, endValue - endOffset, endValue, startWeight, endWeight, type);
        ++(*pKeyFrameIterator);
      }
    }
    else
    {
      pFbxInfo->pAnimationGraphs[*pGraphIterator].keyFrameCount = 1; //Key frame for the stationary component.
      fbxsdk::FbxDouble3 property = properties[type]->Get();
      float value = (float)property.mData[j];
      const float ONE_THIRD = 1.f / 3.f;
      _SetKeyFrame(pFbxInfo->pAnimationKeyFrames + *pKeyFrameIterator, 0.f, value, value, value, value, ONE_THIRD, ONE_THIRD, type);
      ++(*pKeyFrameIterator);
    }
    ++(*pGraphIterator);
    ++(*pOutputGraphCount);
  }
}

static void _PushAnimationNode(AdkModelFbxInfo *pFbxInfo, AdkModelFbxNodeData *pNode, AdkAnimationNode *pAnimationNode, AdkMesh *pMesh, uint32_t *pAnimationIterator, uint32_t *pGraphIterator, uint32_t *pKeyFrameIterator)
{
  FbxNode *pFbxNode = pNode->pFbxNode;
  AdkAnimationNode *pTranslation = pAnimationNode;
  AdkAnimationNode *pPreRotation = &pNode->preRotationNode;
  AdkAnimationNode *pRotationAndScale = &pNode->translationAndScaleNode;

  //Translation
  pTranslation->pName = _StoreAnimationNodeName(pFbxInfo, "Translation: ", pFbxNode->GetName());
  pTranslation->isBone = false;
  pTranslation->pMesh = nullptr;
  _PushAnimationGraphsForProperty(pFbxInfo, pFbxNode, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_TRANSLATION, pGraphIterator, pKeyFrameIterator, &pTranslation->graphCount, &pTranslation->pGraphs);
  pTranslation->childCount = 1;
  pTranslation->pChildren = pPreRotation;

  //Pre-rotation
  fbxsdk::FbxDouble3 fbxPreRotation = pFbxNode->PreRotation.Get();
  adkVector3<float> preRotation = adkVector3<float>::create((float)fbxPreRotation.mData[0], (float)fbxPreRotation.mData[1], (float)fbxPreRotation.mData[2]);

  pPreRotation->pName = _StoreAnimationNodeName(pFbxInfo, "Pre-rotation: ", pFbxNode->GetName());
  pPreRotation->isBone = false;
  pPreRotation->pMesh = nullptr;
  pPreRotation->graphCount = 3;
  pPreRotation->pGraphs = &pFbxInfo->pAnimationGraphs[*pGraphIterator];
  pPreRotation->childCount = 1;
  pPreRotation->pChildren = pRotationAndScale;
  (*pGraphIterator) += pPreRotation->graphCount;

  pPreRotation->pGraphs[0].type = ADK_ANIMATION_GRAPH_TYPE_ROTATION_X;
  pPreRotation->pGraphs[1].type = ADK_ANIMATION_GRAPH_TYPE_ROTATION_Y;
  pPreRotation->pGraphs[2].type = ADK_ANIMATION_GRAPH_TYPE_ROTATION_Z;

  pPreRotation->pGraphs[0].keyFrameCount = 1;
  pPreRotation->pGraphs[1].keyFrameCount = 1;
  pPreRotation->pGraphs[2].keyFrameCount = 1;

  pPreRotation->pGraphs[0].pKeyFrames = &pFbxInfo->pAnimationKeyFrames[(*pKeyFrameIterator)++];
  pPreRotation->pGraphs[1].pKeyFrames = &pFbxInfo->pAnimationKeyFrames[(*pKeyFrameIterator)++];
  pPreRotation->pGraphs[2].pKeyFrames = &pFbxInfo->pAnimationKeyFrames[(*pKeyFrameIterator)++];

  const float ONE_THIRD = 1.f / 3.f;
  _SetKeyFrame(&pPreRotation->pGraphs[0].pKeyFrames[0], 0.f, preRotation.x, preRotation.x, preRotation.x, preRotation.x, ONE_THIRD, ONE_THIRD, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION);
  _SetKeyFrame(&pPreRotation->pGraphs[1].pKeyFrames[0], 0.f, preRotation.y, preRotation.y, preRotation.y, preRotation.y, ONE_THIRD, ONE_THIRD, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION);
  _SetKeyFrame(&pPreRotation->pGraphs[2].pKeyFrames[0], 0.f, preRotation.z, preRotation.z, preRotation.z, preRotation.z, ONE_THIRD, ONE_THIRD, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION);

  //Rotation and scale
  uint32_t rotationGraphCount = 0;
  uint32_t scaleGraphCount = 0;
  AdkAnimationGraph *pRotationGraphs = nullptr;
  AdkAnimationGraph *pScaleGraphs = nullptr;
  _PushAnimationGraphsForProperty(pFbxInfo, pFbxNode, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_ROTATION, pGraphIterator, pKeyFrameIterator, &rotationGraphCount, &pRotationGraphs);
  _PushAnimationGraphsForProperty(pFbxInfo, pFbxNode, ADK_MODEL_TRANSFORM_PROPERTY_TYPE_SCALE, pGraphIterator, pKeyFrameIterator, &scaleGraphCount, &pScaleGraphs);

  pRotationAndScale->pName = _StoreAnimationNodeName(pFbxInfo, "", pFbxNode->GetName());
  pRotationAndScale->isBone = pNode->isBone;

  pRotationAndScale->pMesh = pMesh;
  pRotationAndScale->graphCount = rotationGraphCount + scaleGraphCount;
  pRotationAndScale->pGraphs = pRotationGraphs;
  pRotationAndScale->childCount = (uint32_t)pFbxNode->GetChildCount();
  pRotationAndScale->pChildren = &pFbxInfo->pAnimationNodes[*pAnimationIterator];

  *pAnimationIterator += pRotationAndScale->childCount;
}

static void _PushNodeAttributes(AdkModelFbxInfo *pFbxInfo, AdkModel *pModel, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue)
{
  uint32_t meshIterator = 0;
  uint32_t animationIterator = 1;
  uint32_t graphIterator = 0;
  uint32_t keyFrameIterator = 0;

  for (uint32_t i = 0; i < pFbxInfo->nodeCount; ++i)
  {
    fbxsdk::FbxNode *pFbxNode = pFbxInfo->pNodes[i].pFbxNode;
    AdkAnimationNode *pAnimationNode = &pFbxInfo->pAnimationNodes[i];
    int attributeCount = pFbxNode->GetNodeAttributeCount();
    AdkMesh *pMesh = nullptr;

    //Iterate each attribute belonging to this node.
    for (int j = 0; j < attributeCount; ++j)
    {
      fbxsdk::FbxNodeAttribute *pAttribute = pFbxNode->GetNodeAttributeByIndex(j);
      fbxsdk::FbxNodeAttribute::EType type = pAttribute->GetAttributeType();

      if (type == fbxsdk::FbxNodeAttribute::EType::eMesh)
      {
        //This attribute is a mesh.
        fbxsdk::FbxMesh *pFbxMesh = (fbxsdk::FbxMesh*)pAttribute;
        uint32_t meshIndex;
        //Check if mesh is an instance (if it already exists in the FBX mesh array).
        if (_GetArrayIndexOfMesh(pFbxMesh, pFbxInfo->meshCount, pFbxInfo->ppFbxMeshes, &meshIndex))
        {
          //Mesh already exists; point to the mesh that exists in the ADK mesh array.
          pMesh = pModel->ppMeshes[meshIndex];
        }
        else
        {
          //Mesh doesn't yet exist; add it to the ADK mesh array, point to it, and increment the mesh iterator.
          _PushMesh(pFbxInfo, pModel, pGraphicsQueue, pTransferQueue, pFbxMesh, pModel->pAllocator, &meshIterator);
          pMesh = pModel->ppMeshes[meshIterator - 1];
        }
      }
    }

    //Add an animation node for this FBX node. Push various animation elements to their respective arrays and point the animation node to that. Use the ADK mesh retrieved previously for this animation node.
    _PushAnimationNode(pFbxInfo, &pFbxInfo->pNodes[i], pAnimationNode, pMesh, &animationIterator, &graphIterator, &keyFrameIterator);
  }
}

AdkBool32 adkModel_Create(AdkInstance *pInstance, AdkModelCreateFlags flags, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const char *pFilename, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel)
{
  AdkBool32 success = false;
  AdkModel *pModel = nullptr;

  fbxsdk::FbxManager *pFbxManager = fbxsdk::FbxManager::Create();
  fbxsdk::FbxIOSettings *pFbxIoSettings = fbxsdk::FbxIOSettings::Create(pFbxManager, IOSROOT);
  pFbxManager->SetIOSettings(pFbxIoSettings);
  fbxsdk::FbxImporter *pFbxImporter = fbxsdk::FbxImporter::Create(pFbxManager, "");

  if (!pFbxImporter->Initialize(pFilename, -1, pFbxManager->GetIOSettings()))
    goto epilogue;
  
  fbxsdk::FbxScene *pScene = fbxsdk::FbxScene::Create(pFbxManager, "myScene");
  pFbxImporter->Import(pScene);
  fbxsdk::FbxNode *pRootNode = pScene->GetRootNode();
  fbxsdk::FbxTime::EMode timeMode = pScene->GetGlobalSettings().GetTimeMode();
  float frameRate = (float)fbxsdk::FbxTime::GetFrameRate(timeMode);
  
  pModel = adkAllocTypeCall(AdkModel, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pModel->pInstance = pInstance;
  pModel->pGraphicsQueue = pGraphicsQueue;
  pModel->pTransferQueue = pTransferQueue;
  pModel->pAllocator = pAllocator;

  fbxsdk::FbxAnimStack *pAnimStack = pScene->GetCurrentAnimationStack();
  fbxsdk::FbxAnimLayer *pAnimLayer = pAnimStack->GetMember<fbxsdk::FbxAnimLayer>(0);

  AdkModelFbxInfo *pFbxInfo = nullptr;
  _CreateFbxInfo(pRootNode, pAnimLayer, pAllocator, &pFbxInfo);
  _DetectBonesAndGetMeshCount(pFbxInfo);
  _DetectGraphAndKeyFrameCounts(pFbxInfo);

  //Allocate enough memory for animations.
  pFbxInfo->pAnimationNodes = adkAllocTypeCall(AdkAnimationNode, pFbxInfo->nodeCount, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pFbxInfo->pAnimationGraphs = adkAllocTypeCall(AdkAnimationGraph, pFbxInfo->graphCount + pFbxInfo->nodeCount * EXTRA_ANIMATION_GRAPHS_PER_NODE, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  pFbxInfo->pAnimationKeyFrames = adkAllocTypeCall(AdkKeyFrame, pFbxInfo->keyFrameCount + pFbxInfo->nodeCount * EXTRA_ANIMATION_KEY_FRAMES_PER_NODE, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);
  _IndexBonesAndGetBoneCount(pFbxInfo);

  //Allocate enough memory for meshes.
  pModel->meshCount = pFbxInfo->meshCount;
  pModel->ppMeshes = adkAllocTypeCall(AdkMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pFbxInfo->ppFbxMeshes = adkAllocTypeCall(fbxsdk::FbxMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  //Allocate enough memory for physics meshes.
  if (flags & ADK_MODEL_CREATE_PHYSICS_MESH_BIT)
    pModel->ppPhysicsMeshes = adkAllocTypeCall(AdkPhysicsMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  //Allocate enough memory for clip meshes.
  if (flags & ADK_MODEL_CREATE_CLIP_MESH_BIT)
    pModel->ppClipMeshes = adkAllocTypeCall(AdkClipMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  
  pFbxInfo->pInstance = pInstance;
  pFbxInfo->flags = flags;
  pFbxInfo->frameRate = frameRate;

  //Retrieve data from FBX.
  _PushNodeAttributes(pFbxInfo, pModel, pGraphicsQueue, pTransferQueue);
  
  //Create animation from retrieved data.
  adkAnimation_Create(pInstance, pFbxInfo->pAnimationNodes, pAllocator, &pModel->pAnimation);

  (*ppModel) = pModel;
  success = true;

epilogue:
  _DestroyFbxInfo(&pFbxInfo);

  if (pFbxImporter != nullptr)
    pFbxImporter->Destroy();

  if (pFbxManager != nullptr)
    pFbxManager->Destroy();

  if (!success)
    adkFreeCall(pModel, pAllocator);

  return success;
}

void adkModel_Destroy(AdkInstance * /*pInstance*/, AdkModel **ppModel)
{
  if (*ppModel == nullptr)
    return;

  AdkModel *pModel = (*ppModel);

  for (uint32_t i = 0; i < pModel->meshCount; ++i)
    adkMesh_Destroy(pModel->ppMeshes + i);

  adkFreeCall(pModel->ppClipMeshes, pModel->pAllocator);
  adkFreeCall(pModel->ppPhysicsMeshes, pModel->pAllocator);
  adkFreeCall(pModel->ppMeshes, pModel->pAllocator);
  adkFreeCall(pModel, pModel->pAllocator);

  (*ppModel) = nullptr;
}

bool adkModel_Serialize(AdkModel *pModel, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, pModel->meshCount);

  for (uint32_t i = 0; i < pModel->meshCount; ++i)
    adkMesh_Serialize(pModel->ppMeshes[i], pBinaryWriter);

  adkAnimation_Serialize(pModel->pAnimation, pBinaryWriter);

  adkBinaryWriter_Write(pBinaryWriter, pModel->ppPhysicsMeshes != nullptr);

  if (pModel->ppPhysicsMeshes != nullptr)
  {
    for (uint32_t i = 0; i < pModel->meshCount; ++i)
      adkPhysicsMesh_Serialize(pModel->ppPhysicsMeshes[i], pBinaryWriter);
  }

  adkBinaryWriter_Write(pBinaryWriter, pModel->ppClipMeshes != nullptr);

  if (pModel->ppClipMeshes != nullptr)
  {
    for (uint32_t i = 0; i < pModel->meshCount; ++i)
      adkClipMesh_Serialize(pModel->ppClipMeshes[i], pBinaryWriter);
  }

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkModel_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel)
{
  bool success = false;

  AdkModel *pModel = adkAllocTypeCall(AdkModel, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pModel->pInstance = pInstance;
  pModel->pGraphicsQueue = pGraphicsQueue;
  pModel->pTransferQueue = pTransferQueue;
  pModel->pAllocator = pAllocator;

  adkBinaryReader_Read(pBinaryReader, &pModel->meshCount);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  pModel->ppMeshes = adkAllocTypeCall(AdkMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pModel->meshCount; ++i)
  {
    if (!adkMesh_Deserialize(pBinaryReader, pInstance, pGraphicsQueue, pTransferQueue, pAllocator, &pModel->ppMeshes[i]))
      goto epilogue;
  }

  adkAnimation_Deserialize(pBinaryReader, pInstance, pAllocator, &pModel->pAnimation);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  for (uint32_t i = 0; i < pModel->meshCount; ++i)
    adkAnimation_SetMesh(pModel->pAnimation, i, pModel->ppMeshes[i]);

  bool hasPhysicsMeshes = false;
  adkBinaryReader_Read(pBinaryReader, &hasPhysicsMeshes);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  if (hasPhysicsMeshes)
  {
    pModel->ppPhysicsMeshes = adkAllocTypeCall(AdkPhysicsMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

    for (uint32_t i = 0; i < pModel->meshCount; ++i)
    {
      if (!adkPhysicsMesh_Deserialize(pBinaryReader, pAllocator, &pModel->ppPhysicsMeshes[i]))
        goto epilogue;
    }
  }

  bool hasClipMeshes = false;
  adkBinaryReader_Read(pBinaryReader, &hasClipMeshes);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  if (hasClipMeshes)
  {
    pModel->ppClipMeshes = adkAllocTypeCall(AdkClipMesh*, pModel->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

    for (uint32_t i = 0; i < pModel->meshCount; ++i)
    {
      if (!adkClipMesh_Deserialize(pBinaryReader, pAllocator, &pModel->ppClipMeshes[i]))
        goto epilogue;
    }
  }

  *ppModel = pModel;

  success = true;

epilogue:

  if (!success)
  {
    for (uint32_t i = 0; i < pModel->meshCount; ++i)
      adkMesh_Destroy(&pModel->ppMeshes[i]);

    adkFreeCall(pModel->ppMeshes, pAllocator);

    for (uint32_t i = 0; i < pModel->meshCount; ++i)
      adkPhysicsMesh_Destroy(&pModel->ppPhysicsMeshes[i]);

    adkFreeCall(pModel->ppPhysicsMeshes, pAllocator);

    adkFreeCall(pModel, pAllocator);
  }

  return success;
}

bool adkModel_Deserialize(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const char *pFilename, const adkAllocationCallbacks *pAllocator, AdkModel **ppModel)
{
  bool success = false;
  AdkBinaryReader *pBinaryReader = nullptr;
  AdkModel *pModel = nullptr;

  size_t bufferSize = 0;
  char *pBuffer = nullptr;

  if (!adkFile_Load(pFilename, &bufferSize, (void**)&pBuffer))
    goto epilogue;

  AdkBinaryReaderCreateInfo binaryReaderCreateInfo;
  binaryReaderCreateInfo.bufferSize = bufferSize;
  binaryReaderCreateInfo.pBuffer = pBuffer;

  adkBinaryReader_Create(&binaryReaderCreateInfo, pAllocator, &pBinaryReader);

  if (!adkModel_Deserialize(pBinaryReader, pInstance, pGraphicsQueue, pTransferQueue, pAllocator, &pModel))
    goto epilogue;

  *ppModel = pModel;
  success = true;

epilogue:
  if (!success)
    adkModel_Destroy(pInstance, &pModel);

  adkBinaryReader_Destroy(&pBinaryReader);
  adkFree(pBuffer);
  return success;
}

uint32_t adkModel_GetMeshCount(AdkModel *pModel)
{
  return pModel->meshCount;
}

void adkModel_CreateMesh(AdkModel *pModel, uint32_t index, AdkMesh **ppMesh)
{
  adkMesh_Copy(pModel->ppMeshes[index], ppMesh);
}

void adkModel_CreatePhysicsMesh(AdkModel *pModel, uint32_t index, AdkPhysicsMesh **ppPhysicsMesh)
{
  ADK_ASSERT(pModel->ppPhysicsMeshes != nullptr, "Cannot create physics mesh because ADK_MODEL_CREATE_PHYSICS_MESH_BIT was not specified in flags parameter this adkModel was created using adkModel_Create.");
  adkPhysicsMesh_Copy(pModel->ppPhysicsMeshes[index], ppPhysicsMesh);
}

void adkModel_CreateClipMesh(AdkModel *pModel, uint32_t index, AdkClipMesh **ppClipMesh)
{
  ADK_ASSERT(pModel->ppClipMeshes != nullptr, "Cannot create clip mesh because ADK_MODEL_CREATE_CLIP_MESH_BIT was not specified in flags parameter this adkModel was created using adkModel_Create.");
  adkClipMesh_Copy(pModel->ppClipMeshes[index], ppClipMesh);
}

void adkModel_CreateAnimation(AdkModel *pModel, AdkAnimation **ppAnimation)
{
  adkAnimation_Copy(pModel->pAnimation, ppAnimation);
}