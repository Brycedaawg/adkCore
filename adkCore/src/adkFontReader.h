#ifndef adkFontReader_h__
#define adkFontReader_h__

struct AdkFontReader;
struct AdkInstance;

typedef struct AdkFontReaderCreateInfo
{

} AdkFontReaderCreateInfo;

void adkFontReader_Create(AdkInstance *pInstance, const AdkFontReaderCreateInfo *pCreateInfo, AdkFontReader **ppFontReader);
void adkFontReader_Destroy(AdkInstance *pInstance, AdkFontReader **ppFontReader);

#endif // adkFontReader_h__
