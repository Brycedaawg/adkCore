#include "AdkRTree.h"
#include "adkMemory.h"
#include "adkMath.h"
#include "AdkLineRendererUtil.h"
#include "adkAssert.h"
#include "AdkPhysicsUtil.h"

typedef struct AdkRTreeNode
{
  adkVector3<float> min;
  adkVector3<float> max;
  AdkRTreeNode *children[4];
  uint32_t entity; //Index of the triangle that belongs to this node.
} AdkRTreeNode;

typedef struct AdkRTree
{
  const adkAllocationCallbacks *pAllocator;
  uint32_t nodeCount;
  uint32_t entityCount;
  uint32_t maxEntityCount;
  uint32_t maxNodeCount;
  AdkRTreeNode *pNodes;
  AdkRTreeEntity *pEntities;
} AdkRTree;

typedef struct AdkRTreeSweepSphereWork
{
  adkVector3<float> sweepMin;
  adkVector3<float> sweepMax;
  adkVector3<float> start;
  adkVector3<float> end;
  float radius;
  uint32_t resultEntity;
  AdkSweepingSphereOnSweepingSphereCollision<float> resultCollision;
  AdkSweepingSphereOnSweepingSphereCollision<float> compareCollision;
  uint32_t ignoreEntity;
  AdkLineRenderer *pLineRenderer;
} AdkRTreeSweepSphereWork;

typedef struct AdkRTreeGetCollisionsWork
{
  adkVector3<float> entityPosition;
  float entityRadius;
  adkVector3<float> entityMin;
  adkVector3<float> entityMax;
  uint32_t entity;
  uint32_t collisionCount;
  uint32_t collisionsLength;
  AdkRTreeCollision *pCollisions;
  AdkLineRenderer *pLineRenderer;
} AdkRTreeGetCollisionsWork;

static const uint32_t ENTITY_NONE = 0xFFFFFFFF;

void adkRTree_Create(uint32_t maxEntityCount, const adkAllocationCallbacks *pAllocator, AdkRTree **ppRTree)
{
  ADK_ASSERT(maxEntityCount != 0, "maxEntityCount must not be 0");

  AdkRTree *pRTree = adkAllocTypeCall(AdkRTree, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pRTree->pAllocator = pAllocator;
  pRTree->maxEntityCount = maxEntityCount;
  pRTree->maxNodeCount = 2 * maxEntityCount;
  pRTree->pNodes = adkAllocTypeCall(AdkRTreeNode, pRTree->maxNodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pRTree->pEntities = adkAllocTypeCall(AdkRTreeEntity, pRTree->maxEntityCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  *ppRTree = pRTree;
}

void adkRTree_Destroy(AdkRTree **ppRTree)
{
  AdkRTree *pRTree = *ppRTree;

  adkFreeCall(pRTree->pEntities, pRTree->pAllocator);
  adkFreeCall(pRTree->pNodes, pRTree->pAllocator);

  adkFreeCall(*ppRTree, pRTree->pAllocator);
}

//Initializes the node using the given entity's bounds.
static void _InitNode(AdkRTree *pRTree, AdkRTreeNode *pNode, uint32_t entityIndex)
{
  memset(pNode, 0, sizeof(*pNode));
  AdkRTreeEntity *pEntity = &pRTree->pEntities[entityIndex];

  //Generate bounds for entity based on entity bounds.
  pNode->min.x = pEntity->position.x - pEntity->radius;
  pNode->min.y = pEntity->position.y - pEntity->radius;
  pNode->min.z = pEntity->position.z - pEntity->radius;

  pNode->max.x = pEntity->position.x + pEntity->radius;
  pNode->max.y = pEntity->position.y + pEntity->radius;
  pNode->max.z = pEntity->position.z + pEntity->radius;

  //The child is now a leaf with an entity.
  pNode->entity = entityIndex;
}

//Creates a child node in the given entity's children array at the given index. Initializes the child using the given entity's bounds.
static void _CreateChild(AdkRTree *pRTree, uint32_t entityIndex, AdkRTreeNode *pNode, uint32_t childIndex)
{
  //Child empty; create a new one and use it.
  AdkRTreeNode *pChild = &pRTree->pNodes[pRTree->nodeCount];
  pNode->children[childIndex] = pChild;
  ++pRTree->nodeCount;

  _InitNode(pRTree, pChild, entityIndex);
}

//Encapsulates the input bounds and the input entity and outputs them as a new set of bounds.
static void _Encapsulate(adkVector3<float> min, const adkVector3<float> max, const AdkRTreeEntity &entity, adkVector3<float> *pMin, adkVector3<float> *pMax)
{
  pMin->x = adkMin(min.x, entity.position.x - entity.radius);
  pMin->y = adkMin(min.y, entity.position.y - entity.radius);
  pMin->z = adkMin(min.z, entity.position.z - entity.radius);

  pMax->x = adkMax(max.x, entity.position.x + entity.radius);
  pMax->y = adkMax(max.y, entity.position.y + entity.radius);
  pMax->z = adkMax(max.z, entity.position.z + entity.radius);
}

void adkRTree_Link(AdkRTree *pRTree, uint32_t entityCount, AdkRTreeEntity *pEntities, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  uint32_t maxDepth = 0;
  ADK_ASSERT(entityCount <= pRTree->maxEntityCount, "entityCount must not be greater than the max entity count specified during creation");

  pRTree->nodeCount = 0;
  pRTree->entityCount = 0;

  if (entityCount == 0)
    return;

  //Initialize the root node with the first entity.
  pRTree->pEntities[0] = pEntities[0];
  ++pRTree->entityCount;
  _InitNode(pRTree, &pRTree->pNodes[0], 0);
  ++pRTree->nodeCount;

  //Add all the entities to the hierarchy except for the first because it's already been added.
  for (uint32_t en = 1; en < entityCount; ++en)
  {
    //Add entity to R tree's buffer.
    pRTree->pEntities[en] = pEntities[en];
    AdkRTreeEntity *pEntity = &pRTree->pEntities[en];

    //Start at the root node.
    AdkRTreeNode *pNode = &pRTree->pNodes[0];

    uint32_t depth = 0;

    //Descend into the r-tree to find a suitable node to store the entity.
    while (true)
    {
      uint32_t bestCh = 0;
      float bestVol = adkFloatMax;
      adkVector3<float> bestMin = adkVector3<float>::zero();
      adkVector3<float> bestMax = adkVector3<float>::zero();

      //Is this node a leaf?
      if (pNode->children[0] == nullptr)
      {
        //Turn the node from a leaf to a branch by moving its entity to a new child.
        _CreateChild(pRTree, pNode->entity, pNode, 0);
        pNode->entity = ENTITY_NONE;

        //Create another child for the current entity.
        _CreateChild(pRTree, en, pNode, 1);

        //Include both entities in this node's bounds.
        _Encapsulate(pNode->min, pNode->max, pRTree->pEntities[en], &pNode->min, &pNode->max);

        break;

      }

      //Does this node have any free space for another child?
      if (pNode->children[ADK_ARRAY_SIZE(pNode->children) - 1] == nullptr)
      {
        //Find the next free space, create a new child, set the entity, and encapsulate the entity's bounds in this node.
        for (uint32_t ch = 0; ch < ADK_ARRAY_SIZE(pNode->children); ++ch)
        {
          if (pNode->children[ch] != nullptr)
            continue;

          _CreateChild(pRTree, en, pNode, ch);
          _Encapsulate(pNode->min, pNode->max, *pEntity, &pNode->min, &pNode->max);
          break;
        }

        break;
      }

      //Find the child to which adding this entity will increase the node's bounds the least.
      for (uint32_t ch = 0; ch < ADK_ARRAY_SIZE(pNode->children); ++ch)
      {
        AdkRTreeNode *pChild = pNode->children[ch];

        if (pChild == nullptr)
          break;

        //Use this child if it's better than the current.
        adkVector3<float> min, max;
        _Encapsulate(pChild->min, pChild->max, *pEntity, &min, &max);

        adkVector3<float> extents = max - min;
        float volume = extents.x * extents.y * extents.z;

        if (volume <= bestVol)
        {
          bestCh = ch;
          bestVol = volume;
          bestMin = min;
          bestMax = max;
        }
      }

      //The entity will be added to a sub-node of this node so encapsulate the entity's bounds in this node.
      _Encapsulate(pNode->min, pNode->max, *pEntity, &pNode->min, &pNode->max);

      //Descend into the child.
      pNode = pNode->children[bestCh];
      ++depth;
    }

    if (depth > maxDepth)
      maxDepth = depth;
  }

  ADK_ASSERT(pRTree->nodeCount <= pRTree->maxNodeCount, "Too many nodes");
  ADK_ASSERT(pRTree->entityCount <= pRTree->maxEntityCount, "Too many entities");

  if (pLineRenderer != nullptr)
  {
    for (uint32_t n = 0; n < pRTree->nodeCount; ++n)
      adkLineRenderer_AddBox(pLineRenderer, pRTree->pNodes[n].min, pRTree->pNodes[n].max, 0xFF0000FF);
  }
}

static void _GetCollisionsRecursive(AdkRTree *pRTree, AdkRTreeNode *pNode, AdkRTreeGetCollisionsWork *pWork)
{
  //Return if entity bounds and node bounds don't intersect.
  if (pNode->min.x > pWork->entityMax.x || pNode->max.x < pWork->entityMin.x ||
      pNode->min.y > pWork->entityMax.y || pNode->max.y < pWork->entityMin.y ||
      pNode->min.z > pWork->entityMax.z || pNode->max.z < pWork->entityMin.z)
    return;

  if (pWork->pLineRenderer)
    adkLineRenderer_AddBox(pWork->pLineRenderer, pNode->min, pNode->max, 0xFF0000FF);

  if (pNode->children[0] == nullptr && pNode->entity != pWork->entity)
  {
    ADK_ASSERT(pWork->collisionCount != pWork->collisionsLength, "Too many collisions");

    //Leaf
    AdkRTreeEntity *pEntity = &pRTree->pEntities[pNode->entity];
    float radii = pEntity->radius + pWork->entityRadius;
    if (adkVector3<float>::squareDistance(pEntity->position, pWork->entityPosition) < radii * radii)
    {
      pWork->pCollisions[pWork->collisionCount].entity = pNode->entity;
      ++pWork->collisionCount;
    }
  }
  else
  {
    //Branch
    for (uint32_t ch = 0; ch < ADK_ARRAY_SIZE(pNode->children); ++ch)
    {
      AdkRTreeNode *pChild = pNode->children[ch];

      if (pChild == nullptr)
        break;

      _GetCollisionsRecursive(pRTree, pChild, pWork);
    }
  }
}

void adkRTree_GetCollisions(AdkRTree *pRTree, uint32_t entity, uint32_t collisionsLength, uint32_t *pCollisionCount, AdkRTreeCollision *pCollisions, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  adkRTree_GetCollisions(pRTree, pRTree->pEntities[entity].position, pRTree->pEntities[entity].radius, collisionsLength, pCollisionCount, pCollisions, entity, pLineRenderer);
}

void adkRTree_GetCollisions(AdkRTree *pRTree, const adkVector3<float> &position, float radius, uint32_t collisionsLength, uint32_t *pCollisionCount, AdkRTreeCollision *pCollisions, uint32_t ignoreEntity /*= 0xFFFFFFFF*/, AdkLineRenderer *pLineRenderer /*= nullptr*/)
{
  AdkRTreeGetCollisionsWork work;
  memset(&work, 0, sizeof(work));

  work.entityPosition = position;
  work.entityRadius = radius;
  work.entity = ignoreEntity;

  work.entityMin = position - radius;
  work.entityMax = position + radius;

  work.collisionsLength = collisionsLength;
  work.pCollisions = pCollisions;
  work.pLineRenderer = pLineRenderer;

  AdkRTreeNode *pRoot = &pRTree->pNodes[0];

  if (pRTree->entityCount != 0)
    _GetCollisionsRecursive(pRTree, pRoot, &work);

  *pCollisionCount = work.collisionCount;
}

static void _DrawRecursive(AdkRTreeNode *pNode, AdkLineRenderer *pLineRenderer)
{
  adkLineRenderer_AddBox(pLineRenderer, pNode->min, pNode->max, 0x00FF00FF);

  for (uint32_t ch = 0; ch < ADK_ARRAY_SIZE(pNode->children); ++ch)
  {
    AdkRTreeNode *pChild = pNode->children[ch];

    if (pChild == nullptr)
      break;

    _DrawRecursive(pChild, pLineRenderer);
  }
}

void adkRTree_Draw(AdkRTree *pRTree, AdkLineRenderer *pLineRenderer)
{
  _DrawRecursive(&pRTree->pNodes[0], pLineRenderer);
}