#ifndef adkDirectory_h__
#define adkDirectory_h__

#include "adkMemory.h"
#include "adkTypedefs.h"

struct AdkDirectory;

#define ADK_FILE_ITERATOR_MAX_FILENAME_SIZE 256

typedef enum AdkDirectoryOpenFlagBits
{
  ADK_DIRECTORY_OPEN_ITERATE_BIT = 0x00000001,
  ADK_DIRECTORY_OPEN_LISTEN_BIT = 0x00000002,
} AdkDirectoryOpenFlagBits;
typedef AdkFlags AdkDirectoryOpenFlags;

typedef enum AdkDirectoryItemFlagBits
{
  ADK_DIRECTORY_ITEM_SUBDIRECTORY_BIT = 0x00000001,
} AdkDirectoryItemFlagBits;
typedef AdkFlags AdkDirectoryItemFlags;

typedef enum AdkDirectoryChangeType
{
  ADK_DIRECTORY_CHANGE_ADD = 0x00000001,
  ADK_DIRECTORY_CHANGE_REMOVE = 0x00000002,
  ADK_DIRECTORY_CHANGE_RENAME = 0x00000004,
  ADK_DIRECTORY_CHANGE_MODIFY = 0x00000008,
} AdkDirectoryChangeType;

typedef enum AdkDirectoryPopResult
{
  ADK_DIRECTORY_POP_SUCCESS,
  ADK_DIRECTORY_POP_NO_CHANGES,
  ADK_DIRECTORY_POP_OVERFLOW,
} AdkDirectoryPopResult;

typedef struct AdkDirectoryOpenInfo
{
  AdkDirectoryOpenFlags flags;
  const char *pName;
} AdkDirectoryOpenInfo;

typedef struct AdkDirectoryItemProperties
{
  AdkDirectoryItemFlags flags;
  char item[ADK_FILE_ITERATOR_MAX_FILENAME_SIZE];
} AdkDirectoryItemProperties;

typedef struct AdkDirectoryChangeProperties
{
  AdkDirectoryChangeType type;
  char item[ADK_FILE_ITERATOR_MAX_FILENAME_SIZE];
} AdkDirectoryChangeProperties;

AdkBool32 adkDirectory_Open(AdkDirectoryOpenInfo *pOpenInfo, const adkAllocationCallbacks *pAllocator, AdkDirectory **ppDirectory);
void adkDirectory_Close(const adkAllocationCallbacks *pAllocator, AdkDirectory **ppDirectory);
AdkBool32 adkDirectory_Next(AdkDirectory *pDirectory, AdkDirectoryItemProperties *pProperties);
AdkBool32 adkDirectory_Descend(AdkDirectory *pDirectory);
AdkBool32 adkDirectory_Ascend(AdkDirectory *pDirectory);
AdkBool32 adkDirectory_Seek(AdkDirectory *pDirectory, const char *pName);
void adkDirectory_Restart(AdkDirectory *pDirectory);
AdkDirectoryPopResult adkDirectory_PopChanges(AdkDirectory *pDirectory, uint32_t *pPropertyCount, AdkDirectoryChangeProperties *pProperties);
AdkBool32 adkDirectory_Exists(const char *pName);

#endif // adkDirectory_h__
