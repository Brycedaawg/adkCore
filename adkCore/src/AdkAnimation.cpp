#include "AdkAnimation.h"
#include "adkMemory.h"
#include "adkMesh.h"
#include "adkString.h"
#include "AdkBinaryReader.h"
#include "AdkBinaryWriter.h"

enum
{
  ADK_ANIMATION_MAX_CHILDREN = 128,
};

float ADK_ANIMATION_GRAPH_DEFAULT_VALUES[] =
{
  0.f, //ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_X,
  0.f, //ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Y,
  0.f, //ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Z,
  0.f, //ADK_ANIMATION_GRAPH_TYPE_ROTATION_X,
  0.f, //ADK_ANIMATION_GRAPH_TYPE_ROTATION_Y,
  0.f, //ADK_ANIMATION_GRAPH_TYPE_ROTATION_Z,
  1.f, //ADK_ANIMATION_GRAPH_TYPE_SCALE_X,
  1.f, //ADK_ANIMATION_GRAPH_TYPE_SCALE_Y,
  1.f, //ADK_ANIMATION_GRAPH_TYPE_SCALE_Z,
};

#define ADK_ANIMATION_MESH_INDEX_NONE 0xFFFFFFFF

typedef struct AdkAnimationTimeline
{
  AdkRange<uint32_t> keyFrames;
} AdkAnimationTimeline;

typedef struct AdkAnimationNodeInternal
{
  char name[ADK_ANIMATION_NODE_NAME_MAX_LENGTH];
  uint32_t meshIndex;
  uint32_t timelineMask;
  AdkAnimationTimeline timelines[ADK_ANIMATION_GRAPH_TYPE_COUNT];
  uint32_t childCount;
  AdkAnimationNodeInternal *pChildren;
  bool isBone;
} AdkAnimationNodeInternal;

typedef struct AdkAnimation
{
  AdkInstance *pInstance;
  const adkAllocationCallbacks *pAllocator;
  uint32_t nodeCount;
  AdkAnimationNodeInternal *pNodes;
  uint32_t keyFrameCount;
  AdkKeyFrame *pKeyFrames;
  uint32_t meshCount;
  AdkMesh **ppMeshes;
  uint32_t boneCount;
  uint32_t *pBoneIndices;
} AdkAnimation;

typedef struct AdkAnimationRenderer
{
  AdkAnimation *pAnimation;
  adkMatrix4x4<float> *pMatrices;
  adkMatrix4x4<float> *pBoneMatrices;
  AdkMeshRenderer **ppMeshRenderers;
  uint32_t modelCount;
} AdkAnimationRenderer;

static void _GetCounts(AdkAnimationNode *pNode, uint32_t *pNodeCount, uint32_t *pMeshCount, uint32_t *pKeyFrameCount)
{
  ++(*pNodeCount);

  for (uint32_t i = 0; i < pNode->graphCount; ++i)
    *pKeyFrameCount += pNode->pGraphs[i].keyFrameCount;

  if (pNode->pMesh != nullptr)
    ++(*pMeshCount);

  for (uint32_t i = 0; i < pNode->childCount; ++i)
    _GetCounts(pNode->pChildren + i, pNodeCount, pMeshCount, pKeyFrameCount);
}

static void _SortAndCopyKeyFrames(AdkKeyFrame *pDestination, AdkKeyFrame *pSource, uint32_t count)
{
  //Copy position key frames, sorting by time.
  for (uint32_t i = 0; i < count; ++i)
  {
    AdkKeyFrame *pBest = pSource + i;
    for (uint32_t j = i + 1; j < count; ++j)
    {
      if (pSource[j].time < pBest->time)
        pBest = pSource + j;
    }
  
    pDestination[i] = *pBest;
  }
}

static void _CopyAnimationNode(AdkAnimationNode *pSource, uint32_t destinationOffset, uint32_t destinationChildrenOffset, AdkAnimationNodeInternal *pDestinationArray, uint32_t *pMeshesOffset, AdkMesh **ppMeshes, uint32_t *pKeyFramesOffset, AdkKeyFrame *pKeyFrames)
{
  AdkAnimationNodeInternal *pDestination = pDestinationArray + destinationOffset;
  
  //Copy the animation node from source to destination, duplicating key frames array and pointing children array to specified memory.
  if (pSource->pMesh == nullptr)
  {
    pDestination->meshIndex = ADK_ANIMATION_MESH_INDEX_NONE;
  }
  else
  {
    ppMeshes[*pMeshesOffset] = pSource->pMesh;
    pDestination->meshIndex = (*pMeshesOffset)++;
  }
  pDestination->childCount = pSource->childCount;
  pDestination->pChildren = pDestinationArray + destinationChildrenOffset;
  adkStrcpy(pDestination->name, ADK_ANIMATION_NODE_NAME_MAX_LENGTH, pSource->pName);
  pDestination->isBone = pSource->isBone;

  //Allocate key frames and sort them by time.
  pDestination->timelineMask = 0;
  for (uint32_t i = 0; i < pSource->graphCount; ++i)
  {
    AdkAnimationGraph *pGraph = pSource->pGraphs + i;
    pDestination->timelineMask |= 1 << pGraph->type;
    AdkAnimationTimeline *pTimeline = &pDestination->timelines[pGraph->type];
    pTimeline->keyFrames.start = *pKeyFramesOffset;
    pTimeline->keyFrames.length = pGraph->keyFrameCount;
    _SortAndCopyKeyFrames(pKeyFrames + *pKeyFramesOffset, pGraph->pKeyFrames, pGraph->keyFrameCount);
    *pKeyFramesOffset += pGraph->keyFrameCount;
  }
}

void adkAnimation_Create(AdkInstance *pInstance, AdkAnimationNode *pHierarchy, const adkAllocationCallbacks *pAllocator, AdkAnimation **ppAnimation)
{
  AdkAnimation *pAnimation = adkAllocTypeCall(AdkAnimation, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->pInstance = pInstance;
  pAnimation->pAllocator = pAllocator;

  //Get node and mesh count. Allocate node and mesh arrays.
  _GetCounts(pHierarchy, &pAnimation->nodeCount, &pAnimation->meshCount, &pAnimation->keyFrameCount);
  pAnimation->pNodes = adkAllocTypeCall(AdkAnimationNodeInternal, pAnimation->nodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->ppMeshes = adkAllocTypeCall(AdkMesh*, pAnimation->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->pKeyFrames = adkAllocTypeCall(AdkKeyFrame, pAnimation->keyFrameCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  AdkAnimationNode **ppNodeQueue = adkAllocTypeCall(AdkAnimationNode*, pAnimation->nodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  //Convert nodes to breadth first and copy them to animation.
  uint32_t meshOffset = 0, keyFrameOffset = 0;
  uint32_t nodeQueueStart = 0, nodeQueueEnd = 0;
  ppNodeQueue[nodeQueueEnd++] = pHierarchy;
  while (nodeQueueStart != nodeQueueEnd)
  {
    //Copy node to animation.
    AdkAnimationNode *pNode = ppNodeQueue[nodeQueueStart];
    _CopyAnimationNode(pNode, nodeQueueStart, nodeQueueEnd, pAnimation->pNodes, &meshOffset, pAnimation->ppMeshes, &keyFrameOffset, pAnimation->pKeyFrames);

    if (pNode->isBone)
      ++pAnimation->boneCount;

    //Add children to queue.
    for (uint32_t i = 0; i < pNode->childCount; ++i)
      ppNodeQueue[nodeQueueEnd++] = pNode->pChildren + i;

    ++nodeQueueStart;
  }

  pAnimation->pBoneIndices = adkAllocTypeCall(uint32_t, pAnimation->boneCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  // Add bones indices to array.
  for (uint32_t i = 0, boneIterator = 0; i < pAnimation->nodeCount; ++i)
  {
    if (pAnimation->pNodes[i].isBone)
    {
      pAnimation->pBoneIndices[boneIterator] = i;
      ++boneIterator;
    }
  }

  adkFreeCall(ppNodeQueue, pAllocator);

  (*ppAnimation) = pAnimation;
}

void adkAnimation_Destroy(AdkAnimation **ppAnimation)
{
  if (*ppAnimation == nullptr)
    return;

  AdkAnimation *pAnimation = (*ppAnimation);
  adkFreeCall(pAnimation->pNodes, pAnimation->pAllocator);
  adkFreeCall(pAnimation->ppMeshes, pAnimation->pAllocator);
  adkFreeCall(pAnimation->pKeyFrames, pAnimation->pAllocator);
  adkFreeCall(pAnimation->pBoneIndices, pAnimation->pAllocator);
  adkFreeCall(pAnimation, pAnimation->pAllocator);
  *ppAnimation = pAnimation;
}

bool adkAnimation_Serialize(AdkAnimation *pAnimation, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, pAnimation->nodeCount);
  adkBinaryWriter_Write(pBinaryWriter, pAnimation->keyFrameCount);
  adkBinaryWriter_Write(pBinaryWriter, pAnimation->meshCount);
  adkBinaryWriter_Write(pBinaryWriter, pAnimation->boneCount);

  for (uint32_t i = 0; i < pAnimation->nodeCount; ++i)
  {
    adkBinaryWriter_WriteString(pBinaryWriter, pAnimation->pNodes[i].name);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].meshIndex);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].timelineMask);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].childCount);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].isBone);
    adkBinaryWriter_Write(pBinaryWriter, (uint32_t)(pAnimation->pNodes[i].pChildren - pAnimation->pNodes));
    
    for (uint32_t j = 0; j < ADK_ANIMATION_GRAPH_TYPE_COUNT; ++j)
    {
      adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].timelines[j].keyFrames.start);
      adkBinaryWriter_Write(pBinaryWriter, pAnimation->pNodes[i].timelines[j].keyFrames.length);
    }
  }

  for (uint32_t i = 0; i < pAnimation->keyFrameCount; ++i)
  {
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pKeyFrames[i].time);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pKeyFrames[i].curve.a);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pKeyFrames[i].curve.b);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pKeyFrames[i].curve.c);
    adkBinaryWriter_Write(pBinaryWriter, pAnimation->pKeyFrames[i].curve.d);
  }

  adkBinaryWriter_Write(pBinaryWriter, pAnimation->boneCount * sizeof(*pAnimation->pBoneIndices), pAnimation->pBoneIndices);

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkAnimation_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, const adkAllocationCallbacks *pAllocator, AdkAnimation **ppAnimation)
{
  bool success = false;

  AdkAnimation *pAnimation = adkAllocTypeCall(AdkAnimation, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->pInstance = pInstance;
  pAnimation->pAllocator = pAllocator;

  adkBinaryReader_Read(pBinaryReader, &pAnimation->nodeCount);
  adkBinaryReader_Read(pBinaryReader, &pAnimation->keyFrameCount);
  adkBinaryReader_Read(pBinaryReader, &pAnimation->meshCount);
  adkBinaryReader_Read(pBinaryReader, &pAnimation->boneCount);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  pAnimation->pAllocator = pAllocator;
  pAnimation->pNodes = adkAllocTypeCall(AdkAnimationNodeInternal, pAnimation->nodeCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->pKeyFrames = adkAllocTypeCall(AdkKeyFrame, pAnimation->keyFrameCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->ppMeshes = adkAllocTypeCall(AdkMesh*, pAnimation->meshCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimation->pBoneIndices = adkAllocTypeCall(uint32_t, pAnimation->boneCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pAnimation->nodeCount; ++i)
  {
    adkBinaryReader_ReadString(pBinaryReader, ADK_ANIMATION_NODE_NAME_MAX_LENGTH, pAnimation->pNodes[i].name);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].meshIndex);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].timelineMask);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].childCount);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].isBone);

    uint32_t childrenOffset = 0;
    adkBinaryReader_Read(pBinaryReader, &childrenOffset);
    pAnimation->pNodes[i].pChildren = &pAnimation->pNodes[childrenOffset];

    for (uint32_t j = 0; j < ADK_ANIMATION_GRAPH_TYPE_COUNT; ++j)
    {
      adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].timelines[j].keyFrames.start);
      adkBinaryReader_Read(pBinaryReader, &pAnimation->pNodes[i].timelines[j].keyFrames.length);
    }
  }

  for (uint32_t i = 0; i < pAnimation->keyFrameCount; ++i)
  {
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pKeyFrames[i].time);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pKeyFrames[i].curve.a);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pKeyFrames[i].curve.b);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pKeyFrames[i].curve.c);
    adkBinaryReader_Read(pBinaryReader, &pAnimation->pKeyFrames[i].curve.d);
  }

  adkBinaryReader_Read(pBinaryReader, pAnimation->boneCount * sizeof(*pAnimation->pBoneIndices), pAnimation->pBoneIndices);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  *ppAnimation = pAnimation;
  success = true;

epilogue:
  if (!success)
  {
    adkFreeCall(pAnimation->pBoneIndices, pAllocator);
    adkFreeCall(pAnimation->ppMeshes, pAllocator);
    adkFreeCall(pAnimation->pKeyFrames, pAllocator);
    adkFreeCall(pAnimation->pNodes, pAllocator);
    adkFreeCall(pAnimation, pAllocator);
  }

  return success;
}

void adkAnimation_Copy(AdkAnimation *pSource, AdkAnimation **ppCopy)
{
  AdkAnimation *pCopy = adkAllocTypeCall(AdkAnimation, 1, adkAF_Zero, pSource->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pCopy->pInstance = pSource->pInstance;
  pCopy->pAllocator = pSource->pAllocator;
  pCopy->nodeCount = pSource->nodeCount;
  pCopy->pNodes = adkAllocTypeCall(AdkAnimationNodeInternal, pSource->nodeCount, adkAF_None, pSource->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pCopy->pNodes, pSource->pNodes, pSource->nodeCount * sizeof(AdkAnimationNodeInternal));
  pCopy->meshCount = pSource->meshCount;
  pCopy->ppMeshes = adkAllocTypeCall(AdkMesh*, pSource->meshCount, adkAF_None, pSource->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pCopy->ppMeshes, pSource->ppMeshes, pSource->meshCount * sizeof(AdkMesh*));
  pCopy->boneCount = pSource->boneCount;
  pCopy->pBoneIndices = adkAllocTypeCall(uint32_t, pSource->boneCount, adkAF_None, pSource->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pCopy->pBoneIndices, pSource->pBoneIndices, pSource->boneCount * sizeof(uint32_t));
  pCopy->keyFrameCount = pSource->keyFrameCount;
  pCopy->pKeyFrames = adkAllocTypeCall(AdkKeyFrame, pSource->keyFrameCount, adkAF_None, pSource->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memcpy(pCopy->pKeyFrames, pSource->pKeyFrames, pSource->keyFrameCount * sizeof(AdkKeyFrame));
  *ppCopy = pCopy;
}

uint32_t adkAnimation_GetMeshCount(AdkAnimation *pAnimation)
{
  return pAnimation->meshCount;
}

void adkAnimation_SetShader(AdkAnimation *pAnimation, uint32_t index, const AdkMeshShaderInfo *pMeshShaderInfo)
{
  adkMesh_SetShader(pAnimation->ppMeshes[index], pMeshShaderInfo);
}

void adkAnimation_SetMesh(AdkAnimation *pAnimation, uint32_t index, AdkMesh *pMesh)
{
  pAnimation->ppMeshes[index] = pMesh;
}

bool adkAnimation_GetNodeByName(AdkAnimation *pAnimation, const char *pName, uint32_t *pIndex)
{
  for (uint32_t i = 0; i < pAnimation->nodeCount; ++i)
  {
    if (adkStrcmp(pAnimation->pNodes[i].name, pName) != 0)
      continue;

    *pIndex = i;
    return true;
  }

  return false;
}

void adkAnimationRenderer_Create(AdkAnimation *pAnimation, adkWindow *pWindow, uint32_t modelCount, AdkAnimationRenderer **ppAnimationRenderer)
{
  AdkAnimationRenderer *pAnimationRenderer = adkAllocTypeCall(AdkAnimationRenderer, 1, adkAF_Zero, pAnimation->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimationRenderer->pMatrices = adkAllocTypeCall(adkMatrix4x4<float>, pAnimation->nodeCount * modelCount, adkAF_Zero, pAnimation->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimationRenderer->pBoneMatrices = adkAllocTypeCall(adkMatrix4x4<float>, pAnimation->boneCount, adkAF_Zero, pAnimation->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pAnimationRenderer->pAnimation = pAnimation;
  pAnimationRenderer->ppMeshRenderers = adkAllocTypeCall(AdkMeshRenderer*, pAnimation->meshCount, adkAF_Zero, pAnimation->pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  for (uint32_t i = 0; i < pAnimation->meshCount; ++i)
    adkMeshRenderer_Create(pAnimation->ppMeshes[i], pWindow, modelCount, pAnimationRenderer->ppMeshRenderers + i);
  pAnimationRenderer->modelCount = modelCount;

  *ppAnimationRenderer = pAnimationRenderer;
}

void adkAnimationRenderer_Destroy(AdkAnimationRenderer **ppAnimationRenderer)
{
  if (*ppAnimationRenderer == nullptr)
    return;

  AdkAnimationRenderer *pAnimationRenderer = *ppAnimationRenderer;
  adkFreeCall(pAnimationRenderer->pMatrices, pAnimationRenderer->pAnimation->pAllocator);
  adkFreeCall(pAnimationRenderer->pBoneMatrices, pAnimationRenderer->pAnimation->pAllocator);
  for (uint32_t i = 0; i < pAnimationRenderer->pAnimation->meshCount; ++i)
    adkMeshRenderer_Destroy(pAnimationRenderer->ppMeshRenderers + i);
  adkFreeCall(pAnimationRenderer->ppMeshRenderers, pAnimationRenderer->pAnimation->pAllocator);
  adkFreeCall(pAnimationRenderer, pAnimationRenderer->pAnimation->pAllocator);
  *ppAnimationRenderer = nullptr;
}

static uint32_t _GetKeyFrameAt(uint32_t keyFrameCount, AdkKeyFrame *pKeyFrames, float time)
{
  //Get key frame.
  for (uint32_t i = 1; i < keyFrameCount; ++i)
  {
    if (time < pKeyFrames[i].time)
      return i - 1;
  }

  return keyFrameCount - 1;
}

static adkMatrix4x4<float> _GetAnimationMatrix(uint32_t nodeIndex, AdkAnimationTransform *pTransform)
{
  adkVector3<float> blendTranslation = adkVector3<float>::zero();
  adkQuaternion<float> blendRotation = adkQuaternion<float>::identity();
  adkVector3<float> blendScale = adkVector3<float>::zero();

  for (uint32_t blendLayerIndex = 0; blendLayerIndex < pTransform->blendLayerCount; ++blendLayerIndex)
  {
    const AdkAnimationBlendLayer *pBlendLayer = &pTransform->pBlendLayers[blendLayerIndex];
    AdkAnimationNodeInternal *pNode = &pBlendLayer->pAnimation->pNodes[nodeIndex];
    AdkKeyFrame *pKeyFrames = pBlendLayer->pAnimation->pKeyFrames;

    float time = pBlendLayer->time;
    if (time < 0.f)
      time = 0.f;

    //Get transform by interpolating between key frames given the current time.
    float values[ADK_ANIMATION_GRAPH_TYPE_COUNT];
    uint32_t graphMask = 1;
    for (uint32_t graphIndex = 0; graphIndex < ADK_ANIMATION_GRAPH_TYPE_COUNT; ++graphIndex, graphMask <<= 1)
    {
      if (pNode->timelineMask & graphMask)
      {
        AdkAnimationTimeline *pTimeline = pNode->timelines + graphIndex;
        AdkKeyFrame *pTimelineKeyFrames = pKeyFrames + pTimeline->keyFrames.start;

        if (pTimeline->keyFrames.length == 1)
        {
          values[graphIndex] = pTimelineKeyFrames[0].curve.at(0.f);
        }
        else
        {
          float startTime = pTimelineKeyFrames[0].time;
          float endTime = pTimelineKeyFrames[pTimeline->keyFrames.length - 1].time;
          float timeInAnimation = adkRepeat(time, startTime, endTime);
          uint32_t keyFrameIndex = _GetKeyFrameAt(pTimeline->keyFrames.length, pTimelineKeyFrames, timeInAnimation);

          uint32_t nextKeyFrameIndex = (keyFrameIndex + 1) % pTimeline->keyFrames.length;
          float t = adkInverseLerp(pTimelineKeyFrames[keyFrameIndex].time, pTimelineKeyFrames[nextKeyFrameIndex].time, timeInAnimation);
          values[graphIndex] = pTimelineKeyFrames[keyFrameIndex].curve.at(t);
        }
      }
      else
      {
        values[graphIndex] = ADK_ANIMATION_GRAPH_DEFAULT_VALUES[graphIndex];
      }
    }

    //Blend transform for current layer with accumulated transforms
    adkVector3<float> translation;
    translation.x = values[ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_X];
    translation.y = values[ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Y];
    translation.z = values[ADK_ANIMATION_GRAPH_TYPE_TRANSLATION_Z];
    blendTranslation += pBlendLayer->weight * translation;

    adkVector3<float> rotation;
    rotation.x = values[ADK_ANIMATION_GRAPH_TYPE_ROTATION_X];
    rotation.y = values[ADK_ANIMATION_GRAPH_TYPE_ROTATION_Y];
    rotation.z = values[ADK_ANIMATION_GRAPH_TYPE_ROTATION_Z];
    adkQuaternion<float> rotationQuat = adkQuaternion<float>::create(rotation);
    adkQuaternion<float> weightedQuat = adkQuaternion<float>::slerp(adkQuaternion<float>::identity(), rotationQuat, pBlendLayer->weight);
    blendRotation = blendRotation * weightedQuat;


    adkVector3<float> scale;
    scale.x = values[ADK_ANIMATION_GRAPH_TYPE_SCALE_X];
    scale.y = values[ADK_ANIMATION_GRAPH_TYPE_SCALE_Y];
    scale.z = values[ADK_ANIMATION_GRAPH_TYPE_SCALE_Z];
    blendScale += pBlendLayer->weight * scale;
  }

  adkMatrix4x4<float> translationMatrix = adkMatrix4x4<float>::translate(blendTranslation);
  adkMatrix4x4<float> rotationMatrix = adkMatrix4x4<float>::rotateQuaternion(blendRotation);
  adkMatrix4x4<float> scaleMatrix = adkMatrix4x4<float>::scaleNonUniform(blendScale);

  return translationMatrix * rotationMatrix * scaleMatrix;
}

adkMatrix4x4<float> adkAnimationRenderer_GetNodeTransform(AdkAnimationRenderer *pAnimationRenderer, uint32_t nodeIndex)
{
  return pAnimationRenderer->pMatrices[nodeIndex];
}

void adkAnimationRenderer_Transform(AdkAnimationRenderer *pAnimationRenderer, float time)
{
  AdkAnimationBlendLayer blendLayer;
  blendLayer.pAnimation = pAnimationRenderer->pAnimation;
  blendLayer.time = time;
  blendLayer.weight = 1.f;

  //TODO: Make this work with multiple transforms.
  AdkAnimationTransform transform;
  transform.blendLayerCount = 1;
  transform.pBlendLayers = &blendLayer;

  adkAnimationRenderer_Transform(pAnimationRenderer, 1, &transform);
}

void adkAnimationRenderer_Transform(AdkAnimationRenderer *pAnimationRenderer, uint32_t transformCount, AdkAnimationTransform *pTransforms)
{
  adkMatrix4x4<float> identity = adkMatrix4x4<float>::identity();

  for (uint32_t transformIndex = 0; transformIndex < transformCount; ++transformIndex)
  {
    adkMatrix4x4<float> *pInstanceMatrices = &pAnimationRenderer->pMatrices[transformIndex * pAnimationRenderer->pAnimation->nodeCount];

    uint32_t nextParentIndex = 0;
    uint32_t childCount = 1;
    adkMatrix4x4<float> *pParentMatrix = &identity;

    for (uint32_t nodeIndex = 0; nodeIndex < pAnimationRenderer->pAnimation->nodeCount; ++nodeIndex)
    {
      pInstanceMatrices[nodeIndex] = (*pParentMatrix) * _GetAnimationMatrix(nodeIndex, &pTransforms[transformIndex]);

      while (--childCount == 0 && nextParentIndex < pAnimationRenderer->pAnimation->nodeCount)
      {
        pParentMatrix = &pInstanceMatrices[nextParentIndex];
        childCount = pAnimationRenderer->pAnimation->pNodes[nextParentIndex].childCount + 1; //Add one here so when the while loop checks again, the child count will be correct.
        ++nextParentIndex;
      }
    }
  }
}

void adkAnimationRenderer_Render(AdkAnimationRenderer *pAnimationRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount, const adkMatrix4x4<float> *pModels)
{
  //Render all the meshes contained in this skeleton.
  for (uint32_t nodeIndex = 0; nodeIndex < pAnimationRenderer->pAnimation->nodeCount; ++nodeIndex)
  {
    if (pAnimationRenderer->pAnimation->pNodes[nodeIndex].meshIndex != ADK_ANIMATION_MESH_INDEX_NONE)
    {
      //Render this node for each model.
      AdkMeshRenderer *pMeshRenderer = pAnimationRenderer->ppMeshRenderers[pAnimationRenderer->pAnimation->pNodes[nodeIndex].meshIndex];
      adkMatrix4x4<float> *pMappedModels = nullptr;
      adkMatrix4x4<float> *pMappedBones = nullptr;

      adkMeshRenderer_MapModels(pMeshRenderer, &pMappedModels);

      for (uint32_t modelIndex = 0; modelIndex < modelCount; ++modelIndex)
      {
        uint32_t matrixIndex = modelIndex * pAnimationRenderer->pAnimation->nodeCount + nodeIndex;
        pMappedModels[modelIndex] = pModels[modelIndex] * pAnimationRenderer->pMatrices[matrixIndex];
      }

      if (adkMeshRenderer_HasSkin(pMeshRenderer))
      {
        adkMeshRenderer_MapBones(pMeshRenderer, &pMappedBones);

        for (uint32_t modelIndex = 0; modelIndex < pAnimationRenderer->modelCount; ++modelIndex)
        {
          //Offset to this instance's matrices.
          adkMatrix4x4<float> *pInstanceMatrices = &pAnimationRenderer->pMatrices[modelIndex * pAnimationRenderer->pAnimation->nodeCount];

          for (uint32_t boneIndex = 0; boneIndex < pAnimationRenderer->pAnimation->boneCount; ++boneIndex)
          {
            uint32_t mappedBoneIndex = modelIndex * pAnimationRenderer->pAnimation->boneCount + boneIndex;
            uint32_t boneMatrixIndex = pAnimationRenderer->pAnimation->pBoneIndices[boneIndex];
            pMappedBones[mappedBoneIndex] = pInstanceMatrices[boneMatrixIndex];
          }
        }

        adkMeshRenderer_UnmapBones(pMeshRenderer, &pMappedBones);
      }

      adkMeshRenderer_UnmapModels(pMeshRenderer, &pMappedModels);

      //Render
      adkMeshRenderer_Render(pMeshRenderer, pView, pProjection, modelCount);
    }
  }
}