#ifndef AdkRigidbody_h__
#define AdkRigidbody_h__

#include "adkMath.h"
#include "adkMemory.h"
#include "AdkOctree.h"

struct AdkRigidbodyBuffer;

typedef struct AdkRigidbodyProperties
{
  bool enabled;
  float mass;
  float drag;
  float radius;
  float bounce;
  float slide;
  adkVector3<float> position;
  adkVector3<float> realPosition;
  adkVector3<float> velocity;
  adkVector3<float> storedDisplacement;
  adkQuaternion<float> rotation;
} AdkRigidbodyProperties;

typedef enum AdkRigidbodyCollisionResolveFlagBits
{
  ADK_RIGIDBODY_COLLISION_RESOLVE_NONE_BIT = 0x00000000,
  ADK_RIGIDBODY_COLLISION_RESOLVE_POSITION_BIT = 0x00000001,
  ADK_RIGIDBODY_COLLISION_RESOLVE_VELOCITY_BIT = 0x00000002,
  ADK_RIGIDBODY_COLLISION_RESOLVE_REMAINING_DISPLACEMENT_BIT = 0x00000004,
  ADK_RIGIDBODY_COLLISION_RESOLVE_PERPENDICULAR_OFFSET_BIT = 0x00000008,
  ADK_RIGIDBODY_COLLISION_RESOLVE_ALL_BIT = 0x0000000F,
} AdkRigidbodyCollisionResolveFlagBits;
typedef AdkFlags AdkRigidbodyCollisionResolveFlags;

typedef struct AdkRigidbodyCollisionResolveData
{
  const AdkSphereSweepCollision *pCollision;
  AdkRigidbodyProperties *pRigidbodyProperties;
  AdkRigidbodyCollisionResolveFlags resolvedFlags;
} AdkRigidbodyCollisionResolveData;

typedef void (ADKAPI_CALL AdkRigidbodyCollisionResolveFunction)(void *pUserData, uint32_t collisionCount, AdkRigidbodyCollisionResolveData *pCollisionResolveData);

typedef struct AdkRigidbodyBufferCreateInfo
{
  uint32_t rigidbodyCount;
  AdkRigidbodyCollisionResolveFunction *pCollisionResolveFunction;
  void *pCollisionResolveUserData;
} AdkRigidbodyBufferCreateInfo;

void adkRigidbodyBuffer_Create(const AdkRigidbodyBufferCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkRigidbodyBuffer **ppRigidbodyBuffer);
void adkRigidbodyBuffer_Destroy(AdkRigidbodyBuffer **ppRigidbodyBuffer);
uint32_t adkRigidbodyBuffer_GetCount(AdkRigidbodyBuffer *pRigidbodyBuffer);
void adkRigidbodyBuffer_Map(AdkRigidbodyBuffer *pRigidbodyBuffer, AdkRigidbodyProperties **ppProperties);
void adkRigidbodyBuffer_Unmap(AdkRigidbodyBuffer *pRigidbodyBuffer, AdkRigidbodyProperties **ppProperties);
uint32_t adkRigidbodyBuffer_Find(AdkRigidbodyBuffer *pRigidbodyBuffer, const adkVector3<float> &position, float radius, uint32_t foundLength, uint32_t *pFound);

#endif // AdkRigidbody_h__
