#include "AdkRigidbody.inl"
#include "AdkTime.h"

void adkRigidbodyBuffer_Create(const AdkRigidbodyBufferCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkRigidbodyBuffer **ppRigidbodyBuffer)
{
  AdkRigidbodyBuffer *pRigidbodyBuffer = adkAllocTypeCall(AdkRigidbodyBuffer, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pRigidbodyBuffer->pAllocator = pAllocator;
  pRigidbodyBuffer->pCollisionResolveFunction = pCreateInfo->pCollisionResolveFunction;
  pRigidbodyBuffer->pCollisionResolveUserData = pCreateInfo->pCollisionResolveUserData;
  pRigidbodyBuffer->rigidbodyCount = pCreateInfo->rigidbodyCount;

  pRigidbodyBuffer->pRigidbodies = adkAllocTypeCall(AdkRigidbodyProperties, pCreateInfo->rigidbodyCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  *ppRigidbodyBuffer = pRigidbodyBuffer;
}

void adkRigidbodyBuffer_Destroy(AdkRigidbodyBuffer **ppRigidbodyBuffer)
{
  if (*ppRigidbodyBuffer == nullptr)
    return;

  AdkRigidbodyBuffer *pRigidbodyBuffer = *ppRigidbodyBuffer;

  adkFreeCall(*ppRigidbodyBuffer, pRigidbodyBuffer->pAllocator);
}

uint32_t adkRigidbodyBuffer_GetCount(AdkRigidbodyBuffer *pRigidbodyBuffer)
{
  return pRigidbodyBuffer->rigidbodyCount;
}

void adkRigidbodyBuffer_Map(AdkRigidbodyBuffer *pRigidbodyBuffer, AdkRigidbodyProperties **ppProperties)
{
  *ppProperties = pRigidbodyBuffer->pRigidbodies;
}

void adkRigidbodyBuffer_Unmap(AdkRigidbodyBuffer * /*pRigidbodyBuffer*/, AdkRigidbodyProperties **ppProperties)
{
  ppProperties = nullptr;
}

uint32_t adkRigidbodyBuffer_Find(AdkRigidbodyBuffer *pRigidbodyBuffer, const adkVector3<float> &position, float radius, uint32_t foundLength, uint32_t *pFound)
{
  uint32_t foundCount = 0;

  if (foundLength != 0)
  {
    for (uint32_t i = 0; i < pRigidbodyBuffer->rigidbodyCount; ++i)
    {
      float radii = pRigidbodyBuffer->pRigidbodies[i].radius + radius;
      if (adkVector3<float>::squareDistance(position, pRigidbodyBuffer->pRigidbodies[i].realPosition) <= radii * radii)
      {
        pFound[foundCount] = i;
        ++foundCount;

        if (foundCount == foundLength)
          break;
      }
    }
  }

  return foundCount;
}