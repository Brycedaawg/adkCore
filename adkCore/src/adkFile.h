#ifndef adkFile_h__
#define adkFile_h__

#include "adkTypedefs.h"

struct AdkFile;

typedef enum AdkFileOpenFlagBits
{
  ADK_FILE_OPEN_READ_BIT = 0x00000001,
  ADK_FILE_OPEN_WRITE_BIT = 0x00000002,
  ADK_FILE_OPEN_CREATE_BIT = 0x00000004,
  ADK_FILE_OPEN_APPEND_BIT = 0x00000008,
} AdkFileOpenFlagBits;
typedef AdkFlags AdkFileOpenFlags;

typedef enum AdkFileOrigin
{
  ADK_FILE_ORIGIN_START,
  ADK_FILE_ORIGIN_CURRENT,
  ADK_FILE_ORIGIN_END,
} AdkFileOrigin;

AdkBool32 adkFile_Load(const char *pFilename, size_t *pBufferSize, void** ppBuffer);
AdkBool32 adkFile_Open(const char *pFilename, AdkFileOpenFlags flags, size_t *pFileSize, AdkFile **ppFile);
void adkFile_Close(AdkFile **ppFile);
AdkBool32 adkFile_Read(AdkFile *pFile, size_t bufferSize, void *pBuffer);
AdkBool32 adkFile_Write(AdkFile *pFile, size_t bufferSize, void *pBuffer);
AdkBool32 adkFile_Seek(AdkFile *pFile, AdkFileOrigin origin, int32_t offset);

#endif // adkFile_h__