#ifndef AdkQueue_inl__
#define AdkQueue_inl__

#include "AdkQueue.h"

typedef struct AdkQueue
{
  AdkInstance *pInstance;
  const adkAllocationCallbacks *pAllocator;
  uint32_t familyIndex;
  uint32_t index;
} AdkQueue;

#endif // AdkQueue_inl__