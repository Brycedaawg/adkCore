#include "adkPerlin3d.h"
#include "adkShader.h"
#include "AdkComputePipeline.h"
#include "adkPerlin.comp.spv.h"

typedef struct AdkPerlin3d
{
  AdkInstance *pInstance;
  AdkBuffer *pInfoBuffer;
  AdkTexture *pTexture;
  AdkShader *pShader;
  AdkComputePipeline *pComputePipeline;
  const adkAllocationCallbacks *pAllocator;
} AdkPerlin3d;

typedef struct ShaderInfoLayout
{
  adkVector4<float> sampleOffset;
  adkVector4<int32_t> storeOffset;
  adkVector4<int32_t> storeExtent;
  uint32_t seed;
  uint32_t octaves;
  float frequency;
  float persistence;
  uint32_t test;
} ShaderInfoLayout;

void adkPerlin3d_Create(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pComputeQueue, AdkQueue *pTransferQueue, const adkVector3<uint32_t> &extent, const adkAllocationCallbacks *pAllocator, AdkPerlin3d **ppPerlin3d)
{
  AdkPerlin3d *pPerlin3d = adkAllocTypeCall(AdkPerlin3d, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pPerlin3d->pInstance = pInstance;
  pPerlin3d->pAllocator = pAllocator;

  AdkBufferCreateInfo infoBufferCreateInfo;
  infoBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  infoBufferCreateInfo.type = ADK_BUFFER_TYPE_UINT8;
  infoBufferCreateInfo.elementCount = sizeof(ShaderInfoLayout);
  infoBufferCreateInfo.copyCount = 0;
  infoBufferCreateInfo.pCopies = nullptr;
  infoBufferCreateInfo.pTransferQueue = pTransferQueue;

  adkBuffer_Create(pInstance, &infoBufferCreateInfo, &pPerlin3d->pInfoBuffer);

  AdkTextureCreateInfo textureCreateInfo;
  textureCreateInfo.flags = 0;
  textureCreateInfo.dimensions = ADK_TEXTURE_DIMENSIONS_3D;
  textureCreateInfo.extent = extent;
  textureCreateInfo.pColors = nullptr;
  textureCreateInfo.colorType = ADK_COLOR_TYPE_R32_SFLOAT;
  textureCreateInfo.minFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.magFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.mipmapMode = ADK_TEXTURE_MIPMAP_MODE_NONE;
  textureCreateInfo.addressModeU = ADK_TEXTURE_ADDRESS_MODE_REPEAT;
  textureCreateInfo.addressModeV = ADK_TEXTURE_ADDRESS_MODE_REPEAT;
  textureCreateInfo.anisotropyMode = ADK_TEXTURE_ANISOTROPY_MODE_DISABLED;
  textureCreateInfo.pGraphicsQueue = pGraphicsQueue;

  adkTexture_Create(pInstance, &textureCreateInfo, &pPerlin3d->pTexture);

  AdkShaderCreateInfo shaderCreateInfo;
  shaderCreateInfo.flags = 0;
  shaderCreateInfo.pVertexShader = nullptr;
  shaderCreateInfo.pGeometryShader = nullptr;
  shaderCreateInfo.pFragmentShader = nullptr;
  shaderCreateInfo.computeShaderSize = sizeof(ADKPERLIN_COMP_SHADER_CODE);
  shaderCreateInfo.pComputeShader = (const char*)ADKPERLIN_COMP_SHADER_CODE;

  adkShader_Create(pInstance, &shaderCreateInfo, &pPerlin3d->pShader);

  AdkComputeStorageBuffer computeStorageBuffers[1];
  computeStorageBuffers[0].binding = 0;
  computeStorageBuffers[0].pBuffer = pPerlin3d->pInfoBuffer;

  AdkComputeStorageImage computeStorageImages[1];
  computeStorageImages[0].binding = 1;
  computeStorageImages[0].pTexture = pPerlin3d->pTexture;

  AdkComputePipelineCreateInfo perlinComputePipelineCreateInfo;
  perlinComputePipelineCreateInfo.storageBufferCount = ADK_ARRAY_SIZE(computeStorageBuffers);
  perlinComputePipelineCreateInfo.pStorageBuffers = computeStorageBuffers;
  perlinComputePipelineCreateInfo.storageImageCount = ADK_ARRAY_SIZE(computeStorageImages);
  perlinComputePipelineCreateInfo.pStorageImages = computeStorageImages;
  perlinComputePipelineCreateInfo.pShader = pPerlin3d->pShader;
  perlinComputePipelineCreateInfo.pComputeQueue = pComputeQueue;

  adkComputePipeline_Create(pInstance, &perlinComputePipelineCreateInfo, pAllocator, &pPerlin3d->pComputePipeline);

  *ppPerlin3d = pPerlin3d;
}

void adkPerlin3d_Destroy(AdkPerlin3d **ppPerlin3d)
{
  AdkPerlin3d *pPerlin3d = *ppPerlin3d;

  adkComputePipeline_Destroy(&pPerlin3d->pComputePipeline);
  adkShader_Destroy(&pPerlin3d->pShader);
  adkTexture_Destroy(pPerlin3d->pInstance, &pPerlin3d->pTexture);
  adkBuffer_Destroy(pPerlin3d->pInstance, &pPerlin3d->pInfoBuffer);
  

  adkFreeCall(ppPerlin3d, pPerlin3d->pAllocator);
}

void adkPerlin3d_Generate(AdkPerlin3d *pPerlin3d, uint32_t seed, uint32_t octaves, float frequency, float persistence, const adkVector3<float> &sampleOffset, const adkVector3<uint32_t> &storeOffset, const adkVector3<uint32_t> &storeExtent)
{
  ShaderInfoLayout layout;
  layout.sampleOffset = adkVector4<float>::create(sampleOffset.x, sampleOffset.y, sampleOffset.z, 0.f);
  layout.storeOffset = adkVector4<int32_t>::create(storeOffset.x, storeOffset.y, storeOffset.z, 0);
  layout.storeExtent = adkVector4<int32_t>::create(storeExtent.x, storeExtent.y, storeExtent.z, 0);
  layout.seed = seed;
  layout.octaves = octaves;
  layout.frequency = frequency;
  layout.persistence = persistence;

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = sizeof(layout);
  bufferCopy.pData = &layout;

  adkBuffer_Set(pPerlin3d->pInstance, pPerlin3d->pInfoBuffer, 1, &bufferCopy);

  adkVector3<uint32_t> groupCount = adkVector3<uint32_t>::create((storeExtent.x + 7) / 8, (storeExtent.y + 7) / 8, (storeExtent.z + 15) / 16);
  adkComputePipeline_Dispatch(pPerlin3d->pComputePipeline, groupCount);
}

void adkPerlin3d_GetTexture(AdkPerlin3d *pPerlin3d, AdkTexture **ppTexture)
{
  *ppTexture = pPerlin3d->pTexture;
}