#include "adkMemoryPool.inl"
#include "vulkan.h"
#include "adkMath.h"
#include "adkVkTools.inl"

static const uint32_t ADK_MEMORY_POOL_COUNT = 1024;
static const size_t ADK_MEMORY_POOL_MIN_BUFFER_SIZE = 1024 * 1024 * 64; //This is the minimum size a single pool's buffer can be. The buffer's size will be affected by alignment. It will end up being a power of two.
static const uint32_t ADK_MEMORY_POOL_BITS_PER_NODE = 2; //Each node consumes this many bits is the binary tree.

typedef enum AdkMemoryHeapNodeState
{
  ADK_MEMORY_HEAP_NODE_STATE_FREE = 0x00000000,
  ADK_MEMORY_HEAP_NODE_STATE_PARTIALLY_ALLOCATED = 0x00000001,
  ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED = 0x00000002,
} AdkMemoryHeapNodeState;

typedef struct AdkMemoryHeap
{
  const VkAllocationCallbacks *pVkAllocator;
  const adkAllocationCallbacks *pAllocator;
  VkDevice device;
  VkDeviceMemory deviceMemory;
  //Each binary tree node represents a position in memory and a size.
  //The first node is the first byte with a size of the whole memory.
  //The second node is the first byte with a size of half the memory.
  //The third node is the second byte with a size of half the memory.
  //The fourth node is the first byte with a size of quarter the memory (and so on).
  //If a node is on it means one of these:
  //  - One or two child nodes are on; an allocation exists further down the tree.
  //  - No child nodes are on; this node represents an allocation and a size.
  char *pBinaryTree;
  size_t freeBlocks;
  uint32_t memoryTypeIndex;
  size_t blockCount;
  size_t alignment;
  void *pMappedMemory;
} AdkMemoryHeap;

typedef struct AdkMemoryPool
{
  const adkAllocationCallbacks *pAllocator;
  AdkMemoryHeap memoryHeaps[ADK_MEMORY_POOL_COUNT];
} AdkMemoryPool;

static size_t adkMemoryPool_GetPreviousPowerOfTwo(size_t value)
{
  size_t iterations = 0;
  size_t previousPowerOfTwo = value;
  while (previousPowerOfTwo != 1)
  {
    ++iterations;
    previousPowerOfTwo >>= 1;
  }
  while (iterations != 0)
  {
    --iterations;
    previousPowerOfTwo <<= 1;
  }
  return previousPowerOfTwo;
}

static void _AllocateMemoryAndMapIfApplicable(VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, size_t size, uint32_t memoryTypeIndex, VkDeviceMemory *pDeviceMemory, void **ppMappedMemory)
{
  VkMemoryAllocateInfo memoryAllocateInfo;
  memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  memoryAllocateInfo.pNext = nullptr;
  memoryAllocateInfo.allocationSize = size;
  memoryAllocateInfo.memoryTypeIndex = memoryTypeIndex;
  ADK_ASSERT_VK_RESULT(vkAllocateMemory(device, &memoryAllocateInfo, pVkAllocator, pDeviceMemory));

  VkPhysicalDeviceMemoryProperties memoryProperties;
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);
  VkMemoryPropertyFlags memoryPropertyFlags = memoryProperties.memoryTypes[memoryTypeIndex].propertyFlags;

  if ((memoryPropertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0)
    ADK_ASSERT_VK_RESULT(vkMapMemory(device, *pDeviceMemory, 0, VK_WHOLE_SIZE, 0, ppMappedMemory));
}

static size_t adkMemoryHeap_GetHeapSize(size_t alignment)
{
  size_t nextMultipleOfAlignment = adkMax((size_t)1, (ADK_MEMORY_POOL_MIN_BUFFER_SIZE + alignment - 1) / alignment) * alignment;
  return adkMemoryPool_GetPreviousPowerOfTwo(nextMultipleOfAlignment - 1) << 1;
}

static size_t adkMemoryHeap_GetBlocksRequiredForSize(size_t size, size_t alignment)
{
  return (size + alignment - 1) / alignment;
}

static void adkMemoryHeap_Init(AdkMemoryHeap *pMemoryHeap, VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, const adkAllocationCallbacks *pAllocator, size_t alignment, uint32_t memoryTypeIndex)
{
  size_t realSize = adkMemoryHeap_GetHeapSize(alignment);
  size_t blockCount = realSize / alignment;

  _AllocateMemoryAndMapIfApplicable(physicalDevice, device, pVkAllocator, realSize, memoryTypeIndex, &pMemoryHeap->deviceMemory, &pMemoryHeap->pMappedMemory);

  const size_t BITS_PER_BYTE = 8;
  const size_t NODE_BIT_COUNT = 2;
  size_t nodeCount = 2 * blockCount;

  pMemoryHeap->pVkAllocator = pVkAllocator;
  pMemoryHeap->pAllocator = pAllocator;
  pMemoryHeap->device = device;
  pMemoryHeap->pBinaryTree = adkAllocTypeCall(char, nodeCount * NODE_BIT_COUNT / BITS_PER_BYTE, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pMemoryHeap->freeBlocks = blockCount;
  pMemoryHeap->memoryTypeIndex = memoryTypeIndex;
  pMemoryHeap->blockCount = blockCount;
  pMemoryHeap->alignment = alignment;
}

static void adkMemoryHeap_Deinit(AdkMemoryHeap *pMemoryHeap)
{
  if (pMemoryHeap->pMappedMemory != nullptr)
    vkUnmapMemory(pMemoryHeap->device, pMemoryHeap->deviceMemory);

  vkFreeMemory(pMemoryHeap->device, pMemoryHeap->deviceMemory, pMemoryHeap->pVkAllocator);
  pMemoryHeap->deviceMemory = VK_NULL_HANDLE;
  adkFreeCall(pMemoryHeap->pBinaryTree, pMemoryHeap->pAllocator);
}

static AdkMemoryHeapNodeState adkMemoryHeap_GetNodeState(AdkMemoryHeap *pMemoryHeap, size_t index)
{
  const size_t BITS_PER_BYTE = 8;
  size_t indexA = index * 2 + 0;
  size_t indexB = index * 2 + 1;
  size_t byteIndexA = indexA / BITS_PER_BYTE;
  size_t byteIndexB = indexB / BITS_PER_BYTE;
  size_t bitIndexA = indexA % BITS_PER_BYTE;
  size_t bitIndexB = indexB % BITS_PER_BYTE;
  uint32_t partA = (pMemoryHeap->pBinaryTree[byteIndexA] >> bitIndexA) & 0x1;
  uint32_t partB = (pMemoryHeap->pBinaryTree[byteIndexB] >> bitIndexB) & 0x1;
  return (AdkMemoryHeapNodeState)((partB << 1) | partA);
}

static inline char _SetBit(char number, char value, char bit)
{
  char nValue = (uint32_t)-(int32_t)value;
  return number ^ ((nValue ^ number) & ((uint32_t)1 << bit));
}

static void adkMemoryHeap_SetNodeState(AdkMemoryHeap *pMemoryHeap, size_t index, AdkMemoryHeapNodeState state)
{
  const size_t BITS_PER_BYTE = 8;
  size_t indexA = index * 2 + 0;
  size_t indexB = index * 2 + 1;
  size_t byteIndexA = indexA / BITS_PER_BYTE;
  size_t byteIndexB = indexB / BITS_PER_BYTE;
  char bitIndexA = (char)(indexA % BITS_PER_BYTE);
  char bitIndexB = (char)(indexB % BITS_PER_BYTE);
  char partA = (char)state & 0x1;
  char partB = (char)state >> 1;
  pMemoryHeap->pBinaryTree[byteIndexA] = _SetBit(pMemoryHeap->pBinaryTree[byteIndexA], partA, bitIndexA);
  pMemoryHeap->pBinaryTree[byteIndexB] = _SetBit(pMemoryHeap->pBinaryTree[byteIndexB], partB, bitIndexB);
}

static void adkMemoryHeap_IndexToBlockPositionAndCount(AdkMemoryHeap *pMemoryHeap, size_t index, size_t *pBlockPosition, size_t *pBlockCount)
{
  size_t previousPowerOfTwo = adkMemoryPool_GetPreviousPowerOfTwo(index);
  *pBlockCount = pMemoryHeap->blockCount / previousPowerOfTwo;
  *pBlockPosition = (index - previousPowerOfTwo) * (*pBlockCount);
}

static size_t adkMemoryHeap_BlockPositionToIndex(AdkMemoryHeap *pMemoryHeap, size_t blockPosition)
{
  size_t index = pMemoryHeap->blockCount + blockPosition;

  while (adkMemoryHeap_GetNodeState(pMemoryHeap, index) == ADK_MEMORY_HEAP_NODE_STATE_FREE)
    index /= 2;

  return index;
}

static void adkMemoryHeap_FindSmallestMemoryRecursive(AdkMemoryHeap *pMemoryHeap, size_t index, size_t requestedblockCount, size_t chunkBlockCount, size_t *pFoundIndex, size_t *pSmallestFound)
{
  if (requestedblockCount > chunkBlockCount || requestedblockCount > *pSmallestFound)
    return;

  size_t lrChunkBlockCount = chunkBlockCount / 2;
  size_t lIndex = index * 2 + 0;
  size_t rIndex = index * 2 + 1;
  adkMemoryHeap_FindSmallestMemoryRecursive(pMemoryHeap, lIndex, requestedblockCount, lrChunkBlockCount, pFoundIndex, pSmallestFound);
  adkMemoryHeap_FindSmallestMemoryRecursive(pMemoryHeap, rIndex, requestedblockCount, lrChunkBlockCount, pFoundIndex, pSmallestFound);

  if (chunkBlockCount < *pSmallestFound)
  {
    *pFoundIndex = index;
    *pSmallestFound = chunkBlockCount;
  }
}

static void adkMemoryHeap_FindSmallestMemory(AdkMemoryHeap *pMemoryHeap, size_t index, size_t requestedSize, size_t blockSize, size_t *pFoundIndex)
{
  size_t smallestFound = SIZE_MAX; //This ensures at least one iteration of the following function outputs a value.
  adkMemoryHeap_FindSmallestMemoryRecursive(pMemoryHeap, index, requestedSize, blockSize, pFoundIndex, &smallestFound);
}

static bool adkMemoryHeap_FindMemoryRecursive(AdkMemoryHeap *pMemoryHeap, size_t index, size_t requestedblockCount, size_t chunkBlockCount, size_t *pFoundIndex)
{
  AdkMemoryHeapNodeState nodeState = adkMemoryHeap_GetNodeState(pMemoryHeap, index);

  if (requestedblockCount > chunkBlockCount || nodeState == ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED)
    return false;

  if (nodeState == ADK_MEMORY_HEAP_NODE_STATE_FREE)
  {
    adkMemoryHeap_FindSmallestMemory(pMemoryHeap, index, requestedblockCount, chunkBlockCount, pFoundIndex);
    return true;
  }

  size_t lrChunkBlockCount = chunkBlockCount / 2;
  size_t lIndex = index * 2 + 0;
  size_t rIndex = index * 2 + 1;

  if (adkMemoryHeap_FindMemoryRecursive(pMemoryHeap, lIndex, requestedblockCount, lrChunkBlockCount, pFoundIndex))
    return true;
  if (adkMemoryHeap_FindMemoryRecursive(pMemoryHeap, rIndex, requestedblockCount, lrChunkBlockCount, pFoundIndex))
    return true;

  return false;
}

static bool adkMemoryHeap_FindMemory(AdkMemoryHeap *pMemoryHeap, size_t blockCount, size_t *pFoundIndex)
{
  return adkMemoryHeap_FindMemoryRecursive(pMemoryHeap, 1, blockCount, pMemoryHeap->blockCount, pFoundIndex);
}

static void adkMemoryHeap_TakeMemory(AdkMemoryHeap *pMemoryHeap, size_t index, size_t *pMemoryPosition, size_t *pMemorySize)
{
  size_t blockPosition;
  size_t blockSize;
  adkMemoryHeap_IndexToBlockPositionAndCount(pMemoryHeap, index, &blockPosition, &blockSize);
  pMemoryHeap->freeBlocks -= blockSize;

  adkMemoryHeap_SetNodeState(pMemoryHeap, index, ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED);
  index >>= 1;

  while (index != 0)
  {
    bool leftChildFullyAllocated = adkMemoryHeap_GetNodeState(pMemoryHeap, index * 2) == ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED;
    bool rightChildFullyAllocated = adkMemoryHeap_GetNodeState(pMemoryHeap, index * 2 + 1) == ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED;
    bool bothChildrenFullyAllocated = leftChildFullyAllocated && rightChildFullyAllocated;
    AdkMemoryHeapNodeState nodeState = bothChildrenFullyAllocated ? ADK_MEMORY_HEAP_NODE_STATE_FULLY_ALLOCATED : ADK_MEMORY_HEAP_NODE_STATE_PARTIALLY_ALLOCATED;
    adkMemoryHeap_SetNodeState(pMemoryHeap, index, nodeState);
    index >>= 1;
  }

  *pMemoryPosition = blockPosition * pMemoryHeap->alignment;
  *pMemorySize = blockSize * pMemoryHeap->alignment;
}

static void adkMemoryHeap_GiveMemory(AdkMemoryHeap *pMemoryHeap, size_t memoryPosition, size_t memorySize)
{
  size_t blockPosition = memoryPosition / pMemoryHeap->alignment;
  size_t index = adkMemoryHeap_BlockPositionToIndex(pMemoryHeap, blockPosition);

  adkMemoryHeap_SetNodeState(pMemoryHeap, index, ADK_MEMORY_HEAP_NODE_STATE_FREE);

  while (index != 1)
  {
    index /= 2;

    AdkMemoryHeapNodeState lState = adkMemoryHeap_GetNodeState(pMemoryHeap, index * 2 + 0);
    AdkMemoryHeapNodeState rState = adkMemoryHeap_GetNodeState(pMemoryHeap, index * 2 + 1);

    if (lState == ADK_MEMORY_HEAP_NODE_STATE_FREE && rState == ADK_MEMORY_HEAP_NODE_STATE_FREE)
    {
      adkMemoryHeap_SetNodeState(pMemoryHeap, index, ADK_MEMORY_HEAP_NODE_STATE_FREE);
    }
    else
    {
      adkMemoryHeap_SetNodeState(pMemoryHeap, index, ADK_MEMORY_HEAP_NODE_STATE_PARTIALLY_ALLOCATED);
      break;
    }
  }

  pMemoryHeap->freeBlocks += memorySize / pMemoryHeap->alignment;
}

void adkMemoryPool_Create(const adkAllocationCallbacks *pAllocator, AdkMemoryPool **ppMemoryPool)
{
  AdkMemoryPool *pMemoryPool = adkAllocTypeCall(AdkMemoryPool, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  *ppMemoryPool = pMemoryPool;
}

void adkMemoryPool_Destroy(AdkMemoryPool **ppMemoryPool)
{
  AdkMemoryPool *pMemoryPool = *ppMemoryPool;
  for (uint32_t i = 0; i < ADK_MEMORY_POOL_COUNT; ++i)
  {
    if (pMemoryPool->memoryHeaps[i].deviceMemory == VK_NULL_HANDLE)
      break;
    adkMemoryHeap_Deinit(&pMemoryPool->memoryHeaps[i]);
  }
  adkFreeCall(*ppMemoryPool, pMemoryPool->pAllocator);
}

typedef struct AllocationTest
{
  size_t start;
  size_t end;
  VkDeviceMemory deviceMemory;
} AllocationTest;

void adkMemoryPool_AllocateDeviceMemory(AdkMemoryPool *pMemoryPool, VkPhysicalDevice physicalDevice, VkDevice device, const VkAllocationCallbacks *pVkAllocator, const adkAllocationCallbacks *pAllocator, size_t allocationSize, size_t alignment, uint32_t memoryTypeIndex, AdkDeviceMemory *pDeviceMemory)
{
  VkDeviceMemory deviceMemory = VK_NULL_HANDLE;
  size_t memoryPosition = 0;
  size_t memorySize = allocationSize;
  void *pMappedMemory = nullptr;
  size_t blockCount = adkMemoryHeap_GetBlocksRequiredForSize(allocationSize, alignment);

  if (allocationSize <= adkMemoryHeap_GetHeapSize(alignment))
  {
    //Get a memory pool with enough contiguous memory.
    for (uint32_t i = 0; i < ADK_MEMORY_POOL_COUNT; ++i)
    {
      if (pMemoryPool->memoryHeaps[i].deviceMemory == VK_NULL_HANDLE)
      {
        //There are no pools with enough contiguous memory; create one.
        adkMemoryHeap_Init(&pMemoryPool->memoryHeaps[i], physicalDevice, device, pVkAllocator, pAllocator, alignment, memoryTypeIndex);
      }

      if (pMemoryPool->memoryHeaps[i].freeBlocks >= blockCount && pMemoryPool->memoryHeaps[i].alignment == alignment && pMemoryPool->memoryHeaps[i].memoryTypeIndex == memoryTypeIndex)
      {
        size_t blockIndex;
        if (adkMemoryHeap_FindMemory(&pMemoryPool->memoryHeaps[i], blockCount, &blockIndex))
        {
          adkMemoryHeap_TakeMemory(&pMemoryPool->memoryHeaps[i], blockIndex, &memoryPosition, &memorySize);
          deviceMemory = pMemoryPool->memoryHeaps[i].deviceMemory;
          pMappedMemory = &((char*)pMemoryPool->memoryHeaps[i].pMappedMemory)[memoryPosition];
          break;
        }
      }
    }
  }

  if (deviceMemory == VK_NULL_HANDLE)
    _AllocateMemoryAndMapIfApplicable(physicalDevice, device, pVkAllocator, memorySize, memoryTypeIndex, &deviceMemory, &pMappedMemory);

  pDeviceMemory->vkDeviceMemory = deviceMemory;
  pDeviceMemory->offset = memoryPosition;
  pDeviceMemory->size = memorySize;
  pDeviceMemory->pMapped = pMappedMemory;
}

void adkMemoryPool_FreeDeviceMemory(AdkMemoryPool *pMemoryPool, AdkDeviceMemory *pDeviceMemory)
{
  for (uint32_t i = 0; i < ADK_MEMORY_POOL_COUNT; ++i)
  {
    if (pMemoryPool->memoryHeaps[i].deviceMemory == VK_NULL_HANDLE)
      return;

    if (pMemoryPool->memoryHeaps[i].deviceMemory == pDeviceMemory->vkDeviceMemory)
    {
      AdkMemoryHeap *pMemoryHeap = &pMemoryPool->memoryHeaps[i];
      adkMemoryHeap_GiveMemory(pMemoryHeap, pDeviceMemory->offset, pDeviceMemory->size);
      memset(pDeviceMemory, 0, sizeof(*pDeviceMemory));
      break;
    }
  }
}

void adkMemoryPool_FlushDeviceMemory(AdkDeviceMemory *pMemory, VkDevice device)
{
  VkMappedMemoryRange mappedMemoryRange;
  mappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
  mappedMemoryRange.pNext = nullptr;
  mappedMemoryRange.memory = pMemory->vkDeviceMemory;
  mappedMemoryRange.offset = pMemory->offset;
  mappedMemoryRange.size = pMemory->size;
  ADK_ASSERT_VK_RESULT(vkFlushMappedMemoryRanges(device, 1, &mappedMemoryRange));
}