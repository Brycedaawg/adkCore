#ifndef AdkLineRendererUtil_h__
#define AdkLineRendererUtil_h__

#include "AdkLineRenderer.h"

struct AdkLineRenderer;
template<typename T> struct adkVector3;

void adkLineRenderer_AddBox(AdkLineRenderer *pLineRenderer, const adkVector3<float> &min, const adkVector3<float> &max, uint32_t color);
void adkLineRenderer_AddCircle(AdkLineRenderer *pLineRenderer, const adkVector3<float> &point, float radius, const adkQuaternion<float> &rotation, uint32_t color);
void adkLineRenderer_AddSphere(AdkLineRenderer *pLineRenderer, const adkVector3<float> &point, float radius, const adkQuaternion<float> &rotation, uint32_t color);
void adkLineRenderer_AddTriangle(AdkLineRenderer *pLineRenderer, adkVector3<float> points[3], uint32_t color);
void adkLineRenderer_AddCapsule(AdkLineRenderer *pLineRenderer, const adkVector3<float> &position, float height, float radius, const adkQuaternion<float> &rotation, uint32_t color);

#endif // AdkLineRendererUtil_h__
