#ifndef AdkBinaryWriter_h__
#define AdkBinaryWriter_h__

#include "adkTypedefs.h"

struct AdkBinaryWriter;
struct adkAllocationCallbacks;

typedef struct AdkBinaryWriterCreateInfo
{
  size_t bufferSize;
  void *pBuffer;
} AdkSerializerCreateInfo;

void adkBinaryWriter_Create(const AdkBinaryWriterCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkBinaryWriter **ppSerializer);
void adkBinaryWriter_Destroy(AdkBinaryWriter **ppBinaryWriter);
void adkBinaryWriter_Reset(AdkBinaryWriter *pBinaryWriter);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, bool value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int8_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int16_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int32_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, int32_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint8_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint16_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint32_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, uint64_t value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, float value);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, double value);
bool adkBinaryWriter_WriteString(AdkBinaryWriter *pBinaryWriter, const char *pValue);
bool adkBinaryWriter_Write(AdkBinaryWriter *pBinaryWriter, size_t valueLength, const void *pValue);
size_t adkBinaryWriter_GetOffset(AdkBinaryWriter *pBinaryWriter);
bool adkBinaryWriter_GetSuccess(AdkBinaryWriter *pBinarWriter);

#endif // AdkBinaryWriter_h__
