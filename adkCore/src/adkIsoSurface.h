#ifndef adkIsoSurface_h__
#define adkIsoSurface_h__

#include "adkMath.h"
#include "adkMemory.h"
#include "adkWindow.h"
#include "adkTexture.h"
#include "adkBuffer.h"
#include "AdkTime.h"

struct AdkIsoSurface;
struct AdkIsoSurfaceBuffer;

typedef struct AdkIsoSurfaceCreateInfo
{
  AdkTexture *pDensity;
  AdkTexture *pNormals;
  uint32_t maxVertices;
  AdkQueue *pGraphicsQueue;
  AdkQueue *pComputeQueue;
  AdkQueue *pTransferQueue;
} AdkIsoSurfaceCreateInfo;

void adkIsoSurface_Create(AdkInstance *pInstance, const AdkIsoSurfaceCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkIsoSurface **ppIsoSurface);
void adkIsoSurface_Destroy(AdkIsoSurface **ppIsoSurface);
void adkIsoSurface_Reset(AdkIsoSurface *pIsoSurface);
void adkIsoSurface_Generate(AdkIsoSurface *pIsoSurface, float isoLevel, const adkVector3<uint32_t> &inset, const adkVector3<uint32_t> &scale, const adkVector3<float> &offset = adkVector3<float>::zero());
uint32_t adkIsoSurface_GetVertexCount(AdkIsoSurface *pIsoSurface);
void adkIsoSurface_GetPositions(AdkIsoSurface *pIsoSurface, AdkBuffer **ppPositions);
void adkIsoSurface_GetNormals(AdkIsoSurface *pIsoSurface, AdkBuffer **ppNormals);
AdkBool32 adkIsoSurface_Wait(AdkIsoSurface *pIsoSurface, uint64_t timeout);

#endif // adkIsoSurface_h__
