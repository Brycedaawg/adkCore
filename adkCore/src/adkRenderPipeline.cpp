#include "adkRenderPipeline.h"
#include "adkInstance.inl"
#include "adkShader.inl"
#include "vulkan.h"
#include "adkString.h"
#include "adkWindow.inl"
#include "adkVkTools.inl"
#include "adkBuffer.inl"
#include "adkTexture.inl"
#include "adkQueue.inl"

typedef enum ShaderStage
{
  SHADER_STAGE_VERTEX,
  SHADER_STAGE_GEOMETRY,
  SHADER_STAGE_FRAGMENT,
  SHADER_STAGE_COMPUTE,
  SHADER_STAGE_COUNT,
} ShaderStage;

typedef enum ImageSemaphoreType
{
  IMAGE_SEMAPHORE_TYPE_COLOR,
  IMAGE_SEMAPHORE_TYPE_DEPTH,
  IMAGE_SEMAPHORE_TYPE_COUNT,
} AdkRenderPipelineSemaphore;

static inline VkVertexInputRate _AdkToVkInputRate(AdkAttributeInputRate inputRate)
{
  switch (inputRate)
  {
  case ADK_ATTRIBUTE_INPUT_RATE_VERTEX:
    return VK_VERTEX_INPUT_RATE_VERTEX;
  case ADK_ATTRIBUTE_INPUT_RATE_INSTANCE:
    return VK_VERTEX_INPUT_RATE_INSTANCE;
  default:
    return VK_VERTEX_INPUT_RATE_VERTEX;
  }
}

static inline VkPrimitiveTopology _AdkToVkPrimitiveTopology(AdkPrimitiveTopology primitiveTopology)
{
  switch (primitiveTopology)
  {
  case ADK_PRIMITIVE_TOPOLOGY_POINT_LIST:
    return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
  case ADK_PRIMITIVE_TOPOLOGY_LINE_LIST:
    return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
  case ADK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
    return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
  case ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
  case ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
  case ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
  case ADK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY:
    return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
  case ADK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY:
    return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
  case ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
  case ADK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY:
    return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
  case ADK_PRIMITIVE_TOPOLOGY_PATCH_LIST:
    return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
  default:
    return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    }
}

static inline VkPolygonMode _AdkToVkPolygonMode(AdkPolygonMode polygonMode)
{
  switch (polygonMode)
  {
  case ADK_POLYGON_MODE_FILL:
    return VK_POLYGON_MODE_FILL;
  case ADK_POLYGON_MODE_LINE:
    return VK_POLYGON_MODE_LINE;
  case ADK_POLYGON_MODE_POINT:
    return VK_POLYGON_MODE_POINT;
  default:
    return VK_POLYGON_MODE_FILL;
  }
}

static inline VkBlendFactor _AdkToVkBlendFactor(AdkBlendFactor blendFactor)
{
  switch (blendFactor)
  {
  case ADK_BLEND_FACTOR_ZERO:
    return VK_BLEND_FACTOR_ZERO;
  case ADK_BLEND_FACTOR_ONE:
    return VK_BLEND_FACTOR_ONE;
  case ADK_BLEND_FACTOR_SRC_COLOR:
    return VK_BLEND_FACTOR_SRC_COLOR;
  case ADK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR:
    return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
  case ADK_BLEND_FACTOR_DST_COLOR:
    return VK_BLEND_FACTOR_DST_COLOR;
  case ADK_BLEND_FACTOR_ONE_MINUS_DST_COLOR:
    return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
  case ADK_BLEND_FACTOR_SRC_ALPHA:
    return VK_BLEND_FACTOR_SRC_ALPHA;
  case ADK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA:
    return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  case ADK_BLEND_FACTOR_DST_ALPHA:
    return VK_BLEND_FACTOR_DST_ALPHA;
  case ADK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA:
    return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
  case ADK_BLEND_FACTOR_CONSTANT_COLOR:
    return VK_BLEND_FACTOR_CONSTANT_COLOR;
  case ADK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR:
    return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
  case ADK_BLEND_FACTOR_CONSTANT_ALPHA:
    return VK_BLEND_FACTOR_CONSTANT_ALPHA;
  case ADK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA:
    return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
  case ADK_BLEND_FACTOR_SRC_ALPHA_SATURATE:
    return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
  default:
    return VK_BLEND_FACTOR_ZERO;
  }
}

static inline VkShaderStageFlagBits _AdkToVkShaderStage(ShaderStage shaderStage)
{
  switch (shaderStage)
  {
  case SHADER_STAGE_VERTEX:
    return VK_SHADER_STAGE_VERTEX_BIT;
  case SHADER_STAGE_GEOMETRY:
    return VK_SHADER_STAGE_GEOMETRY_BIT;
  case SHADER_STAGE_FRAGMENT:
    return VK_SHADER_STAGE_FRAGMENT_BIT;
  case SHADER_STAGE_COMPUTE:
    return VK_SHADER_STAGE_COMPUTE_BIT;
  default:
    return VK_SHADER_STAGE_VERTEX_BIT;
  }
}

static inline size_t adkRenderPipeline_SizeOfElementType(AdkSpecializationType type)
{
  switch (type)
  {
  case ADK_SPECIALIZATION_TYPE_UINT32:
    return 4;
  }

  ADK_ASSERT(false, "Couldn't resolve specialization type size for value %d", type);
}

typedef struct AdkRenderPipeline
{
  AdkRenderPipelineFlags flags;
  AdkRenderTargetType colorTargetType;
  AdkRenderTarget colorTarget;
  VkFormat colorFormat;
  VkFormat depthFormat;
  uint32_t imageCount;
  adkVector3<uint32_t> imageExtent;
  VkImageView *pImageViews;
  VkPipelineLayout pipelineLayout;
  AdkDeviceMemory indirectDeviceMemory;
  VkBuffer indirectBuffer;
  VkRenderPass renderPass;
  VkPipeline pipeline;
  VkCommandBuffer *pCommandBuffers;
  adkRect<uint32_t> viewport;
  AdkBool32 *pDirtyDynamicStates;
  VkCommandBuffer *pDynamicStateCommandBuffers;
  VkFramebuffer *pFramebuffers;
  VkFence fence;
  VkDescriptorPool descriptorPool;
  VkDescriptorSetLayout descriptorSetLayout;
  VkDescriptorSet descriptorSet;
  AdkQueue *pGraphicsQueue;
  VkSemaphore semaphore;
  uint32_t renderSemaphoreCount;
  VkSemaphore *pRenderSemaphores;
  VkPipelineStageFlags *pWaitFlags;
  uint32_t textureCount;
  AdkTexture **ppTextures;
} AdkRenderPipeline;

static VkViewport _AdkRectToVkViewport(const adkRect<uint32_t> &rect)
{
  VkViewport viewport;
  viewport.x = (float)rect.x;
  viewport.y = (float)rect.y;
  viewport.width = (float)rect.width;
  viewport.height = (float)rect.height;
  viewport.minDepth = 0.f;
  viewport.maxDepth = 1.f;

  //Flip the viewport vertically to go from world y-up to screen space y-down.
  viewport.y += viewport.height;
  viewport.height = -viewport.height;

  return viewport;
}

static inline void _RecordDynamicStateCommandBuffer(AdkRenderPipeline *pRenderPipeline, uint32_t index)
{
  VkCommandBufferInheritanceInfo inheritanceInfo;
  inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
  inheritanceInfo.pNext = nullptr;
  inheritanceInfo.renderPass = pRenderPipeline->renderPass;
  inheritanceInfo.subpass = 0;
  inheritanceInfo.occlusionQueryEnable = false;
  inheritanceInfo.queryFlags = 0;
  inheritanceInfo.pipelineStatistics = 0;

  VkCommandBufferBeginInfo beginInfo;
  beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  beginInfo.pNext = nullptr;
  beginInfo.flags = VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
  beginInfo.pInheritanceInfo = &inheritanceInfo;

  VkViewport viewport = _AdkRectToVkViewport(pRenderPipeline->viewport);

  VkRect2D scissors;
  scissors.offset.x = (int32_t)pRenderPipeline->viewport.x;
  scissors.offset.y = (int32_t)pRenderPipeline->viewport.y;
  scissors.extent.width = (uint32_t)pRenderPipeline->viewport.width;
  scissors.extent.height = (uint32_t)pRenderPipeline->viewport.height;

  vkResetCommandBuffer(pRenderPipeline->pDynamicStateCommandBuffers[index], 0);

  inheritanceInfo.framebuffer = pRenderPipeline->pFramebuffers[index];

  ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pRenderPipeline->pDynamicStateCommandBuffers[index], &beginInfo));
  vkCmdSetViewport(pRenderPipeline->pDynamicStateCommandBuffers[index], 0, 1, &viewport);
  vkCmdSetScissor(pRenderPipeline->pDynamicStateCommandBuffers[index], 0, 1, &scissors);
  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pRenderPipeline->pDynamicStateCommandBuffers[index]));
}

void adkRenderPipeline_Create(AdkInstance *pInstance, const AdkRenderPipelineCreateInfo *pCreateInfo, AdkRenderPipeline **ppRenderPipeline)
{
  AdkRenderPipeline *pRenderPipeline = adkAllocTypeCall(AdkRenderPipeline, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  
  pRenderPipeline->flags = pCreateInfo->flags;
  pRenderPipeline->colorTargetType = pCreateInfo->colorTargetType;
  pRenderPipeline->colorTarget = pCreateInfo->colorTarget;
  pRenderPipeline->pGraphicsQueue = pCreateInfo->pGraphicsQueue;

  VkPhysicalDevice physicalDevice = pInstance->pVkPhysicalDevices[pInstance->primaryDeviceIndex];
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkQueue queue = pInstance->pVkQueues[pRenderPipeline->pGraphicsQueue->index];

  size_t specializationDataSize = 0;
  for (uint32_t i = 0; i < pCreateInfo->specializationCount; ++i)
    specializationDataSize += adkRenderPipeline_SizeOfElementType(pCreateInfo->pSpecializations[i].type);

  char *pSpecializationData = adkAllocTypeCall(char, specializationDataSize, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);
  VkSpecializationMapEntry *pSpecializationMapEntries = adkAllocTypeCall(VkSpecializationMapEntry, pCreateInfo->specializationCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

  size_t specializationDataOffset = 0;
  for (uint32_t i = 0; i < pCreateInfo->specializationCount; ++i)
  {
    size_t elementSize = adkRenderPipeline_SizeOfElementType(pCreateInfo->pSpecializations[i].type);
    memcpy(&pSpecializationData[specializationDataOffset], &pCreateInfo->pSpecializations[i].value, elementSize);
    pSpecializationMapEntries[i].constantID = pCreateInfo->pSpecializations[i].constantId;
    pSpecializationMapEntries[i].offset = (uint32_t)specializationDataOffset;
    pSpecializationMapEntries[i].size = elementSize;
    specializationDataOffset += elementSize;
  }

  VkSpecializationInfo specializationInfo;
  specializationInfo.mapEntryCount = pCreateInfo->specializationCount;
  specializationInfo.pMapEntries = pSpecializationMapEntries;
  specializationInfo.dataSize = specializationDataSize;
  specializationInfo.pData = pSpecializationData;

  uint32_t shaderStageCount = 0;
  VkPipelineShaderStageCreateInfo shaderStageCreateInfos[SHADER_STAGE_COUNT];

  for (uint32_t i = 0; i < SHADER_STAGE_COUNT; ++i)
  {
    if (pCreateInfo->pShader->modules[i] == VK_NULL_HANDLE)
      continue;

    shaderStageCreateInfos[shaderStageCount].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStageCreateInfos[shaderStageCount].pNext = nullptr;
    shaderStageCreateInfos[shaderStageCount].flags = 0;
    shaderStageCreateInfos[shaderStageCount].stage = _AdkToVkShaderStage((ShaderStage)i);
    shaderStageCreateInfos[shaderStageCount].module = pCreateInfo->pShader->modules[i];
    shaderStageCreateInfos[shaderStageCount].pName = "main";
    shaderStageCreateInfos[shaderStageCount].pSpecializationInfo = &specializationInfo;
    ++shaderStageCount;
  }

  //Find how many bindings will be needed for these attributes.
  uint32_t locationCount = 0;

  for (uint32_t i = 0; i < pCreateInfo->attributeCount; ++i)
    locationCount += adkBuffer_LocationCountOfType(pCreateInfo->pAttributes[i].pBuffer->type) * pCreateInfo->pAttributes[i].elementStride;

  VkVertexInputBindingDescription *bindingDescriptions = (VkVertexInputBindingDescription*)adkAllocCall(pCreateInfo->attributeCount * (sizeof(VkVertexInputBindingDescription) + sizeof(VkBuffer)) + locationCount * sizeof(VkVertexInputAttributeDescription), pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);
  VkVertexInputAttributeDescription *attributeDescriptions = (VkVertexInputAttributeDescription*)((char*)bindingDescriptions + pCreateInfo->attributeCount * sizeof(VkVertexInputBindingDescription));
  VkBuffer *pBuffers = (VkBuffer*)((char*)bindingDescriptions + pCreateInfo->attributeCount * sizeof(VkVertexInputBindingDescription) + locationCount * sizeof(VkVertexInputAttributeDescription));

  uint32_t locationIterator = 0;
  uint32_t vertexBinding = 0;

  for (uint32_t i = 0; i < pCreateInfo->attributeCount; ++i)
  {
    AdkBufferType type = pCreateInfo->pAttributes[i].pBuffer->type;
    uint32_t bindingLocationCount = adkBuffer_LocationCountOfType(type) * pCreateInfo->pAttributes[i].elementStride;
    AdkBufferType locationType = adkBuffer_LocationTypeOfType(type);
    VkFormat locationFormat = adkBuffer_TypeToVkFormat(locationType);
    uint32_t locationSize = (uint32_t)adkBuffer_SizeOfElementType(locationType);
    uint32_t baseLocation = pCreateInfo->pAttributes[i].location;

    bindingDescriptions[vertexBinding].binding = vertexBinding;
    bindingDescriptions[vertexBinding].stride = (uint32_t)adkBuffer_SizeOfElementType(type) * pCreateInfo->pAttributes[i].elementStride;
    bindingDescriptions[vertexBinding].inputRate = _AdkToVkInputRate(pCreateInfo->pAttributes[i].inputRate);

    pBuffers[vertexBinding] = pCreateInfo->pAttributes[i].pBuffer->buffer;

    for (uint32_t j = 0; j < bindingLocationCount; ++j)
    {
      attributeDescriptions[locationIterator].location = baseLocation + j;
      attributeDescriptions[locationIterator].binding = vertexBinding;
      attributeDescriptions[locationIterator].format = locationFormat;
      attributeDescriptions[locationIterator].offset = j * locationSize;
      ++locationIterator;
    }

    ++vertexBinding;
  }

  //Create descriptor pool.
  VkDescriptorPoolSize samplerDescriptorPoolSizes[2];
  uint32_t poolSizesCount = 0;

  if (pCreateInfo->bufferUniformCount != 0)
  {
    samplerDescriptorPoolSizes[poolSizesCount].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    samplerDescriptorPoolSizes[poolSizesCount].descriptorCount = 0;

    for (uint32_t i = 0; i < pCreateInfo->bufferUniformCount; ++i)
      samplerDescriptorPoolSizes[poolSizesCount].descriptorCount += pCreateInfo->pBufferUniforms[i].pBuffer->count;

    ++poolSizesCount;
  }
  
  if (pCreateInfo->samplerUniformCount != 0)
  {
    samplerDescriptorPoolSizes[poolSizesCount].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerDescriptorPoolSizes[poolSizesCount].descriptorCount = 0;

    for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
      samplerDescriptorPoolSizes[poolSizesCount].descriptorCount += pCreateInfo->pSamplerUniforms[i].count;

    ++poolSizesCount;
  }

  bool hasDescriptorSets = poolSizesCount != 0;

  if (hasDescriptorSets)
  {
    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo;
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.pNext = nullptr;
    descriptorPoolCreateInfo.flags = 0;
    descriptorPoolCreateInfo.maxSets = 1;
    descriptorPoolCreateInfo.poolSizeCount = poolSizesCount;
    descriptorPoolCreateInfo.pPoolSizes = samplerDescriptorPoolSizes;
    ADK_ASSERT_VK_RESULT(vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, pInstance->pVkAllocationCallbacks, &pRenderPipeline->descriptorPool));

    uint32_t uniformCount = pCreateInfo->bufferUniformCount + pCreateInfo->samplerUniformCount;
    uint32_t boundBufferCount = 0;
    uint32_t boundSamplerCount = 0;
    uint32_t boundUniformCount = 0;

    //Create descriptor set layout.
    VkDescriptorSetLayoutBinding *pBindings = adkAllocTypeCall(VkDescriptorSetLayoutBinding, uniformCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

    uint32_t uniformBinding = 0;

    for (uint32_t i = 0; i < pCreateInfo->bufferUniformCount; ++i)
    {
      //Important! In the case of an array inside a uniform block, only a single descriptor is needed for this binding.
      //This is not the case with samplers, which need a descriptor per-sampler for this binding.
      //The distinguisher might be that sampler arrays require the keyword 'uniform' to qualify both the array itself and the uniform block. buffers don't seem to need this.

      pBindings[uniformBinding].binding = pCreateInfo->pBufferUniforms[i].binding;
      pBindings[uniformBinding].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      pBindings[uniformBinding].descriptorCount = 1;
      pBindings[uniformBinding].stageFlags = VK_SHADER_STAGE_ALL;
      pBindings[uniformBinding].pImmutableSamplers = nullptr;
      ++uniformBinding;
      ++boundBufferCount;
    }

    uint32_t samplerCount = 0;

    for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
      samplerCount += pCreateInfo->pSamplerUniforms[i].count;

    uint32_t *pBoundSamplerUniformIndices = adkAllocTypeCall(uint32_t, pCreateInfo->samplerUniformCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

    VkSampler *pSamplers = adkAllocTypeCall(VkSampler, samplerCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);
    uint32_t samplerIterator = 0;

    for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
    {
      pBindings[uniformBinding].binding = pCreateInfo->pSamplerUniforms[i].binding;

      pBoundSamplerUniformIndices[samplerIterator] = i;

      for (uint32_t j = 0; j < pCreateInfo->pSamplerUniforms[i].count; ++j)
        pSamplers[samplerIterator + j] = pCreateInfo->pSamplerUniforms[i].ppTextures[j]->sampler;

      pBindings[uniformBinding].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
      pBindings[uniformBinding].descriptorCount = pCreateInfo->pSamplerUniforms[i].count;
      pBindings[uniformBinding].stageFlags = VK_SHADER_STAGE_ALL;
      pBindings[uniformBinding].pImmutableSamplers = pSamplers + samplerIterator;

      samplerIterator += pCreateInfo->pSamplerUniforms[i].count;
      ++uniformBinding;
      ++boundSamplerCount;
    }

    boundUniformCount = boundBufferCount + boundSamplerCount;

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.pNext = nullptr;
    descriptorSetLayoutCreateInfo.flags = 0;
    descriptorSetLayoutCreateInfo.bindingCount = uniformBinding;
    descriptorSetLayoutCreateInfo.pBindings = pBindings;
    ADK_ASSERT_VK_RESULT(vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, pInstance->pVkAllocationCallbacks, &pRenderPipeline->descriptorSetLayout));

    //Create descriptor set.
    VkDescriptorSetAllocateInfo descriptorSetAllocateInfo;
    descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptorSetAllocateInfo.pNext = nullptr;
    descriptorSetAllocateInfo.descriptorPool = pRenderPipeline->descriptorPool;
    descriptorSetAllocateInfo.descriptorSetCount = 1;
    descriptorSetAllocateInfo.pSetLayouts = &pRenderPipeline->descriptorSetLayout;
    ADK_ASSERT_VK_RESULT(vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &pRenderPipeline->descriptorSet));

    VkWriteDescriptorSet *pWriteDescriptorSets = (VkWriteDescriptorSet*)adkAllocCall((boundUniformCount) * (sizeof(VkWriteDescriptorSet) + boundBufferCount * sizeof(VkDescriptorBufferInfo) + boundSamplerCount * sizeof(VkDescriptorImageInfo)), pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);
    VkDescriptorBufferInfo *pDescriptorBufferInfos = (VkDescriptorBufferInfo*)((char*)pWriteDescriptorSets + boundUniformCount * sizeof(VkWriteDescriptorSet));
    VkDescriptorImageInfo *pDescriptorImageInfos = (VkDescriptorImageInfo*)((char*)pWriteDescriptorSets + boundUniformCount * sizeof(VkWriteDescriptorSet) + boundBufferCount * sizeof(VkDescriptorBufferInfo));
    pDescriptorImageInfos; pDescriptorBufferInfos; pWriteDescriptorSets;
    uniformBinding = 0;

    //Update descriptor set buffers and samplers.
    for (uint32_t i = 0; i < boundBufferCount; ++i)
    {
      pDescriptorBufferInfos[i].buffer = pCreateInfo->pBufferUniforms[i].pBuffer->buffer;
      pDescriptorBufferInfos[i].offset = 0;
      pDescriptorBufferInfos[i].range = VK_WHOLE_SIZE;

      pWriteDescriptorSets[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      pWriteDescriptorSets[i].pNext = nullptr;
      pWriteDescriptorSets[i].dstSet = pRenderPipeline->descriptorSet;
      pWriteDescriptorSets[i].dstBinding = pBindings[uniformBinding].binding;
      pWriteDescriptorSets[i].dstArrayElement = 0;
      pWriteDescriptorSets[i].descriptorCount = 1;
      pWriteDescriptorSets[i].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      pWriteDescriptorSets[i].pImageInfo = nullptr;
      pWriteDescriptorSets[i].pBufferInfo = pDescriptorBufferInfos + i;
      pWriteDescriptorSets[i].pTexelBufferView = nullptr;
      ++uniformBinding;
    }

    samplerIterator = 0;

    for (uint32_t i = 0; i < boundSamplerCount; ++i)
    {
      uint32_t writeIndex = boundBufferCount + i;
      uint32_t samplerIndex = pBoundSamplerUniformIndices[i];

      for (uint32_t j = 0; j < pCreateInfo->pSamplerUniforms[samplerIndex].count; ++j)
      {
        pDescriptorImageInfos[samplerIterator + j].sampler = pCreateInfo->pSamplerUniforms[samplerIndex].ppTextures[j]->sampler;
        pDescriptorImageInfos[samplerIterator + j].imageView = pCreateInfo->pSamplerUniforms[samplerIndex].ppTextures[j]->imageView;
        pDescriptorImageInfos[samplerIterator + j].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
      }

      pWriteDescriptorSets[writeIndex].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
      pWriteDescriptorSets[writeIndex].pNext = nullptr;
      pWriteDescriptorSets[writeIndex].dstSet = pRenderPipeline->descriptorSet;
      pWriteDescriptorSets[writeIndex].dstBinding = pBindings[uniformBinding].binding;
      pWriteDescriptorSets[writeIndex].dstArrayElement = 0;
      pWriteDescriptorSets[writeIndex].descriptorCount = pCreateInfo->pSamplerUniforms[samplerIndex].count;
      pWriteDescriptorSets[writeIndex].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
      pWriteDescriptorSets[writeIndex].pImageInfo = pDescriptorImageInfos + samplerIterator;
      pWriteDescriptorSets[writeIndex].pBufferInfo = nullptr;
      pWriteDescriptorSets[writeIndex].pTexelBufferView = nullptr;

      samplerIterator += pCreateInfo->pSamplerUniforms[samplerIndex].count;
      ++uniformBinding;
    }

    vkUpdateDescriptorSets(device, uniformBinding, pWriteDescriptorSets, 0, nullptr);

    adkFreeCall(pBindings, pInstance->pAllocationCallbacks);
    adkFreeCall(pSamplers, pInstance->pAllocationCallbacks);
    adkFreeCall(pWriteDescriptorSets, pInstance->pAllocationCallbacks);
    adkFreeCall(pBoundSamplerUniformIndices, pInstance->pAllocationCallbacks);
  }

  VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo;
  pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
  pipelineVertexInputStateCreateInfo.pNext = nullptr;
  pipelineVertexInputStateCreateInfo.flags = 0;
  pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = vertexBinding;
  pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = bindingDescriptions;
  pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = locationIterator;
  pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = attributeDescriptions;

  VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo;
  pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  pipelineInputAssemblyStateCreateInfo.pNext = nullptr;
  pipelineInputAssemblyStateCreateInfo.flags = 0;
  pipelineInputAssemblyStateCreateInfo.topology = _AdkToVkPrimitiveTopology(pCreateInfo->primitiveTopology);
  pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = false;

  VkViewport viewport = _AdkRectToVkViewport(pCreateInfo->viewport);

  VkRect2D scissors;
  scissors.offset.x = (int32_t)pCreateInfo->viewport.x;
  scissors.offset.y = (int32_t)pCreateInfo->viewport.y;
  scissors.extent.width = (uint32_t)pCreateInfo->viewport.width;
  scissors.extent.height = (uint32_t)pCreateInfo->viewport.height;

  VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo;
  pipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  pipelineViewportStateCreateInfo.pNext = nullptr;
  pipelineViewportStateCreateInfo.flags = 0;
  pipelineViewportStateCreateInfo.viewportCount = 1;
  pipelineViewportStateCreateInfo.pViewports = &viewport;
  pipelineViewportStateCreateInfo.scissorCount = 1;
  pipelineViewportStateCreateInfo.pScissors = &scissors;

  VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo;
  pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  pipelineRasterizationStateCreateInfo.pNext = nullptr;
  pipelineRasterizationStateCreateInfo.flags = 0;
  pipelineRasterizationStateCreateInfo.depthClampEnable = false;
  pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = false;
  pipelineRasterizationStateCreateInfo.polygonMode = _AdkToVkPolygonMode(pCreateInfo->polygonMode);
  pipelineRasterizationStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
  pipelineRasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_CLOCKWISE; //Because the viewport is flipped vertically this is actually counter clockwise.
  pipelineRasterizationStateCreateInfo.depthBiasEnable = false;
  pipelineRasterizationStateCreateInfo.depthBiasConstantFactor = 0.f;
  pipelineRasterizationStateCreateInfo.depthBiasClamp = 0.f;
  pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor = 0.f;
  pipelineRasterizationStateCreateInfo.lineWidth = 1.f;

  VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo;
  pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  pipelineMultisampleStateCreateInfo.pNext = nullptr;
  pipelineMultisampleStateCreateInfo.flags = 0;
  pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
  pipelineMultisampleStateCreateInfo.sampleShadingEnable = false;
  pipelineMultisampleStateCreateInfo.minSampleShading = 0.f;
  pipelineMultisampleStateCreateInfo.pSampleMask = nullptr;
  pipelineMultisampleStateCreateInfo.alphaToCoverageEnable = false;
  pipelineMultisampleStateCreateInfo.alphaToOneEnable = false;

  VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo;
  pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  pipelineDepthStencilStateCreateInfo.pNext = nullptr;
  pipelineDepthStencilStateCreateInfo.flags = 0;
  pipelineDepthStencilStateCreateInfo.depthTestEnable = (pCreateInfo->flags & ADK_RENDER_PIPELINE_DEPTH_READ_BIT) != 0;
  pipelineDepthStencilStateCreateInfo.depthWriteEnable = (pCreateInfo->flags & ADK_RENDER_PIPELINE_DEPTH_WRITE_BIT) != 0;
  pipelineDepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
  pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = false;
  pipelineDepthStencilStateCreateInfo.stencilTestEnable = false;
  pipelineDepthStencilStateCreateInfo.front.failOp = VK_STENCIL_OP_KEEP;
  pipelineDepthStencilStateCreateInfo.front.passOp = VK_STENCIL_OP_KEEP;
  pipelineDepthStencilStateCreateInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
  pipelineDepthStencilStateCreateInfo.front.compareOp = VK_COMPARE_OP_LESS;
  pipelineDepthStencilStateCreateInfo.front.compareMask = 0xFFFFFFFF;
  pipelineDepthStencilStateCreateInfo.front.writeMask = 0xFFFFFFFF;
  pipelineDepthStencilStateCreateInfo.front.reference = 0xFFFFFFFF;
  pipelineDepthStencilStateCreateInfo.back = pipelineDepthStencilStateCreateInfo.front;
  pipelineDepthStencilStateCreateInfo.minDepthBounds = 0.f;
  pipelineDepthStencilStateCreateInfo.maxDepthBounds = 1.f;

  VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState;
  pipelineColorBlendAttachmentState.blendEnable = pCreateInfo->blending.enable != 0;
  pipelineColorBlendAttachmentState.srcColorBlendFactor = _AdkToVkBlendFactor(pCreateInfo->blending.srcColor);
  pipelineColorBlendAttachmentState.dstColorBlendFactor = _AdkToVkBlendFactor(pCreateInfo->blending.dstColor);
  pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
  pipelineColorBlendAttachmentState.srcAlphaBlendFactor = _AdkToVkBlendFactor(pCreateInfo->blending.srcAlpha);
  pipelineColorBlendAttachmentState.dstAlphaBlendFactor = _AdkToVkBlendFactor(pCreateInfo->blending.dstAlpha);
  pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
  pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

  VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo;
  pipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  pipelineColorBlendStateCreateInfo.pNext = nullptr;
  pipelineColorBlendStateCreateInfo.flags = 0;
  pipelineColorBlendStateCreateInfo.logicOpEnable = false;
  pipelineColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_NO_OP;
  pipelineColorBlendStateCreateInfo.attachmentCount = 1;
  pipelineColorBlendStateCreateInfo.pAttachments = &pipelineColorBlendAttachmentState;
  pipelineColorBlendStateCreateInfo.blendConstants[0] = 0.f;
  pipelineColorBlendStateCreateInfo.blendConstants[1] = 0.f;
  pipelineColorBlendStateCreateInfo.blendConstants[2] = 0.f;
  pipelineColorBlendStateCreateInfo.blendConstants[3] = 0.f;

  VkDynamicState dynamicState[] = { VK_DYNAMIC_STATE_VIEWPORT };

  VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo;
  pipelineDynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  pipelineDynamicStateCreateInfo.pNext = nullptr;
  pipelineDynamicStateCreateInfo.flags = 0;
  pipelineDynamicStateCreateInfo.dynamicStateCount = ADK_ARRAY_SIZE(dynamicState);
  pipelineDynamicStateCreateInfo.pDynamicStates = dynamicState;

  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
  pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutCreateInfo.pNext = nullptr;
  pipelineLayoutCreateInfo.flags = 0;
  pipelineLayoutCreateInfo.setLayoutCount = hasDescriptorSets ? 1 : 0;
  pipelineLayoutCreateInfo.pSetLayouts = &pRenderPipeline->descriptorSetLayout;
  pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
  pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

  ADK_ASSERT_VK_RESULT(vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pRenderPipeline->pipelineLayout));

  if (pCreateInfo->colorTargetType == ADK_RENDER_TARGET_TYPE_WINDOW)
  {
    pRenderPipeline->colorFormat = pCreateInfo->colorTarget.pWindow->swapchainFormat;
    pRenderPipeline->imageCount = pCreateInfo->colorTarget.pWindow->swapchainImageCount;
    pRenderPipeline->imageExtent = adkVector3<uint32_t>::create(pCreateInfo->colorTarget.pWindow->extent.x, pCreateInfo->colorTarget.pWindow->extent.y, 1);
    pRenderPipeline->pImageViews = pCreateInfo->colorTarget.pWindow->pVkSwapchainImageViews;
  }
  else
  {
    pRenderPipeline->colorFormat = pCreateInfo->colorTarget.pTexture->vkFormat;
    pRenderPipeline->imageCount = 1;
    pRenderPipeline->imageExtent = pCreateInfo->colorTarget.pTexture->extent;
    pRenderPipeline->pImageViews = &pCreateInfo->colorTarget.pTexture->imageView;
  }

  VkAttachmentDescription attachmentDescriptions[2];

  //Color
  attachmentDescriptions[0].flags = 0;
  attachmentDescriptions[0].format = pRenderPipeline->colorFormat;
  attachmentDescriptions[0].samples = VK_SAMPLE_COUNT_1_BIT;
  attachmentDescriptions[0].loadOp = (pCreateInfo->flags & ADK_RENDER_PIPELINE_CLEAR_COLOR_BIT) ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
  attachmentDescriptions[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attachmentDescriptions[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attachmentDescriptions[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attachmentDescriptions[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
  attachmentDescriptions[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentReference colorAttachmentReference;
  colorAttachmentReference.attachment = 0;
  colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentReference depthAttachmentReference;
  depthAttachmentReference.attachment = 1;
  depthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkSubpassDescription subpassDescription;
  subpassDescription.flags = 0;
  subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpassDescription.inputAttachmentCount = 0;
  subpassDescription.pInputAttachments = nullptr;
  subpassDescription.colorAttachmentCount = 1;
  subpassDescription.pColorAttachments = &colorAttachmentReference;
  subpassDescription.pResolveAttachments = nullptr;
  subpassDescription.preserveAttachmentCount = 0;
  subpassDescription.pPreserveAttachments = nullptr;

  VkRenderPassCreateInfo renderPassCreateInfo;
  renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  renderPassCreateInfo.pNext = nullptr;
  renderPassCreateInfo.flags = 0;
  renderPassCreateInfo.pAttachments = attachmentDescriptions;
  renderPassCreateInfo.subpassCount = 1;
  renderPassCreateInfo.pSubpasses = &subpassDescription;
  renderPassCreateInfo.dependencyCount = 0;
  renderPassCreateInfo.pDependencies = nullptr;

  AdkTexture *pDepthTarget;
  bool renderDepth = (pCreateInfo->flags & (ADK_RENDER_PIPELINE_DEPTH_READ_BIT | ADK_RENDER_PIPELINE_DEPTH_WRITE_BIT)) != 0;

  //Depth
  if (renderDepth)
  {
    pDepthTarget = pCreateInfo->depthTargetType == ADK_RENDER_TARGET_TYPE_WINDOW ? pCreateInfo->depthTarget.pWindow->pDepthTexture : pCreateInfo->depthTarget.pTexture;

    attachmentDescriptions[1].flags = 0;
    attachmentDescriptions[1].format = pDepthTarget->vkFormat;
    attachmentDescriptions[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachmentDescriptions[1].loadOp = (pCreateInfo->flags & ADK_RENDER_PIPELINE_CLEAR_DEPTH_BIT) ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD;
    attachmentDescriptions[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachmentDescriptions[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachmentDescriptions[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachmentDescriptions[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachmentDescriptions[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    subpassDescription.pDepthStencilAttachment = &depthAttachmentReference;

    renderPassCreateInfo.attachmentCount = 2;
  }
  else
  {
    pDepthTarget = nullptr;
    subpassDescription.pDepthStencilAttachment = nullptr;

    renderPassCreateInfo.attachmentCount = 1;
  }

  ADK_ASSERT_VK_RESULT(vkCreateRenderPass(device, &renderPassCreateInfo, pInstance->pVkAllocationCallbacks, &pRenderPipeline->renderPass));

  VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo;
  graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  graphicsPipelineCreateInfo.pNext = nullptr;
  graphicsPipelineCreateInfo.flags = 0;
  graphicsPipelineCreateInfo.stageCount = shaderStageCount;
  graphicsPipelineCreateInfo.pStages = shaderStageCreateInfos;
  graphicsPipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
  graphicsPipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;
  graphicsPipelineCreateInfo.pTessellationState = nullptr;
  graphicsPipelineCreateInfo.pViewportState = &pipelineViewportStateCreateInfo;
  graphicsPipelineCreateInfo.pRasterizationState = &pipelineRasterizationStateCreateInfo;
  graphicsPipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
  graphicsPipelineCreateInfo.pDepthStencilState = &pipelineDepthStencilStateCreateInfo;
  graphicsPipelineCreateInfo.pColorBlendState = &pipelineColorBlendStateCreateInfo;
  graphicsPipelineCreateInfo.pDynamicState = (pCreateInfo->flags & ADK_RENDER_PIPELINE_MUTABLE_BIT ? &pipelineDynamicStateCreateInfo : nullptr);
  graphicsPipelineCreateInfo.layout = pRenderPipeline->pipelineLayout;
  graphicsPipelineCreateInfo.renderPass = pRenderPipeline->renderPass;
  graphicsPipelineCreateInfo.subpass = 0;
  graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
  graphicsPipelineCreateInfo.basePipelineIndex = 0;

  ADK_ASSERT_VK_RESULT(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, pInstance->pVkAllocationCallbacks, &pRenderPipeline->pipeline));

  VkCommandPool commandPool = pInstance->pvkCommandPools[pCreateInfo->pGraphicsQueue->index];
  VkCommandPool resetCommandPool = pInstance->pvkResetCommandPools[pCreateInfo->pGraphicsQueue->index];

  pRenderPipeline->pCommandBuffers = adkAllocTypeCall(VkCommandBuffer, pRenderPipeline->imageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  VkCommandBufferAllocateInfo commandBufferAllocateInfo;
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.pNext = nullptr;
  commandBufferAllocateInfo.commandPool = commandPool;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = pRenderPipeline->imageCount;

  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, pRenderPipeline->pCommandBuffers));

  pRenderPipeline->pFramebuffers = adkAllocTypeCall(VkFramebuffer, pRenderPipeline->imageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  VkDeviceSize *pVertexBufferOffsets = adkAllocTypeCall(VkDeviceSize, pCreateInfo->attributeCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_COMMAND);

  VkImageView framebufferImageViews[2];

  VkFramebufferCreateInfo framebufferCreateInfo;
  framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  framebufferCreateInfo.pNext = nullptr;
  framebufferCreateInfo.flags = 0;
  framebufferCreateInfo.renderPass = pRenderPipeline->renderPass;

  if (pDepthTarget == nullptr)
  {
    framebufferCreateInfo.attachmentCount = 1;
  }
  else
  {
    framebufferCreateInfo.attachmentCount = 2;
    framebufferImageViews[1] = pDepthTarget->imageView;
  }

  framebufferCreateInfo.width = pRenderPipeline->imageExtent.x;
  framebufferCreateInfo.height = pRenderPipeline->imageExtent.y;
  framebufferCreateInfo.layers = 1;

  for (uint32_t i = 0; i < pRenderPipeline->imageCount; ++i)
  {
    framebufferImageViews[0] = pRenderPipeline->pImageViews[i];
    framebufferCreateInfo.pAttachments = framebufferImageViews;

    ADK_ASSERT_VK_RESULT(vkCreateFramebuffer(device, &framebufferCreateInfo, pInstance->pVkAllocationCallbacks, pRenderPipeline->pFramebuffers + i));
  }

  if (pCreateInfo->flags & ADK_RENDER_PIPELINE_MUTABLE_BIT)
  {
    pRenderPipeline->pDynamicStateCommandBuffers = adkAllocTypeCall(VkCommandBuffer, pRenderPipeline->imageCount, adkAF_None, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
    pRenderPipeline->pDirtyDynamicStates = adkAllocTypeCall(AdkBool32, pRenderPipeline->imageCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

    pRenderPipeline->viewport = pCreateInfo->viewport;

    VkCommandBufferAllocateInfo dynamicStateCommandBufferAllocateInfo;
    dynamicStateCommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    dynamicStateCommandBufferAllocateInfo.pNext = nullptr;
    dynamicStateCommandBufferAllocateInfo.commandPool = resetCommandPool;
    dynamicStateCommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;
    dynamicStateCommandBufferAllocateInfo.commandBufferCount = pRenderPipeline->imageCount;
    ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(device, &dynamicStateCommandBufferAllocateInfo, pRenderPipeline->pDynamicStateCommandBuffers));

    for (uint32_t i = 0; i < pRenderPipeline->imageCount; ++i)
      _RecordDynamicStateCommandBuffer(pRenderPipeline, i);
  }

  size_t indirectBufferSize = (pCreateInfo->flags & ADK_RENDER_PIPELINE_INDEXED_BIT) != 0 ? sizeof(VkDrawIndexedIndirectCommand) : sizeof(VkDrawIndirectCommand);
  _CreateBuffer(pInstance, physicalDevice, device, pInstance->pVkAllocationCallbacks, pInstance->pAllocationCallbacks, VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, indirectBufferSize, &pRenderPipeline->indirectBuffer, &pRenderPipeline->indirectDeviceMemory);

  for (uint32_t i = 0; i < pRenderPipeline->imageCount; ++i)
  {
    VkCommandBufferBeginInfo commandBufferBeginInfo;
    commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    commandBufferBeginInfo.pNext = nullptr;
    commandBufferBeginInfo.flags = 0;
    commandBufferBeginInfo.pInheritanceInfo = nullptr;

    VkClearValue clearValues[2];
    clearValues[0].color = _CreateVkClearColorValueFloat(pCreateInfo->clearColor);
    clearValues[1].depthStencil.depth = 1.f;
    clearValues[1].depthStencil.stencil = 0;

    VkRenderPassBeginInfo renderPassBeginInfo;
    renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassBeginInfo.pNext = nullptr;
    renderPassBeginInfo.renderPass = pRenderPipeline->renderPass;
    renderPassBeginInfo.framebuffer = pRenderPipeline->pFramebuffers[i];
    renderPassBeginInfo.renderArea.offset.x = pCreateInfo->viewport.x;
    renderPassBeginInfo.renderArea.offset.y = pCreateInfo->viewport.y;
    renderPassBeginInfo.renderArea.extent.width = pCreateInfo->viewport.width;
    renderPassBeginInfo.renderArea.extent.height = pCreateInfo->viewport.height;
    renderPassBeginInfo.clearValueCount = 1;
    renderPassBeginInfo.pClearValues = clearValues;

    ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pRenderPipeline->pCommandBuffers[i], &commandBufferBeginInfo));
    vkCmdBeginRenderPass(pRenderPipeline->pCommandBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(pRenderPipeline->pCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pRenderPipeline->pipeline);

    if (pCreateInfo->flags & ADK_RENDER_PIPELINE_MUTABLE_BIT)
    {
      vkCmdSetViewport(pRenderPipeline->pCommandBuffers[i], 0, 1, &viewport);
      vkCmdExecuteCommands(pRenderPipeline->pCommandBuffers[i], 1, pRenderPipeline->pDynamicStateCommandBuffers + i);
    }

    if (hasDescriptorSets)
      vkCmdBindDescriptorSets(pRenderPipeline->pCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pRenderPipeline->pipelineLayout, 0, 1, &pRenderPipeline->descriptorSet, 0, nullptr);

    vkCmdBindVertexBuffers(pRenderPipeline->pCommandBuffers[i], 0, vertexBinding, pBuffers, pVertexBufferOffsets);

    if (pCreateInfo->flags & ADK_RENDER_PIPELINE_INDEXED_BIT)
    {
      vkCmdBindIndexBuffer(pRenderPipeline->pCommandBuffers[i], pCreateInfo->pIndices->buffer, 0, VK_INDEX_TYPE_UINT32);
      vkCmdDrawIndexedIndirect(pRenderPipeline->pCommandBuffers[i], pRenderPipeline->indirectBuffer, 0, 1, 0);
    }
    else
    {
      vkCmdDrawIndirect(pRenderPipeline->pCommandBuffers[i], pRenderPipeline->indirectBuffer, 0, 1, 0);
    }

    vkCmdEndRenderPass(pRenderPipeline->pCommandBuffers[i]);
    ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pRenderPipeline->pCommandBuffers[i]));
  }

  VkFenceCreateInfo fenceCreateInfo;
  fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fenceCreateInfo.pNext = nullptr;
  fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
  ADK_ASSERT_VK_RESULT(vkCreateFence(device, &fenceCreateInfo, pInstance->pVkAllocationCallbacks, &pRenderPipeline->fence));

  _CreateSemaphore(device, queue, pInstance->pVkAllocationCallbacks, &pRenderPipeline->semaphore);

  uint32_t samplerTextureCount = 0;

  for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
    samplerTextureCount += pCreateInfo->pSamplerUniforms[i].count;

  pRenderPipeline->renderSemaphoreCount = pCreateInfo->attributeCount + pCreateInfo->bufferUniformCount + samplerTextureCount + (renderDepth ? 2 : 1) + 1;
  pRenderPipeline->pRenderSemaphores = adkAllocTypeCall(VkSemaphore, pRenderPipeline->renderSemaphoreCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pRenderPipeline->pWaitFlags = adkAllocTypeCall(VkPipelineStageFlags, pRenderPipeline->renderSemaphoreCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  uint32_t semaphoreIterator = 0;

  //for (uint32_t i = 0; i < pCreateInfo->attributeCount; ++i)
  //{
  //  pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->pAttributes[i].pBuffer->semaphore;
  //  pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_TRANSFER_BIT;
  //  ++semaphoreIterator;
  //}
  //
  for (uint32_t i = 0; i < pCreateInfo->bufferUniformCount; ++i)
  {
    pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->pBufferUniforms[i].pBuffer->semaphore;
    pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_TRANSFER_BIT;
    ++semaphoreIterator;
  }
  //
  //for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
  //{
  //  for (uint32_t j = 0; j < pCreateInfo->pSamplerUniforms[i].count; ++j)
  //  {
  //    pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->pSamplerUniforms[i].ppTextures[j]->semaphore;
  //    pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_TRANSFER_BIT;
  //    ++semaphoreIterator;
  //  }
  //}

  //Store textures so that their layout can be transitioned before rendering
  pRenderPipeline->textureCount = samplerTextureCount;
  pRenderPipeline->ppTextures = adkAllocTypeCall(AdkTexture*, samplerTextureCount, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);
  uint32_t textureIterator = 0;
  for (uint32_t i = 0; i < pCreateInfo->samplerUniformCount; ++i)
  {
    for (uint32_t j = 0; j < pCreateInfo->pSamplerUniforms[i].count; ++j)
    {
      pRenderPipeline->ppTextures[textureIterator] = pCreateInfo->pSamplerUniforms[i].ppTextures[j];
      ++textureIterator;
    }
  }

  if (pCreateInfo->colorTargetType == ADK_RENDER_TARGET_TYPE_WINDOW)
    pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->colorTarget.pWindow->swapchainSemaphore;
  else
    pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->colorTarget.pTexture->semaphore;

  pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

  ++semaphoreIterator;

  if (renderDepth)
  {
    if (pCreateInfo->depthTargetType == ADK_RENDER_TARGET_TYPE_WINDOW)
      pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->depthTarget.pWindow->swapchainSemaphore;
    else
      pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pCreateInfo->depthTarget.pTexture->semaphore;

    pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    ++semaphoreIterator;
  }

  pRenderPipeline->pRenderSemaphores[semaphoreIterator] = pRenderPipeline->semaphore;
  pRenderPipeline->pWaitFlags[semaphoreIterator] = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
  ++semaphoreIterator;

  pRenderPipeline->renderSemaphoreCount = semaphoreIterator;

  //Remove duplicate semaphores.
  for (uint32_t i = 0; i < pRenderPipeline->renderSemaphoreCount; ++i)
  {
    for (uint32_t j = i; j < pRenderPipeline->renderSemaphoreCount; ++j)
    {
      if (i != j && pRenderPipeline->pRenderSemaphores[i] == pRenderPipeline->pRenderSemaphores[j])
      {
        pRenderPipeline->pRenderSemaphores[j] = pRenderPipeline->pRenderSemaphores[pRenderPipeline->renderSemaphoreCount - 1];
        pRenderPipeline->pWaitFlags[j] = pRenderPipeline->pWaitFlags[pRenderPipeline->renderSemaphoreCount - 1];
        --pRenderPipeline->renderSemaphoreCount;
      }
    }
  }

  adkFreeCall(pSpecializationMapEntries, pInstance->pAllocationCallbacks);
  adkFreeCall(pSpecializationData, pInstance->pAllocationCallbacks);
  adkFreeCall(bindingDescriptions, pInstance->pAllocationCallbacks);
  adkFreeCall(pVertexBufferOffsets, pInstance->pAllocationCallbacks);

  (*ppRenderPipeline) = pRenderPipeline;
}

void adkRenderPipeline_Destroy(AdkInstance *pInstance, AdkRenderPipeline **ppRenderPipeline)
{
  VkResult result;
  AdkRenderPipeline *pRenderPipeline = (*ppRenderPipeline);
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  uint32_t commandPoolIndex = pInstance->pVkDeviceQueueFamilyRanges[pInstance->primaryDeviceIndex].start;
  VkCommandPool commandPool = pInstance->pvkCommandPools[commandPoolIndex];

  do
  {
    result = vkWaitForFences(device, 1, &pRenderPipeline->fence, false, 1000000000);
  } while (result != VK_SUCCESS);

  adkFreeCall(pRenderPipeline->pWaitFlags, pInstance->pAllocationCallbacks);
  adkFreeCall(pRenderPipeline->pRenderSemaphores, pInstance->pAllocationCallbacks);
  vkDestroySemaphore(device, pRenderPipeline->semaphore, pInstance->pVkAllocationCallbacks);
  vkDestroyRenderPass(device, pRenderPipeline->renderPass, pInstance->pVkAllocationCallbacks);
  vkDestroyPipelineLayout(device, pRenderPipeline->pipelineLayout, pInstance->pVkAllocationCallbacks);
  vkDestroyPipeline(device, pRenderPipeline->pipeline, pInstance->pVkAllocationCallbacks);
  vkDestroyFence(device, pRenderPipeline->fence, pInstance->pVkAllocationCallbacks);
  vkFreeCommandBuffers(device, commandPool, pRenderPipeline->imageCount, pRenderPipeline->pCommandBuffers);
  vkDestroyBuffer(device, pRenderPipeline->indirectBuffer, pInstance->pVkAllocationCallbacks);
  _FreeDeviceMemory(pInstance, &pRenderPipeline->indirectDeviceMemory);
  adkFreeCall(pRenderPipeline->pCommandBuffers, pInstance->pAllocationCallbacks);

  if (pRenderPipeline->flags & ADK_RENDER_PIPELINE_MUTABLE_BIT)
  {
    vkFreeCommandBuffers(device, commandPool, pRenderPipeline->imageCount, pRenderPipeline->pDynamicStateCommandBuffers);
    adkFreeCall(pRenderPipeline->pDynamicStateCommandBuffers, pInstance->pAllocationCallbacks);
  }

  adkFreeCall(pRenderPipeline->pDirtyDynamicStates, pInstance->pAllocationCallbacks);
  adkFreeCall(pRenderPipeline->ppTextures, pInstance->pAllocationCallbacks);
  adkFreeCall(*ppRenderPipeline, pInstance->pAllocationCallbacks);
}

void adkRenderPipeline_Render(AdkInstance *pInstance, AdkRenderPipeline *pRenderPipeline, uint32_t vertexCount, uint32_t instanceCount)
{
  VkResult result;
  VkDevice device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkQueue queue = pInstance->pVkQueues[pRenderPipeline->pGraphicsQueue->index];
  uint32_t imageIndex;

  if (pRenderPipeline->colorTargetType == ADK_RENDER_TARGET_TYPE_WINDOW)
    imageIndex = pRenderPipeline->colorTarget.pWindow->swapchainImageIndex;
  else
    imageIndex = 0;

  do
  {
    result = vkWaitForFences(device, 1, &pRenderPipeline->fence, false, 1000000000);
  } while (result != VK_SUCCESS);

  vkResetFences(device, 1, &pRenderPipeline->fence);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;

  //Record dynamic state.
  if ((pRenderPipeline->flags & ADK_RENDER_PIPELINE_MUTABLE_BIT) && pRenderPipeline->pDirtyDynamicStates[imageIndex])
  {
    _RecordDynamicStateCommandBuffer(pRenderPipeline, imageIndex);
    pRenderPipeline->pDirtyDynamicStates[imageIndex] = false;
  }

  //Update indirect buffer.
  VkMappedMemoryRange mappedMemoryRange;
  mappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
  mappedMemoryRange.pNext = nullptr;
  mappedMemoryRange.memory = pRenderPipeline->indirectDeviceMemory.vkDeviceMemory;
  mappedMemoryRange.offset = pRenderPipeline->indirectDeviceMemory.offset;
  mappedMemoryRange.size = pRenderPipeline->indirectDeviceMemory.size;

  if (pRenderPipeline->flags & ADK_RENDER_PIPELINE_INDEXED_BIT)
  {
    VkDrawIndexedIndirectCommand *pIndexedIndirectCommand = (VkDrawIndexedIndirectCommand*)pRenderPipeline->indirectDeviceMemory.pMapped;
    pIndexedIndirectCommand->indexCount = vertexCount;
    pIndexedIndirectCommand->instanceCount = instanceCount;
    pIndexedIndirectCommand->firstIndex = 0;
    pIndexedIndirectCommand->vertexOffset = 0;
    pIndexedIndirectCommand->firstInstance = 0;
  }
  else
  {
    VkDrawIndirectCommand *pIndirectCommand = (VkDrawIndirectCommand*)pRenderPipeline->indirectDeviceMemory.pMapped;
    pIndirectCommand->vertexCount = vertexCount;
    pIndirectCommand->instanceCount = instanceCount;
    pIndirectCommand->firstVertex = 0;
    pIndirectCommand->firstInstance = 0;
  }

  ADK_ASSERT_VK_RESULT(vkFlushMappedMemoryRanges(device, 1, &mappedMemoryRange));

  //Transition texture layouts
  for (uint32_t i = 0; i < pRenderPipeline->textureCount; ++i)
    adkTexture_ChangeLayout(pRenderPipeline->ppTextures[i], VK_ACCESS_UNIFORM_READ_BIT, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);

  //Submit render.
  submitInfo.waitSemaphoreCount = pRenderPipeline->renderSemaphoreCount;
  submitInfo.pWaitSemaphores = pRenderPipeline->pRenderSemaphores;
  submitInfo.pWaitDstStageMask = pRenderPipeline->pWaitFlags;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pRenderPipeline->pCommandBuffers[imageIndex];
  submitInfo.signalSemaphoreCount = pRenderPipeline->renderSemaphoreCount;
  submitInfo.pSignalSemaphores = pRenderPipeline->pRenderSemaphores;

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pRenderPipeline->fence));

  //Wait for rendering to complete before doing more with the swapchain.
  //submitInfo.waitSemaphoreCount = pRenderPipeline->renderSemaphoreCount;
  //submitInfo.pWaitSemaphores = pRenderPipeline->pRenderSemaphores;
  //submitInfo.pWaitDstStageMask = pRenderPipeline->pWaitFlags;
  //submitInfo.commandBufferCount = 0;
  //submitInfo.pCommandBuffers = nullptr;
  //submitInfo.signalSemaphoreCount = pRenderPipeline->renderSemaphoreCount;
  //submitInfo.pSignalSemaphores = pRenderPipeline->pRenderSemaphores;
  //ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pRenderPipeline->fence));
}

void adkRenderPipeline_SetViewport(AdkRenderPipeline *pRenderPipeline, const adkRect<uint32_t> *pViewport)
{
  pRenderPipeline->viewport = (*pViewport);

  for (uint32_t i = 0; i < pRenderPipeline->imageCount; ++i)
    pRenderPipeline->pDirtyDynamicStates[i] = true;
}