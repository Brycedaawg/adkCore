#include "adkMath.h"

template <typename T>
bool adkPhysicsUtil_LineOnPlaneIntersection(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &p0, const adkVector3<T> &pn, T *pLt);

template <typename T>
bool adkPhysicsUtil_Quadratic(T &a, T &b, T &c, T *pX0, T *pX1);

template <typename T>
T adkPhysicsUtil_PointOnLineClosestToPoint(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &p);

template <typename T>
bool adkPhysicsUtil_LineOnSphereIntersection(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &sc, const T &sr, T *pLT0, T *pLT1);

template <typename T>
bool adkPhysicsUtil_LineOnInfiniteCylinder(const adkVector3<T> &l0, const adkVector3<T> &ln, const adkVector3<T> &c0, const adkVector3<T> &cn, const T &cr, T *pLT0, T *pLT1);

template <typename T>
bool adkPhysicsUtil_LineOnLineClosestPoints(const adkVector3<T> &p0, const adkVector3<T> &n0, const adkVector3<T> &p1, const adkVector3<T> &n1, T *pT0, T *pT1);

template <typename T>
struct AdkCollision
{
  adkVector3<T> contact;
  adkVector3<T> resolution;
  T resolutionT;
};

template <typename T>
bool adkPhysicsUtil_SweepingSphereOnTriangle(const adkVector3<T> &s0, const adkVector3<T> sv, const T &sr, const adkVector3<T> &t0, const adkVector3<T> &t1, const adkVector3<T> &t2, AdkCollision<T> *pCollision);

template <typename T>
struct AdkSweepingSphereOnSweepingSphereCollision
{
  adkVector3<T> contact;
  adkVector3<T> resolutionA;
  adkVector3<T> resolutionB;
  T resolutionT;
};

template <typename T>
bool adkPhysicsUtil_SweepingSphereOnSweepingSphere(const adkVector3<T> &startA, const adkVector3<T> &velocityA, const T &radiusA, const adkVector3<T> &startB, const adkVector3<T> &velocityB, const T &radiusB, AdkSweepingSphereOnSweepingSphereCollision<T> *pCollision);

#include "AdkPhysicsUtil.inl"