#ifndef adkMaterial_inl__
#define adkMaterial_inl__

#include "AdkMaterial.h"
#include <stdint.h>

#define ADK_MATERIAL_INVALID_TEXTURE_INDEX 0xFFFFFFFF

enum
{
  ADK_MATERIAL_MAX_TEXTURE_COUNT = 32,
};

typedef struct AdkMaterial
{
  AdkInstance *pInstance;
  AdkShader *pShader;
  uint32_t textureCount;
  AdkTexture *textures[ADK_MATERIAL_MAX_TEXTURE_COUNT];
  uint32_t textureBindings[ADK_MATERIAL_MAX_TEXTURE_COUNT];
} AdkMaterial;

#endif // adkMaterial_inl__
