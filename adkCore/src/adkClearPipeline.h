#ifndef adkClearPipeline_h__
#define adkClearPipeline_h__

#include "adkTypedefs.h"
#include "adkRenderPipeline.h"
#include "AdkQueue.h"

struct AdkClearPipeline;

typedef enum AdkClearPipelineCreateFlagBits
{
  ADK_CLEAR_PIPELINE_CREATE_MUTABLE_BIT = 0x00000004,
} AdkClearPipelineCreateFlagBits;
typedef AdkFlags AdkClearPipelineCreateFlags;

typedef enum AdkClearTargetType
{
  ADK_CLEAR_TARGET_TYPE_WINDOW_COLOR,
  ADK_CLEAR_TARGET_TYPE_WINDOW_DEPTH,
  ADK_CLEAR_TARGET_TYPE_TEXTURE,
} AdkClearTargetType;

typedef union AdkClearTarget
{
  adkWindow *pWindow;
  AdkTexture *pTexture;
} AdkClearTarget;

typedef struct AdkClearPipelineCreateInfo
{
  AdkClearPipelineCreateFlags flags;
  const void *pValue;
  AdkClearTargetType targetType;
  AdkClearTarget target;
  AdkQueue *pQueue;
} AdkClearPipelineCreateInfo;

void adkClearPipeline_Create(AdkInstance *pInstance, const AdkClearPipelineCreateInfo *pCreateInfo, AdkClearPipeline **ppClearPipeline);
void adkClearPipeline_Destroy(AdkInstance *pInstance, AdkClearPipeline **ppClearPipeline);
void adkClearPipeline_SetValue(AdkInstance *pInstance, AdkClearPipeline *pClearPipeline, void *pValue);
void adkClearPipeline_Clear(AdkInstance *pInstance, AdkClearPipeline *pClearPipeline);

#endif // adkClearPipeline_h__
