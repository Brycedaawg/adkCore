#ifndef adkMaterial_h__
#define adkMaterial_h__

#include <cstdint>

struct AdkMaterial;
struct AdkInstance;
struct AdkTexture;
struct AdkShader;

void adkMaterial_Create(AdkInstance *pInstance, AdkShader *pShader, AdkMaterial **ppMaterial);
void adkMaterial_Destroy(AdkMaterial **ppMaterial);
void adkMaterial_SetTexture(AdkMaterial *pMaterial, uint32_t binding, AdkTexture *pTexture);

#endif // adkMaterial_h__
