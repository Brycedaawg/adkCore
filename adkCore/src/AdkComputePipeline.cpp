#include "AdkComputePipeline.h"
#include "vulkan.h"
#include "adkVkTools.inl"
#include "adkShader.inl"
#include "adkBuffer.inl"
#include "adkTexture.inl"
#include "AdkQueue.inl"

typedef struct AdkComputePipeline
{
  AdkInstance *pInstance;
  VkDevice device;
  const adkAllocationCallbacks *pAllocator;
  VkDescriptorSetLayout descriptorSetLayout;
  VkPipelineLayout pipelineLayout;
  VkDescriptorPool descriptorPool;
  VkDescriptorSet descriptorSet;
  VkBuffer indirectBuffer;
  AdkDeviceMemory indirectDeviceMemory;
  VkPipeline pipeline;
  VkCommandBuffer commandBuffer;
  VkSemaphore dispatchComputeSemaphore;
  uint32_t storageBufferCount;
  AdkBuffer **ppStorageBuffer;
  uint32_t storageTextureCount;
  AdkTexture **ppStorageTextures;
  VkSemaphore *pDispatchSemaphores;
  VkPipelineStageFlags *pDispatchWaitDstStageMasks;
  VkFence dispatchFence;
  AdkQueue *pComputeQueue;
} AdkComputePipeline;

enum
{
  ADK_COMPUTE_PIPELINE_DISPATCH_COMPUTE_SEMAPHORE_COUNT = 1,
};

void adkComputePipeline_Create(AdkInstance *pInstance, const AdkComputePipelineCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkComputePipeline **ppComputePipeline)
{
  AdkComputePipeline *pComputePipeline = adkAllocTypeCall(AdkComputePipeline, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pComputePipeline->pInstance = pInstance;
  pComputePipeline->pAllocator = pAllocator;
  pComputePipeline->pComputeQueue = pCreateInfo->pComputeQueue;

  pComputePipeline->device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  VkPhysicalDevice physicalDevice = pInstance->pVkPhysicalDevices[pInstance->primaryDeviceIndex];
  VkAllocationCallbacks vkAllocationCallbacks, *pVkAllocator = _AdkToVkAllocationCallbacks(pAllocator, &vkAllocationCallbacks);
  VkCommandPool commandPool = pInstance->pvkCommandPools[pCreateInfo->pComputeQueue->index];

  uint32_t uniformCount = pCreateInfo->storageBufferCount + pCreateInfo->storageImageCount;
  uint32_t uniformIndex = 0;

  VkQueue queue = pInstance->pVkQueues[pCreateInfo->pComputeQueue->index];

  VkDescriptorSetLayoutBinding *pDescriptorSetLayoutBindings = adkAllocTypeCall(VkDescriptorSetLayoutBinding, uniformCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  for (uint32_t i = 0; i < pCreateInfo->storageBufferCount; ++i)
  {
    pDescriptorSetLayoutBindings[uniformIndex].binding = pCreateInfo->pStorageBuffers[i].binding;
    pDescriptorSetLayoutBindings[uniformIndex].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    pDescriptorSetLayoutBindings[uniformIndex].descriptorCount = 1;
    pDescriptorSetLayoutBindings[uniformIndex].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    pDescriptorSetLayoutBindings[uniformIndex].pImmutableSamplers = nullptr;

    ++uniformIndex;
  }

  for (uint32_t i = 0; i < pCreateInfo->storageImageCount; ++i)
  {
    pDescriptorSetLayoutBindings[uniformIndex].binding = pCreateInfo->pStorageImages[i].binding;
    pDescriptorSetLayoutBindings[uniformIndex].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    pDescriptorSetLayoutBindings[uniformIndex].descriptorCount = 1;
    pDescriptorSetLayoutBindings[uniformIndex].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
    pDescriptorSetLayoutBindings[uniformIndex].pImmutableSamplers = nullptr;

    ++uniformIndex;
  }

  VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
  descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
  descriptorSetLayoutCreateInfo.pNext = nullptr;
  descriptorSetLayoutCreateInfo.flags = 0;
  descriptorSetLayoutCreateInfo.bindingCount = uniformCount;
  descriptorSetLayoutCreateInfo.pBindings = pDescriptorSetLayoutBindings;

  ADK_ASSERT_VK_RESULT(vkCreateDescriptorSetLayout(pComputePipeline->device, &descriptorSetLayoutCreateInfo, pVkAllocator, &pComputePipeline->descriptorSetLayout));

  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
  pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
  pipelineLayoutCreateInfo.pNext = nullptr;
  pipelineLayoutCreateInfo.flags = 0;
  pipelineLayoutCreateInfo.setLayoutCount = 1;
  pipelineLayoutCreateInfo.pSetLayouts = &pComputePipeline->descriptorSetLayout;
  pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
  pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

  ADK_ASSERT_VK_RESULT(vkCreatePipelineLayout(pComputePipeline->device, &pipelineLayoutCreateInfo, pVkAllocator, &pComputePipeline->pipelineLayout));

  VkComputePipelineCreateInfo computePipelineCreateInfo;
  computePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
  computePipelineCreateInfo.pNext = nullptr;
  computePipelineCreateInfo.flags = 0;
  computePipelineCreateInfo.stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  computePipelineCreateInfo.stage.pNext = nullptr;
  computePipelineCreateInfo.stage.flags = 0;
  computePipelineCreateInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
  computePipelineCreateInfo.stage.module = pCreateInfo->pShader->modules[ADK_SHADER_STAGE_COMPUTE];
  computePipelineCreateInfo.stage.pName = "main";
  computePipelineCreateInfo.stage.pSpecializationInfo = nullptr;
  computePipelineCreateInfo.layout = pComputePipeline->pipelineLayout;
  computePipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
  computePipelineCreateInfo.basePipelineIndex = 0;

  ADK_ASSERT_VK_RESULT(vkCreateComputePipelines(pComputePipeline->device, VK_NULL_HANDLE, 1, &computePipelineCreateInfo, pVkAllocator, &pComputePipeline->pipeline));

  VkCommandBufferAllocateInfo commandBufferAllocateInfo;
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.pNext = nullptr;
  commandBufferAllocateInfo.commandPool = commandPool;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = 1;

  ADK_ASSERT_VK_RESULT(vkAllocateCommandBuffers(pComputePipeline->device, &commandBufferAllocateInfo, &pComputePipeline->commandBuffer));

  uint32_t descriptorPoolSizesIndex = 0;
  VkDescriptorPoolSize descriptorPoolSizes[2];

  if (pCreateInfo->storageBufferCount != 0)
  {
    descriptorPoolSizes[descriptorPoolSizesIndex].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorPoolSizes[descriptorPoolSizesIndex].descriptorCount = pCreateInfo->storageBufferCount;

    ++descriptorPoolSizesIndex;
  }

  if (pCreateInfo->storageImageCount != 0)
  {
    descriptorPoolSizes[descriptorPoolSizesIndex].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    descriptorPoolSizes[descriptorPoolSizesIndex].descriptorCount = pCreateInfo->storageImageCount;

    ++descriptorPoolSizesIndex;
  }

  VkDescriptorPoolCreateInfo descriptorPoolCreateInfo;
  descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
  descriptorPoolCreateInfo.pNext = nullptr;
  descriptorPoolCreateInfo.flags = 0;
  descriptorPoolCreateInfo.maxSets = 1;
  descriptorPoolCreateInfo.poolSizeCount = descriptorPoolSizesIndex;
  descriptorPoolCreateInfo.pPoolSizes = descriptorPoolSizes;

  ADK_ASSERT_VK_RESULT(vkCreateDescriptorPool(pComputePipeline->device, &descriptorPoolCreateInfo, pVkAllocator, &pComputePipeline->descriptorPool));

  VkDescriptorSetAllocateInfo descriptorSetAllocateInfo;
  descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
  descriptorSetAllocateInfo.pNext = nullptr;
  descriptorSetAllocateInfo.descriptorPool = pComputePipeline->descriptorPool;
  descriptorSetAllocateInfo.descriptorSetCount = 1;
  descriptorSetAllocateInfo.pSetLayouts = &pComputePipeline->descriptorSetLayout;

  ADK_ASSERT_VK_RESULT(vkAllocateDescriptorSets(pComputePipeline->device, &descriptorSetAllocateInfo, &pComputePipeline->descriptorSet));

  VkWriteDescriptorSet *pWriteDescriptorSets = adkAllocTypeCall(VkWriteDescriptorSet, uniformCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  uint32_t descriptorIndex = 0;


  VkDescriptorBufferInfo *pDescriptorStorageBufferInfos = adkAllocTypeCall(VkDescriptorBufferInfo, pCreateInfo->storageBufferCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  for (uint32_t i = 0; i < pCreateInfo->storageBufferCount; ++i)
  {
    pDescriptorStorageBufferInfos[i].buffer = pCreateInfo->pStorageBuffers[i].pBuffer->buffer;
    pDescriptorStorageBufferInfos[i].offset = 0;
    pDescriptorStorageBufferInfos[i].range = VK_WHOLE_SIZE;

    pWriteDescriptorSets[descriptorIndex].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    pWriteDescriptorSets[descriptorIndex].pNext = nullptr;
    pWriteDescriptorSets[descriptorIndex].dstSet = pComputePipeline->descriptorSet;
    pWriteDescriptorSets[descriptorIndex].dstBinding = pCreateInfo->pStorageBuffers[i].binding;
    pWriteDescriptorSets[descriptorIndex].dstArrayElement = 0;
    pWriteDescriptorSets[descriptorIndex].descriptorCount = 1;
    pWriteDescriptorSets[descriptorIndex].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    pWriteDescriptorSets[descriptorIndex].pImageInfo = nullptr;
    pWriteDescriptorSets[descriptorIndex].pBufferInfo = &pDescriptorStorageBufferInfos[i];
    pWriteDescriptorSets[descriptorIndex].pTexelBufferView = nullptr;

    ++descriptorIndex;
  }

  VkDescriptorImageInfo *pDescriptorStorageImageInfos = adkAllocTypeCall(VkDescriptorImageInfo, pCreateInfo->storageImageCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_COMMAND);

  for (uint32_t i = 0; i < pCreateInfo->storageImageCount; ++i)
  {
    pDescriptorStorageImageInfos[i].sampler = pCreateInfo->pStorageImages[i].pTexture->sampler;
    pDescriptorStorageImageInfos[i].imageView = pCreateInfo->pStorageImages[i].pTexture->imageView;
    pDescriptorStorageImageInfos[i].imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    pWriteDescriptorSets[descriptorIndex].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    pWriteDescriptorSets[descriptorIndex].pNext = nullptr;
    pWriteDescriptorSets[descriptorIndex].dstSet = pComputePipeline->descriptorSet;
    pWriteDescriptorSets[descriptorIndex].dstBinding = pCreateInfo->pStorageImages[i].binding;
    pWriteDescriptorSets[descriptorIndex].dstArrayElement = 0;
    pWriteDescriptorSets[descriptorIndex].descriptorCount = 1;
    pWriteDescriptorSets[descriptorIndex].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    pWriteDescriptorSets[descriptorIndex].pImageInfo = &pDescriptorStorageImageInfos[i];
    pWriteDescriptorSets[descriptorIndex].pBufferInfo = nullptr;
    pWriteDescriptorSets[descriptorIndex].pTexelBufferView = nullptr;

    adkTexture_ChangeLayout(pCreateInfo->pStorageImages[i].pTexture, VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);

    ++descriptorIndex;
  }


  vkUpdateDescriptorSets(pComputePipeline->device, uniformCount, pWriteDescriptorSets, 0, nullptr);

  _CreateBuffer(pInstance, physicalDevice, pComputePipeline->device, pVkAllocator, pAllocator, VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, sizeof(VkDispatchIndirectCommand), &pComputePipeline->indirectBuffer, &pComputePipeline->indirectDeviceMemory);

  VkCommandBufferBeginInfo commandBufferBeginInfo;
  commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
  commandBufferBeginInfo.pNext = nullptr;
  commandBufferBeginInfo.flags = 0;
  commandBufferBeginInfo.pInheritanceInfo = nullptr;

  ADK_ASSERT_VK_RESULT(vkBeginCommandBuffer(pComputePipeline->commandBuffer, &commandBufferBeginInfo));

  vkCmdBindPipeline(pComputePipeline->commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pComputePipeline->pipeline);
  vkCmdBindDescriptorSets(pComputePipeline->commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pComputePipeline->pipelineLayout, 0, 1, &pComputePipeline->descriptorSet, 0, nullptr);
  //vkCmdDispatch(pComputePipeline->commandBuffer, pCreateInfo->groupCount.x, pCreateInfo->groupCount.y, pCreateInfo->groupCount.z);
  vkCmdDispatchIndirect(pComputePipeline->commandBuffer, pComputePipeline->indirectBuffer, 0);

  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pComputePipeline->commandBuffer));

  _CreateSemaphore(pComputePipeline->device, queue, pVkAllocator, &pComputePipeline->dispatchComputeSemaphore);

  pComputePipeline->storageTextureCount = pCreateInfo->storageImageCount;
  pComputePipeline->pDispatchWaitDstStageMasks = adkAllocTypeCall(uint32_t, uniformCount + ADK_COMPUTE_PIPELINE_DISPATCH_COMPUTE_SEMAPHORE_COUNT, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pComputePipeline->pDispatchSemaphores = adkAllocTypeCall(VkSemaphore, uniformCount + ADK_COMPUTE_PIPELINE_DISPATCH_COMPUTE_SEMAPHORE_COUNT, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pComputePipeline->ppStorageTextures = adkAllocTypeCall(AdkTexture*, pCreateInfo->storageImageCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  for (uint32_t i = 0; i < pCreateInfo->storageImageCount; ++i)
    pComputePipeline->ppStorageTextures[i] = pCreateInfo->pStorageImages[i].pTexture;

  _CreateFence(pComputePipeline->device, pVkAllocator, &pComputePipeline->dispatchFence, true);

  adkFreeCall(pDescriptorStorageImageInfos, pAllocator);
  adkFreeCall(pDescriptorStorageBufferInfos, pAllocator);
  adkFreeCall(pWriteDescriptorSets, pAllocator);
  adkFreeCall(pDescriptorSetLayoutBindings, pAllocator);

  *ppComputePipeline = pComputePipeline;
}

void adkComputePipeline_Destroy(AdkComputePipeline **ppComputePipeline)
{
  AdkComputePipeline *pComputePipeline = *ppComputePipeline;
  VkAllocationCallbacks vkAllocationCallbacks, *pVkAllocator = _AdkToVkAllocationCallbacks(pComputePipeline->pAllocator, &vkAllocationCallbacks);
  VkCommandPool commandPool = pComputePipeline->pInstance->pvkCommandPools[pComputePipeline->pComputeQueue->index];

  adkFreeCall(pComputePipeline->pDispatchSemaphores, pComputePipeline->pAllocator);
  adkFreeCall(pComputePipeline->ppStorageTextures, pComputePipeline->pAllocator);

  vkDestroyFence(pComputePipeline->device, pComputePipeline->dispatchFence, pVkAllocator);
  vkFreeCommandBuffers(pComputePipeline->device, commandPool, 1, &pComputePipeline->commandBuffer);
  vkDestroyBuffer(pComputePipeline->device, pComputePipeline->indirectBuffer, pComputePipeline->pInstance->pVkAllocationCallbacks);
  _FreeDeviceMemory(pComputePipeline->pInstance, &pComputePipeline->indirectDeviceMemory);
  vkDestroyDescriptorPool(pComputePipeline->device, pComputePipeline->descriptorPool, pVkAllocator);
  vkDestroyPipeline(pComputePipeline->device, pComputePipeline->pipeline, pVkAllocator);
  vkDestroyPipelineLayout(pComputePipeline->device, pComputePipeline->pipelineLayout, pVkAllocator);
  vkDestroyDescriptorSetLayout(pComputePipeline->device, pComputePipeline->descriptorSetLayout, pVkAllocator);

  adkFreeCall(*ppComputePipeline, pComputePipeline->pAllocator);
}

void adkComputePipeline_Dispatch(AdkComputePipeline *pComputePipeline, const adkVector3<uint32_t> &groupCount)
{
  uint32_t semaphoreCount = 0;
  VkQueue queue = pComputePipeline->pInstance->pVkQueues[pComputePipeline->pComputeQueue->index];

  _WaitForFence(pComputePipeline->device, pComputePipeline->dispatchFence, true);

  for (uint32_t i = 0; i < pComputePipeline->storageTextureCount; ++i)
  {
    pComputePipeline->pDispatchWaitDstStageMasks[semaphoreCount] = pComputePipeline->ppStorageTextures[i]->stageMask;
    pComputePipeline->pDispatchSemaphores[semaphoreCount] = pComputePipeline->ppStorageTextures[i]->semaphore;

    ++semaphoreCount;

    adkTexture_ChangeLayout(pComputePipeline->ppStorageTextures[i], VK_ACCESS_SHADER_WRITE_BIT, VK_IMAGE_LAYOUT_GENERAL, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT);
  }

  pComputePipeline->pDispatchWaitDstStageMasks[semaphoreCount] = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
  pComputePipeline->pDispatchSemaphores[semaphoreCount] = pComputePipeline->dispatchComputeSemaphore;

  ++semaphoreCount;

  VkDispatchIndirectCommand *pIndirectCommand = (VkDispatchIndirectCommand*)pComputePipeline->indirectDeviceMemory.pMapped;
  pIndirectCommand->x = groupCount.x;
  pIndirectCommand->y = groupCount.y;
  pIndirectCommand->z = groupCount.z;

  adkMemoryPool_FlushDeviceMemory(&pComputePipeline->indirectDeviceMemory, pComputePipeline->device);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0/*semaphoreCount*/;
  submitInfo.pWaitSemaphores = nullptr/*pComputePipeline->pDispatchSemaphores*/;
  submitInfo.pWaitDstStageMask = pComputePipeline->pDispatchWaitDstStageMasks;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pComputePipeline->commandBuffer;
  submitInfo.signalSemaphoreCount = 0/*semaphoreCount*/;
  submitInfo.pSignalSemaphores = nullptr/*pComputePipeline->pDispatchSemaphores*/;

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, pComputePipeline->dispatchFence));
}

AdkBool32 adkComputePipeline_Wait(AdkComputePipeline *pComputePipeline, uint64_t timeout)
{
  VkResult result;
  result = vkWaitForFences(pComputePipeline->device, 1, &pComputePipeline->dispatchFence, false, timeout);

  if (result != VK_SUCCESS && result != VK_TIMEOUT)
    ADK_ASSERT_VK_RESULT(result);

  return result == VK_SUCCESS;
}