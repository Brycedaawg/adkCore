#ifndef adkTimer_h__
#define adkTimer_h__

#include "adkTypedefs.h"
#include "adkMemory.h"

struct AdkTime;

typedef struct AdkTimeCreateFlagBits
{

} AdkTimeCreateFlagBits;
typedef AdkFlags AdkTimeCreateFlags;

typedef struct AdkTimeCreateInfo
{
  AdkTimeCreateFlags flags;
} AdkTimeCreateInfo;

void adkTime_Create(const AdkTimeCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkTime **ppTime);
void adkTime_Destroy(AdkTime **ppTime);
void adkTime_GetSeconds(AdkTime *pTime, float *pDelta);
uint64_t adkTime_GetMilliseconds(AdkTime *pTime);
void adkTime_Reset(AdkTime *pTime);

#endif // adkTimer_h__
