#ifndef adkMutex_h__
#define adkMutex_h__

#include "adkMemory.h"

struct AdkMutex;

void adkMutex_Create(const adkAllocationCallbacks *pAllocator, AdkMutex **ppMutex);
void adkMutex_Destroy(const adkAllocationCallbacks *pAllocator, AdkMutex **ppMutex);
void adkMutex_Lock(AdkMutex *pMutex);
void adkMutex_Release(AdkMutex *pMutex);

#endif // adkMutex_h__
