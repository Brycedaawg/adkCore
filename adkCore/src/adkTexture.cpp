#include "adkTexture.inl"
#include "adkMemory.h"
#include "adkInstance.inl"
#include "AdkQueue.inl"

static VkFilter _adkToVkFilter(AdkTextureFilter textureFilter)
{
  switch (textureFilter)
  {
  case ADK_TEXTURE_FILTER_NEAREST:
    return VK_FILTER_NEAREST;
  case ADK_TEXTURE_FILTER_LINEAR:
    return VK_FILTER_LINEAR;
  default:
    return VK_FILTER_NEAREST;
  }
}

static VkSamplerMipmapMode _adkToVkMipmapMode(AdkTextureMipmapMode mipmapMode)
{
  switch (mipmapMode)
  {
  case ADK_TEXTURE_MIPMAP_MODE_NONE:
    return VK_SAMPLER_MIPMAP_MODE_NEAREST;
  case ADK_TEXTURE_MIPMAP_MODE_NEAREST:
    return VK_SAMPLER_MIPMAP_MODE_NEAREST;
  case ADK_TEXTURE_MIPMAP_MODE_LINEAR:
    return VK_SAMPLER_MIPMAP_MODE_LINEAR;
  default:
    return VK_SAMPLER_MIPMAP_MODE_NEAREST;

  }
}

static VkSamplerAddressMode _adkToVkAddressMode(AdkTextureAddressMode addressMode)
{
  switch (addressMode)
  {
  case ADK_TEXTURE_ADDRESS_MODE_REPEAT:
    return VK_SAMPLER_ADDRESS_MODE_REPEAT;
  case ADK_TEXTURE_ADDRESS_MODE_MIRROR_REPEAT:
    return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
  case ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE:
    return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  case ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_BORDER:
    return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
  case ADK_TEXTURE_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE:
    return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
  default:
    return VK_SAMPLER_ADDRESS_MODE_REPEAT;
  }
}

static VkBool32 _adkToVkAnisotropyMode(AdkTextureAnisotropyMode anisotropyMode)
{
  switch (anisotropyMode)
  {
  case ADK_TEXTURE_ANISOTROPY_MODE_DISABLED:
    return false;
  case ADK_TEXTURE_ANISOTROPY_MODE_ENABLED:
    return true;
  default:
    return false;
  }
}

static inline VkImageType adkTexture_DimensionsToVkImageType(AdkTextureDimensions dimensions)
{
  switch (dimensions)
  {
  case ADK_TEXTURE_DIMENSIONS_1D:
    return VK_IMAGE_TYPE_1D;
  case ADK_TEXTURE_DIMENSIONS_2D:
    return VK_IMAGE_TYPE_2D;
  case ADK_TEXTURE_DIMENSIONS_3D:
    return VK_IMAGE_TYPE_3D;
  default:
    return VK_IMAGE_TYPE_1D;
  }
}

static inline VkImageViewType adkTexture_DimensionsToVkImageViewType(AdkTextureDimensions dimensions)
{
  switch (dimensions)
  {
  case ADK_TEXTURE_DIMENSIONS_1D:
    return VK_IMAGE_VIEW_TYPE_1D;
  case ADK_TEXTURE_DIMENSIONS_2D:
    return VK_IMAGE_VIEW_TYPE_2D;
  case ADK_TEXTURE_DIMENSIONS_3D:
    return VK_IMAGE_VIEW_TYPE_3D;
  default:
    return VK_IMAGE_VIEW_TYPE_1D;
  }
}

static void _CopyImage(VkDevice device, VkQueue graphicsQueue, AdkDeviceMemory stagingDeviceMemory, VkCommandBuffer copyCommandBuffer, VkFence fence, const adkVector3<uint32_t> &extent, AdkColorType colorType, bool flipY, void *pColors)
{
  size_t colorTypeSize = adkTexture_SizeOfColorType(colorType);
  size_t size = extent.x * extent.y * extent.z;

  _WaitForFence(device, fence, true);

  //Copy image to staging buffer and ensure r is interpreted as least significant byte.
  char *pMappedStagingMemory = (char*)stagingDeviceMemory.pMapped;

  //Copy image and flip on y-axis.
  if (pColors != nullptr)
  {
    if (flipY)
    {
      uint64_t rowSize = extent.x * colorTypeSize;
      uint64_t layerSize = extent.x * extent.y * colorTypeSize;

      for (uint64_t aisle = 0; aisle < size; aisle += layerSize)
      {
        for (uint64_t rowA = 0, rowB = (extent.y - 1) * rowSize; rowA < layerSize; rowA += rowSize, rowB -= rowSize)
          memcpy((char*)pMappedStagingMemory + aisle + rowA, (char*)pColors + aisle + rowB, (size_t)rowSize);
      }
    }
    else
    {
      memcpy(pMappedStagingMemory, pColors, colorTypeSize * size);
    }
  }

  adkMemoryPool_FlushDeviceMemory(&stagingDeviceMemory, device);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pWaitDstStageMask = nullptr;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &copyCommandBuffer;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;
  ADK_ASSERT_VK_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, fence));

  _WaitForFence(device, fence, false);
}

void adkTexture_Create(AdkInstance *pInstance, const AdkTextureCreateInfo *pCreateInfo, AdkTexture **ppTexture)
{
  AdkTexture *pTexture = adkAllocTypeCall(AdkTexture, 1, adkAF_Zero, pInstance->pAllocationCallbacks, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pTexture->pInstance = pInstance;
  pTexture->pGraphicsQueue = pCreateInfo->pGraphicsQueue;

  VkPhysicalDevice physicalDevice = pInstance->pVkPhysicalDevices[pInstance->primaryDeviceIndex];
  pTexture->device = pInstance->pVkDevices[pInstance->primaryDeviceIndex];
  pTexture->commandPool = pInstance->pvkCommandPools[pCreateInfo->pGraphicsQueue->index];
  pTexture->resetCommandPool = pInstance->pvkResetCommandPools[pCreateInfo->pGraphicsQueue->index];
  VkQueue graphicsQueue = pInstance->pVkQueues[pCreateInfo->pGraphicsQueue->index];

  pTexture->mipLevels = pCreateInfo->mipmapMode == ADK_TEXTURE_MIPMAP_MODE_NONE ? 1 : (uint32_t)adkLog2(adkMax(pCreateInfo->extent.x, pCreateInfo->extent.y));

  //Create staging buffer and image.
  VkBuffer stagingBuffer;
  AdkDeviceMemory stagingDeviceMemory;
  pTexture->colorType = pCreateInfo->colorType;
  pTexture->imageType = adkTexture_DimensionsToVkImageType(pCreateInfo->dimensions);
  pTexture->vkFormat = adkTexture_ColorTypeToVkFormat(pCreateInfo->colorType);
  pTexture->extent = pCreateInfo->extent;
  size_t colorTypeSize = adkTexture_SizeOfColorType(pCreateInfo->colorType);
  size_t size = pCreateInfo->extent.x * pCreateInfo->extent.y * pCreateInfo->extent.z * colorTypeSize;

  _CreateBuffer(pInstance, physicalDevice, pTexture->device, pInstance->pVkAllocationCallbacks, pInstance->pAllocationCallbacks, VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, size, &stagingBuffer, &stagingDeviceMemory);
  _CreateImage(pInstance, physicalDevice, pTexture->device, pInstance->pVkAllocationCallbacks, pInstance->pAllocationCallbacks, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | adkTexture_ColorTypeToImageUsage(pCreateInfo->colorType), VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, pCreateInfo->extent.x, pCreateInfo->extent.y, pCreateInfo->extent.z, pTexture->mipLevels, pTexture->imageType, pTexture->vkFormat, &pTexture->image, &pTexture->deviceMemory);

  //Copy image to staging buffer and ensure r is interpreted as least significant byte.
  char *pMappedStagingMemory = (char*)stagingDeviceMemory.pMapped;
  //Copy image and flip on y-axis.
  if (pCreateInfo->pColors != nullptr)
  {
    uint64_t rowSize = pCreateInfo->extent.x * colorTypeSize;
    uint64_t layerSize = pCreateInfo->extent.x * pCreateInfo->extent.y * colorTypeSize;
    
    for (uint64_t aisle = 0; aisle < size; aisle += layerSize)
    {
      for (uint64_t rowA = 0, rowB = (pCreateInfo->extent.y - 1) * rowSize; rowA < layerSize; rowA += rowSize, rowB -= rowSize)
        memcpy((char*)pMappedStagingMemory + aisle + rowA, (char*)pCreateInfo->pColors + aisle + rowB, (size_t)rowSize);
    }
  }

  //if (pCreateInfo->pColors != nullptr)
  //  memcpy(pMappedStagingMemory, pCreateInfo->pColors, size);

  //for (size_t i = 0; i < pCreateInfo->width * pCreateInfo->height; ++i)
  //  pMappedStagingMemory[i] = _ForceLeftLSB(pMappedStagingMemory[i]);

  adkMemoryPool_FlushDeviceMemory(&stagingDeviceMemory, pTexture->device);

  //Begin command buffer.
  VkCommandBuffer commandBuffer;
  _CreateAndBeginCommandBuffer(pTexture->device, pTexture->commandPool, &commandBuffer);

  //Change image layout to transfer dst optimal.
  VkImageMemoryBarrier imageMemoryBarrier;
  imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  imageMemoryBarrier.pNext = nullptr;
  imageMemoryBarrier.srcAccessMask = 0;
  imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
  imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  imageMemoryBarrier.image = pTexture->image;
  imageMemoryBarrier.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
  imageMemoryBarrier.subresourceRange.baseMipLevel = 0;
  imageMemoryBarrier.subresourceRange.levelCount = 1;
  imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
  imageMemoryBarrier.subresourceRange.layerCount = 1;
  vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

  //Copy image from staging buffer to image.
  VkBufferImageCopy bufferImageCopy;
  bufferImageCopy.bufferOffset = 0;
  bufferImageCopy.bufferRowLength = 0;
  bufferImageCopy.bufferImageHeight = 0;
  bufferImageCopy.imageSubresource.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
  bufferImageCopy.imageSubresource.mipLevel = 0;
  bufferImageCopy.imageSubresource.baseArrayLayer = 0;
  bufferImageCopy.imageSubresource.layerCount = 1;
  bufferImageCopy.imageOffset.x = 0;
  bufferImageCopy.imageOffset.y = 0;
  bufferImageCopy.imageOffset.z = 0;
  bufferImageCopy.imageExtent.width = pCreateInfo->extent.x;
  bufferImageCopy.imageExtent.height = pCreateInfo->extent.y;
  bufferImageCopy.imageExtent.depth = pCreateInfo->extent.z;
  vkCmdCopyBufferToImage(commandBuffer, stagingBuffer, pTexture->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageCopy);

  adkVector3<int32_t> previousMipExtent = adkVector3<int32_t>::create(pCreateInfo->extent);
  adkVector3<int32_t> mipExtent;

  for (uint32_t i = 1; i < pTexture->mipLevels; ++i)
  {
    mipExtent.x = adkMax(previousMipExtent.x >> 1, 1);
    mipExtent.y = adkMax(previousMipExtent.y >> 1, 1);
    mipExtent.z = adkMax(previousMipExtent.z >> 1, 1);

    //Change previous mip layout to transfer src optimal for copying to current mip level.
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.pNext = nullptr;
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.image = pTexture->image;
    imageMemoryBarrier.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
    imageMemoryBarrier.subresourceRange.baseMipLevel = i - 1;
    imageMemoryBarrier.subresourceRange.levelCount = 1;
    imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    imageMemoryBarrier.subresourceRange.layerCount = 1;
    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

    //Change current mip layout to transfer dst optimal for being copied to from previous mip level.
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.pNext = nullptr;
    imageMemoryBarrier.srcAccessMask = 0;
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.image = pTexture->image;
    imageMemoryBarrier.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
    imageMemoryBarrier.subresourceRange.baseMipLevel = i;
    imageMemoryBarrier.subresourceRange.levelCount = 1;
    imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    imageMemoryBarrier.subresourceRange.layerCount = 1;
    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

    VkImageBlit imageBlit;
    imageBlit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageBlit.srcSubresource.mipLevel = i - 1;
    imageBlit.srcSubresource.baseArrayLayer = 0;
    imageBlit.srcSubresource.layerCount = 1;
    imageBlit.srcOffsets[0].x = 0;
    imageBlit.srcOffsets[0].y = 0;
    imageBlit.srcOffsets[0].z = 0;
    imageBlit.srcOffsets[1].x = previousMipExtent.x;
    imageBlit.srcOffsets[1].y = previousMipExtent.y;
    imageBlit.srcOffsets[1].z = previousMipExtent.z;
    imageBlit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imageBlit.dstSubresource.mipLevel = i;
    imageBlit.dstSubresource.baseArrayLayer = 0;
    imageBlit.dstSubresource.layerCount = 1;
    imageBlit.dstOffsets[0].x = 0;
    imageBlit.dstOffsets[0].y = 0;
    imageBlit.dstOffsets[0].z = 0;
    imageBlit.dstOffsets[1].x = mipExtent.x;
    imageBlit.dstOffsets[1].y = mipExtent.y;
    imageBlit.dstOffsets[1].z = mipExtent.z;

    vkCmdBlitImage(commandBuffer, pTexture->image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, pTexture->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imageBlit, VK_FILTER_LINEAR);

    //Change previous mip layout back to transfer dst optimal.
    imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imageMemoryBarrier.pNext = nullptr;
    imageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    imageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    imageMemoryBarrier.image = pTexture->image;
    imageMemoryBarrier.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
    imageMemoryBarrier.subresourceRange.baseMipLevel = i - 1;
    imageMemoryBarrier.subresourceRange.levelCount = 1;
    imageMemoryBarrier.subresourceRange.baseArrayLayer = 0;
    imageMemoryBarrier.subresourceRange.layerCount = 1;
    vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);

    previousMipExtent = mipExtent;
  }

  if (pCreateInfo->flags & ADK_TEXTURE_CREATE_MUTABLE_BIT)
  {
    //Create command buffer for copying texture back to staging buffer.
    _CreateAndBeginCommandBuffer(pTexture->device, pTexture->commandPool, &pTexture->copyFromDeviceCommandBuffer);

    VkBufferImageCopy copyFromImageBufferImageCopy;
    copyFromImageBufferImageCopy.bufferOffset = 0;
    copyFromImageBufferImageCopy.bufferRowLength = 0;
    copyFromImageBufferImageCopy.bufferImageHeight = 0;
    copyFromImageBufferImageCopy.imageSubresource.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
    copyFromImageBufferImageCopy.imageSubresource.mipLevel = 0;
    copyFromImageBufferImageCopy.imageSubresource.baseArrayLayer = 0;
    copyFromImageBufferImageCopy.imageSubresource.layerCount = 1;
    copyFromImageBufferImageCopy.imageOffset.x = 0;
    copyFromImageBufferImageCopy.imageOffset.y = 0;
    copyFromImageBufferImageCopy.imageOffset.z = 0;
    copyFromImageBufferImageCopy.imageExtent.width = pCreateInfo->extent.x;
    copyFromImageBufferImageCopy.imageExtent.height = pCreateInfo->extent.y;
    copyFromImageBufferImageCopy.imageExtent.depth = pCreateInfo->extent.z;

    vkCmdCopyImageToBuffer(pTexture->copyFromDeviceCommandBuffer, pTexture->image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, stagingBuffer, 1, &copyFromImageBufferImageCopy);

    ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(pTexture->copyFromDeviceCommandBuffer));
  }

  //Submit and wait for command buffer.
  _CreateFence(pTexture->device, pInstance->pVkAllocationCallbacks, &pTexture->transitionFence, true);

  ADK_ASSERT_VK_RESULT(vkEndCommandBuffer(commandBuffer));

  _CopyImage(pTexture->device, graphicsQueue, stagingDeviceMemory, commandBuffer, pTexture->transitionFence, pCreateInfo->extent, pCreateInfo->colorType, (pCreateInfo->flags & ADK_TEXTURE_CREATE_FLIP_Y_BIT) != 0, pCreateInfo->pColors);

  if (pCreateInfo->flags & ADK_TEXTURE_CREATE_MUTABLE_BIT)
  {
    pTexture->stagingDeviceMemory = stagingDeviceMemory;
    pTexture->stagingBuffer = stagingBuffer;
    pTexture->copyCommandBuffer = commandBuffer;
  }
  else
  {
    //Clean up.
    vkDestroyBuffer(pTexture->device, stagingBuffer, pInstance->pVkAllocationCallbacks);
    vkFreeCommandBuffers(pTexture->device, pTexture->commandPool, 1, &commandBuffer);
    adkMemoryPool_FreeDeviceMemory(pInstance->pMemoryPool, &stagingDeviceMemory);
  }

  VkSamplerCreateInfo samplerCreateInfo;
  samplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
  samplerCreateInfo.pNext = nullptr;
  samplerCreateInfo.flags = 0;
  samplerCreateInfo.magFilter = _adkToVkFilter(pCreateInfo->magFilter);
  samplerCreateInfo.minFilter = _adkToVkFilter(pCreateInfo->minFilter);
  samplerCreateInfo.mipmapMode = _adkToVkMipmapMode(pCreateInfo->mipmapMode);
  samplerCreateInfo.addressModeU = _adkToVkAddressMode(pCreateInfo->addressModeU);
  samplerCreateInfo.addressModeV = _adkToVkAddressMode(pCreateInfo->addressModeV);
  samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
  samplerCreateInfo.mipLodBias = 0.f;
  samplerCreateInfo.anisotropyEnable = _adkToVkAnisotropyMode(pCreateInfo->anisotropyMode);
  samplerCreateInfo.maxAnisotropy = 16.f;
  samplerCreateInfo.compareEnable = false;
  samplerCreateInfo.compareOp = VK_COMPARE_OP_NEVER;
  samplerCreateInfo.minLod = 0.f;
  samplerCreateInfo.maxLod = (float)pTexture->mipLevels;
  samplerCreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
  samplerCreateInfo.unnormalizedCoordinates = false;
  ADK_ASSERT_VK_RESULT(vkCreateSampler(pTexture->device, &samplerCreateInfo, pInstance->pVkAllocationCallbacks, &pTexture->sampler));

  VkImageViewCreateInfo imageViewCreateInfo;
  imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  imageViewCreateInfo.pNext = nullptr;
  imageViewCreateInfo.flags = 0;
  imageViewCreateInfo.image = pTexture->image;
  imageViewCreateInfo.viewType = adkTexture_DimensionsToVkImageViewType(pCreateInfo->dimensions);
  imageViewCreateInfo.format = pTexture->vkFormat;
  imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
  imageViewCreateInfo.subresourceRange.aspectMask = adkTexture_ColorTypeToImageAspect(pCreateInfo->colorType);
  imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
  imageViewCreateInfo.subresourceRange.levelCount = pTexture->mipLevels;
  imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
  imageViewCreateInfo.subresourceRange.layerCount = 1;
  ADK_ASSERT_VK_RESULT(vkCreateImageView(pTexture->device, &imageViewCreateInfo, pInstance->pVkAllocationCallbacks, &pTexture->imageView));

  _CreateSemaphore(pTexture->device, graphicsQueue, pInstance->pVkAllocationCallbacks, &pTexture->semaphore);

  VkCommandBufferAllocateInfo commandBufferAllocateInfo;
  commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
  commandBufferAllocateInfo.pNext = nullptr;
  commandBufferAllocateInfo.commandPool = pTexture->resetCommandPool;
  commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
  commandBufferAllocateInfo.commandBufferCount = 1;
  vkAllocateCommandBuffers(pTexture->device, &commandBufferAllocateInfo, &pTexture->transitionCommandBuffer);

  pTexture->accessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
  pTexture->layout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  pTexture->stageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;

  if (adkTexture_ColorTypeIsDepth(pTexture->colorType))
    adkTexture_ChangeLayout(pTexture, VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT);
  else
    adkTexture_ChangeLayout(pTexture, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);

  pTexture->createFlags = pCreateInfo->flags;

  (*ppTexture) = pTexture;
}

AdkBool32 adkTexture_Read(AdkInstance *pInstance, const AdkTextureReadInfo *pReadInfo, AdkTexture **ppTexture)
{
  AdkBool32 success = false;
  void *pColors = nullptr;
  adkVector2<uint32_t> extent;
  AdkTextureCreateInfo textureCreateInfo;

  if (!adkTextureReader_Read(pReadInfo->pFilename, &extent, &pColors))
    goto epilogue;

  textureCreateInfo.flags = ADK_TEXTURE_CREATE_FLIP_Y_BIT;
  textureCreateInfo.dimensions = ADK_TEXTURE_DIMENSIONS_2D;
  textureCreateInfo.extent = adkVector3<uint32_t>::create(extent.x, extent.y, 1);
  textureCreateInfo.pColors = pColors;
  textureCreateInfo.colorType = ADK_COLOR_TYPE_R8G8B8A8;
  textureCreateInfo.minFilter = pReadInfo->minFilter;
  textureCreateInfo.magFilter = pReadInfo->magFilter;
  textureCreateInfo.mipmapMode = pReadInfo->mipmapMode;
  textureCreateInfo.addressModeU = pReadInfo->addressModeU;
  textureCreateInfo.addressModeV = pReadInfo->addressModeV;
  textureCreateInfo.anisotropyMode = pReadInfo->anisotropyMode;
  textureCreateInfo.pGraphicsQueue = pReadInfo->pGraphicsQueue;
  adkTexture_Create(pInstance, &textureCreateInfo, ppTexture);

  success = true;

epilogue:
  adkFree(pColors);

  return success;
}

void adkTexture_Destroy(AdkInstance *pInstance, AdkTexture **ppTexture)
{
  AdkTexture *pTexture = (*ppTexture);

  VkPipelineStageFlags waitStageMask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

  VkQueue queue = pInstance->pVkQueues[pTexture->pGraphicsQueue->index];

  _WaitForFence(pTexture->device, pTexture->transitionFence, true);

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &pTexture->semaphore;
  submitInfo.pWaitDstStageMask = &waitStageMask;
  submitInfo.commandBufferCount = 0;
  submitInfo.pCommandBuffers = nullptr;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;
  vkQueueSubmit(queue, 1, &submitInfo, pTexture->transitionFence);

  _WaitForFence(pTexture->device, pTexture->transitionFence, true);

  if (pTexture->createFlags & ADK_TEXTURE_CREATE_MUTABLE_BIT)
  {
    vkFreeCommandBuffers(pTexture->device, pTexture->commandPool, 1, &pTexture->copyFromDeviceCommandBuffer);
    vkFreeCommandBuffers(pTexture->device, pTexture->commandPool, 1, &pTexture->copyCommandBuffer);
    vkDestroyBuffer(pTexture->device, pTexture->stagingBuffer, pInstance->pVkAllocationCallbacks);
    adkMemoryPool_FreeDeviceMemory(pInstance->pMemoryPool, &pTexture->stagingDeviceMemory);
  }

  vkDestroyFence(pTexture->device, pTexture->transitionFence, pInstance->pVkAllocationCallbacks);
  vkDestroySemaphore(pTexture->device, pTexture->semaphore, pInstance->pVkAllocationCallbacks);
  vkFreeCommandBuffers(pTexture->device, pTexture->resetCommandPool, 1, &pTexture->transitionCommandBuffer);
  vkDestroyImageView(pTexture->device, pTexture->imageView, pInstance->pVkAllocationCallbacks);
  vkDestroyImage(pTexture->device, pTexture->image, pInstance->pVkAllocationCallbacks);
  vkDestroySampler(pTexture->device, pTexture->sampler, pInstance->pVkAllocationCallbacks);
  adkMemoryPool_FreeDeviceMemory(pInstance->pMemoryPool, &pTexture->deviceMemory);

  adkFreeCall(*ppTexture, pInstance->pAllocationCallbacks);
}

void adkTexture_Map(AdkTexture *pTexture, void **ppData)
{
  VkQueue graphicsQueue = pTexture->pInstance->pVkQueues[pTexture->pGraphicsQueue->index];

  VkSubmitInfo submitInfo;
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.pNext = nullptr;
  submitInfo.waitSemaphoreCount = 0;
  submitInfo.pWaitSemaphores = nullptr;
  submitInfo.pWaitDstStageMask = nullptr;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &pTexture->copyFromDeviceCommandBuffer;
  submitInfo.signalSemaphoreCount = 0;
  submitInfo.pSignalSemaphores = nullptr;

  adkTexture_ChangeLayout(pTexture, VK_ACCESS_TRANSFER_READ_BIT, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT);

  _WaitForFence(pTexture->device, pTexture->transitionFence, true);

  ADK_ASSERT_VK_RESULT(vkQueueSubmit(graphicsQueue, 1, &submitInfo, pTexture->transitionFence));

  _WaitForFence(pTexture->device, pTexture->transitionFence, false);

  *ppData = pTexture->stagingDeviceMemory.pMapped;
}

void adkTexture_Unmap(AdkTexture *pTexture, void **ppData)
{
  VkQueue queue = pTexture->pInstance->pVkQueues[pTexture->pGraphicsQueue->index];

  adkMemoryPool_FlushDeviceMemory(&pTexture->stagingDeviceMemory, pTexture->device);

  adkTexture_ChangeLayout(pTexture, VK_ACCESS_TRANSFER_WRITE_BIT, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT);

  _CopyImage(pTexture->device, queue, pTexture->stagingDeviceMemory, pTexture->copyCommandBuffer, pTexture->transitionFence, pTexture->extent, pTexture->colorType, (pTexture->createFlags & ADK_TEXTURE_CREATE_FLIP_Y_BIT) != 0, *ppData);
  *ppData = nullptr;
}

adkVector3<uint32_t> adkTexture_GetExtent(AdkTexture *pTexture)
{
  return pTexture->extent;
}