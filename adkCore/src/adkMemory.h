#ifndef adkMemory_h__
#define adkMemory_h__

#include <cstdlib>
#include <cstring>
#include <stdint.h>

#define ADKAPI_CALL __stdcall
#define ADKARRAYSIZE(a) (sizeof(a) / sizeof(*a))
#define adkAlloc(size) malloc(size)
#define adkAllocType(type, count, flags) (type*)(flags == adkAF_Zero ? calloc(count, sizeof(type)) : malloc((count) * sizeof(type)))
#define adkReallocType(block, count) (decltype(block))realloc(block, (count) * sizeof(decltype(block)))
#define adkMemZero(block, count) (decltype(block))memset(block, 0, (count) * sizeof(decltype(block)))

#define adkAllocCall(size, pAllocationCallbacks, allocationScope) (pAllocationCallbacks == nullptr ? adkAlloc(size) : (pAllocationCallbacks)->pAllocationFunction(((pAllocationCallbacks)->pUserData), (size), (allocationScope)))
#define adkAllocTypeCall(type, count, flags, pAllocationCallbacks, allocationScope) (type*)((pAllocationCallbacks) == nullptr ? (adkAllocType(type, count, flags)) : (flags == adkAF_Zero ? memset((pAllocationCallbacks)->pAllocationFunction((pAllocationCallbacks)->pUserData, (count) * sizeof(type), allocationScope), 0, (count) * sizeof(type)) : (pAllocationCallbacks)->pAllocationFunction((pAllocationCallbacks)->pUserData, (count) * sizeof(type), allocationScope)))
#define adkReallocTypeCall(block, count, pAllocationCallbacks) (decltype(block))((pAllocationCallbacks) == nullptr ? (adkReallocType((pAllocationCallbacks)->pUserData, block, count)) : (pAllocationCallbacks)->pReallocationFunction((pAllocationCallbacks)->pUserData, block, (count) * sizeof(decltype(block))))
#define adkFree(block) _adkFree((void*&)block)
#define adkFreeCall(block, pAllocationCallbacks) (pAllocationCallbacks) == nullptr ? (adkFree((void*&)block)) : _adkFreeCall((void*&)block, (pAllocationCallbacks))
#define adkMemDup(block, size) (decltype(block))_adkMemDupCall(block, size, nullptr, ADK_ALLOCATION_SCOPE_DYNAMIC)
#define adkMemDupType(block, count) (decltype(block))_adkMemDupCall(block, (count) * sizeof(decltype(*block)), nullptr, ADK_ALLOCATION_SCOPE_DYNAMIC)
#define adkMemDupCall(block, size, pAllocator, allocationScope) (decltype(block))_adkMemDupCall(block, size, pAllocator, allocationScope)
#define adkMemDupTypeCall(block, count, pAllocator, allocationScope) (decltype(block))_adkMemDupCall(block, (count) * sizeof(decltype(*block)), pAllocator, allocationScope)

#ifdef _WIN32
#define adkMemoryBarrier() MemoryBarrier()
#endif

#define ADK_ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

typedef enum AdkAllocFlag
{
  adkAF_None,
  adkAF_Zero,
} adkAllocFlag;

typedef enum AdkAllocationScope
{
  ADK_ALLOCATION_SCOPE_COMMAND = 0,
  ADK_ALLOCATION_SCOPE_DYNAMIC = 1,
} AdkAllocationScope;

typedef void* (ADKAPI_CALL adkAllocationFunction)(void *pUserData, size_t size, AdkAllocationScope scope);
typedef void* (ADKAPI_CALL adkReallocationFunction)(void *pUserData, void *pOriginal, size_t size, AdkAllocationScope scope);
typedef void (ADKAPI_CALL adkFreeFunction)(void *pUserData, void *pMemory);

typedef struct adkAllocationCallbacks
{
  void *pUserData;
  adkAllocationFunction *pAllocationFunction;
  adkReallocationFunction *pReallocationFunction;
  adkFreeFunction *pFreeFunction;
} adkAllocationCallbacks;

inline void* adkMalloc(size_t size) { return malloc(size); }
inline void* adkCalloc(size_t count, size_t size) { return calloc(count, size); }
inline void* adkMemset(void* destination, int value, size_t size) { return memset(destination, value, size); }
inline void* adkMemCpy(void* destination, void* source, size_t size) { return memcpy(destination, source, size); }
inline void _adkFree(void*& block) { free(block); block = nullptr; }
inline void* adkRealloc(void* block, size_t size) { return realloc(block, size); }
inline void _adkFreeCall(void*& block, const adkAllocationCallbacks *pAllocator) { pAllocator == nullptr ? _adkFree(block) : pAllocator->pFreeFunction(pAllocator->pUserData, block), block = nullptr; }
inline void* _adkMemDupCall(void* block, size_t size, const adkAllocationCallbacks *pAllocator, AdkAllocationScope allocationScope) { void *pDst = (pAllocator == nullptr ? adkAlloc(size) : pAllocator->pAllocationFunction(pAllocator->pUserData, size, allocationScope)); memcpy(pDst, block, size); return pDst; }

#endif //adkMemory_h__