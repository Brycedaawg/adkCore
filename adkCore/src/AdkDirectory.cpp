#include "AdkDirectory.h"
#include "adkString.h"
#include "adkLog.h"
#include "adkAssert.h"

#ifdef _WIN32
#include <windows.h>
DWORD ADK_DIRECTORY_LISTEN_FLAGS = FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_ATTRIBUTES;
const char WILDCARD_AMMENDMENT[] = "/*";
#endif

enum
{
  ADK_DIRECTORY_MAX_DIRECTORY_NAME_SIZE = 1024,
};

typedef struct AdkDirectory
{
#ifdef _WIN32
  char *pWildcardDirectory;
  HANDLE iterateHandle;
  HANDLE listenHandle;
  WIN32_FIND_DATAA fileData;
  char listenBuffer[256 * (sizeof(FILE_NOTIFY_INFORMATION) + MAX_PATH * sizeof(WCHAR))];
  OVERLAPPED pollingOverlap;
  AdkBool32 hasFoundFirst;
#endif
} AdkDirectory;

#ifdef _WIN32
static inline AdkBool32 _First(AdkDirectory *pDirectory)
{
  if (pDirectory->iterateHandle != INVALID_HANDLE_VALUE)
    FindClose(pDirectory->iterateHandle);

  pDirectory->iterateHandle = FindFirstFileA(pDirectory->pWildcardDirectory, &pDirectory->fileData);

  if (pDirectory->iterateHandle == INVALID_HANDLE_VALUE)
    return ADK_FALSE;

  //Skip the first two items, '.', and '..'.
  for (uint32_t i = 0; i < 2; ++i)
  {
    if (!FindNextFileA(pDirectory->iterateHandle, &pDirectory->fileData))
    {
      FindClose(pDirectory->iterateHandle);
      pDirectory->iterateHandle = INVALID_HANDLE_VALUE;
      return ADK_FALSE;
    }
  }

  pDirectory->hasFoundFirst = ADK_TRUE;
  return ADK_TRUE;
}
#endif

AdkBool32 adkDirectory_Open(AdkDirectoryOpenInfo *pOpenInfo, const adkAllocationCallbacks *pAllocator, AdkDirectory **ppDirectory)
{
#ifdef _WIN32
  if (!adkDirectory_Exists(pOpenInfo->pName))
    return ADK_FALSE;

  size_t allocationSize = sizeof(AdkDirectory);

  if (pOpenInfo->flags & ADK_DIRECTORY_OPEN_ITERATE_BIT)
    allocationSize += ADK_DIRECTORY_MAX_DIRECTORY_NAME_SIZE + sizeof(WILDCARD_AMMENDMENT);

  AdkDirectory *pDirectory = (AdkDirectory*)adkAllocCall(allocationSize, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  memset(pDirectory, 0, allocationSize);

  pDirectory->iterateHandle = INVALID_HANDLE_VALUE;

  if (pOpenInfo->flags & ADK_DIRECTORY_OPEN_ITERATE_BIT)
  {
    pDirectory->pWildcardDirectory = (char*)pDirectory + sizeof(AdkDirectory);
    size_t nameSize = adkStrcpy(pDirectory->pWildcardDirectory, ADK_DIRECTORY_MAX_DIRECTORY_NAME_SIZE, pOpenInfo->pName);
    memcpy(pDirectory->pWildcardDirectory + nameSize - 1, WILDCARD_AMMENDMENT, sizeof(WILDCARD_AMMENDMENT));
  }

  if (pOpenInfo->flags & ADK_DIRECTORY_OPEN_LISTEN_BIT)
  {
    pDirectory->listenHandle = CreateFileA(pOpenInfo->pName, GENERIC_READ | FILE_LIST_DIRECTORY, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, nullptr, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, nullptr);
    if (pDirectory->listenHandle == INVALID_HANDLE_VALUE)
    {
      adkFreeCall(pDirectory, pAllocator);
      return ADK_FALSE;
    }
    
    pDirectory->pollingOverlap.hEvent = CreateEvent(nullptr, TRUE, FALSE, nullptr);
    ReadDirectoryChangesW(pDirectory->listenHandle, pDirectory->listenBuffer, sizeof(pDirectory->listenBuffer), TRUE, ADK_DIRECTORY_LISTEN_FLAGS, nullptr, &pDirectory->pollingOverlap, nullptr);
  }
#endif

  (*ppDirectory) = pDirectory;
  return ADK_TRUE;
}

void adkDirectory_Close(const adkAllocationCallbacks *pAllocator, AdkDirectory **ppDirectory)
{
  AdkDirectory *pDirectory = (*ppDirectory);

#ifdef _WIN32
  if (pDirectory->iterateHandle != INVALID_HANDLE_VALUE)
    FindClose(pDirectory->iterateHandle);

  if (pDirectory->listenHandle != INVALID_HANDLE_VALUE)
    FindCloseChangeNotification(pDirectory->listenHandle);
#endif

  adkFreeCall(pDirectory, pAllocator);

  (*ppDirectory) = nullptr;
}

static inline AdkBool32 _Next(AdkDirectory *pDirectory)
{
  if (!pDirectory->hasFoundFirst)
    return _First(pDirectory);

  if (pDirectory->iterateHandle == INVALID_HANDLE_VALUE)
    return ADK_FALSE;

  if (FindNextFileA(pDirectory->iterateHandle, &pDirectory->fileData))
    return ADK_TRUE;

  FindClose(pDirectory->iterateHandle);
  pDirectory->iterateHandle = INVALID_HANDLE_VALUE;
  return ADK_FALSE;
}

AdkBool32 adkDirectory_Next(AdkDirectory *pDirectory, AdkDirectoryItemProperties *pProperties)
{
#ifdef _WIN32
  if (!_Next(pDirectory))
    return ADK_FALSE;

  pProperties->flags = 0;

  if (pDirectory->fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
    pProperties->flags |= ADK_DIRECTORY_ITEM_SUBDIRECTORY_BIT;

  adkStrcpy(pProperties->item, ADK_FILE_ITERATOR_MAX_FILENAME_SIZE, pDirectory->fileData.cFileName);

  return ADK_TRUE;
#endif
}

AdkBool32 adkDirectory_Descend(AdkDirectory *pDirectory)
{
#ifdef _WIN32
  if ((pDirectory->fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
    return ADK_FALSE;

  size_t parentNameSize = adkStrLen(pDirectory->pWildcardDirectory) + 1 - sizeof(WILDCARD_AMMENDMENT);
  adkSprintf(pDirectory->pWildcardDirectory + parentNameSize, ADK_DIRECTORY_MAX_DIRECTORY_NAME_SIZE - parentNameSize, "/%s%s", pDirectory->fileData.cFileName, WILDCARD_AMMENDMENT);

  pDirectory->hasFoundFirst = ADK_FALSE;

  return ADK_TRUE;
#endif
}

AdkBool32 adkDirectory_Ascend(AdkDirectory *pDirectory)
{
#ifdef _WIN32
  size_t parentNameSize = adkStrLen(pDirectory->pWildcardDirectory) + 1 - sizeof(WILDCARD_AMMENDMENT);
  size_t grandParentNameSize = parentNameSize - 1;

  while ((grandParentNameSize != 0) & (pDirectory->pWildcardDirectory[grandParentNameSize] != '/') & (pDirectory->pWildcardDirectory[grandParentNameSize] != '\\'))
    --grandParentNameSize;

  if (grandParentNameSize == 0)
    return ADK_FALSE;

  char parentNameExclusive[ADK_DIRECTORY_MAX_DIRECTORY_NAME_SIZE];
  size_t parentNameExclusiveSize = parentNameSize - grandParentNameSize - 1;
  memcpy(parentNameExclusive, pDirectory->pWildcardDirectory + grandParentNameSize + 1, parentNameExclusiveSize);
  parentNameExclusive[parentNameExclusiveSize] = 0;

  memcpy(pDirectory->pWildcardDirectory + grandParentNameSize, WILDCARD_AMMENDMENT, sizeof(WILDCARD_AMMENDMENT));
  pDirectory->pWildcardDirectory[grandParentNameSize + sizeof(WILDCARD_AMMENDMENT)] = 0;
  pDirectory->hasFoundFirst = ADK_FALSE;
  adkDirectory_Seek(pDirectory, parentNameExclusive);
  //_NextFile(pDirectory);

  return ADK_TRUE;
#endif
}

AdkBool32 adkDirectory_Seek(AdkDirectory *pDirectory, const char *pName)
{
  if (pDirectory->hasFoundFirst || _First(pDirectory))
  {
    do
    {
      if (strcmp(pName, pDirectory->fileData.cFileName) == 0)
        return ADK_TRUE;
    } while (_Next(pDirectory));
  }

  return ADK_FALSE;
}

void adkDirectory_Restart(AdkDirectory *pDirectory)
{
#ifdef _WIN32
  pDirectory->hasFoundFirst = ADK_FALSE;
#endif
}

AdkDirectoryPopResult adkDirectory_PopChanges(AdkDirectory *pDirectory, uint32_t *pPropertyCount, AdkDirectoryChangeProperties *pProperties)
{
#ifdef _WIN32
  DWORD bytesReturned;
  BOOL success = GetOverlappedResult(pDirectory->listenHandle, &pDirectory->pollingOverlap, &bytesReturned, FALSE);

  if (success)
  {
    FILE_NOTIFY_INFORMATION *pInformation;
    uint32_t propertyCount = 0;
    uint32_t bytesIterated = 0;

    if (pProperties == nullptr) //Get Property count.
    {
      do
      {
        pInformation = (FILE_NOTIFY_INFORMATION*)((char*)pDirectory->listenBuffer + bytesIterated);
        ++propertyCount;
        bytesIterated += pInformation->NextEntryOffset;
      } while (pInformation->NextEntryOffset != 0);

      (*pPropertyCount) = propertyCount;
    }
    else
    {
      do
      {
        pInformation = (FILE_NOTIFY_INFORMATION*)((char*)pDirectory->listenBuffer + bytesIterated);

        switch (pInformation->Action)
        {
        case FILE_ACTION_ADDED:
          pProperties[propertyCount].type = ADK_DIRECTORY_CHANGE_ADD;
          break;

        case FILE_ACTION_REMOVED:
          pProperties[propertyCount].type = ADK_DIRECTORY_CHANGE_REMOVE;
          break;

        case FILE_ACTION_RENAMED_OLD_NAME:
          //Fall through.
        case FILE_ACTION_RENAMED_NEW_NAME:
          pProperties[propertyCount].type = ADK_DIRECTORY_CHANGE_RENAME;
          break;

        case FILE_ACTION_MODIFIED:
          pProperties[propertyCount].type = ADK_DIRECTORY_CHANGE_MODIFY;
          break;
        }

        wcstombs_s(nullptr, pProperties[propertyCount].item, pInformation->FileName, ADK_FILE_ITERATOR_MAX_FILENAME_SIZE);

        ++propertyCount;
        bytesIterated += pInformation->NextEntryOffset;
      } while ((pInformation->NextEntryOffset != 0) & (propertyCount < (*pPropertyCount)));
    }

    ReadDirectoryChangesW(pDirectory->listenHandle, pDirectory->listenBuffer, sizeof(pDirectory->listenBuffer), TRUE, ADK_DIRECTORY_LISTEN_FLAGS, nullptr, &pDirectory->pollingOverlap, nullptr);
    return ADK_DIRECTORY_POP_SUCCESS;
  }
  else
  {
    if (!ReadDirectoryChangesW(pDirectory->listenHandle, pDirectory->listenBuffer, sizeof(pDirectory->listenBuffer), TRUE, ADK_DIRECTORY_LISTEN_FLAGS, nullptr, &pDirectory->pollingOverlap, nullptr))
    {
      if (GetLastError() == ERROR_NOTIFY_ENUM_DIR)
        return ADK_DIRECTORY_POP_OVERFLOW;
    }
  }

  return ADK_DIRECTORY_POP_NO_CHANGES;
#endif
}

AdkBool32 adkDirectory_Exists(const char *pName)
{
#ifdef _WIN32
  DWORD attributes = GetFileAttributesA(pName);

  return (attributes & FILE_ATTRIBUTE_DIRECTORY) ? ADK_TRUE : ADK_FALSE;
#endif
}