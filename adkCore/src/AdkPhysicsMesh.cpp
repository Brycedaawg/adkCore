#include "AdkPhysicsMesh.h"
#include "adkAssert.h"
#include "AdkPhysicsUtil.h"
#include "AdkOctree.h"
#include "AdkBinaryReader.h"
#include "AdkBinaryWriter.h"

typedef struct AdkPhysicsMesh
{
  const adkAllocationCallbacks *pAllocator;
  bool enabled;
  uint32_t maxTriangleCount;
  uint32_t triangleCount;
  adkVector3<float> *pTriangles;
  AdkOctree *pOctree;
} AdkPhysicsMesh;

void adkPhysicsMesh_Create(const AdkPhysicsMeshCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkPhysicsMesh **ppPhysicsMesh)
{
  AdkPhysicsMesh *pPhysicsMesh = adkAllocTypeCall(AdkPhysicsMesh, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pPhysicsMesh->pAllocator = pAllocator;
  pPhysicsMesh->maxTriangleCount = pCreateInfo->maxTriangleCount;

  pPhysicsMesh->pTriangles = adkAllocTypeCall(adkVector3<float>, 3 * pCreateInfo->maxTriangleCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  adkOctree_Create(pCreateInfo->maxTriangleCount, pAllocator, &pPhysicsMesh->pOctree);

  *ppPhysicsMesh = pPhysicsMesh;
}

void adkPhysicsMesh_Destroy(AdkPhysicsMesh **ppPhysicsMesh)
{
  if (*ppPhysicsMesh == nullptr)
    return;

  AdkPhysicsMesh *pPhysicsMesh = *ppPhysicsMesh;

  adkOctree_Destroy(&pPhysicsMesh->pOctree);

  adkFreeCall(pPhysicsMesh->pTriangles, pPhysicsMesh->pAllocator);

  adkFreeCall(*ppPhysicsMesh, pPhysicsMesh->pAllocator);
}

bool adkPhysicsMesh_Serialize(AdkPhysicsMesh *pPhysicsMesh, AdkBinaryWriter *pBinaryWriter)
{
  adkBinaryWriter_Write(pBinaryWriter, pPhysicsMesh->enabled);
  adkBinaryWriter_Write(pBinaryWriter, pPhysicsMesh->maxTriangleCount);
  adkBinaryWriter_Write(pBinaryWriter, pPhysicsMesh->triangleCount);
  adkBinaryWriter_Write(pBinaryWriter, pPhysicsMesh->enabled);

  adkBinaryWriter_Write(pBinaryWriter, 3 * pPhysicsMesh->maxTriangleCount * sizeof(*pPhysicsMesh->pTriangles), pPhysicsMesh->pTriangles);

  adkOctree_Serialize(pPhysicsMesh->pOctree, pBinaryWriter);

  return adkBinaryWriter_GetSuccess(pBinaryWriter);
}

bool adkPhysicsMesh_Deserialize(AdkBinaryReader *pBinaryReader, const adkAllocationCallbacks *pAllocator, AdkPhysicsMesh **ppPhysicsMesh)
{
  bool success = false;
  AdkPhysicsMesh *pPhysicsMesh = adkAllocTypeCall(AdkPhysicsMesh, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pPhysicsMesh->pAllocator = pAllocator;

  adkBinaryReader_Read(pBinaryReader, &pPhysicsMesh->enabled);
  adkBinaryReader_Read(pBinaryReader, &pPhysicsMesh->maxTriangleCount);
  adkBinaryReader_Read(pBinaryReader, &pPhysicsMesh->triangleCount);
  adkBinaryReader_Read(pBinaryReader, &pPhysicsMesh->enabled);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  pPhysicsMesh->pTriangles = adkAllocTypeCall(adkVector3<float>, 3 * pPhysicsMesh->maxTriangleCount, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  adkBinaryReader_Read(pBinaryReader, 3 * pPhysicsMesh->maxTriangleCount * sizeof(*pPhysicsMesh->pTriangles), pPhysicsMesh->pTriangles);

  if (!adkBinaryReader_GetSuccess(pBinaryReader))
    goto epilogue;

  if (!adkOctree_Deserialize(pBinaryReader, pAllocator, &pPhysicsMesh->pOctree))
    goto epilogue;

  *ppPhysicsMesh = pPhysicsMesh;
  success = true;

epilogue:
  if (!success)
  {
    adkOctree_Destroy(&pPhysicsMesh->pOctree);
    adkFreeCall(pPhysicsMesh->pTriangles, pAllocator);
    adkFreeCall(pPhysicsMesh, pAllocator);
  }
  return success;
}

void adkPhysicsMesh_Copy(AdkPhysicsMesh *pSource, AdkPhysicsMesh **ppCopy)
{
  AdkPhysicsMesh *pCopy = nullptr;
  AdkPhysicsMeshCreateInfo createInfo;
  createInfo.maxTriangleCount = pSource->maxTriangleCount;
  adkPhysicsMesh_Create(&createInfo, pSource->pAllocator, &pCopy);
  adkPhysicsMesh_SetTriangles(pCopy, pSource->triangleCount, pSource->pTriangles);
  adkPhysicsMesh_SetEnabled(pCopy, pSource->enabled);
  *ppCopy = pCopy;
}

void adkPhysicsMesh_SetTriangles(AdkPhysicsMesh *pPhysicsMesh, uint32_t triangleCount, adkVector3<float> *pTriangles)
{
  ADK_ASSERT(triangleCount <= pPhysicsMesh->maxTriangleCount, "triangleCount must be less than or equal to pCreateInfo->maxTriangleCount passed into adkPhysicsMesh_Create when this adkPhysicsMesh was created.");

  pPhysicsMesh->triangleCount = triangleCount;
  adkMemCpy(pPhysicsMesh->pTriangles, pTriangles, triangleCount * 3 * sizeof(*pTriangles));

  adkOctree_Link(pPhysicsMesh->pOctree, triangleCount, (AdkOctreeTriangle*)pTriangles);
}

bool adkPhysicsMesh_SweepSphere(AdkPhysicsMesh *pPhysicsMesh, const adkVector3<float> &sphereStart, const adkVector3<float> &sphereEnd, float sphereRadius, AdkSphereSweepCollision *pCollision)
{
  return adkOctree_SweepSphere(pPhysicsMesh->pOctree, sphereStart, sphereEnd, sphereRadius, pCollision);
}

void adkPhysicsMesh_SetEnabled(AdkPhysicsMesh *pPhysicsmesh, bool enabled)
{
  pPhysicsmesh->enabled = enabled;
}