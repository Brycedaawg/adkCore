#ifndef adkMesh_h__
#define adkMesh_h__

#include "adkMath.h"
#include "adkMemory.h"
#include "adkRenderPipeline.h"

struct AdkMesh;
struct AdkMeshRenderer;
struct AdkInstance;
struct adkWindow;
struct AdkMaterial;
struct AdkQueue;
struct AdkShader;
struct AdkBinaryReader;
struct AdkBinaryWriter;

typedef enum AdkMeshAttributeType
{
  ADK_MESH_ATTRIBUTE_TYPE_POSITION,
  ADK_MESH_ATTRIBUTE_TYPE_NORMAL,
  ADK_MESH_ATTRIBUTE_TYPE_UV,
  ADK_MESH_ATTRIBUTE_TYPE_SKIN_WEIGHT,
  ADK_MESH_ATTRIBUTE_TYPE_SKIN_BONE,
  ADK_MESH_ATTRIBUTE_TYPE_MODEL,
} AdkMeshAttributeType;

typedef struct AdkMeshAttributeInfo
{
  AdkMeshAttributeType type;
  uint32_t location;
} AdkMeshAttributeInfo;

typedef enum AdkMeshUniformType
{
  ADK_MESH_UNIFORM_TYPE_VIEW,
  ADK_MESH_UNIFORM_TYPE_PROJECTION,
  ADK_MESH_UNIFORM_TYPE_BONES,
  ADK_MESH_UNIFORM_TYPE_BONES_PER_VERTEX,
  ADK_MESH_UNIFORM_TYPE_LIGHT_COLOR,
  ADK_MESH_UNIFORM_TYPE_LIGHT_DIRECTION,
} AdkMeshUniformType;

typedef struct AdkMeshUniformInfo
{
  AdkMeshUniformType type;
  uint32_t binding;
} AdkMeshUniformInfo;

typedef enum AdkMeshSpecializationType
{
  ADK_MESH_SPECIALIZATION_TYPE_BONE_COUNT,
} AdkMeshSpecializationType;

typedef struct AdkMeshSpecializationInfo
{
  AdkMeshSpecializationType type;
  uint32_t constantId;
} AdkMeshSpecializationInfo;

typedef struct AdkMeshShaderInfo
{
  AdkShader *pShader;
  uint32_t attributeCount;
  AdkMeshAttributeInfo *pAttributes;
  uint32_t uniformCount;
  AdkMeshUniformInfo *pUniforms;
  uint32_t specializationCount;
  AdkMeshSpecializationInfo *pSpecializations;
  uint32_t additionalAttributeCount;
  AdkAttributeInfo *pAdditionalAttributes;
  uint32_t additionalBufferCount;
  AdkBufferUniformInfo *pAdditionalBuffers;
  uint32_t additionalSamplerCount;
  AdkSamplerUniformInfo *pAdditionalSamplers;
} AdkMeshShaderInfo;

void adkMesh_Create(AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkMesh **ppMesh);
void adkMesh_Destroy(AdkMesh **ppMesh);
void adkMesh_Copy(AdkMesh *pSource, AdkMesh **ppCopy);
void adkMesh_SetPositions(AdkMesh *pMesh, uint32_t count, adkVector3<float> *pPositions);
void adkMesh_SetNormals(AdkMesh *pMesh, uint32_t count, adkVector3<float> *pNormals);
void adkMesh_SetUvs(AdkMesh *pMesh, uint32_t count, adkVector2<float> *pUvs);
void adkMesh_SetSkin(AdkMesh *pMesh, uint32_t boneCount, adkMatrix4x4<float> *pBindPoses, uint32_t bonesPerSkinVertex, uint32_t skinVertexCount, float *pSkinVertexWeights, uint32_t *pSkinVertexBones);
void adkMesh_SetIndices(AdkMesh *pMesh, uint32_t count, uint32_t *pIndices);
void adkMesh_SetShader(AdkMesh *pMesh, const AdkMeshShaderInfo *pMaterialInfo);
uint32_t adkMesh_GetPositionCount(AdkMesh *pMesh);
bool adkMesh_Serialize(AdkMesh *pMesh, AdkBinaryWriter *pBinaryWriter);
bool adkMesh_Deserialize(AdkBinaryReader *pBinaryReader, AdkInstance *pInstance, AdkQueue *pGraphicsQueue, AdkQueue *pTransferQueue, const adkAllocationCallbacks *pAllocator, AdkMesh **ppMesh);

void adkMeshRenderer_Create(AdkMesh *pMesh, adkWindow *pWindow, uint32_t modelCount, AdkMeshRenderer **ppMeshRenderer);
void adkMeshRenderer_Destroy(AdkMeshRenderer **ppMeshRenderer);
AdkBool32 adkMeshRenderer_HasSkin(AdkMeshRenderer *pMeshRenderer);
void adkMeshRenderer_MapModels(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppModels);
void adkMeshRenderer_UnmapModels(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppModels);
void adkMeshRenderer_MapBones(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppBones);
void adkMeshRenderer_UnmapBones(AdkMeshRenderer *pMeshRenderer, adkMatrix4x4<float> **ppBones);
void adkMeshRenderer_SetDirectionalLight(AdkMeshRenderer *pMeshRenderer, uint32_t color, const adkVector3<float> *pDirection);
void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount, const adkMatrix4x4<float> *pModels);
void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection, uint32_t modelCount);
void adkMeshRenderer_Render(AdkMeshRenderer *pMeshRenderer, const adkMatrix4x4<float> *pView, const adkMatrix4x4<float> *pProjection);

#endif // adkMesh_h__
