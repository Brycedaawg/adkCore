#include "adkDensity3dNormal.h"
#include "adkMemory.h"
#include "adkBuffer.h"
#include "adkShader.h"
#include "AdkComputePipeline.h"
#include "adkIsoSurfaceNormal.comp.spv.h"

typedef struct AdkDensity3dNormal
{
  AdkInstance *pInstance;
  AdkTexture *pTexture;
  AdkShader *pShader;
  AdkComputePipeline *pComputePipeline;
  const adkAllocationCallbacks *pAllocator;
} AdkPerlin3d;

void adkDensity3dNormal_Create(AdkInstance *pInstance, AdkTexture *pDensity, AdkQueue *pGraphicsQueue, AdkQueue *pComputeQueue, const adkAllocationCallbacks *pAllocator, AdkDensity3dNormal **ppDensity3dNormal)
{
  AdkPerlin3d *pDensity3dNormal = adkAllocTypeCall(AdkPerlin3d, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pDensity3dNormal->pInstance = pInstance;
  pDensity3dNormal->pAllocator = pAllocator;

  AdkTextureCreateInfo textureCreateInfo;
  textureCreateInfo.flags = 0;
  textureCreateInfo.dimensions = ADK_TEXTURE_DIMENSIONS_3D;
  textureCreateInfo.extent = adkTexture_GetExtent(pDensity);
  textureCreateInfo.pColors = nullptr;
  textureCreateInfo.colorType = ADK_COLOR_TYPE_R32G32B32A32_SFLOAT;
  textureCreateInfo.minFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.magFilter = ADK_TEXTURE_FILTER_NEAREST;
  textureCreateInfo.mipmapMode = ADK_TEXTURE_MIPMAP_MODE_NONE;
  textureCreateInfo.addressModeU = ADK_TEXTURE_ADDRESS_MODE_REPEAT;
  textureCreateInfo.addressModeV = ADK_TEXTURE_ADDRESS_MODE_REPEAT;
  textureCreateInfo.anisotropyMode = ADK_TEXTURE_ANISOTROPY_MODE_DISABLED;
  textureCreateInfo.pGraphicsQueue = pGraphicsQueue;

  adkTexture_Create(pInstance, &textureCreateInfo, &pDensity3dNormal->pTexture);

  AdkShaderCreateInfo shaderCreateInfo;
  shaderCreateInfo.flags = 0;
  shaderCreateInfo.pVertexShader = nullptr;
  shaderCreateInfo.pGeometryShader = nullptr;
  shaderCreateInfo.pFragmentShader = nullptr;
  shaderCreateInfo.computeShaderSize = sizeof(ADKISOSURFACENORMAL_COMP_SHADER_CODE);
  shaderCreateInfo.pComputeShader = (const char*)ADKISOSURFACENORMAL_COMP_SHADER_CODE;

  adkShader_Create(pInstance, &shaderCreateInfo, &pDensity3dNormal->pShader);

  AdkComputeStorageImage computeStorageImages[2];
  
  computeStorageImages[0].binding = 0;
  computeStorageImages[0].pTexture = pDensity;

  computeStorageImages[1].binding = 1;
  computeStorageImages[1].pTexture = pDensity3dNormal->pTexture;

  AdkComputePipelineCreateInfo computePipelineCreateInfo;;
  computePipelineCreateInfo.storageBufferCount = 0;
  computePipelineCreateInfo.pStorageBuffers = nullptr;
  computePipelineCreateInfo.storageImageCount = ADK_ARRAY_SIZE(computeStorageImages);
  computePipelineCreateInfo.pStorageImages = computeStorageImages;
  computePipelineCreateInfo.pShader = pDensity3dNormal->pShader;
  computePipelineCreateInfo.pComputeQueue = pComputeQueue;

  adkComputePipeline_Create(pInstance, &computePipelineCreateInfo, pAllocator, &pDensity3dNormal->pComputePipeline);

  *ppDensity3dNormal = pDensity3dNormal;
}

void adkDensity3dNormal_Destroy(AdkDensity3dNormal **ppDensity3dNormal)
{
  AdkPerlin3d *pDensity3dNormal = *ppDensity3dNormal;

  adkComputePipeline_Destroy(&pDensity3dNormal->pComputePipeline);
  adkShader_Destroy(&pDensity3dNormal->pShader);
  adkTexture_Destroy(pDensity3dNormal->pInstance, &pDensity3dNormal->pTexture);

  adkFreeCall(*ppDensity3dNormal, pDensity3dNormal->pAllocator);
}

void adkDensity3dNormal_Generate(AdkDensity3dNormal *pDensity3dNormal)
{
  adkVector3<uint32_t> extent = adkTexture_GetExtent(pDensity3dNormal->pTexture);
  adkVector3<uint32_t> groupCount = adkVector3<uint32_t>::create((extent.x + 7) / 8, (extent.y + 7) / 8, (extent.z + 15) / 16);
  adkComputePipeline_Dispatch(pDensity3dNormal->pComputePipeline, groupCount);
}

void adkDensity3dNormal_GetTexture(AdkDensity3dNormal *pDensity3dNormal, AdkTexture **ppTexture)
{
  *ppTexture = pDensity3dNormal->pTexture;
}