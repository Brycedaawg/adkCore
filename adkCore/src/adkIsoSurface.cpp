#include "adkIsoSurface.h"
#include "adkBuffer.h"
#include "adkShader.h"
#include "adkTexture.h"
#include "AdkComputePipeline.h"
#include "adkThread.h"
#include "adkIsoSurface.comp.spv.h"

static const adkVector3<uint32_t> ADK_ISO_SURFACE_WORK_GROUP_SIZE = adkVector3<uint32_t>::create(8, 8, 16);

typedef struct AdkIsoSurfaceOctet
{
  adkVector3<float> position;
  adkVector3<float> normal;
} AdkIsoSurfaceOctet;

typedef struct AdkIsoSurface
{
  AdkInstance *pInstance;
  const adkAllocationCallbacks *pAllocator;
  AdkTexture *pAtomicIndexTexture;
  AdkBuffer *pViewProjectionBuffer;
  AdkBuffer *pPositionBuffer;
  AdkBuffer *pNormalBuffer;
  AdkBuffer *pInfoBuffer;
  AdkShader *pIsoSurfaceShader;
  AdkComputePipeline *pIsoSurfaceComputePipeline;
  bool vertexCountDirty;
  uint32_t vertexCount;
} AdkIsoSurface;

typedef struct ShaderInfoLayout
{
  adkVector4<int32_t> inset;
  adkVector4<int32_t> scale;
  adkVector4<float> offset;
  float isoLevel;
} ShaderInfoLayout;

void adkIsoSurface_Create(AdkInstance *pInstance, const AdkIsoSurfaceCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkIsoSurface **ppIsoSurface)
{
  AdkIsoSurface *pIsoSurface = adkAllocTypeCall(AdkIsoSurface, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  pIsoSurface->pInstance = pInstance;
  pIsoSurface->pAllocator = pAllocator;
  pIsoSurface->vertexCountDirty = true;

  adkVector3<uint32_t> densitySize = adkTexture_GetExtent(pCreateInfo->pDensity);

  float atomicIndexValue = 0.f;

  AdkTextureCreateInfo atomicIndexTextureCreateInfo;
  atomicIndexTextureCreateInfo.flags = ADK_TEXTURE_CREATE_MUTABLE_BIT;
  atomicIndexTextureCreateInfo.dimensions = ADK_TEXTURE_DIMENSIONS_1D;
  atomicIndexTextureCreateInfo.extent = adkVector3<uint32_t>::one();
  atomicIndexTextureCreateInfo.pColors = &atomicIndexValue;
  atomicIndexTextureCreateInfo.colorType = ADK_COLOR_TYPE_R32;
  atomicIndexTextureCreateInfo.minFilter = ADK_TEXTURE_FILTER_NEAREST;
  atomicIndexTextureCreateInfo.magFilter = ADK_TEXTURE_FILTER_NEAREST;
  atomicIndexTextureCreateInfo.mipmapMode = ADK_TEXTURE_MIPMAP_MODE_NONE;
  atomicIndexTextureCreateInfo.addressModeU = ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE;
  atomicIndexTextureCreateInfo.addressModeV = ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE;
  atomicIndexTextureCreateInfo.anisotropyMode = ADK_TEXTURE_ANISOTROPY_MODE_DISABLED;
  atomicIndexTextureCreateInfo.pGraphicsQueue = pCreateInfo->pGraphicsQueue;

  adkTexture_Create(pInstance, &atomicIndexTextureCreateInfo, &pIsoSurface->pAtomicIndexTexture);

  AdkBufferCreateInfo positionBufferCreateInfo;
  positionBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  positionBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4;
  positionBufferCreateInfo.elementCount = pCreateInfo->maxVertices;
  positionBufferCreateInfo.copyCount = 0;
  positionBufferCreateInfo.pCopies = nullptr;
  positionBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;

  adkBuffer_Create(pInstance, &positionBufferCreateInfo, &pIsoSurface->pPositionBuffer);

  AdkBufferCreateInfo normalBufferCreateInfo;
  normalBufferCreateInfo.flags = 0;
  normalBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4;
  normalBufferCreateInfo.elementCount = pCreateInfo->maxVertices;
  normalBufferCreateInfo.copyCount = 0;
  normalBufferCreateInfo.pCopies = nullptr;
  normalBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;

  adkBuffer_Create(pInstance, &normalBufferCreateInfo, &pIsoSurface->pNormalBuffer);

  AdkBufferCreateInfo infoBufferCreateInfo;
  infoBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  infoBufferCreateInfo.type = ADK_BUFFER_TYPE_UINT8;
  infoBufferCreateInfo.elementCount = sizeof(ShaderInfoLayout);
  infoBufferCreateInfo.copyCount = 0;
  infoBufferCreateInfo.pCopies = nullptr;
  infoBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;

  adkBuffer_Create(pInstance, &infoBufferCreateInfo, &pIsoSurface->pInfoBuffer);

  AdkBufferCreateInfo viewProjectionBufferCreateInfo;
  viewProjectionBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  viewProjectionBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4X4;
  viewProjectionBufferCreateInfo.elementCount = 1;
  viewProjectionBufferCreateInfo.copyCount = 0;
  viewProjectionBufferCreateInfo.pCopies = nullptr;
  viewProjectionBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;

  adkBuffer_Create(pInstance, &viewProjectionBufferCreateInfo, &pIsoSurface->pViewProjectionBuffer);

  AdkShaderCreateInfo isoSurfaceShaderCreateInfo;
  isoSurfaceShaderCreateInfo.flags = 0;
  isoSurfaceShaderCreateInfo.pVertexShader = nullptr;
  isoSurfaceShaderCreateInfo.pGeometryShader = nullptr;
  isoSurfaceShaderCreateInfo.pFragmentShader = nullptr;
  isoSurfaceShaderCreateInfo.computeShaderSize = sizeof(ADKISOSURFACE_COMP_SHADER_CODE);
  isoSurfaceShaderCreateInfo.pComputeShader = (const char*)ADKISOSURFACE_COMP_SHADER_CODE;

  adkShader_Create(pInstance, &isoSurfaceShaderCreateInfo, &pIsoSurface->pIsoSurfaceShader);
  
  AdkComputeStorageImage isoSurfaceStorageImages[3];

  isoSurfaceStorageImages[0].binding = 0;
  isoSurfaceStorageImages[0].pTexture = pCreateInfo->pDensity;

  isoSurfaceStorageImages[1].binding = 1;
  isoSurfaceStorageImages[1].pTexture = pCreateInfo->pNormals;

  isoSurfaceStorageImages[2].binding = 2;
  isoSurfaceStorageImages[2].pTexture = pIsoSurface->pAtomicIndexTexture;

  AdkComputeStorageBuffer isoSurfaceStorageBuffers[3];

  isoSurfaceStorageBuffers[0].binding = 3;
  isoSurfaceStorageBuffers[0].pBuffer = pIsoSurface->pPositionBuffer;

  isoSurfaceStorageBuffers[1].binding = 4;
  isoSurfaceStorageBuffers[1].pBuffer = pIsoSurface->pNormalBuffer;

  isoSurfaceStorageBuffers[2].binding = 5;
  isoSurfaceStorageBuffers[2].pBuffer = pIsoSurface->pInfoBuffer;

  AdkComputePipelineCreateInfo isoSurfaceComputePipelineCreateInfo;
  isoSurfaceComputePipelineCreateInfo.storageBufferCount = ADK_ARRAY_SIZE(isoSurfaceStorageBuffers);
  isoSurfaceComputePipelineCreateInfo.pStorageBuffers = isoSurfaceStorageBuffers;
  isoSurfaceComputePipelineCreateInfo.storageImageCount = ADK_ARRAY_SIZE(isoSurfaceStorageImages);
  isoSurfaceComputePipelineCreateInfo.pStorageImages = isoSurfaceStorageImages;
  isoSurfaceComputePipelineCreateInfo.pShader = pIsoSurface->pIsoSurfaceShader;
  isoSurfaceComputePipelineCreateInfo.pComputeQueue = pCreateInfo->pComputeQueue;

  adkComputePipeline_Create(pInstance, &isoSurfaceComputePipelineCreateInfo, pAllocator, &pIsoSurface->pIsoSurfaceComputePipeline);
  
  *ppIsoSurface = pIsoSurface;
}

void adkIsoSurface_Destroy(AdkIsoSurface **ppIsoSurface)
{
  AdkIsoSurface *pIsoSurface = *ppIsoSurface;

  adkComputePipeline_Destroy(&pIsoSurface->pIsoSurfaceComputePipeline);
  adkBuffer_Destroy(pIsoSurface->pInstance, &pIsoSurface->pViewProjectionBuffer);
  adkTexture_Destroy(pIsoSurface->pInstance, &pIsoSurface->pAtomicIndexTexture);
  adkBuffer_Destroy(pIsoSurface->pInstance, &pIsoSurface->pInfoBuffer);
  adkBuffer_Destroy(pIsoSurface->pInstance, &pIsoSurface->pNormalBuffer);
  adkBuffer_Destroy(pIsoSurface->pInstance, &pIsoSurface->pPositionBuffer);
  adkShader_Destroy(&pIsoSurface->pIsoSurfaceShader);

  adkFreeCall(*ppIsoSurface, pIsoSurface->pAllocator);
}

static uint32_t _GetVertexCount(AdkIsoSurface *pIsoSurface)
{
  if (pIsoSurface->vertexCountDirty)
  {
    //Retrieve the vertex count from the atomic index and reset the atomic index.
    uint32_t *pMappedAtomicIndex = nullptr;
    adkTexture_Map(pIsoSurface->pAtomicIndexTexture, (void**)&pMappedAtomicIndex);
    pIsoSurface->vertexCount = *pMappedAtomicIndex;
    adkTexture_Unmap(pIsoSurface->pAtomicIndexTexture, (void**)&pMappedAtomicIndex);
    pIsoSurface->vertexCountDirty = false;
  }

  return pIsoSurface->vertexCount;
}

void adkIsoSurface_Reset(AdkIsoSurface *pIsoSurface)
{
  //Retrieve the vertex count from the atomic index and reset the atomic index.
  uint32_t *pMappedAtomicIndex = nullptr;
  adkTexture_Map(pIsoSurface->pAtomicIndexTexture, (void**)&pMappedAtomicIndex);
  *pMappedAtomicIndex = 0;
  adkTexture_Unmap(pIsoSurface->pAtomicIndexTexture, (void**)&pMappedAtomicIndex);
  pIsoSurface->vertexCountDirty = false;
}

void adkIsoSurface_Generate(AdkIsoSurface *pIsoSurface, float isoLevel, const adkVector3<uint32_t> &inset, const adkVector3<uint32_t> &scale, const adkVector3<float> &offset /*= adkVector3<float>::zero()*/)
{
  ShaderInfoLayout layout;
  layout.inset = adkVector4<int32_t>::create(inset.x, inset.y, inset.z, 0);
  layout.scale = adkVector4<int32_t>::create(scale.x, scale.y, scale.z, 0);
  layout.offset = adkVector4<float>::create(offset.x, offset.y, offset.z, 0.f);
  layout.isoLevel = isoLevel;

  AdkBufferCopy bufferCopy;
  bufferCopy.offset = 0;
  bufferCopy.size = sizeof(layout);
  bufferCopy.pData = &layout;

  adkBuffer_Set(pIsoSurface->pInstance, pIsoSurface->pInfoBuffer, 1, &bufferCopy);

  adkVector3<uint32_t> groupCount = (scale + ADK_ISO_SURFACE_WORK_GROUP_SIZE - (uint32_t)1) / ADK_ISO_SURFACE_WORK_GROUP_SIZE;
  adkComputePipeline_Dispatch(pIsoSurface->pIsoSurfaceComputePipeline, groupCount);

  pIsoSurface->vertexCountDirty = true;
}

uint32_t adkIsoSurface_GetVertexCount(AdkIsoSurface *pIsoSurface)
{
  uint32_t vertexCount = _GetVertexCount(pIsoSurface);

  return vertexCount;
}

void adkIsoSurface_GetPositions(AdkIsoSurface *pIsoSurface, AdkBuffer **ppPositions)
{
  *ppPositions = pIsoSurface->pPositionBuffer;
}

void adkIsoSurface_GetNormals(AdkIsoSurface *pIsoSurface, AdkBuffer **ppNormals)
{
  *ppNormals = pIsoSurface->pNormalBuffer;
}

AdkBool32 adkIsoSurface_Wait(AdkIsoSurface *pIsoSurface, uint64_t timeout)
{
  return adkComputePipeline_Wait(pIsoSurface->pIsoSurfaceComputePipeline, timeout);
}