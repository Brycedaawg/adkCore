#ifndef adkTexture_h__
#define adkTexture_h__

#include "adkMath.h"
#include "adkTextureReader.h"
#include "AdkTime.h"

struct AdkTexture;
struct AdkInstance;
struct AdkQueue;

typedef enum AdkTextureCreateFlagBits
{
  ADK_TEXTURE_CREATE_MUTABLE_BIT = 0x00000001,
  ADK_TEXTURE_CREATE_FLIP_Y_BIT = 0x00000002,
} AdkTextureCreateFlagBits;
typedef AdkFlags AdkTextureCreateFlags;

typedef enum AdkTextureDimensions
{
  ADK_TEXTURE_DIMENSIONS_1D,
  ADK_TEXTURE_DIMENSIONS_2D,
  ADK_TEXTURE_DIMENSIONS_3D,
} AdkTextureDimensions;

typedef enum AdkColorType
{
  ADK_COLOR_TYPE_R8,
  ADK_COLOR_TYPE_R8G8,
  ADK_COLOR_TYPE_R8G8B8,
  ADK_COLOR_TYPE_R8G8B8A8,
  ADK_COLOR_TYPE_R32,
  ADK_COLOR_TYPE_R32_SFLOAT,
  ADK_COLOR_TYPE_R32G32_SFLOAT,
  ADK_COLOR_TYPE_R32G32B32_SFLOAT,
  ADK_COLOR_TYPE_R32G32B32A32_SFLOAT,
  ADK_COLOR_TYPE_D32_SFLOAT,
} AdkColorType;

typedef enum AdkTextureFilter
{
  ADK_TEXTURE_FILTER_NEAREST,
  ADK_TEXTURE_FILTER_LINEAR,
} AdkTextureFilter;

typedef enum AdkTextureMipmapMode
{
  ADK_TEXTURE_MIPMAP_MODE_NONE,
  ADK_TEXTURE_MIPMAP_MODE_NEAREST,
  ADK_TEXTURE_MIPMAP_MODE_LINEAR,
} AdkTextureMipmapMode;

typedef enum AdkTextureAddressMode
{
  ADK_TEXTURE_ADDRESS_MODE_REPEAT,
  ADK_TEXTURE_ADDRESS_MODE_MIRROR_REPEAT,
  ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_EDGE,
  ADK_TEXTURE_ADDRESS_MODE_CLAMP_TO_BORDER,
  ADK_TEXTURE_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE,
} AdkTextureAddressMode;

typedef enum AdkTextureAnisotropyMode
{
  ADK_TEXTURE_ANISOTROPY_MODE_DISABLED,
  ADK_TEXTURE_ANISOTROPY_MODE_ENABLED,
} AdkTextureAnisotropyMode;

typedef struct AdkTextureCreateInfo
{
  AdkTextureCreateFlags flags;
  AdkTextureDimensions dimensions;
  adkVector3<uint32_t> extent;
  void *pColors;
  AdkColorType colorType;
  AdkTextureFilter minFilter;
  AdkTextureFilter magFilter;
  AdkTextureMipmapMode mipmapMode;
  AdkTextureAddressMode addressModeU;
  AdkTextureAddressMode addressModeV;
  AdkTextureAnisotropyMode anisotropyMode;
  AdkQueue *pGraphicsQueue;
} AdkTextureCreateInfo;

typedef struct AdkTextureReadInfo
{
  const char *pFilename;
  AdkTextureFilter minFilter;
  AdkTextureFilter magFilter;
  AdkTextureMipmapMode mipmapMode;
  AdkTextureAddressMode addressModeU;
  AdkTextureAddressMode addressModeV;
  AdkTextureAnisotropyMode anisotropyMode;
  AdkQueue *pGraphicsQueue;
} AdkTextureReadInfo;

void adkTexture_Create(AdkInstance *pInstance, const AdkTextureCreateInfo *pCreateInfo, AdkTexture **ppTexture);
AdkBool32 adkTexture_Read(AdkInstance *pInstance, const AdkTextureReadInfo *pReadInfo, AdkTexture **ppTexture);
void adkTexture_Destroy(AdkInstance *pInstance, AdkTexture **ppTexture);
void adkTexture_Map(AdkTexture *pTexture, void **ppData);
void adkTexture_Unmap(AdkTexture *pTexture, void **ppData);
adkVector3<uint32_t> adkTexture_GetExtent(AdkTexture *pTexture);

#endif // adkTexture_h__
