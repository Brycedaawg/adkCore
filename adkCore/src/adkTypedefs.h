#ifndef adkTypedefs_h__
#define adkTypedefs_h__

#include "stdint.h"

enum AdkResult
{
  ADK_SUCCESS,
  ADK_ERROR,
};

typedef uint32_t AdkFlags;
typedef uint32_t AdkBool32;

#define ADK_TRUE 1
#define ADK_FALSE 0

#endif //adkTypedefs_h__