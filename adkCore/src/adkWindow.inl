#ifndef adkWindow_inl__
#define adkWindow_inl__

#include "adkWindow.h"
#include "adkInstance.inl"

struct AdkTexture;

typedef enum AdkWindowParameter
{
  ADK_WINDOW_PARAMETER_MAX_INPUT_EVENTS = 256,
} AdkWindowParameter;

typedef struct adkWindow
{
  AdkInstance *pInstance;
  uint32_t deviceIndex;
  AdkWindowCallbacks callbacks;
  VkSurfaceKHR vkSurface;
  VkSurfaceCapabilitiesKHR *pVkPhysicalDeviceSurfaceCapabilities;
  uint32_t vkSurfaceFormatCount;
  AdkRange<uint32_t> *pVkSurfaceFormatDeviceRanges;
  VkSurfaceFormatKHR *pVkSurfaceFormats;
  uint32_t vkSurfacePresentModeCount;
  AdkRange<uint32_t> *pVkSurfacePresentModeDeviceRanges;
  VkPresentModeKHR *pVkSurfacePresentModes;
  VkSwapchainKHR vkSwapchain;
  VkFormat swapchainFormat;
  uint32_t swapchainImageCount;
  VkImage *pVkSwapchainImages;
  VkImageView *pVkSwapchainImageViews;
  VkCommandBuffer *pSwapchainCommandBuffers;
  AdkTexture *pDepthTexture;
  uint32_t queueFamilyIndex;
  uint32_t queueIndex;
  adkRect<uint32_t> rect;
  adkVector2<uint32_t> extent;
  uint32_t swapchainImageIndex;
  VkSemaphore swapchainSemaphore;
  VkCommandBuffer *pPrePresentCommandBuffers;
  VkCommandBuffer *pPostPresentCommandBuffers;
  VkFence fence;
  uint32_t inputEventCount;
  AdkInputEvent inputEvents[ADK_WINDOW_PARAMETER_MAX_INPUT_EVENTS];
  AdkInputState inputState;
  AdkKey systemToAdkKey[ADK_KEY_COUNT];

#ifdef _WIN32
  HWND windowHandle;
  HINSTANCE moduleHandle;
  char rawInputData[0xFF];
#endif
} adkWindow;

#endif // adkWindow_inl__
