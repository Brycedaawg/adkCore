#include "adkMutex.h"
#include "adkMemory.h"

#ifdef _WIN32
#include <windows.h>
#endif
#include "adkAssert.h"

typedef struct AdkMutex
{
#ifdef _WIN32
  HANDLE handle;
#endif
} AdkMutex;

void adkMutex_Create(const adkAllocationCallbacks *pAllocator, AdkMutex **ppMutex)
{
  AdkMutex *pMutex = adkAllocTypeCall(AdkMutex, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

#ifdef _WIN32
  pMutex->handle = CreateMutex(nullptr, FALSE, nullptr);
#endif

  (*ppMutex) = pMutex;
}

void adkMutex_Destroy(const adkAllocationCallbacks *pAllocator, AdkMutex **ppMutex)
{
  AdkMutex *pMutex = (*ppMutex);

#ifdef _WIN32
  CloseHandle(pMutex->handle);
#endif

  adkFreeCall(pMutex, pAllocator);

  (*ppMutex) = nullptr;
}

void adkMutex_Lock(AdkMutex *pMutex)
{
#ifdef _WIN32
  ADK_ASSERT(WaitForSingleObject(pMutex->handle, INFINITE) == WAIT_OBJECT_0, "Could not lock mutex\n");
#endif
}

void adkMutex_Release(AdkMutex *pMutex)
{
#ifdef _WIN32
  ADK_ASSERT(ReleaseMutex(pMutex->handle) == 0, "Could not release mutex\n");
#endif
}