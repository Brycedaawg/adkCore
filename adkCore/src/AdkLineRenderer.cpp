#include "AdkLineRenderer.h"
#include "adkBuffer.h"
#include "adkRenderPipeline.h"
#include "adkShader.h"
#include "adkLineRenderer.vert.spv.h"
#include "adkLineRenderer.frag.spv.h"

typedef struct AdkLineRenderer
{
  AdkInstance *pInstance;
  const adkAllocationCallbacks *pAllocator;
  uint32_t pointCount;
  uint32_t maxPoints;
  adkVector3<float> *pLines;
  adkVector4<float> *pColors;
  AdkBuffer *pLineBuffer;
  AdkBuffer *pColorBuffer;
  AdkBuffer *pViewProjectionBuffer;
  AdkShader *pShader;
  AdkRenderPipeline *pRenderPipeline;
} AdkLineRenderer;

void adkLineRenderer_Create(AdkInstance *pInstance, const AdkLineRendererCreateInfo *pCreateInfo, const adkAllocationCallbacks *pAllocator, AdkLineRenderer **ppLineRenderer)
{
  AdkLineRenderer *pLineRenderer = adkAllocTypeCall(AdkLineRenderer, 1, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pLineRenderer->pInstance = pInstance;
  pLineRenderer->pAllocator = pAllocator;
  pLineRenderer->maxPoints = pCreateInfo->maxLines * 2;

  pLineRenderer->pColors = adkAllocTypeCall(adkVector4<float>, pLineRenderer->maxPoints, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);
  pLineRenderer->pLines = adkAllocTypeCall(adkVector3<float>, pLineRenderer->maxPoints, adkAF_None, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  AdkBufferCreateInfo lineBufferCreateInfo;
  lineBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  lineBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_3;
  lineBufferCreateInfo.elementCount = pLineRenderer->maxPoints;
  lineBufferCreateInfo.copyCount = 0;
  lineBufferCreateInfo.pCopies = nullptr;
  lineBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;
  adkBuffer_Create(pInstance, &lineBufferCreateInfo, &pLineRenderer->pLineBuffer);

  AdkBufferCreateInfo colorBufferCreateInfo;
  colorBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  colorBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4;
  colorBufferCreateInfo.elementCount = pLineRenderer->maxPoints;
  colorBufferCreateInfo.copyCount = 0;
  colorBufferCreateInfo.pCopies = nullptr;
  colorBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;
  adkBuffer_Create(pInstance, &colorBufferCreateInfo, &pLineRenderer->pColorBuffer);

  AdkBufferCreateInfo viewProjectionBufferCreateInfo;
  viewProjectionBufferCreateInfo.flags = ADK_BUFFER_CREATE_MUTABLE_BIT;
  viewProjectionBufferCreateInfo.type = ADK_BUFFER_TYPE_FLOAT32_4X4;
  viewProjectionBufferCreateInfo.elementCount = 1;
  viewProjectionBufferCreateInfo.copyCount = 0;
  viewProjectionBufferCreateInfo.pCopies = nullptr;
  viewProjectionBufferCreateInfo.pTransferQueue = pCreateInfo->pTransferQueue;
  adkBuffer_Create(pInstance, &viewProjectionBufferCreateInfo, &pLineRenderer->pViewProjectionBuffer);

  AdkShaderCreateInfo shaderCreateInfo;
  shaderCreateInfo.flags = 0;
  shaderCreateInfo.vertexShaderSize = sizeof(ADKLINERENDERER_VERT_SHADER_CODE);
  shaderCreateInfo.pVertexShader = (const char*)ADKLINERENDERER_VERT_SHADER_CODE;
  shaderCreateInfo.pGeometryShader = nullptr;
  shaderCreateInfo.fragmentShaderSize = sizeof(ADKLINERENDERER_FRAG_SHADER_CODE);
  shaderCreateInfo.pFragmentShader = (const char*)ADKLINERENDERER_FRAG_SHADER_CODE;
  shaderCreateInfo.pComputeShader = nullptr;
  adkShader_Create(pInstance, &shaderCreateInfo, &pLineRenderer->pShader);

  adkVector2<uint32_t> viewPortSize = adkWindow_GetContentSize(pCreateInfo->pWindow);

  AdkBufferUniformInfo uniformBufferInfos[1];

  //View projection uniform buffer
  uniformBufferInfos[0].binding = 0;
  uniformBufferInfos[0].pBuffer = pLineRenderer->pViewProjectionBuffer;

  AdkAttributeInfo lineBufferAttributeInfos[2];

  //Line attribute buffer
  lineBufferAttributeInfos[0].location = 0;
  lineBufferAttributeInfos[0].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
  lineBufferAttributeInfos[0].elementStride = 1;
  lineBufferAttributeInfos[0].pBuffer = pLineRenderer->pLineBuffer;

  //Color attribute buffer
  lineBufferAttributeInfos[1].location = 1;
  lineBufferAttributeInfos[1].inputRate = ADK_ATTRIBUTE_INPUT_RATE_VERTEX;
  lineBufferAttributeInfos[1].elementStride = 1;
  lineBufferAttributeInfos[1].pBuffer = pLineRenderer->pColorBuffer;

  AdkRenderPipelineCreateInfo renderPipelineCreateInfo;
  renderPipelineCreateInfo.flags = ADK_RENDER_PIPELINE_DEPTH_READ_BIT | ADK_RENDER_PIPELINE_DEPTH_WRITE_BIT;
  renderPipelineCreateInfo.primitiveTopology = ADK_PRIMITIVE_TOPOLOGY_LINE_LIST;
  renderPipelineCreateInfo.polygonMode = ADK_POLYGON_MODE_LINE;
  renderPipelineCreateInfo.blending.enable = false;
  renderPipelineCreateInfo.attributeCount = ADK_ARRAY_SIZE(lineBufferAttributeInfos);
  renderPipelineCreateInfo.pAttributes = lineBufferAttributeInfos;
  renderPipelineCreateInfo.bufferUniformCount = ADKARRAYSIZE(uniformBufferInfos);
  renderPipelineCreateInfo.pBufferUniforms = uniformBufferInfos;
  renderPipelineCreateInfo.samplerUniformCount = 0;
  renderPipelineCreateInfo.pSamplerUniforms = nullptr;
  renderPipelineCreateInfo.specializationCount = 0;
  renderPipelineCreateInfo.pSpecializations = nullptr;
  renderPipelineCreateInfo.pIndices = nullptr;
  renderPipelineCreateInfo.pShader = pLineRenderer->pShader;
  renderPipelineCreateInfo.colorTargetType = ADK_RENDER_TARGET_TYPE_WINDOW;
  renderPipelineCreateInfo.colorTarget.pWindow = pCreateInfo->pWindow;
  renderPipelineCreateInfo.depthTargetType = ADK_RENDER_TARGET_TYPE_WINDOW;
  renderPipelineCreateInfo.depthTarget.pWindow = pCreateInfo->pWindow;
  renderPipelineCreateInfo.viewport = adkRect<uint32_t>::create(0, 0, viewPortSize.x, viewPortSize.y);
  renderPipelineCreateInfo.pGraphicsQueue = pCreateInfo->pGraphicsQueue;
  adkRenderPipeline_Create(pInstance, &renderPipelineCreateInfo, &pLineRenderer->pRenderPipeline);

  *ppLineRenderer = pLineRenderer;
}

void adkLineRenderer_Destroy(AdkLineRenderer **ppLineRenderer)
{
  AdkLineRenderer *pLineRenderer = *ppLineRenderer;

  adkFreeCall(pLineRenderer->pColors, pLineRenderer->pAllocator);
  adkFreeCall(pLineRenderer->pLines, pLineRenderer->pAllocator);
  adkRenderPipeline_Destroy(pLineRenderer->pInstance, &pLineRenderer->pRenderPipeline);
  adkShader_Destroy(&pLineRenderer->pShader);
  adkBuffer_Destroy(pLineRenderer->pInstance, &pLineRenderer->pViewProjectionBuffer);
  adkBuffer_Destroy(pLineRenderer->pInstance, &pLineRenderer->pColorBuffer);
  adkBuffer_Destroy(pLineRenderer->pInstance, &pLineRenderer->pLineBuffer);

  adkFreeCall(*ppLineRenderer, pLineRenderer->pAllocator);
}

void adkLineRenderer_Clear(AdkLineRenderer *pLineRenderer)
{
  pLineRenderer->pointCount = 0;
}

static adkVector4<float> _ColorUint32ToColorF32_4(uint32_t color)
{
  adkVector4<float> output;
  output.x = (float)(color >> 24 & 0xFF) / 255.f;
  output.y = (float)(color >> 16 & 0xFF) / 255.f;
  output.z = (float)(color >> 8 & 0xFF) / 255.f;
  output.w = (float)(color >> 0 & 0xFF) / 255.f;
  return output;
}

void adkLineRenderer_Add(AdkLineRenderer *pLineRenderer, const adkVector3<float> &pointA, uint32_t colorA, const adkVector3<float> &pointB, uint32_t colorB)
{
  if (pLineRenderer->pointCount + 2 <= pLineRenderer->maxPoints)
  {
    pLineRenderer->pLines[pLineRenderer->pointCount + 0] = pointA;
    pLineRenderer->pLines[pLineRenderer->pointCount + 1] = pointB;

    pLineRenderer->pColors[pLineRenderer->pointCount + 0] = _ColorUint32ToColorF32_4(colorA);
    pLineRenderer->pColors[pLineRenderer->pointCount + 1] = _ColorUint32ToColorF32_4(colorB);

    pLineRenderer->pointCount += 2;
  }
}

void adkLineRenderer_Render(AdkLineRenderer *pLineRenderer, const adkMatrix4x4<float> &viewProjection)
{ 
  //Upload colors
  adkVector4<float> *pMappedColors = nullptr;
  adkBuffer_Map(pLineRenderer->pInstance, pLineRenderer->pColorBuffer, (void**)&pMappedColors);
  memcpy(pMappedColors, pLineRenderer->pColors, pLineRenderer->pointCount * sizeof(*pMappedColors));
  adkBuffer_Unmap(pLineRenderer->pInstance, pLineRenderer->pColorBuffer, (void**)&pMappedColors);

  //Upload points
  adkVector3<float> *pMappedLines = nullptr;
  adkBuffer_Map(pLineRenderer->pInstance, pLineRenderer->pLineBuffer, (void**)&pMappedLines);
  memcpy(pMappedLines, pLineRenderer->pLines, pLineRenderer->pointCount * sizeof(*pMappedLines));
  adkBuffer_Unmap(pLineRenderer->pInstance, pLineRenderer->pLineBuffer, (void**)&pMappedLines);

  //Upload view projection
  adkMatrix4x4<float> *pMappedViewProjection = nullptr;
  adkBuffer_Map(pLineRenderer->pInstance, pLineRenderer->pViewProjectionBuffer, (void**)&pMappedViewProjection);
  *pMappedViewProjection = viewProjection;
  adkBuffer_Unmap(pLineRenderer->pInstance, pLineRenderer->pViewProjectionBuffer, (void**)&pMappedViewProjection);

  adkRenderPipeline_Render(pLineRenderer->pInstance, pLineRenderer->pRenderPipeline, pLineRenderer->pointCount, 1);
}