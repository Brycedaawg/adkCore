#ifndef adkShader_h__
#define adkShader_h__

#include "adkTypedefs.h"

struct AdkShader;
struct AdkInstance;

typedef enum AdkShaderCreateFlagBits
{
  ADK_SHADER_CREATE_LOAD_FROM_FILE_BIT = 0x00000001,
} AdkShaderCreateFlagBits;
typedef AdkFlags AdkShaderCreateFlags;

typedef struct AdkShaderCreateInfo
{
  AdkShaderCreateFlags flags;
  size_t vertexShaderSize;
  const char *pVertexShader;
  size_t geometryShaderSize;
  const char *pGeometryShader;
  size_t fragmentShaderSize;
  const char *pFragmentShader;
  size_t computeShaderSize;
  const char *pComputeShader;
} AdkShaderCreateInfo;

AdkBool32 adkShader_Create(AdkInstance *pInstance, const AdkShaderCreateInfo *pCreateInfo, AdkShader **ppShader);
void adkShader_Destroy(AdkShader **ppShader);

#endif // adkShader_h__
