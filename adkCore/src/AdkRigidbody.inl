#include "AdkRigidbody.h"

typedef struct AdkRigidbodyBuffer
{
  const adkAllocationCallbacks *pAllocator;
  AdkRigidbodyCollisionResolveFunction *pCollisionResolveFunction;
  void *pCollisionResolveUserData;
  uint32_t rigidbodyCount;
  AdkRigidbodyProperties *pRigidbodies;
} AdkRigidbodyBuffer;