#include <stdio.h>
#ifndef adkString_h__
#define adkString_h__

#ifdef _WIN32
#define adkSprintf(dst, dstLength, format, ...) sprintf_s(dst, dstLength, format, __VA_ARGS__)
#endif

inline size_t adkStrLen(const char *pStr)
{
  size_t i = 0;

  while (pStr[i])
  {
    ++i;
  }

  return i;
}

inline size_t adkStrcpy(char *pDst, size_t dstLength, const char *pSrc)
{
  size_t offset = 0;

  if (dstLength != 0)
  {
    --dstLength;

    while ((offset != dstLength) & ((pDst[offset] = pSrc[offset]) != 0))
      ++offset;
  }

  return offset + 1;
}

inline int32_t adkStrcmp(const char *pStr1, const char *pStr2)
{
  for (; (*pStr1) == (*pStr2); ++pStr1, ++pStr2)
  {
    if ((*pStr1) == 0)
      return 0;
  }

  return (*pStr1) - (*pStr2);
}

#endif // adkString_h__
