project "adkCore"

  language "C++"
  
  kind "StaticLib"

  flags "Symbols"
  flags "ExtraWarnings"
  flags "FatalWarnings"
  
  files "project.lua"
  files "src/**"
  files "shaders/**"

  includedirs "../3rdparty/vulkan/include"
  includedirs "../3rdparty/fbxsdk/include"
  includedirs "../3rdparty/glslang/glslang/glslang/Public"
  includedirs "../3rdparty/freeType/freeType/include"
  includedirs "../3rdparty/lodePng/src"
  
  libdirs "../3rdparty/vulkan/lib/%{cfg.platform}"
  libdirs "../3rdparty/fbxsdk/lib/%{cfg.buildcfg}_%{cfg.platform}"
  libdirs "../3rdparty/freeType/build/%{cfg.buildcfg}_%{cfg.platform}"
  libdirs "../3rdparty/glslang/build/%{cfg.buildcfg}_%{cfg.platform}"
  libdirs "../3rdparty/lodePng/build/%{cfg.buildcfg}_%{cfg.platform}"
  
  links "vulkan-1.lib"
  links "libfbxsdk-md.lib"
  links "freeType.lib"
  links "glslang.lib"
  links "lodePng.lib"
  
  dependson "freeType"
  dependson "glslang"
  dependson "lodePng"
  dependson "adkShaders"

  includedirs "../adkShaders/build/%{cfg.buildcfg}_%{cfg.platform}"

  targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"
  
  debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"
  
  objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"
  
  configuration "windows"
    linkoptions "/IGNORE:4221"
    
  configuration "Release"
    runtime "Release"
    
  configuration "Debug"
    runtime "Debug"