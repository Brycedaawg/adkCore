function buildShaders(adkCoreDir, targetDir)
  filter "files:**.vert or files:**.geom or files:**.frag or files:**.comp"

  local glslangValidatorPath = os.getenv("VULKAN_SDK") .. "/Bin/glslangValidator.exe"
  local cDumpFilePath = adkCoreDir .. "/adkCDump/build/%{cfg.buildcfg}_%{cfg.platform}/adkCDump"

  local inputFilename = "%{file.relpath}"
  local spirVFilename = targetDir .. "/%{file.name}.spv"
  local headerFilename = targetDir .. "/%{file.name}.spv.h"

  local cArrayName = "%{file.name:gsub('%.', '_'):upper()}_SHADER_CODE"

  buildmessage('Compiling %{file.relpath} using ' .. glslangValidatorPath .. ' -V ' .. inputFilename .. ' -x --vn ' .. cArrayName ..  ' -o ' .. spirVFilename)
  buildcommands { glslangValidatorPath .. ' -V ' .. inputFilename .. ' -x --vn ' .. cArrayName ..  ' -o ' .. headerFilename }
  buildoutputs { headerFilename }

  filter {}
end

project "adkShaders"

  language "C++"

  kind "StaticLib"

  objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"
  targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

  dependson "adkCDump"

  files "src/**.vert"
  files "src/**.geom"
  files "src/**.frag"
  files "src/**.comp"

  buildShaders("./%{cfg.targetdir}/../../../", "%{cfg.targetdir}")