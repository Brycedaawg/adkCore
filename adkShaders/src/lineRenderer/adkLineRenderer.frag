#version 430

layout(location = 0) in vec4 v2fColor;

layout(location = 0) out vec4 fragmentColor;

void main()
{
  fragmentColor = v2fColor;
}