#version 430

layout(binding = 0) uniform uniformBuffer
{
  mat4 viewProjection;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;

out gl_PerVertex
{
  vec4 gl_Position;
};

layout(location = 0) out vec4 v2fColor;

void main()
{
  gl_Position = viewProjection * vec4(position, 1.0);
  v2fColor = color;
}