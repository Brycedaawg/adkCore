project "adkCDump"

	language "C++"

	kind "ConsoleApp"

	flags "Symbols"
	flags "ExtraWarnings"
	flags "FatalWarnings"

	files "project.lua"
	files "src/**"

	targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

	debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"

	objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"