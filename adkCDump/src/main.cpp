#include <stdint.h>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <direct.h>

static const size_t BUFFER_SIZE = 1024 * 1024;
static const size_t HEX_CHAR_SIZE = 6;
static const size_t CHAR_BUFFER_SIZE = HEX_CHAR_SIZE * BUFFER_SIZE + 1;

static void _MakeDirectoryChain(const char *pDirectory)
{
  uint32_t i = 0;
  char buffer[1024];

  while (pDirectory[i] != '\0')
  {
    if (pDirectory[i] == '\\' || pDirectory[i] == '/')
    {
      char mkDirStr[] = "mkdir \"";
      memcpy(buffer, mkDirStr, sizeof(mkDirStr));
      memcpy(&buffer[strlen(mkDirStr)], pDirectory, i);
      buffer[strlen(mkDirStr) + i] = '\"';
      buffer[strlen(mkDirStr) + i + 1] = '\0';
      system(buffer);
    }

    ++i;
  }
}

int main(uint32_t argc, const char **ppArgv)
{
  if (argc != 4)
  {
    printf("Error: invalid syntax. Correct syntax is: adkCDump [filename] [cFilename] [cVariableName]\n");
    return -1;
  }

  const char *pFilename = ppArgv[1];
  const char *pCFilename = ppArgv[2];
  const char *pCVariableName = ppArgv[3];

  errno_t error = 0;

  FILE *pFile = nullptr;
  FILE *pCFile = nullptr;
  error = fopen_s(&pFile, pFilename, "rb");

  if (error != 0)
  {
    printf("Error: invalid filename \"%s\"\n", pFilename);
    return -1;
  }

  _MakeDirectoryChain(pCFilename);

  error = fopen_s(&pCFile, pCFilename, "wb");

  if (error != 0)
  {
    printf("Error: invalid cFilename \"%s\"\n", pCFilename);
    return -1;
  }

  fseek(pFile, 0, SEEK_END);

  size_t fileLength = (size_t)ftell(pFile);

  fseek(pFile, 0, SEEK_SET);

  const char C_DECLARE_START[] = "static unsigned const char ";
  const char C_DECLARE_END[] = "[] = { ";

  char *pBuffer = (char*)malloc(BUFFER_SIZE);
  char *pCharBuffer = (char*)malloc(CHAR_BUFFER_SIZE);
  char *pCopyTo = pBuffer;

  pCopyTo = (char*)memcpy(pCopyTo, C_DECLARE_START, strlen(C_DECLARE_START)) + strlen(C_DECLARE_START);
  pCopyTo = (char*)memcpy(pCopyTo, pCVariableName, strlen(pCVariableName)) + strlen(pCVariableName);
  pCopyTo = (char*)memcpy(pCopyTo, C_DECLARE_END, strlen(C_DECLARE_END) + 1) + strlen(C_DECLARE_END);

  fwrite(pBuffer, 1, (char*)pCopyTo - pBuffer, pCFile);

  for (size_t i = 0; i < fileLength; i += BUFFER_SIZE)
  {
    size_t readLength = fread(pBuffer, 1, BUFFER_SIZE, pFile);

    int written = 0;

    for (size_t j = 0; j < readLength; ++j)
      written += sprintf_s(&pCharBuffer[j * HEX_CHAR_SIZE], CHAR_BUFFER_SIZE - j * HEX_CHAR_SIZE, "0x%02X, ", (unsigned char)pBuffer[j]);

    fwrite(pCharBuffer, 1, readLength * HEX_CHAR_SIZE, pCFile);
  }

  const char C_DEFINE_END[] = " };";//"0x00, };";
  
  fwrite(C_DEFINE_END, 1, strlen(C_DEFINE_END), pCFile);

  fclose(pCFile);
  fclose(pFile);

  free(pBuffer);
  free(pCharBuffer);
}