kind "StaticLib"
language "C++"

defines "_CRT_SECURE_NO_WARNINGS"
defines "FT_DEBUG_LEVEL_ERROR"
defines "FT_DEBUG_LEVEL_TRACE"
defines "FT2_BUILD_LIBRARY"

files "project.lua"

files "freeType/src/autofit/autofit.c"
files "freeType/src/bdf/bdf.c"
files "freeType/src/cff/cff.c"
files "freeType/src/base/ftbase.c"
files "freeType/src/base/ftbitmap.c"
files "freeType/src/cache/ftcache.c"
files "freeType/src/base/ftfstype.c"
files "freeType/src/base/ftgasp.c"
files "freeType/src/gzip/ftgzip.c"
files "freeType/src/base/ftinit.c"
files "freeType/src/lzw/ftlzw.c"
files "freeType/src/base/ftsystem.c"
files "freeType/src/smooth/smooth.c"

files "freeType/src/base/ftbbox.c"
files "freeType/src/base/ftfntfmt.c"
files "freeType/src/base/ftgxval.c"
files "freeType/src/base/ftlcdfil.c"
files "freeType/src/base/ftotval.c"
files "freeType/src/base/ftpatent.c"
files "freeType/src/base/ftpfr.c"
files "freeType/src/base/ftsynth.c"
files "freeType/src/base/fttype1.c"
files "freeType/src/base/ftwinfnt.c"
files "freeType/src/pcf/pcf.c"
files "freeType/src/pfr/pfr.c"
files "freeType/src/psaux/psaux.c"
files "freeType/src/pshinter/pshinter.c"
files "freeType/src/psnames/psmodule.c"
files "freeType/src/raster/raster.c"
files "freeType/src/sfnt/sfnt.c"
files "freeType/src/truetype/truetype.c"
files "freeType/src/type1/type1.c"
files "freeType/src/cid/type1cid.c"
files "freeType/src/type42/type42.c"
files "freeType/src/winfonts/winfnt.c"

files "freeType/builds/windows/ftdebug.c"

includedirs "freeType/include"

excludes "freeType/src/autofit/aflatin2.c"

targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"

objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"

configuration "Release"
    runtime "Release"
    
  configuration "Debug"
    runtime "Debug"