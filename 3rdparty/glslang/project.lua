kind "StaticLib"

language "C++"

files "project.lua"
files "glslang/**.cpp"
files "glslang/**.h"

excludes "glslang/gtests/**.cpp"
excludes "glslang/gtests/**.h"
excludes "glslang/glslang/Include/**.h"
excludes "glslang/StandAlone/**.cpp"
excludes "glslang/StandAlone/**.h"

defines "NOMINMAX"
defines "ENABLE_HLSL"

if os.get() == "windows" then
	excludes "glslang/glslang/OSDependent/Unix/**.cpp"
else
	excludes "glslang/glslang/OSDependent/Windows/**.cpp"
end

includedirs "glslang/OGLCompilersDLL"
includedirs "glslang"

targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"

objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"

configuration "Release"
    runtime "Release"
    
  configuration "Debug"
    runtime "Debug"