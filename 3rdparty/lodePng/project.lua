kind "StaticLib"
language "C++"

files { "project.lua", "src/**.cpp", "src/**.h" }

targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"

objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"

configuration "Release"
    runtime "Release"
    
  configuration "Debug"
    runtime "Debug"