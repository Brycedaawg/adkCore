THIRD_PARTY_PROJECT_NAMES = { "glslang", "freeType", "lodePng" }

for i = 1, #THIRD_PARTY_PROJECT_NAMES do
	project(THIRD_PARTY_PROJECT_NAMES[i])
	flags "Symbols"
	location("3rdparty/" .. THIRD_PARTY_PROJECT_NAMES[i] .. "/")
	include("3rdparty/" .. THIRD_PARTY_PROJECT_NAMES[i] .. "/project.lua")
end

include "adkCore/project.lua"
include "adkCDump/project.lua"
include "adkModelBuilder/project.lua"
include "adkShaders/project.lua"