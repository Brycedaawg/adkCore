project "adkModelBuilder"

  language "C++"

  kind "ConsoleApp"

  flags "Symbols"
  flags "ExtraWarnings"
  flags "FatalWarnings"

  files "project.lua"
  files "src/**"

  includedirs "../adkCore/src"
  
  libdirs "../adkCore/build/%{cfg.buildcfg}_%{cfg.platform}"

  links "adkCore.lib"

  dependson "adkCore"

  targetdir "build/%{cfg.buildcfg}_%{cfg.platform}"

  debugdir "build/%{cfg.buildcfg}_%{cfg.platform}"

  objdir "obj/%{cfg.buildcfg}_%{cfg.platform}"
  
  configuration "Release"
    runtime "Release"

  configuration "Debug"
    runtime "Debug"