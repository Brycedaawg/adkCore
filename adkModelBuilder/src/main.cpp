#include "adkModel.h"
#include "adkFile.h"
#include "AdkQueue.h"
#include "AdkBinaryWriter.h"
#include "adkString.h"

static const char APPLICATION_NAME[] = "AdkModelImport";
static const size_t MODEL_BUFFER_SIZE = 1024 * 1024 * 32;

int main(int argC, const char **ppArgV)
{
  adkAllocationCallbacks *pAllocator = nullptr;
  AdkInstance *pInstance = nullptr;
  AdkQueue *pGraphicsQueue = nullptr;
  AdkQueue *pTransferQueue = nullptr;
  AdkModel *pModel = nullptr;
  char *pModelBuffer = nullptr;

  bool success = false;

  if (argC < 3)
    goto epilogue;

  const char *pInputFilename = ppArgV[1];
  const char *pOutputFilename = ppArgV[2];
  bool buildPhysics = false;

  for (int i = 3; i < argC; ++i)
  {
    if (adkStrcmp(ppArgV[i], "--physics") == 0)
    {
      buildPhysics = true;
      continue;
    }
    else
    {
      goto epilogue;
    }
  }

  adkInstanceCreateInfo instanceCreateInfo;
  instanceCreateInfo.flags = ADK_INSTANCE_DEBUG_BIT;
  instanceCreateInfo.pApplicationName = APPLICATION_NAME;
  instanceCreateInfo.applicationVersion = 1000;

  if (adkInstance_Create(&instanceCreateInfo, pAllocator, &pInstance) != ADK_SUCCESS)
    goto epilogue;

  AdkQueueCreateInfo graphicsQueueCreateInfo;
  graphicsQueueCreateInfo.flags = ADK_QUEUE_GRAPHICS_BIT;
  if (!adkQueue_Create(pInstance, &graphicsQueueCreateInfo, pAllocator, &pGraphicsQueue))
    goto epilogue;

  AdkQueueCreateInfo transferQueueCreateInfo;
  transferQueueCreateInfo.flags = ADK_QUEUE_TRANSFER_BIT;
  if (!adkQueue_Create(pInstance, &transferQueueCreateInfo, pAllocator, &pTransferQueue))
    goto epilogue;

  AdkModelCreateFlags modelCreateFlags = 0;
  if (buildPhysics)
  {
    modelCreateFlags |= ADK_MODEL_CREATE_PHYSICS_MESH_BIT;
    modelCreateFlags |= ADK_MODEL_CREATE_CLIP_MESH_BIT;
  }

  if (!adkModel_Create(pInstance, modelCreateFlags, pGraphicsQueue, pTransferQueue, pInputFilename, pAllocator, &pModel))
    goto epilogue;

  pModelBuffer = adkAllocTypeCall(char, MODEL_BUFFER_SIZE, adkAF_Zero, pAllocator, ADK_ALLOCATION_SCOPE_DYNAMIC);

  AdkBinaryWriterCreateInfo binaryWriterCreateInfo;
  binaryWriterCreateInfo.bufferSize = MODEL_BUFFER_SIZE;
  binaryWriterCreateInfo.pBuffer = pModelBuffer;

  AdkBinaryWriter *pBinaryWriter = nullptr;
  adkBinaryWriter_Create(&binaryWriterCreateInfo, pAllocator, &pBinaryWriter);

  if (!adkModel_Serialize(pModel, pBinaryWriter))
    goto epilogue;

  AdkFile *pFile = nullptr;
  size_t fileSize = 0;
  if (!adkFile_Open(pOutputFilename, ADK_FILE_OPEN_CREATE_BIT | ADK_FILE_OPEN_WRITE_BIT, &fileSize, &pFile))
    goto epilogue;

  if (!adkFile_Write(pFile, adkBinaryWriter_GetOffset(pBinaryWriter), pModelBuffer))
    goto epilogue;

  success = true;

epilogue:
  adkFile_Close(&pFile);
  adkBinaryWriter_Destroy(&pBinaryWriter);
  adkFreeCall(pModelBuffer, pAllocator);
  adkModel_Destroy(pInstance, &pModel);
  adkQueue_Destroy(&pTransferQueue);
  adkQueue_Destroy(&pGraphicsQueue);
  adkInstance_Destroy(pInstance);

  return success ? 0 : 1;
}